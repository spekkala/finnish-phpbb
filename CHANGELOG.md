# Change Log

## 3.2.1 (2018-01-01)

- Update translations to be compatible with phpBB 3.2.1.
- Alter default date format.
- Various minor improvements.

## 3.2.0 revision 1 (2017-02-27)

- Make the translation for RECORD_ONLINE_USERS more fluent.
- Create a 2x version of the online image for high resolution displays.

## 3.2.0 (2017-02-23)

- Update translations to be compatible with phpBB 3.2.0.
- Include a translation for the VigLink extension.

## 3.1.10 (2016-10-21)

- Update translations to be compatible with phpBB 3.1.10.

## 3.1.9 (2016-05-24)

- Update translations to be compatible with phpBB 3.1.9.

## 3.1.8 (2016-03-14)

- Update translations to be compatible with phpBB 3.1.8.
- Minor change in `acp/board.php`.

## 3.1.7-PL1 (2016-01-15)

- Update translations to be compatible with phpBB 3.1.7-PL1.
- Improve BBCode-related translations.
- Minor changes to wording in various translations.

## 3.1.6 revision 1 (2015-10-03)

- Clarifications to group-related translations.
- Minor changes in `common.php`.

## 3.1.6 (2015-10-01)

- First public release.
