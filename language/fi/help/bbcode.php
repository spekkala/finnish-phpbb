<?php
/**
 *
 * This file is part of the phpBB Forum Software package.
 *
 * @author Sami Pekkala
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

/**
 * DO NOT CHANGE
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'HELP_BBCODE_BLOCK_IMAGES'	=> 'Kuvien käyttö',
	'HELP_BBCODE_BLOCK_INTRO'	=> 'Johdanto',
	'HELP_BBCODE_BLOCK_LINKS'	=> 'Linkkien luominen',
	'HELP_BBCODE_BLOCK_LISTS'	=> 'Luetteloiden luominen',
	'HELP_BBCODE_BLOCK_OTHERS'	=> 'Muut aiheet',
	'HELP_BBCODE_BLOCK_QUOTES'	=> 'Lainaaminen ja tasalevyisen tekstin käyttö',
	'HELP_BBCODE_BLOCK_TEXT'	=> 'Tekstin muotoilu',

	'HELP_BBCODE_IMAGES_ATTACHMENT_ANSWER'	=> 'Voit lisätä liitetiedostoja haluamaasi viestin kohtaan käyttämällä tageja <strong>[attachment=][/attachment]</strong>. Tämä kuitenkin edellyttää, että liitetiedostojen käyttö on sallittu tällä keskustelupalstalla ja että sinulla on tarvittavat oikeudet lähettää liitetiedostoja. Viestien kirjoitussivulla on pudotusvalikko, jonka avulla voit lisätä liitetiedostoja.',
	'HELP_BBCODE_IMAGES_ATTACHMENT_QUESTION'	=> 'Liitetiedostojen käyttö',
	'HELP_BBCODE_IMAGES_BASIC_ANSWER'	=> 'On syytä muistaa kaksi tärkeää asiaa kuvia lisättäessä: monet käyttäjät eivät pidä suuresta määrästä kuvia viesteissä, ja toiseksi haluamasi kuvan täytyy jo olla Internetissä (et voi linkittää kuvaa omalta tietokoneeltasi). Voit lisätä kuvan viestiisi ympäröimällä haluamasi kuvan osoitteen tageilla <strong>[img][/img]</strong>. Esimerkki:<br /><br />

<strong>[img]</strong>https://www.phpbb.com/theme/images/logos/blue/160x52.png<strong>[/img]</strong><br /><br />

Kuten yllä olevassa linkitysohjeessa mainittiin, voit tehdä kuvasta linkin ympäröimällä sen tageihin <strong>[url][/url]</strong>. Esimerkiksi<br /><br />

<strong>[url=https://www.phpbb.com/][img]</strong>https://www.phpbb.com/theme/images/logos/blue/160x52.png<strong>[/img][/url]</strong><br /><br />

tuottaa seuraavan kuvalinkin: <br /><br />

<a href="https://www.phpbb.com/"><img src="https://www.phpbb.com/theme/images/logos/blue/160x52.png" alt="" /></a>',
	'HELP_BBCODE_IMAGES_BASIC_QUESTION'	=> 'Kuvan liittäminen viestiin',

	'HELP_BBCODE_INTRO_BBCODE_ANSWER'	=> 'BBCode on HTML-kielen sovellus. BBCode käyttää hakasulkuja [ ] kulmasulkeiden &lt; &gt; sijaan ja tarjoaa paremmat mahdollisuudet hallita tekstin ulkoasua. Ylläpito päättää, onko BBCoden käyttö sallittu tällä keskustelupalstalla. Voit myös itse poistaa BBCoden käytöstä viestikohtaisesti viestiä kirjoittaessasi. Käytössä olevasta tyylistä riippuen voi olla mahdollista sisällyttää BBCodea viesteihin helposti kuvakkeita napsauttamalla.',
	'HELP_BBCODE_INTRO_BBCODE_QUESTION'	=> 'Mitä on BBCode?',

	'HELP_BBCODE_LINKS_BASIC_ANSWER'	=> 'BBCode tukee useita tapoja luoda linkkejä.

<ul>
<li>Yksi vaihtoehto on käyttää tageja <strong>[url=][/url]</strong>. Tagien sisältö näkyy linkin tekstinä ja =-merkin perässä oleva teksti toimii linkin osoitteena. Jos esimerkiksi haluat linkittää phpBB:n sivustolle, voit kirjoittaa<br /><br />

<strong>[url=https://www.phpbb.com]</strong>Vieraile phpBB:n sivustolla<strong>[/url]</strong><br /><br />

Tämä tuottaisi <a href="https://www.phpbb.com/">Vieraile phpBB:n sivustolla</a> -linkin.</li>

<li>Jos haluat linkin osoitteen näkyvän linkin tekstinä, voit kirjoittaa yksinkertaisesti<br /><br />

<strong>[url]</strong>https://www.phpbb.com/<strong>[/url]</strong><br /><br />

Tämä tuottaa seuraavan linkin: <a href="https://www.phpbb.com/">https://www.phpbb.com/</a></li>

<li>On myös mahdollista käyttää automaattista linkitystä, jolloin jokainen kelvollinen osoite muutetaan linkiksi ilman, että sinun tarvitsee kirjoittaa ainuttakaan tagia tai edes http://-alkuosaa. Jos esimerkiksi kirjoitat viestiisi osoitteen www.phpbb.com, näkyy lopullisessa viestissä automaattisesti <a href="http://www.phpbb.com/">www.phpbb.com</a>-linkki.</li>

<li>Kaksi edellistä kohtaa pätee myös sähköpostiosoitteisiin. Voit syöttää sähköpostiosoitteen joko erikseen kirjoittamalla<br /><br />

<strong>[email]</strong>esimerkki@example.com<strong>[/email]</strong><br /><br />

tai yksinkertaisesti sisällyttämällä osoitteen esimerkki@example.com viestiisi sellaisenaan. Molemmissa tapauksissa lopullinen viesti sisältää linkin <a href="mailto:esimerkki@example.com">esimerkki@example.com</a>.</li>
</ul>

Kaikkien BBCode-tagien tavoin myös linkit voivat sisältää muita tageja. Voit esimerkiksi luoda kuvalinkin seuraavasti:<br /><br />

<strong>[url=https://www.phpbb.com/][img]</strong>https://www.phpbb.com/theme/images/logos/blue/160x52.png<strong>[/img][/url]</strong><br /><br />

Muista kuitenkin huolehtia, että kaikki sisäkkäiset tagit on suljettu oikeassa järjestyksessä. Tagien sulkeminen väärällä tavalla saattaa johtaa viestisi poistamiseen, joten ole tarkkana.',
	'HELP_BBCODE_LINKS_BASIC_QUESTION'	=> 'Linkitys toiselle sivustolle',

	'HELP_BBCODE_LISTS_ORDERER_ANSWER'	=> 'Järjestetty luettelo antaa sinulle mahdollisuuden valita, mitä kunkin luettelon kohdan edessä näkyy. Numeroidun luettelon voi luoda tageilla <strong>[list=1][/list]</strong> ja aakkostetun luettelon tageilla <strong>[list=a][/list]</strong>. Järjestämättömän luettelon tavoin kukin luettelon kohta merkitään tagilla <strong>[*]</strong>. Esimerkiksi<br /><br />

<strong>[list=1]</strong><br />
<strong>[*]</strong>Käy kaupassa<br />
<strong>[*]</strong>Osta uusi tietokone<br />
<strong>[*]</strong>Kiroa tietokoneelle, kun se menee epäkuntoon<br />
<strong>[/list]</strong><br /><br />

tuottaa tuloksen<br /><br />

<ol style="list-style-type: decimal;">
	<li>Käy kaupassa</li>
	<li>Osta uusi tietokone</li>
	<li>Kiroa tietokoneelle, kun se menee epäkuntoon</li>
</ol>

Aakkostettua luetteloa voi käyttää seuraavasti:<br /><br />

<strong>[list=a]</strong><br />
<strong>[*]</strong>Ensimmäinen vaihtoehto<br />
<strong>[*]</strong>Toinen vaihtoehto<br />
<strong>[*]</strong>Kolmas vaihtoehto<br />
<strong>[/list]</strong><br /><br />

tuottaa tuloksen<br /><br />

<ol style="list-style-type: lower-alpha;">
	<li>Ensimmäinen vaihtoehto</li>
	<li>Toinen vaihtoehto</li>
	<li>Kolmas vaihtoehto</li>
</ol>

<strong>[list=A]</strong><br />
<strong>[*]</strong>Ensimmäinen vaihtoehto<br />
<strong>[*]</strong>Toinen vaihtoehto<br />
<strong>[*]</strong>Kolmas vaihtoehto<br />
<strong>[/list]</strong><br /><br />

tuottaa tuloksen<br /><br />

<ol style="list-style-type: upper-alpha">
	<li>Ensimmäinen vaihtoehto</li>
	<li>Toinen vaihtoehto</li>
	<li>Kolmas vaihtoehto</li>
</ol>

<strong>[list=i]</strong><br />
<strong>[*]</strong>Ensimmäinen vaihtoehto<br />
<strong>[*]</strong>Toinen vaihtoehto<br />
<strong>[*]</strong>Kolmas vaihtoehto<br />
<strong>[/list]</strong><br /><br />

tuottaa tuloksen<br /><br />

<ol style="list-style-type: lower-roman">
	<li>Ensimmäinen vaihtoehto</li>
	<li>Toinen vaihtoehto</li>
	<li>Kolmas vaihtoehto</li>
</ol>

<strong>[list=I]</strong><br />
<strong>[*]</strong>Ensimmäinen vaihtoehto<br />
<strong>[*]</strong>Toinen vaihtoehto<br />
<strong>[*]</strong>Kolmas vaihtoehto<br />
<strong>[/list]</strong><br /><br />

tuottaa tuloksen<br /><br />

<ol style="list-style-type: upper-roman">
	<li>Ensimmäinen vaihtoehto</li>
	<li>Toinen vaihtoehto</li>
	<li>Kolmas vaihtoehto</li>
</ol>',
	'HELP_BBCODE_LISTS_ORDERER_QUESTION'	=> 'Järjestetyn luettelon luominen',
	'HELP_BBCODE_LISTS_UNORDERER_ANSWER'	=> 'BBCode tukee kahdenlaisia luetteloita: järjestämättömiä ja järjestettyjä. Ne ovat olennaisilta osin samoja kuin HTML-vastineensa. Järjestämättömän luettelon kohdat näytetään allekkain luettelomerkeillä sisennettyinä. Järjestämätön luettelo luodaan tageilla <strong>[list][/list]</strong> ja kukin luettelon kohta merkitään tagilla <strong>[*]</strong>. Voit esimerkiksi luetella suosikkivärisi seuraavasti:<br /><br />

<strong>[list]</strong><br />
<strong>[*]</strong>Punainen<br />
<strong>[*]</strong>Sininen<br />
<strong>[*]</strong>Keltainen<br />
<strong>[/list]</strong><br /><br />

Tuloksena näkyvä luettelo olisi<br /><br />

<ul>
	<li>Punainen</li>
	<li>Sininen</li>
	<li>Keltainen</li>
</ul>

Voit valita luettelomerkin tyypin käyttämällä joko tagia <strong>[list=disc][/list]</strong>, <strong>[list=circle][/list]</strong> tai <strong>[list=square][/list]</strong>.',
	'HELP_BBCODE_LISTS_UNORDERER_QUESTION'	=> 'Järjestämättömän luettelon luominen',

	'HELP_BBCODE_OTHERS_CUSTOM_ANSWER'	=> 'Jos olet ylläpitäjä tällä keskustelupalstalla ja sinulle on tarvittavat oikeudet, voit luoda uusia tageja Omat BBCode-tagit -osiossa.',
	'HELP_BBCODE_OTHERS_CUSTOM_QUESTION'	=> 'Voinko luoda omia tageja?',

	'HELP_BBCODE_QUOTES_CODE_ANSWER'	=> 'Jos haluat sisällyttää viestiisi koodia tai muuta tasalevyistä fonttia vaativaa tekstiä, ympäröi haluamasi teksti tageilla <strong>[code][/code]</strong>. Esimerkki:<br /><br /><strong>[code]</strong>echo "Tämä on koodia.";<strong>[/code]</strong><br /><br />Kaikki tagien <strong>[code][/code]</strong> sisällä käytetty muotoilu näkyy sellaisenaan, kun katsot viestiä myöhemmin.',
	'HELP_BBCODE_QUOTES_CODE_QUESTION'	=> 'Koodin tai muun tasalevyisen tekstin käyttö',
	'HELP_BBCODE_QUOTES_TEXT_ANSWER'	=> 'Tekstiä voi lainata joko viittauksen kanssa tai ilman.

<ul>
<li>Kun käytät Lainaa-toimintoa vastataksesi toisen käyttäjän viestiin, tulee lainattavan tekstin ympärille tagit <strong>[quote=""][/quote]</strong>. Tämä antaa sinulle mahdollisuuden viitata henkilöön, jonka tekstiä olet lainaamassa. Jos esimerkiksi haluat lainata herra Virtasen kirjoittamaa tekstiä, voit kirjoittaa<br /><br /><strong>[quote="Herra Virtanen"]</strong>Herra Virtasen kirjoittama teksti tulee tähän<strong>[/quote]</strong><br /><br />Teksti ”Herra Virtanen kirjoitti:” tulee automaattisesti näkyviin ennen varsinaista tekstiä. Huomaa, että lainausmerkit lainattavan henkilön nimen ympärillä ovat pakollisia.</li>

<li>Jos haluat lainata ilman viittausta, ympäröi teksti tageilla <strong>[quote][/quote]</strong>. Tällöin lainatun henkilön nimi ei näy lopullisessa viestissä.</li>
</ul>',
	'HELP_BBCODE_QUOTES_TEXT_QUESTION'	=> 'Viestin lainaus vastattaessa',

	'HELP_BBCODE_TEXT_BASIC_ANSWER'	=> 'BBCode sisältää tageja, joiden avulla voit muokata tekstisi ulkoasua.

<ul>
<li>Lihavointiin käytetään tageja <strong>[b][/b]</strong>. Esimerkiksi<br /><br /><strong>[b]</strong>Hei<strong>[/b]</strong><br /><br />näkyy muodossa <strong>Hei</strong>.</li>

<li>Alleviivaukseen käytetään tageja <strong>[u][/u]</strong>. Esimerkiksi<br /><br /><strong>[u]</strong>Hyvää huomenta<strong>[/u]</strong><br /><br />näkyy muodossa <span style="text-decoration: underline;">Hyvää huomenta</span>.</li>

<li>Kursivointiin käytetään tageja <strong>[i][/i]</strong>. Esimerkiksi<br /><br />Tämä on <strong>[i]</strong>hienoa<strong>[/i]</strong><br /><br />näkyy muodossa Tämä on <i>hienoa</i>.</li>
</ul>
',
	'HELP_BBCODE_TEXT_BASIC_QUESTION'	=> 'Kuinka voin lihavoida, kursivoida tai alleviivata tekstiä?',
	'HELP_BBCODE_TEXT_COLOR_ANSWER'	=> 'Tekstin väriä ja kokoa voi muuttaa alla kuvatuilla tageilla. Pidä kuitenkin mielessä, että tekstin ulkoasu riippuu lukijan selaimesta ja järjestelmästä.

<ul>
<li>Tekstin värin muuttamiseen käytetään tageja <strong>[color=][/color]</strong>. Voit antaa joko värin englanninkielisen nimen (esim. red, blue tai yellow) tai heksadesimaalisen arvon (esim. #FFFFFF tai #000000). Esimerkiksi punaista tekstiä voi luoda tageilla<br /><br /><strong>[color=red]</strong>Hei<strong>[/color]</strong><br /><br />tai<br /><br /><strong>[color=#FF0000]</strong>Hei<strong>[/color]</strong><br /><br />Molemmat vaihtoehdot näkyvät muodossa <span style="color: #F00;">Hei</span>.</li>

<li>Tekstin kokoa muutetaan tageilla <strong>[size=][/size]</strong>. Tämän tagin vaikutus riippuu käyttäjän valitsemasta tyylistä, mutta on suositeltavaa käyttää numeerista arvoa, joka kuvaa tekstin kokoa prosentteina. Oletusarvoisesti sallitut koot ovat väliltä 20 (erittäin pientä tekstiä) ja 200 (erittäin suurta tekstiä). Esimerkiksi<br /><br /><strong>[size=30]</strong>PIENI<strong>[/size]</strong><br /><br />näkyy tavallisesti muodossa <span style="font-size: 30%;">PIENI</span>, kun taas<br /><br /><strong>[size=200]</strong>SUURI<strong>[/size]</strong><br /><br />näkyy muodossa <span style="font-size: 200%;">SUURI</span>.</li>
</ul>',
	'HELP_BBCODE_TEXT_COLOR_QUESTION'	=> 'Kuinka muutan tekstin väriä ja kokoa?',
	'HELP_BBCODE_TEXT_COMBINE_ANSWER'	=> 'Kyllä. Jos esimerkiksi haluaisit kiinnittää huomiota, voisit kirjoittaa<br /><br /><strong>[size=200][color=red][b]</strong>KATSO MINUA<strong>[/b][/color][/size]</strong><br /><br />joka näkyisi muodossa <span style="color: #F00; font-size: 200%;"><strong>KATSO MINUA</strong></span>.<br /><br />Ei ole kuitenkaan suositeltavaa käyttää tällaista tekstiä paljon. Muista, että sinun on kirjoittajana huolehdittava, että tagit on suljettu oikeassa järjestyksessä. Esimerkiksi tämä on väärin:<br /><br /><strong>[b][u]</strong>Tämä on väärin<strong>[/b][/u]</strong>',
	'HELP_BBCODE_TEXT_COMBINE_QUESTION'	=> 'Voinko yhdistellä muotoiluja?',
));
