<?php
/**
 *
 * This file is part of the phpBB Forum Software package.
 *
 * @author Sami Pekkala
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

/**
 * DO NOT CHANGE
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'HELP_FAQ_ATTACHMENTS_ALLOWED_ANSWER'	=> 'Keskustelupalstan ylläpito valitsee sallittujen ja kiellettyjen liitetiedostojen tyypit. Jos et ole varma, mitkä liitetiedostot ovat sallittuja, ota yhteyttä ylläpitoon.',
	'HELP_FAQ_ATTACHMENTS_ALLOWED_QUESTION'	=> 'Millaiset liitetiedostot ovat sallittuja tällä keskustelupalstalla?',
	'HELP_FAQ_ATTACHMENTS_OWN_ANSWER'	=> 'Lähettämäsi liitetiedostot näkyvät käyttäjän hallintapaneelin Liitetiedostot-osiossa.',
	'HELP_FAQ_ATTACHMENTS_OWN_QUESTION'	=> 'Mistä löydän lähettämäni liitetiedostot?',

	'HELP_FAQ_BLOCK_ATTACHMENTS'	=> 'Liitetiedostot',
	'HELP_FAQ_BLOCK_BOOKMARKS'	=> 'Seuranta ja kirjanmerkit',
	'HELP_FAQ_BLOCK_FORMATTING'	=> 'Viestien muotoilu ja aiheiden tyypit',
	'HELP_FAQ_BLOCK_FRIENDS'	=> 'Kaverit ja vihamiehet',
	'HELP_FAQ_BLOCK_GROUPS'	=> 'Käyttäjätasot ja ryhmät',
	'HELP_FAQ_BLOCK_ISSUES'	=> 'PhpBB:hen liittyvät kysymykset',
	'HELP_FAQ_BLOCK_LOGIN'	=> 'Sisäänkirjautuminen ja rekisteröityminen',
	'HELP_FAQ_BLOCK_PMS'	=> 'Yksityisviestit',
	'HELP_FAQ_BLOCK_POSTING'	=> 'Viestien kirjoitus',
	'HELP_FAQ_BLOCK_SEARCH'	=> 'Haku keskustelupalstalta',
	'HELP_FAQ_BLOCK_USERSETTINGS'	=> 'Käyttäjäkohtaiset asetukset',

	'HELP_FAQ_BOOKMARKS_DIFFERENCE_ANSWER'	=> 'PhpBB 3.0:ssa kirjanmerkit toimivat samalla tavalla kuin selaimen kirjanmerkit, sillä et saanut ilmoituksia muutoksista. PhpBB 3.1:stä alkaen kirjanmerkit muistuttavat enemmän aiheen seuraamista, koska voit saada ilmoituksen, kun merkityssä aiheessa tapahtuu muutos. Seurantaa hyödyntämällä puolestaan saat ilmoituksen, kun jokin keskustelupalstan aihe tai alue päivittyy. Kirjanmerkkejä ja seurantaa koskevia ilmoitusasetuksia voi muokata käyttäjän hallintapaneelin ”Keskustelupalstan asetukset” -osiossa.',
	'HELP_FAQ_BOOKMARKS_DIFFERENCE_QUESTION'	=> 'Mitä eroa on kirjanmerkeillä ja seurannalla?',
	'HELP_FAQ_BOOKMARKS_FORUM_ANSWER'	=> 'Voit asettaa alueen seurantaan napsauttamalla sivun alalaidassa olevaa ”Aseta alue seurantaan” -linkkiä saapuessasi alueelle.',
	'HELP_FAQ_BOOKMARKS_FORUM_QUESTION'	=> 'Kuinka asetan alueen seurantaan?',
	'HELP_FAQ_BOOKMARKS_REMOVE_ANSWER'	=> 'Voit lopettaa seurannan käyttäjän hallintapaneelin ”Viestien seuranta” -osiossa.',
	'HELP_FAQ_BOOKMARKS_REMOVE_QUESTION'	=> 'Kuinka lopetan seurannan?',
	'HELP_FAQ_BOOKMARKS_TOPIC_ANSWER'	=> 'Voit lisätä kirjanmerkin tai asettaa aiheen seurantaan napsauttamalla sopivaa linkkiä ”Aiheen hallinta” -valikossa, joka sijaitsee lähellä aihesivujen ylä- ja alalaitaa.<br />Aihe asetetaan seurantaan myös, jos valitset ”Ilmoita, kun viestiin vastataan” -asetuksen aiheeseen vastatessasi.',
	'HELP_FAQ_BOOKMARKS_TOPIC_QUESTION'	=> 'Kuinka lisään kirjanmerkin aiheeseen tai asetan sen seurantaan?',

	'HELP_FAQ_FORMATTING_ANNOUNCEMENT_ANSWER'	=> 'Tiedotteet sisältävät yleensä tärkeää tietoa alueesta, jota olet lukemassa, ja sinun tulisi lukea ne aina, kun mahdollista. Tiedotteet näkyvät jokaisen sivun ylimmäisenä sillä alueella, johon ne on lähetetty. Keskustelupalstan ylläpito myöntää oikeuden kirjoittaa tiedotteita.',
	'HELP_FAQ_FORMATTING_ANNOUNCEMENT_QUESTION'	=> 'Mitä tiedotteet ovat?',
	'HELP_FAQ_FORMATTING_BBOCDE_ANSWER'	=> 'BBCode on HTML-kielen sovellus. BBCode käyttää hakasulkuja [ ] kulmasulkeiden &lt; &gt; sijaan ja tarjoaa paremmat mahdollisuudet hallita tekstin ulkoasua. Ylläpito päättää, onko BBCoden käyttö sallittu tällä keskustelupalstalla. Voit myös itse poistaa BBCoden käytöstä viestikohtaisesti viestejä kirjoittaessasi. Lisätietoa BBCodesta saat oppaasta, johon on linkki viestien kirjoitussivulla.',
	'HELP_FAQ_FORMATTING_BBOCDE_QUESTION'	=> 'Mitä on BBCode?',
	'HELP_FAQ_FORMATTING_GLOBAL_ANNOUNCE_ANSWER'	=> 'Yleistiedotteet sisältävät tärkeää tietoa, ja sinun tulisi lukea ne aina, kun mahdollista. Ne näkyvät ylimmäisenä jokaisella alueella sekä käyttäjän hallintapaneelissa. Keskustelupalstan ylläpito myöntää oikeuden kirjoittaa yleistiedotteita.',
	'HELP_FAQ_FORMATTING_GLOBAL_ANNOUNCE_QUESTION'	=> 'Mitä yleistiedotteet ovat?',
	'HELP_FAQ_FORMATTING_HTML_ANSWER'	=> 'Et voi. BBCode kuitenkin antaa käyttöösi suurimman osan muotoiluista, jotka ovat mahdollisia HTML-kieltä käytettäessä.',
	'HELP_FAQ_FORMATTING_HTML_QUESTION'	=> 'Voinko käyttää HTML-kieltä?',
	'HELP_FAQ_FORMATTING_ICONS_ANSWER'	=> 'Aiheiden kuvakkeet on aiheiden aloittajien valitsemia kuvia, jotka kuvailevat aiheiden sisältöä. Mahdollisuus käyttää aiheiden kuvakkeita riippuu oikeuksista, jotka keskustelupalstan ylläpito on asettanut.',
	'HELP_FAQ_FORMATTING_ICONS_QUESTION'	=> 'Mitä aiheiden kuvakkeet ovat?',
	'HELP_FAQ_FORMATTING_IMAGES_ANSWER'	=> 'Kyllä, viestisi voivat sisältää kuvia. Jos ylläpito on sallinut liitetiedostojen käytön, voit siirtää kuvia keskustelupalstalle omalta tietokoneeltasi. Muussa tapauksessa sinun täytyy linkittää kuviin, jotka sijaitsevat julkisesti käytettävissä olevilla palvelimilla. Et voi linkittää omalla tietokoneellasi oleviin kuviin tai kuviin, joihin pääsy vaati salasanan syöttämisen. Kuvien näyttämiseen tarvitset BBCoden [img]-tagia.',
	'HELP_FAQ_FORMATTING_IMAGES_QUESTION'	=> 'Voinko sisällyttää kuvia viesteihini?',
	'HELP_FAQ_FORMATTING_LOCKED_ANSWER'	=> 'Lukitut aiheet ovat aiheita, joihin ei voi enää vastata ja joiden sisältämät äänestykset ovat automaattisesti päättyneet. Valvoja tai ylläpitäjä on voinut lukita aiheen monesta syystä. Ylläpito on myös voinut myöntää sinulle oikeuden lukita omia aiheitasi.',
	'HELP_FAQ_FORMATTING_LOCKED_QUESTION'	=> 'Mitä lukitut aiheet ovat?',
	'HELP_FAQ_FORMATTING_SMILIES_ANSWER'	=> 'Hymiöt ovat pieniä kuvia tai merkkiyhdistelmiä, joiden avulla voi ilmaista tunnetiloja. Esimerkiksi :) tarkoittaa iloista ja :( surullista. Luettelo käytettävissä olevista hymiöistä näkyy viestien kirjoitussivulla. Älä kuitenkaan käytä hymiöitä liikaa, sillä ne saattavat tehdä viestistä nopeasti lukukelvottoman. Tällöin valvoja saattaa poistaa hymiöt tai koko viestin. Keskustelupalstan ylläpito on myös voinut asettaa ylärajan yhden viestin sisältämien hymiöiden lukumäärälle.',
	'HELP_FAQ_FORMATTING_SMILIES_QUESTION'	=> 'Mitä hymiöt ovat?',
	'HELP_FAQ_FORMATTING_STICKIES_ANSWER'	=> 'Pysyvät aiheet näkyvät tiedotteiden alapuolella ja vain alueen ensimmäisellä sivulla. Ne ovat yleensä varsin tärkeitä, joten sinun tulisi lukea ne aina, kun mahdollista. Keskustelupalstan ylläpito myöntää oikeuden kirjoittaa pysyviä aiheita.',
	'HELP_FAQ_FORMATTING_STICKIES_QUESTION'	=> 'Mitä pysyvät aiheet ovat?',

	'HELP_FAQ_FRIENDS_BASIC_ANSWER'	=> 'Näiden luetteloiden avulla voit lajitella muita keskustelupalstan käyttäjiä. Kaverilistallasi olevat käyttäjät näkyvät käyttäjän hallintapaneelissa, jotta voit nopeasti tarkastaa, ovatko he paikalla. Käytössä olevasta tyylistä riippuen kavereiden lähettämät viestit saattavat myös näkyä korostettuina. Vihamiesten luettelossa olevien käyttäjien lähettämät viestit ovat oletusarvoisesti piilotettuja.',
	'HELP_FAQ_FRIENDS_BASIC_QUESTION'	=> 'Mitä ovat kavereiden ja vihamiesten luettelot?',
	'HELP_FAQ_FRIENDS_MANAGE_ANSWER'	=> 'Voit lisätä käyttäjiä luetteloihisi kahdella tavalla. Jokaisen käyttäjän profiilisivulla on linkki, jota napsauttamalla voit lisätä käyttäjän kaveriksesi tai vihamieheksesi. Vaihtoehtoisesti voit siirtyä käyttäjän hallintapaneeliin, jossa voit lisätä käyttäjiä suoraan syöttämällä heidän käyttäjätunnuksensa. Samalla sivulla voit myös poistaa käyttäjiä luetteloistasi.',
	'HELP_FAQ_FRIENDS_MANAGE_QUESTION'	=> 'Kuinka voin lisätä tai poistaa käyttäjiä kavereiden ja vihamiesten luetteloista?',

	'HELP_FAQ_GROUPS_ADMINISTRATORS_ANSWER'	=> 'Ylläpitäjät ovat käyttäjiä, joilla on suurin valta koko keskustelupalstalla. Keskustelupalstan perustaja on voinut myöntää heille oikeuden hallita kaikkia keskustelupalstan toimintoja, kuten oikeuksien asettamista, porttikieltojen antamista, käyttäjäryhmien luomista tai valvojien nimittämistä. Heillä saattaa olla myös täydet valvojan oikeudet kaikilla alueilla.',
	'HELP_FAQ_GROUPS_ADMINISTRATORS_QUESTION'	=> 'Mitä ylläpitäjät ovat?',
	'HELP_FAQ_GROUPS_COLORS_ANSWER'	=> 'Ylläpito voi asettaa eri värejä eri käyttäjäryhmille helpottaakseen ryhmien jäsenten tunnistamista.',
	'HELP_FAQ_GROUPS_COLORS_QUESTION'	=> 'Miksi jotkin käyttäjäryhmät näkyvät erivärisinä?',
	'HELP_FAQ_GROUPS_DEFAULT_ANSWER'	=> 'Jos olet jäsenenä useammassa kuin yhdessä ryhmässä, oletusryhmäsi ratkaisee, mikä väri ja arvonimi käyttäjätunnukseesi yhdistetään. Ylläpito on saattanut myöntää sinulle oikeuden vaihtaa oletusryhmäsi käyttäjän hallintapaneelissa.',
	'HELP_FAQ_GROUPS_DEFAULT_QUESTION'	=> 'Mikä on oletusryhmä?',
	'HELP_FAQ_GROUPS_MODERATORS_ANSWER'	=> 'Valvojat ovat käyttäjiä, jotka huolehtivat keskustelupalstan päivittäisestä toiminnasta. He voivat muokata ja poistaa viestejä sekä lukita, avata, siirtää, poistaa ja jakaa aiheita valvomillaan alueilla. Tavallisesti valvojien tehtävänä on estää käyttäjiä kirjoittamasta asiattomia tai aiheeseen liittymättömiä viestejä.',
	'HELP_FAQ_GROUPS_MODERATORS_QUESTION'	=> 'Mitä valvojat ovat?',
	'HELP_FAQ_GROUPS_TEAM_ANSWER'	=> 'Linkin kohdesivulla on luettelo keskustelupalstan ylläpitäjistä ja valvojista sekä tiedot alueista, joita he valvovat.',
	'HELP_FAQ_GROUPS_TEAM_QUESTION'	=> 'Mikä on Henkilökunta-linkin tarkoitus?',
	'HELP_FAQ_GROUPS_USERGROUPS_ANSWER'	=> 'Käyttäjäryhmien avulla ylläpito voi jakaa käyttäjät helposti hallittaviin osiin. Kukin käyttäjä voi kuulua useaan ryhmään ja kullakin ryhmällä voi olla omat oikeutensa. Tällä tavoin ylläpito pystyy vaivattomasti muuttamaan monen käyttäjän oikeuksia samalla kertaa.',
	'HELP_FAQ_GROUPS_USERGROUPS_JOIN_ANSWER'	=> 'Voit katsoa käyttäjäryhmiä käyttäjän hallintapaneelin Käyttäjäryhmät-osiossa. Jos haluat liittyä ryhmään, merkitse haluamasi ryhmä ja valitse ”Liity valittuihin” -vaihtoehto. Kaikkiin ryhmiin ei kuitenkaan voi liittyä vapaasti. Joihinkin ryhmiin pääsy vaatii hyväksynnän ja jotkin ryhmät saattavat olla suljettuja tai jopa piilotettuja. Jos pyydät jäsenyyttä hyväksynnän vaativassa ryhmässä, ryhmän johtaja saattaa kysyä sinulta syytä halukkuudellesi liittyä ryhmään. Älä häiritse ryhmänjohtajaa, jos hän hylkää pyyntösi.',
	'HELP_FAQ_GROUPS_USERGROUPS_JOIN_QUESTION'	=> 'Mistä käyttäjäryhmät löytyvät ja kuinka liityn niihin?',
	'HELP_FAQ_GROUPS_USERGROUPS_LEAD_ANSWER'	=> 'Tavallisesti ylläpito valitsee käyttäjäryhmän johtajan ryhmän luomisen yhteydessä. Jos olet kiinnostunut käyttäjäryhmän luomisesta, ota yhteyttä ylläpitoon.',
	'HELP_FAQ_GROUPS_USERGROUPS_LEAD_QUESTION'	=> 'Kuinka pääsen käyttäjäryhmän johtajaksi?',
	'HELP_FAQ_GROUPS_USERGROUPS_QUESTION'	=> 'Mitä käyttäjäryhmät ovat?',

	'HELP_FAQ_ISSUES_ADMIN_ANSWER'	=> 'Kaikki käyttäjät voivat täyttää ”Yhteys ylläpitoon” -lomakkeen, jos ylläpito on sallinut kyseisen toiminnon käyttöön.<br />Rekisteröityneet käyttäjät voivat käyttää myös Henkilökunta-linkkiä.',
	'HELP_FAQ_ISSUES_ADMIN_QUESTION'	=> 'Kuinka saan yhteyden keskustelupalstan ylläpitoon?',
	'HELP_FAQ_ISSUES_FEATURE_ANSWER'	=> 'Tämä ohjelmisto on phpBB Limitedin kehittämä ja lisensoima. Jos ohjelmistoon pitäisi mielestäsi lisätä uusi toiminto, käy <a href="https://www.phpbb.com/ideas/">phpBB:n ideakeskuksessa</a>, jossa voit äänestää aikaisempia ideoita tai ehdottaa uusia toimintoja.',
	'HELP_FAQ_ISSUES_FEATURE_QUESTION'	=> 'Miksei jokin tietty toiminto ole käytettävissä?',
	'HELP_FAQ_ISSUES_LEGAL_ANSWER'	=> 'Voit ottaa yhteyttä keneen tahansa Henkilökunta-sivulla lueteltuun ylläpitäjään. Jos et saa heiltä vastausta, sinun tulisi ottaa yhteyttä verkkotunnuksen omistajaan (whois-kyselyn avulla). Jos tämä keskustelupalsta sijaitsee ilmaispalveluja tarjoavalla sivustolla, ota yhteyttä heidän hallinto- tai väärinkäyttöosastoonsa. Huomaa, että phpBB Limitedillä <strong>ei ole minkäänlaista toimivaltaa</strong> eikä heitä voi pitää vastuullisena siitä, miten, missä tai ketkä tätä keskustelupalstaa käyttävät. Älä ota yhteyttä phpBB Limitediin minkäänlaisissa lakia koskevissa asioissa, <strong>jotka eivät suoraan liity</strong> phpbb.com-sivustoon tai itse phpBB-ohjelmistoon. Jos lähetät sähköpostia phpBB Limitedille <strong>missään kolmanteen osapuoleen liittyvässä asiassa</strong>, tulee saamasi vastaus olemaan lyhytsanainen tai et saa vastausta ollenkaan.',
	'HELP_FAQ_ISSUES_LEGAL_QUESTION'	=> 'Keneen otan yhteyttä tähän keskustelupalstaan liittyvissä väärinkäyttötapauksissa ja/tai lakia koskevissa kysymyksissä?',
	'HELP_FAQ_ISSUES_WHOIS_PHPBB_ANSWER'	=> 'Tämän ohjelmiston muokkaamattoman version kehittäjä, julkaisija ja tekijänoikeuksien omistaja on <a href="https://www.phpbb.com/">phpBB Limited</a>. Ohjelmisto on julkaistu GNU General Public License -lisenssin version 2 (GPL-2.0) alaisena ja sitä voi levittää vapaasti. Lisätietoja saat <a href="https://www.phpbb.com/about/">phpBB:n sivustolta</a>.',
	'HELP_FAQ_ISSUES_WHOIS_PHPBB_QUESTION'	=> 'Kuka on luonut tämän ohjelmiston?',

	'HELP_FAQ_LOGIN_AUTO_LOGOUT_ANSWER'	=> 'Jos et merkitse <em>Muista minut</em> -valintaruutua kirjautumisen yhteydessä, keskustelupalsta pitää sinut sisäänkirjautuneena vain ennalta määritetyn ajan. Tämä estää muita henkilöitä väärinkäyttämästä käyttäjätiliäsi. Mikäli haluat pysyä kirjautuneena, merkitse valintaruutu kirjautuessasi sisään. Tämä ei ole suositeltavaa, jos vierailet keskustelupalstalla yhteisessä käytössä olevalta tietokoneelta, kuten kirjastossa, nettikahvilassa tai koulussa. Jos valintaruutu ei ole näkyvissä, keskustelupalstan ylläpito on poistanut toiminnon käytöstä.',
	'HELP_FAQ_LOGIN_AUTO_LOGOUT_QUESTION'	=> 'Miksi kirjaudun ulos automaattisesti?',
	'HELP_FAQ_LOGIN_CANNOT_LOGIN_ANSWER'	=> 'Tämä voi johtua monesta syystä. Ensimmäiseksi tarkista, että käyttäjätunnuksesi ja salasanasi on kirjoitettu oikein. Jos ne ovat oikein, ota yhteyttä keskustelupalstan ylläpitoon varmistaaksesi, ettet ole porttikiellossa. On myös mahdollista, että sivuston omistaja on tehnyt virheen muokatessaan keskustelupalstan asetuksia.',
	'HELP_FAQ_LOGIN_CANNOT_LOGIN_ANYMORE_ANSWER'	=> 'On mahdollista, että ylläpito on poistanut käyttäjätilisi tai estänyt sen käytön. Lisäksi monet keskustelupalstat poistavat säännöllisesti käyttäjiä, jotka eivät ole kirjoittaneet pitkään aikaan pienentääkseen tietokannan kokoa. Jos näin on tapahtunut, yritä rekisteröityä uudelleen ja ottaa enemmän osaa keskusteluihin.',
	'HELP_FAQ_LOGIN_CANNOT_LOGIN_ANYMORE_QUESTION'	=> 'Olen rekisteröitynyt aiemmin, mutta en pysty enää kirjautumaan sisään',
	'HELP_FAQ_LOGIN_CANNOT_LOGIN_QUESTION'	=> 'Miksi en voi kirjautua sisään?',
	'HELP_FAQ_LOGIN_CANNOT_REGISTER_ANSWER'	=> 'Keskustelupalstan ylläpito on voinut poistaa rekisteröitymisen käytöstä kokonaan estääkseen uusien käyttäjätilien luomisen. On myös mahdollista, että ylläpito on antanut porttikiellon IP-osoitteellesi tai kieltänyt valitsemasi käyttäjätunnuksen rekisteröimisen. Ota yhteyttä ylläpitoon saadaksesi lisää tietoa.',
	'HELP_FAQ_LOGIN_CANNOT_REGISTER_QUESTION'	=> 'Miksi en voi rekisteröityä?',
	'HELP_FAQ_LOGIN_COPPA_ANSWER'	=> 'COPPA eli Children’s Online Privacy and Protection Act vuodelta 1998 on Yhdysvalloissa säädetty laki, joka koskee henkilökohtaisten tietojen keräämistä alaikäisiltä WWW-sivustoilla. Lain mukaan WWW-sivuston on saatava kirjallinen lupa alle 13-vuotiaan lapsen tietojen keräämiseen tämän vanhemmilta tai huoltajalta. Jos et ole varma, koskeeko tämä sinua yrittäessäsi rekisteröityä tälle sivustolle, ota yhteyttä lakimieheen saadaksesi neuvoja. Huomaa, etteivät phpBB Limited tai tämän keskustelupalstan omistajat pysty tarjoamaan lakiapua eikä heihin tule ottaa yhteyttä minkäänlaisissa lakia koskevissa ongelmissa lukuun ottamatta tapauksia, jotka on kuvattu kysymyksessä ”Keneen otan yhteyttä tähän keskustelupalstaan liittyvissä väärinkäyttötapauksissa ja/tai lakia koskevissa kysymyksissä?”.',
	'HELP_FAQ_LOGIN_COPPA_QUESTION'	=> 'Mikä on COPPA?',
	'HELP_FAQ_LOGIN_DELETE_COOKIES_ANSWER'	=> '”Poista evästeet” -toiminto poistaa phpBB:n luomat evästeet, joiden avulla pysyt kirjautuneena sisään keskustelupalstalle. Evästeitä käytetään myös muihin toimintoihin kuten luettujen viestien seuraamiseen, jos keskustelupalstan ylläpito on ottanut toiminnot käyttöön. Jos sinulla on vaikeuksia kirjautua sisään tai ulos, evästeiden poisto saattaa auttaa.',
	'HELP_FAQ_LOGIN_DELETE_COOKIES_QUESTION'	=> 'Mitä ”Poista evästeet” -toiminto tekee?',
	'HELP_FAQ_LOGIN_LOST_PASSWORD_ANSWER'	=> 'Vaikka salasanaasi ei voi palauttaa, sen voi vaihtaa uuteen. Siirry kirjautumissivulle ja napsauta <em>Unohdin salasanani</em> -linkkiä. Seuraa annettuja ohjeita, niin pystyt pian kirjautumaan sisään uudelleen. Jos et kuitenkaan onnistu nollaamaan salasanaasi, ota yhteyttä keskustelupalstan ylläpitoon.',
	'HELP_FAQ_LOGIN_LOST_PASSWORD_QUESTION'	=> 'Kadotin salasanani',
	'HELP_FAQ_LOGIN_REGISTER_ANSWER'	=> 'Sinun ei välttämättä tarvitse. Keskustelupalstan ylläpito päättää, vaatiiko viestien lähetys rekisteröitymistä. Rekisteröinti antaa kuitenkin käyttöösi toimintoja, jotka eivät ole sallittuja vierailijoille. Tällaisia toimintoja ovat muun muassa avatar-kuvan määritys, yksityisviestit, sähköpostin lähetys muille käyttäjille ja käyttäjäryhmät. Rekisteröinti vie vain hetken, joten on suositeltavaa rekisteröityä.',
	'HELP_FAQ_LOGIN_REGISTER_CONFIRM_ANSWER'	=> 'Ensimmäiseksi tarkista, että käyttäjätunnuksesi ja salasanasi on kirjoitettu oikein. Jos ne ovat oikein, ongelmalle on kaksi mahdollista selitystä. Mikäli COPPA-tuki on päällä ja rekisteröidyit alle 13-vuotiaana, sinun täytyy noudattaa saamiasi ohjeita. Jotkin keskustelupalstat myös vaativat uusien käyttäjätilien aktivointia joko sinun itsesi tai ylläpitäjän toimesta. Jos sait sähköpostia, seuraa siinä olevia ohjeita. Jos et saanut sähköpostia, antamasi sähköpostiosoite voi olla väärä tai sähköpostiviesti on voinut päätyä roskapostikansioon. Jos antamasi sähköpostiosoite on varmasti oikein, ota yhteyttä keskustelupalstan ylläpitoon.',
	'HELP_FAQ_LOGIN_REGISTER_CONFIRM_QUESTION'	=> 'Olen rekisteröitynyt, mutta en voi kirjautua sisään',
	'HELP_FAQ_LOGIN_REGISTER_QUESTION'	=> 'Miksi minun täytyy rekisteröityä?',

	'HELP_FAQ_PMS_CANNOT_SEND_ANSWER'	=> 'Mahdollisia syitä on kolme: et ole rekisteröitynyt tai kirjautunut sisään, keskustelupalstan ylläpito on poistanut yksityisviestit käytöstä tai ylläpito on estänyt sinua lähettämästä viestejä. Ota yhteyttä ylläpitoon saadaksesi lisätietoa.',
	'HELP_FAQ_PMS_CANNOT_SEND_QUESTION'	=> 'En voi lähettää yksityisviestejä',
	'HELP_FAQ_PMS_SPAM_ANSWER'	=> 'Tämän keskustelupalstan sähköpostitoiminto sisältää turvamekanismeja, jotka seuraavat tällaisia viestejä lähettäviä käyttäjiä, joten lähetä ylläpidolle kopio saamastasi sähköpostista. On erittäin tärkeää, että liität mukaan myös viestin otsaketiedot, joiden avulla viestin lähettäjä voidaan selvittää.',
	'HELP_FAQ_PMS_SPAM_QUESTION'	=> 'Olen saanut roskapostia tai häiritseviä viestejä sähköpostiini tämän keskustelupalstan käyttäjältä',
	'HELP_FAQ_PMS_UNWANTED_ANSWER'	=> 'Voit poistaa valitsemiesi käyttäjien lähettämät yksityisviestit automaattisesti määrittämällä sääntöjä käyttäjän hallintapaneelissa. Jos saat häiritseviä yksityisviestejä tietyltä käyttäjältä, tee viesteistä ilmoitus valvojille. Heillä on valta estää käyttäjiä lähettämästä yksityisviestejä.',
	'HELP_FAQ_PMS_UNWANTED_QUESTION'	=> 'Saan ei-toivottuja yksityisviestejä',

	'HELP_FAQ_POSTING_BUMP_ANSWER'	=> 'Napsauttamalla ”Tönäise aihetta” -linkkiä aihetta katsoessasi voit ”tönäistä” aiheen ylimmäiseksi alueen ensimmäiselle sivulle. Jos linkkiä ei näy, aiheiden tönäisy voi olla kielletty tai tönäisyjen välillä ei ole vielä kulunut riittävästi aikaa. On myös mahdollista tönäistä aihetta vastaamalla siihen. Tarkasta kuitenkin keskustelupalstan säännöt ensin, ennen kuin teet niin.',
	'HELP_FAQ_POSTING_BUMP_QUESTION'	=> 'Kuinka tönäisen aihettani?',
	'HELP_FAQ_POSTING_CREATE_ANSWER'	=> 'Voit aloittaa uuden aiheen napsauttamalla ”Uusi aihe” -linkkiä tai kirjoittaa vastauksen aiheeseen napsauttamalla ”Kirjoita vastaus” -linkkiä. Huomaa, että viestien kirjoittaminen saattaa vaatia rekisteröitymisen. Luettelo oikeuksistasi kullakin alueella näkyy sivun alalaidassa alueita ja aiheita selatessasi.',
	'HELP_FAQ_POSTING_CREATE_QUESTION'	=> 'Kuinka aloitan uuden aiheen tai kirjoitan vastauksen?',
	'HELP_FAQ_POSTING_DRAFT_ANSWER'	=> 'Painiketta napsauttamalla voit tallentaa senhetkisen viestisi, jotta voit viimeistellä ja lähettää sen myöhemmin. Tallennetun luonnoksen voi ladata käyttäjän hallintapaneelista.',
	'HELP_FAQ_POSTING_DRAFT_QUESTION'	=> 'Mikä on ”Tallenna luonnos” -painikkeen tarkoitus viestien kirjoitussivulla?',
	'HELP_FAQ_POSTING_EDIT_DELETE_ANSWER'	=> 'Jos et ole keskustelupalstan ylläpitäjä tai valvoja, voit muokata ja poistaa vain omia viestejäsi. Voit muokata viestiä napsauttamalla viestin yhteydessä olevaa Muokkaa-painiketta, ellei muokkauksen aikaraja ole umpeutunut. Jos jokin toinen käyttäjä on jo vastannut viestiin, viestin loppuun lisätään teksti, joka ilmoittaa muokkausten lukumäärän ja ajankohdan. Tekstiä ei näy, jos ylläpitäjä tai valvoja on muokannut viestiä. He saattavat silti jättää huomautuksen viestin muokkauksesta omasta tahdostaan. Huomaa, että tavalliset käyttäjät eivät voi poistaa viestejä, joihin on jo vastattu.',
	'HELP_FAQ_POSTING_EDIT_DELETE_QUESTION'	=> 'Kuinka muokkaan viestiä tai poistan sen?',
	'HELP_FAQ_POSTING_FORUM_RESTRICTED_ANSWER'	=> 'Joillekin alueille pääsy saattaa olla rajoitettu vain tietyille käyttäjille tai ryhmille. Ota yhteyttä valvojiin tai ylläpitoon pyytääksesi lupaa päästä haluamallesi alueelle.',
	'HELP_FAQ_POSTING_FORUM_RESTRICTED_QUESTION'	=> 'Miksi en pääse tietyille alueille?',
	'HELP_FAQ_POSTING_NO_ATTACHMENTS_ANSWER'	=> 'Keskustelupalstan ylläpito on asettanut liitetiedostoja koskevat oikeudet alue-, ryhmä- ja käyttäjäkohtaisesti. On mahdollista, että liitetiedostojen lähetys on kielletty tietyillä alueilla tai että vain tietyillä ryhmillä on oikeus lähettää liitetiedostoja. Ota yhteyttä ylläpitoon, jos et ole varma, miksi et pysty lähettämään liitetiedostoja.',
	'HELP_FAQ_POSTING_NO_ATTACHMENTS_QUESTION'	=> 'Miksi en voi lähettää liitetiedostoja?',
	'HELP_FAQ_POSTING_POLL_ADD_ANSWER'	=> 'Äänestyksen vaihtoehtojen enimmäismäärä on keskustelupalstan ylläpidon asettama. Jos koet tarvitsevasi enemmän vaihtoehtoja, ota yhteyttä ylläpitoon.',
	'HELP_FAQ_POSTING_POLL_ADD_QUESTION'	=> 'Miksi en voi lisätä enempää vaihtoehtoja äänestykseen?',
	'HELP_FAQ_POSTING_POLL_CREATE_ANSWER'	=> 'Kun aloitat uuden aiheen tai muokkaat aiheen ensimmäistä viestiä, napsauta viestikentän alapuolella olevaa ”Luo äänestys” -välilehteä. Jos välilehteä ei näy, sinulla ei ole vaadittavia oikeuksia luoda äänestyksiä. Anna äänestykselle otsikko ja vähintään kaksi vaihtoehtoa. Varmista, että kukin vaihtoehto on syötetty omalle rivilleen tekstikentässä.',
	'HELP_FAQ_POSTING_POLL_CREATE_QUESTION'	=> 'Kuinka luon äänestyksen?',
	'HELP_FAQ_POSTING_POLL_EDIT_ANSWER'	=> 'Viestien tavoin myös äänestystä voivat muokata vain alkuperäinen kirjoittaja, valvojat ja ylläpitäjät. Pääset muokkaamaan äänestystä muokkaamalla aiheen ensimmäistä viestiä. Jos kukaan ei ole vielä äänestänyt, voit poistaa äänestyksen tai muokata sen vaihtoehtoja. Jos ääniä on jo annettu, vain valvojat ja ylläpitäjät voivat muokata äänestystä tai poistaa sen. Tällä estetään äänestyksen vaihtoehtojen muuttaminen kesken äänestyksen.',
	'HELP_FAQ_POSTING_POLL_EDIT_QUESTION'	=> 'Kuinka muokkaan äänestystä tai poistan sen?',
	'HELP_FAQ_POSTING_QUEUE_ANSWER'	=> 'Keskustelupalstan ylläpito on voinut asettaa tietylle alueelle lähetetyt viestit tarkastettavaksi ennen julkaisua. On myös mahdollista, että ylläpito on sijoittanut sinut ryhmään, jonka jäsenten lähettämät viestit vaativat tarkastuksen. Ota yhteyttä ylläpitoon saadaksesi lisätietoja.',
	'HELP_FAQ_POSTING_QUEUE_QUESTION'	=> 'Miksi viestini vaatii hyväksynnän?',
	'HELP_FAQ_POSTING_REPORT_ANSWER'	=> 'Jos ylläpito on sallinut viestien ilmoittamisen, jokaisen viestin yhteydessä pitäisi näkyä painike, jonka avulla voit ilmoittaa viestistä. Napsauta painiketta ja seuraa annettuja ohjeita.',
	'HELP_FAQ_POSTING_REPORT_QUESTION'	=> 'Kuinka ilmoitan asiattomasta viestistä valvojalle?',
	'HELP_FAQ_POSTING_SIGNATURE_ANSWER'	=> 'Luo ensin allekirjoitus käyttäjän hallintapaneelissa ja merkitse <em>Lisää allekirjoitus</em> -valintaruutu viestiä kirjoittaessasi. Voit myös lisätä allekirjoituksen viesteihisi automaattisesti valitsemalla sopivan vaihtoehdon käyttäjän hallintapaneelin ”Keskustelupalstan asetukset” -osiossa. Voit silti estää allekirjoituksen lisäämisen yksittäiseen viestiin poistamalla merkinnän allekirjoitusta koskevasta valintaruudusta viestiä kirjoittaessasi.',
	'HELP_FAQ_POSTING_SIGNATURE_QUESTION'	=> 'Kuinka lisään allekirjoituksen viestiini?',
	'HELP_FAQ_POSTING_WARNING_ANSWER'	=> 'Jokaisella keskustelupalstalla on omat sääntönsä. Jos rikot sääntöjä, voit saada varoituksen. Huomaa, että tämä on keskustelupalstan ylläpidon päätös eikä phpBB Limited ole millään tavalla tekemisissä tältä sivustolta saamiesi varoitusten kanssa. Ota yhteyttä ylläpitoon, jos et ole varma, miksi sait varoituksen.',
	'HELP_FAQ_POSTING_WARNING_QUESTION'	=> 'Miksi sain varoituksen?',

	'HELP_FAQ_SEARCH_BLANK_ANSWER'	=> 'Hakusi tuotti liian monta tulosta palvelimen käsiteltäväksi. Siirry tarkennettuun hakuun sekä käytä täsmällisempiä hakusanoja ja rajaa haettavia alueita.',
	'HELP_FAQ_SEARCH_BLANK_QUESTION'	=> 'Miksi haku palauttaa vain tyhjän sivun?',
	'HELP_FAQ_SEARCH_FORUM_ANSWER'	=> 'Syötä haettavat sanat sivun ylälaidassa sijaitsevaan hakulaatikkoon ja napsauta Etsi-painiketta. Voit hakea tarkemmilla ehdoilla napsauttamalla hakulaatikon vieressä olevaa ”Tarkennettu haku” -linkkiä, joka näkyy jokaisella keskustelupalstan sivulla. Linkin sijainti riippuu käytössä olevasta tyylistä.',
	'HELP_FAQ_SEARCH_FORUM_QUESTION'	=> 'Kuinka voin etsiä viestejä?',
	'HELP_FAQ_SEARCH_MEMBERS_ANSWER'	=> 'Siirry Käyttäjät-sivulle ja napsauta ”Etsi käyttäjä” -linkkiä.',
	'HELP_FAQ_SEARCH_MEMBERS_QUESTION'	=> 'Kuinka voin etsiä käyttäjiä?',
	'HELP_FAQ_SEARCH_NO_RESULT_ANSWER'	=> 'Hakusi oli luultavasti liian epäselvä ja sisälsi yleisiä sanoja, joita keskustelupalsta ei ole indeksoinut. Käytä täsmällisempiä sanoja ja hyödynnä tarkennetun haun asetuksia.',
	'HELP_FAQ_SEARCH_NO_RESULT_QUESTION'	=> 'Miksi haku ei palauta yhtään tulosta?',
	'HELP_FAQ_SEARCH_OWN_ANSWER'	=> 'Näet lähettämäsi viestit napsauttamalla joko ”Näytä omat viestini” -linkkiä käyttäjän hallintapaneelissa tai ”Hae käyttäjän viestit” -linkkiä profiilisivullasi tai ”Pikalinkit”-valikkoa sivun ylälaidassa. Aloittamasi aiheet löydät käyttämällä tarkennettua hakua ja syöttämällä tarvittavat tiedot hakukenttiin.',
	'HELP_FAQ_SEARCH_OWN_QUESTION'	=> 'Kuinka löydän lähettämäni viestit ja aloittamani aiheet?',

	'HELP_FAQ_USERSETTINGS_AVATAR_ANSWER'	=> 'Käyttäjätunnuksesi alapuolella voi olla kaksi kuvaa. Niistä ensimmäinen on yhdistetty arvonimeesi ja sisältää yleensä tähtiä tai muita kuvioita, jotka ilmaisevat, kuinka monta viestiä olet kirjoittanut keskustelupalstalla. Toinen ja tavallisesti suurempi kuva on nimeltään avatar. Avatarit ovat yleensä yksilöllisiä ja käyttäjien henkilökohtaisesti valitsemia kuvia.',
	'HELP_FAQ_USERSETTINGS_AVATAR_DISPLAY_ANSWER'	=> 'Voit valita avatarin käyttäjän hallintapaneelin Profiili-välilehdellä. Keskustelupalstan ylläpito päättää, ovatko avatarit sallittuja ja kuinka niitä voi käyttää. Jos et voi valita avataria, ota yhteyttä ylläpitoon ja tiedustele heiltä syytä.',
	'HELP_FAQ_USERSETTINGS_AVATAR_DISPLAY_QUESTION'	=> 'Kuinka lisään itselleni avatar-kuvan?',
	'HELP_FAQ_USERSETTINGS_AVATAR_QUESTION'	=> 'Mitä käyttäjätunnukseni vieressä olevat kuvat ovat?',
	'HELP_FAQ_USERSETTINGS_CHANGE_SETTINGS_ANSWER'	=> 'Jos olet rekisteröitynyt käyttäjä, voit muuttaa asetuksiasi käyttäjän hallintapaneelissa. Linkki hallintapaneeliin tulee esiin tavallisesti sivun yläosassa näkyvää käyttäjätunnusta napsauttamalla.',
	'HELP_FAQ_USERSETTINGS_CHANGE_SETTINGS_QUESTION'	=> 'Kuinka muutan asetuksiani?',
	'HELP_FAQ_USERSETTINGS_EMAIL_LOGIN_ANSWER'	=> 'Vain rekisteröityneet käyttäjät voivat lähettää sähköpostia muille käyttäjille sisäänrakennetun järjestelmän kautta. Tällä estetään järjestelmän väärinkäyttö.',
	'HELP_FAQ_USERSETTINGS_EMAIL_LOGIN_QUESTION'	=> 'Miksi minun täytyy kirjautua sisään, kun yritän lähettää käyttäjälle sähköpostia?',
	'HELP_FAQ_USERSETTINGS_HIDE_ONLINE_ANSWER'	=> 'Käyttäjän hallintapaneelissa on ”Keskustelupalstan asetukset” -osio, joka sisältää <em>Piilota paikallaoloni</em> -asetuksen. Jos otat tämän toiminnon käyttöön, näkyy paikallaolosi vain ylläpitäjille, valvojille ja sinulle itsellesi. Tällöin sinut luokitellaan piilotetuksi käyttäjäksi.',
	'HELP_FAQ_USERSETTINGS_HIDE_ONLINE_QUESTION'	=> 'Kuinka estän käyttäjätunnukseni näkymisen paikallaolijoiden luettelossa?',
	'HELP_FAQ_USERSETTINGS_LANGUAGE_ANSWER'	=> 'Joko ylläpito ei ole asentanut tarvittavaa kielipakettia tai kukaan ei ole kääntänyt keskustelupalstaa kielellesi. Kysy ylläpidolta, voivatko he asentaa haluamasi kielipaketin. Jos kielipakettia ei ole olemassa, voit itse luoda uuden käännöksen. Lisätietoja saat <a href="https://www.phpbb.com/">phpBB:n &reg; sivustolta</a>.',
	'HELP_FAQ_USERSETTINGS_LANGUAGE_QUESTION'	=> 'Kieltäni ei näy luettelossa',
	'HELP_FAQ_USERSETTINGS_RANK_ANSWER'	=> 'Käyttäjätunnuksen alapuolella näkyvä arvonimi on osoitus käyttäjän kirjoittamien viestien lukumäärästä tai hänen asemastaan ylläpitäjänä tai valvojana. Tavallisesti et voi muuttaa arvonimeäsi suoraan, koska arvonimet ovat keskustelupalstan ylläpidon valitsemia. Älä kirjoita tarpeettomia viestejä pelkästään nostaaksesi arvoasi, sillä useimmat keskustelupalstat eivät suvaitse sitä, vaan seurauksena saattaa olla viestien lukumäärän alennus, varoitus tai porttikielto.',
	'HELP_FAQ_USERSETTINGS_RANK_QUESTION'	=> 'Mikä arvonimeni on ja kuinka muutan sitä?',
	'HELP_FAQ_USERSETTINGS_SERVERTIME_ANSWER'	=> 'Jos valitsemasi aikavyöhyke on varmasti oikea, on palvelimen kello väärässä ajassa. Ota yhteyttä keskustelupalstan ylläpitoon ja ilmoita heille ongelmasta.',
	'HELP_FAQ_USERSETTINGS_SERVERTIME_QUESTION'	=> 'Vaihdoin aikavyöhykettä, mutta kellonaika on silti väärä',
	'HELP_FAQ_USERSETTINGS_TIMEZONE_ANSWER'	=> 'On mahdollista, että keskustelupalstalla näkyvä aika on eri aikavyöhykkeeltä kuin missä itse olet. Voit ottaa käyttöön oman alueesi aikavyöhykkeen käyttäjän hallintapaneelissa. Huomaa, että monien muiden asetusten tavoin myös aikavyöhykkeen vaihto on mahdollista vain rekisteröityneille käyttäjille.',
	'HELP_FAQ_USERSETTINGS_TIMEZONE_QUESTION'	=> 'Kellonaika on väärä',
));
