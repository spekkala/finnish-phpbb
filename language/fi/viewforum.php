<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ACTIVE_TOPICS'	=> 'Aktiiviset aiheet',
	'ANNOUNCEMENTS'	=> 'Tiedotteet',
	'FORUM_PERMISSIONS'	=> 'Alueen oikeudet',
	'ICON_ANNOUNCEMENT'	=> 'Tiedote',
	'ICON_STICKY'	=> 'Pysyvä',
	'LOGIN_NOTIFY_FORUM'	=> 'Sinulle on ilmoitettu tästä alueesta. Ole hyvä ja kirjaudu sisään katsoaksesi sen sisältöä.',
	'MARK_TOPICS_READ'	=> 'Merkitse aiheet luetuiksi',
	'NEW_POSTS_HOT'	=> 'Uudet viestit (suositut)',	// Not used anymore
	'NEW_POSTS_LOCKED'	=> 'Uudet viestit (lukitut)',	// Not used anymore
	'NO_NEW_POSTS_HOT'	=> 'Ei uusia viestejä (suositut)',	// Not used anymore
	'NO_NEW_POSTS_LOCKED'	=> 'Ei uusia viestejä (lukitut)',	// Not used anymore
	'NO_READ_ACCESS'	=> 'Sinulla ei ole tämän alueen aiheiden lukemiseen vaadittavia oikeuksia.',
	'NO_UNREAD_POSTS_HOT'	=> 'Ei lukemattomia viestejä (suositut)',
	'NO_UNREAD_POSTS_LOCKED'	=> 'Ei lukemattomia viestejä (lukitut)',
	'POST_FORUM_LOCKED'	=> 'Alue on lukittu',
	'TOPICS_MARKED'	=> 'Tämän alueen aiheet on nyt merkitty luetuiksi.',
	'UNREAD_POSTS_HOT'	=> 'Lukemattomat viestit (suositut)',
	'UNREAD_POSTS_LOCKED'	=> 'Lukemattomat viestit (lukitut)',
	'VIEW_FORUM'	=> 'Näytä alue',
	'VIEW_FORUM_TOPICS'		=> array(
		1	=> '%d aihe',
		2	=> '%d aihetta',
	),
));
