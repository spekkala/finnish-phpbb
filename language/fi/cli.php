<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'CLI_CONFIG_CANNOT_CACHED'			=> 'Valitse tämä vaihtoehto, jos asetus muuttuu niin usein, ettei sitä kannata tallentaa välimuistiin.',
	'CLI_CONFIG_CURRENT'				=> 'Asetuksen nykyinen arvo (0 ja 1 ilmaisevat Boolean-arvoja)',
	'CLI_CONFIG_DELETE_SUCCESS'			=> 'Asetus %s on poistettu.',
	'CLI_CONFIG_NEW'					=> 'Asetuksen uusi arvo (0 ja 1 ilmaisevat Boolean-arvoja)',
	'CLI_CONFIG_NOT_EXISTS'				=> 'Asetusta %s ei ole olemassa',
	'CLI_CONFIG_OPTION_NAME'			=> 'Asetuksen nimi',
	'CLI_CONFIG_PRINT_WITHOUT_NEWLINE'	=> 'Valitse tämä vaihtoehto, jos arvo täytyy tulostaa ilman lopussa olevaa rivinvaihtoa.',
	'CLI_CONFIG_INCREMENT_BY'			=> 'Määrä, jolla kasvatetaan',
	'CLI_CONFIG_INCREMENT_SUCCESS'		=> 'Asetuksen %s arvoa on kasvatettu',
	'CLI_CONFIG_SET_FAILURE'			=> 'Asetuksen %s muutos epäonnistui',
	'CLI_CONFIG_SET_SUCCESS'			=> 'Asetus %s on muutettu',

	'CLI_DESCRIPTION_CRON_LIST'					=> 'Näyttää luettelon ajastetuista tehtävistä.',
	'CLI_DESCRIPTION_CRON_RUN'					=> 'Suorittaa kaikki valmiina olevat ajastetut tehtävät.',
	'CLI_DESCRIPTION_CRON_RUN_ARGUMENT_1'		=> 'Suoritettavan tehtävän nimi',
	'CLI_DESCRIPTION_DB_LIST'					=> 'Luettele kaikki asennetut ja käytettävissä olevat siirrokset.',
	'CLI_DESCRIPTION_DB_MIGRATE'				=> 'Päivittää tietokannan käyttämällä siirroksia.',
	'CLI_DESCRIPTION_DB_REVERT'					=> 'Kumoa siirros.',
	'CLI_DESCRIPTION_DELETE_CONFIG'				=> 'Poistaa asetuksen',
	'CLI_DESCRIPTION_DISABLE_EXTENSION'			=> 'Poistaa valitun laajennuksen käytöstä.',
	'CLI_DESCRIPTION_ENABLE_EXTENSION'			=> 'Ottaa valitun laajennuksen käyttöön.',
	'CLI_DESCRIPTION_FIND_MIGRATIONS'			=> 'Etsii siirroksia, jotka eivät ole riippuvuuksia.',
	'CLI_DESCRIPTION_FIX_LEFT_RIGHT_IDS'		=> 'Korjaa alueiden ja moduulien puurakenteen.',
	'CLI_DESCRIPTION_GET_CONFIG'				=> 'Lukee asetuksen arvon',
	'CLI_DESCRIPTION_INCREMENT_CONFIG'			=> 'Kasvattaa asetuksen kokonaislukuarvoa',
	'CLI_DESCRIPTION_LIST_EXTENSIONS'			=> 'Luettelee kaikki tietokannassa ja tiedostojärjestelmässä olevat laajennukset.',

	'CLI_DESCRIPTION_OPTION_ENV'				=> 'Ympäristön nimi.',
	'CLI_DESCRIPTION_OPTION_SAFE_MODE'			=> 'Suorita pelkistetyssä tilassa (ilman laajennuksia).',
	'CLI_DESCRIPTION_OPTION_SHELL'				=> 'Käynnistä komentorivi.',

	'CLI_DESCRIPTION_PURGE_EXTENSION'			=> 'Puhdistaa valitun laajennuksen.',

	'CLI_DESCRIPTION_REPARSER_LIST'						=> 'Luettelee, minkälaisia tekstejä on mahdollista jäsentää.',
	'CLI_DESCRIPTION_REPARSER_AVAILABLE'				=> 'Käytettävissä olevat jäsentimet:',
	'CLI_DESCRIPTION_REPARSER_REPARSE'					=> 'Jäsentää tallennettua tekstiä nykyisillä muotoilupalveluilla.',
	'CLI_DESCRIPTION_REPARSER_REPARSE_ARG_1'			=> 'Jäsennettävän tekstin tyyppi. Jätä kenttä tyhjäksi, jos haluat jäsentää kaiken tekstin.',
	'CLI_DESCRIPTION_REPARSER_REPARSE_OPT_DRY_RUN'		=> 'Älä tallenna muutoksia, vaan näytä suoritettavat toimenpiteet',
	'CLI_DESCRIPTION_REPARSER_REPARSE_OPT_RANGE_MIN'	=> 'Pienin käsiteltävien tietueiden tunniste',
	'CLI_DESCRIPTION_REPARSER_REPARSE_OPT_RANGE_MAX'	=> 'Suurin käsiteltävien tietueiden tunniste',
	'CLI_DESCRIPTION_REPARSER_REPARSE_OPT_RANGE_SIZE'	=> 'Yhdellä kerralla käsiteltävien tietueiden arvioitu lukumäärä',
	'CLI_DESCRIPTION_REPARSER_REPARSE_OPT_RESUME'		=> 'Aloita jäsennys kohdasta, johon edellinen suoritus päättyi',

	'CLI_DESCRIPTION_RECALCULATE_EMAIL_HASH'			=> 'Laskee käyttäjätaulun user_email_hash-sarakkeen arvot uudelleen.',

	'CLI_DESCRIPTION_SET_ATOMIC_CONFIG'					=> 'Muuttaa asetuksen arvoa vain, jos vanha arvo vastaa nykyistä arvoa',
	'CLI_DESCRIPTION_SET_CONFIG'						=> 'Muuttaa asetuksen arvoa',

	'CLI_DESCRIPTION_THUMBNAIL_DELETE'					=> 'Poista kaikki esikatselukuvat.',
	'CLI_DESCRIPTION_THUMBNAIL_GENERATE'				=> 'Luo puuttuvat esikatselukuvat.',
	'CLI_DESCRIPTION_THUMBNAIL_RECREATE'				=> 'Luo kaikki esikatselukuvat uudelleen.',

	'CLI_DESCRIPTION_UPDATE_CHECK'					=> 'Tarkasta, onko keskustelupalsta ajan tasalla.',
	'CLI_DESCRIPTION_UPDATE_CHECK_ARGUMENT_1'		=> 'Tarkastettavan laajennuksen nimi (jos arvo on ”all”, kaikki laajennukset tarkastetaan)',
	'CLI_DESCRIPTION_UPDATE_CHECK_OPTION_CACHE'		=> 'Suorita tarkastus välimuistia käyttäen.',
	'CLI_DESCRIPTION_UPDATE_CHECK_OPTION_STABILITY'	=> 'Suorita komento vain joko vakaille tai epävakaille versioille.',

	'CLI_DESCRIPTION_UPDATE_HASH_BCRYPT'		=> 'Päivittää vanhentuneet salasanatiivisteet bcrypt-algoritmilla.',

	'CLI_ERROR_INVALID_STABILITY' => 'Vakausasteeksi ”%s” täytyy asettaa joko ”stable” tai ”unstable”.',

	'CLI_DESCRIPTION_USER_ACTIVATE'				=> 'Aktivoi (tai poista käytöstä) käyttäjätili.',
	'CLI_DESCRIPTION_USER_ACTIVATE_USERNAME'	=> 'Aktivoitavan tilin käyttäjätunnus.',
	'CLI_DESCRIPTION_USER_ACTIVATE_DEACTIVATE'	=> 'Poista käyttäjän tili käytöstä',
	'CLI_DESCRIPTION_USER_ACTIVATE_ACTIVE'		=> 'Käyttäjä on jo aktiivinen.',
	'CLI_DESCRIPTION_USER_ACTIVATE_INACTIVE'	=> 'Käyttäjän tili on jo poistettu käytöstä.',
	'CLI_DESCRIPTION_USER_ADD'					=> 'Luo uusi käyttäjä.',
	'CLI_DESCRIPTION_USER_ADD_OPTION_USERNAME'	=> 'Uuden käyttäjän nimi',
	'CLI_DESCRIPTION_USER_ADD_OPTION_PASSWORD'	=> 'Uuden käyttäjän salasana',
	'CLI_DESCRIPTION_USER_ADD_OPTION_EMAIL'		=> 'Uuden käyttäjän sähköpostiosoite',
	'CLI_DESCRIPTION_USER_ADD_OPTION_NOTIFY'	=> 'Lähetä tilin aktivointia koskeva sähköpostiviesti uudelle käyttäjälle (viestiä ei lähetetä oletusarvoisesti)',
	'CLI_DESCRIPTION_USER_DELETE'				=> 'Poista käyttäjätili.',
	'CLI_DESCRIPTION_USER_DELETE_USERNAME'		=> 'Poistettavan tilin käyttäjätunnus',
	'CLI_DESCRIPTION_USER_DELETE_OPTION_POSTS'	=> 'Poista kaikki käyttäjän kirjoittamat viestit. Jos tätä asetusta ei ole valittu, käyttäjän viestit säilytetään.',
	'CLI_DESCRIPTION_USER_RECLEAN'				=> 'Puhdista käyttäjänimet uudelleen.',

	'CLI_EXTENSION_DISABLE_FAILURE'		=> 'Laajennuksen %s poisto käytöstä epäonnistui',
	'CLI_EXTENSION_DISABLE_SUCCESS'		=> 'Laajennus %s on poistettu käytöstä',
	'CLI_EXTENSION_DISABLED'			=> 'Laajennus %s ei ole käytössä',
	'CLI_EXTENSION_ENABLE_FAILURE'		=> 'Laajennuksen %s ottaminen käyttöön epäonnistui',
	'CLI_EXTENSION_ENABLE_SUCCESS'		=> 'Laajennus %s on otettu käyttöön',
	'CLI_EXTENSION_ENABLED'				=> 'Laajennus %s on jo käytössä',
	'CLI_EXTENSION_NAME'				=> 'Laajennuksen nimi',
	'CLI_EXTENSION_PURGE_FAILURE'		=> 'Laajennuksen %s puhdistus epäonnistui',
	'CLI_EXTENSION_PURGE_SUCCESS'		=> 'Laajennus %s on puhdistettu',
	'CLI_EXTENSION_UPDATE_FAILURE'		=> 'Laajennuksen %s päivitys epäonnistui',
	'CLI_EXTENSION_UPDATE_SUCCESS'		=> 'Laajennus %s on päivitetty',
	'CLI_EXTENSION_NOT_FOUND'			=> 'Laajennuksia ei löytynyt.',
	'CLI_EXTENSIONS_AVAILABLE'			=> 'Käytettävissä',
	'CLI_EXTENSIONS_DISABLED'			=> 'Pois käytöstä',
	'CLI_EXTENSIONS_ENABLED'			=> 'Käytössä',

	'CLI_FIXUP_FIX_LEFT_RIGHT_IDS_SUCCESS'		=> 'Alueiden ja moduulien puurakenne on korjattu.',
	'CLI_FIXUP_RECALCULATE_EMAIL_HASH_SUCCESS'	=> 'Sähköpostiosoitteiden tiivisteet on laskettu uudelleen.',
	'CLI_FIXUP_UPDATE_HASH_BCRYPT_SUCCESS'		=> 'Vanhentuneet salasanatiivisteet on päivitetty bcrypt-algoritmilla.',

	'CLI_MIGRATION_NAME'					=> 'Siirroksen nimi ja nimiavaruus (käytä kauttaviivoja kenoviivojen sijaan ongelmien välttämiseksi).',
	'CLI_MIGRATIONS_AVAILABLE'				=> 'Käytettävissä olevat siirrokset',
	'CLI_MIGRATIONS_INSTALLED'				=> 'Asennetut siirrokset',
	'CLI_MIGRATIONS_ONLY_AVAILABLE'		    => 'Näytä vain käytettävissä olevat siirrokset',
	'CLI_MIGRATIONS_EMPTY'                  => 'Siirroksia ei ole.',

	'CLI_REPARSER_REPARSE_REPARSING'		=> 'Jäsennetään kohdetta %1$s (väli %2$d..%3$d)',
	'CLI_REPARSER_REPARSE_REPARSING_START'	=> 'Jäsennetään kohdetta %s',
	'CLI_REPARSER_REPARSE_SUCCESS'			=> 'Jäsennys onnistui',

	// In all the case %1$s is the logical name of the file and %2$s the real name on the filesystem
	// eg: big_image.png (2_a51529ae7932008cf8454a95af84cacd) generated.
	'CLI_THUMBNAIL_DELETED'		=> '%1$s (%2$s) poistettu.',
	'CLI_THUMBNAIL_DELETING'	=> 'Poistetaan esikatselukuvia',
	'CLI_THUMBNAIL_SKIPPED'		=> '%1$s (%2$s) ohitettu.',
	'CLI_THUMBNAIL_GENERATED'	=> '%1$s (%2$s) luotu.',
	'CLI_THUMBNAIL_GENERATING'	=> 'Luodaan esikatselukuvia',
	'CLI_THUMBNAIL_GENERATING_DONE'	=> 'Kaikki esikatselukuvat on luotu uudelleen.',
	'CLI_THUMBNAIL_DELETING_DONE'	=> 'Kaikki esikatselukuvat on poistettu.',

	'CLI_THUMBNAIL_NOTHING_TO_GENERATE'	=> 'Luotavia esikatselukuvia ei ole.',
	'CLI_THUMBNAIL_NOTHING_TO_DELETE'	=> 'Poistettavia esikatselukuvia ei ole.',

	'CLI_USER_ADD_SUCCESS'		=> 'Käyttäjä %s on luotu.',
	'CLI_USER_DELETE_CONFIRM'	=> 'Haluatko varmasti poistaa kohteen ”%s”? [y/N]',
	'CLI_USER_RECLEAN_START'	=> 'Puhdistetaan käyttäjänimiä',
	'CLI_USER_RECLEAN_DONE'		=> [
		0	=> 'Puhdistus on valmis (käyttäjänimiä ei tarvinnut puhdistaa).',
		1	=> 'Puhdistus on valmis (%d käyttäjänimi puhdistettiin).',
		2	=> 'Puhdistus on valmis (%d käyttäjänimeä puhdistettiin).',
	],
));

// Additional help for commands.
$lang = array_merge($lang, array(
	'CLI_HELP_CRON_RUN'			=> $lang['CLI_DESCRIPTION_CRON_RUN'] . ' Voit halutessasi suorittaa vain yksittäisen ajastetun tehtävän syöttämällä tehtävän nimen.',
	'CLI_HELP_USER_ACTIVATE'	=> 'Aktivoi käyttäjätili tai vaihtoehtoisesti poista tili käytöstä <info>--deactivate</info>-valitsimella.
Tarvittaessa voit lähettää aktivointiviestin sähköpostitse käyttäjälle <info>--send-email</info>-valitsimella.',
	'CLI_HELP_USER_ADD'			=> 'Komento <info>%command.name%</info> luo uuden käyttäjän:
Jos komento suoritetaan ilman valitsimia, sinua pyydetään syöttämään lisätietoja.
Tarvittaessa voit lähettää sähköpostiviestin uudelle käyttäjälle <info>--send-email</info>-valitsimella.',
	'CLI_HELP_USER_RECLEAN'		=> 'Puhdistustoiminto tarkastaa tallennetut käyttäjänimet ja varmistaa, että niistä on olemassa myös puhdistetut muodot. Puhdistetut käyttäjänimet ovat kirjainkoosta riippumattomia, NFC-normalisoituja ja ASCII-koodattuja.',
));
