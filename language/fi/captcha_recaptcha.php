<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'RECAPTCHA_LANG'            => 'fi',
	'RECAPTCHA_NOT_AVAILABLE'   => 'ReCAPTCHAn käyttö vaatii käyttäjätilin, jonka voit luoda osoitteessa <a href="http://www.google.com/recaptcha">www.google.com/recaptcha</a>.',
	'CAPTCHA_RECAPTCHA'         => 'reAPTCHA',
	'RECAPTCHA_INCORRECT'       => 'Antamasi ratkaisu oli väärä',
	'RECAPTCHA_NOSCRIPT'			=> 'Ota käyttöön selaimesi JavaScript-tuki ladataksesi tehtävän.',

	'RECAPTCHA_PUBLIC'          => 'Julkinen reCAPTCHA-avain',
	'RECAPTCHA_PUBLIC_EXPLAIN'  => 'Julkinen reCAPTCHA-avaimesi. Avaimen voi hankkia osoitteesta <a href="http://www.google.com/recaptcha">www.google.com/recaptcha</a>.',
	'RECAPTCHA_PRIVATE'         => 'Yksityinen reCAPTCHA-avain',
	'RECAPTCHA_PRIVATE_EXPLAIN' => 'Yksityinen reCAPTCHA-avaimesi. Avaimen voi hankkia osoitteesta <a href="http://www.google.com/recaptcha">www.google.com/recaptcha</a>.',

	'RECAPTCHA_EXPLAIN'         => 'Roskapostia lähettävien bottien estämiseksi tämä keskustelupalsta vaatii, että suoritat seuraavan tehtävän.'
));
