<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ABOUT_USER'	=> 'Profiili',
	'ACTIVE_IN_FORUM'	=> 'Aktiivisin alueella',
	'ACTIVE_IN_TOPIC'	=> 'Aktiivisin aiheessa',
	'ADD_FOE'	=> 'Lisää vihamieheksi',
	'ADD_FRIEND'	=> 'Lisää kaveriksi',
	'AFTER'	=> 'Jälkeen',
	'ALL'	=> 'Kaikki',

	'BEFORE'	=> 'Ennen',

	'CC_SENDER'	=> 'Lähetä kopio minulle',
	'CONTACT_ADMIN'			=> 'Ota yhteyttä keskustelupalstan ylläpitoon',

	'DEST_LANG'	=> 'Kieli',
	'DEST_LANG_EXPLAIN'	=> 'Valitse sopiva kieli (jos saatavilla) tämän viestin vastaanottajalle.',

	'EDIT_PROFILE'			=> 'Muokkaa profiilia',
	'EMAIL_BODY_EXPLAIN'	=> 'Tämä viesti lähetetään tavallisena tekstinä. Älä käytä HTML:ää tai BBCodea. Viestin vastausosoitteeksi asetetaan sinun sähköpostiosoitteesi.',
	'EMAIL_DISABLED'	=> 'Valitettavasti kaikki sähköpostiin liittyvät toiminnot on poistettu käytöstä.',
	'EMAIL_SENT'	=> 'Sähköposti on lähetetty.',
	'EMAIL_TOPIC_EXPLAIN'	=> 'Tämä viesti lähetetään tavallisena tekstinä. Älä käytä HTML:ää tai BBCodea. Huomaa, että aiheen tiedot on jo sisällytetty viestiin. Viestin vastausosoitteeksi asetetaan sinun sähköpostiosoitteesi.',
	'EMPTY_ADDRESS_EMAIL'	=> 'Anna kelvollinen sähköpostiosoite.',
	'EMPTY_MESSAGE_EMAIL'	=> 'Kirjoita viesti.',
	'EMPTY_MESSAGE_IM'	=> 'Kirjoita viesti.',
	'EMPTY_NAME_EMAIL'	=> 'Anna vastaanottajan oikea nimi.',
	'EMPTY_SENDER_EMAIL'	=> 'Anna kelvollinen sähköpostiosoite.',
	'EMPTY_SENDER_NAME'		=> 'Anna nimi.',
	'EMPTY_SUBJECT_EMAIL'	=> 'Anna viestille otsikko.',
	'EQUAL_TO'	=> 'Yhtä kuin',

	'FIND_USERNAME_EXPLAIN'	=> 'Käytä tätä lomaketta etsiäksesi käyttäjiä. Kaikkia kenttiä ei tarvitse täyttää. Käytä merkkiä * jokerimerkkinä osittaisissa hauissa. Syötä päivämäärät muodossa <kbd>VVVV-KK-PP</kbd> (esim. 2004-02-29). Valitse yksi tai useampi käyttäjätunnus valintaruutujen avulla ja napsauta Valitse merkityt -painiketta palataksesi edelliseen lomakkeeseen.',
	'FLOOD_EMAIL_LIMIT'	=> 'Et voi lähettää uutta sähköpostiviestiä tällä hetkellä. Yritä myöhemmin uudelleen.',

	'GROUP_LEADER'	=> 'Ryhmänjohtaja',

	'HIDE_MEMBER_SEARCH'	=> 'Piilota jäsenhaku',

	'IM_ADD_CONTACT'	=> 'Lisää yhteystieto',
	'IM_DOWNLOAD_APP'	=> 'Lataa sovellus',
	'IM_JABBER'	=> 'Huomaa, että käyttäjät voivat estää kutsumattomat yhteydenotot pikaviestimillä.',
	'IM_JABBER_SUBJECT'	=> 'Tämä on automaattisesti lähetetty viesti. Älä vastaa! Viesti käyttäjältä %1$s kohteessa %2$s.',
	'IM_MESSAGE'	=> 'Viestisi',
	'IM_NAME'	=> 'Nimesi',
	'IM_NO_DATA'	=> 'Tämän käyttäjän yhteystietoja ei ole saatavilla.',
	'IM_NO_JABBER'	=> 'Valitettavasti suora viestintä Jabber-käyttäjien kanssa ei ole mahdollista tällä keskustelupalstalla. Sinulla täytyy olla Jabber-ohjelma asennettuna ottaaksesi yhteyttä yllä olevaan henkilöön.',
	'IM_RECIPIENT'	=> 'Vastaanottaja',
	'IM_SEND'	=> 'Lähetä viesti',
	'IM_SEND_MESSAGE'	=> 'Lähetä viesti',
	'IM_SENT_JABBER'	=> 'Viestisi käyttäjälle %1$s on lähetetty.',
	'IM_USER'	=> 'Lähetä pikaviesti',

	'LAST_ACTIVE'	=> 'Viimeisin käynti',
	'LESS_THAN'	=> 'Vähemmän kuin',
	'LIST_USERS'				=> array(
		1	=> '%d käyttäjä',
		2	=> '%d käyttäjää',
	),
	'LOGIN_EXPLAIN_TEAM'	=> 'Kirjaudu sisään nähdäksesi ryhmien tiedot.',
	'LOGIN_EXPLAIN_MEMBERLIST'	=> 'Kirjaudu sisään nähdäksesi jäsenluettelon.',
	'LOGIN_EXPLAIN_SEARCHUSER'	=> 'Kirjaudu sisään etsiäksesi käyttäjiä.',
	'LOGIN_EXPLAIN_VIEWPROFILE'	=> 'Kirjaudu sisään katsoaksesi tätä profiilia.',

	'MANAGE_GROUP' => 'Hallitse ryhmää',
	'MORE_THAN'	=> 'Enemmän kuin',

	'NO_CONTACT_FORM'		=> 'Keskustelupalstan ylläpidon yhteydenottolomake ei ole käytettävissä.',
	'NO_CONTACT_PAGE'		=> 'Keskustelupalstan ylläpidon yhteystiedot eivät ole nähtävissä.',
	'NO_EMAIL'	=> 'Et voi lähettää sähköpostia tälle käyttäjälle.',
	'NO_VIEW_USERS'	=> 'Sinulla ei ole oikeuksia katsoa käyttäjäluetteloa tai profiileja.',

	'ORDER'	=> 'Järjestys',
	'OTHER'	=> 'Muu',

	'POST_IP'	=> 'Lähetetty IP-osoitteesta/verkkotunnuksesta',

	'REAL_NAME'	=> 'Vastaanottajan nimi',
	'RECIPIENT'	=> 'Vastaanottaja',
	'REMOVE_FOE'	=> 'Poista vihamies',
	'REMOVE_FRIEND'	=> 'Poista kaveri',

	'SELECT_MARKED'	=> 'Valitse merkityt',
	'SELECT_SORT_METHOD'	=> 'Valitse järjestys',
	'SENDER_EMAIL_ADDRESS'	=> 'Sähköpostiosoitteesi',
	'SENDER_NAME'			=> 'Nimesi',
	'SEND_ICQ_MESSAGE'	=> 'Lähetä ICQ-viesti',
	'SEND_IM'	=> 'Pikaviestintä',
	'SEND_JABBER_MESSAGE'	=> 'Lähetä Jabber-viesti',
	'SEND_MESSAGE'	=> 'Viesti',
	'SEND_YIM_MESSAGE'	=> 'Lähetä YIM-viesti',
	'SORT_EMAIL'	=> 'Sähköposti',
	'SORT_LAST_ACTIVE'	=> 'Viimeisin käynti',
	'SORT_POST_COUNT'	=> 'Viestien lukumäärä',

	'USERNAME_BEGINS_WITH'	=> 'Käyttäjätunnuksen ensimmäinen kirjain',
	'USER_ADMIN'	=> 'Hallinnoi käyttäjää',
	'USER_BAN'	=> 'Porttikielto',
	'USER_FORUM'	=> 'Käyttäjän tilastot',
	'USER_LAST_REMINDED'	=> array(
		0		=> 'Muistutuksia ei ole lähetetty',
		1		=> '%1$d muistutus lähetetty<br />» %2$s',
		2		=> '%1$d muistutusta lähetetty<br />» %2$s',
	),
	'USER_ONLINE'	=> 'Paikalla',
	'USER_PRESENCE'	=> 'Läsnäolo keskustelupalstalla',
	'USERS_PER_PAGE' => 'Käyttäjiä sivua kohti',

	'VIEWING_PROFILE'	=> 'Näytä profiili: %s',
	'VIEW_FACEBOOK_PROFILE'	=> 'Näytä Facebook-sivu',
	'VIEW_SKYPE_PROFILE'	=> 'Näytä Skype-profiili',
	'VIEW_TWITTER_PROFILE'	=> 'Näytä Twitter-tili',
	'VIEW_YOUTUBE_CHANNEL'	=> 'Näytä YouTube-kanava',
	'VIEW_GOOGLEPLUS_PROFILE' => 'Näytä Google+-sivu',
));
