<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'CONFIG_NOT_EXIST'					=> 'Asetusta ”%s” ei löydy.',
	'GROUP_NOT_EXIST'					=> 'Ryhmää ”%s” ei löydy.',
	'MIGRATION_APPLY_DEPENDENCIES'		=> 'Asenna kohteen %s riippuvuudet.',
	'MIGRATION_DATA_DONE'				=> 'Tiedot asennettiin: %1$s. Kesto: %2$.2f sekuntia.',
	'MIGRATION_DATA_IN_PROGRESS'		=> 'Asennetaan tietoja: %1$s. Kesto: %2$.2f sekuntia.',
	'MIGRATION_DATA_RUNNING'			=> 'Asennetaan tietoja: %s.',
	'MIGRATION_EFFECTIVELY_INSTALLED'	=> 'Siirros on jo asennettu: %s',
	'MIGRATION_EXCEPTION_ERROR'			=> 'Pyynnön aikana ilmeni virhe, joka aiheutti poikkeustilanteen. Ennen virhettä tehdyt muutokset kumottiin siltä osin kuin mahdollista, mutta suosittelemme kuitenkin tarkistamaan keskustelupalstan virheiden varalta.',
	'MIGRATION_NOT_FULFILLABLE'			=> 'Siirrosta ”%1$s” ei voi toteuttaa, koska siirros ”%2$s” puuttuu.',
	'MIGRATION_NOT_INSTALLED'			=> 'Siirrosta ”%s” ei ole asennettu.',
	'MIGRATION_NOT_VALID'				=> '%s ei ole kelvollinen siirros.',
	'MIGRATION_SCHEMA_DONE'				=> 'Skeema asennettu: %1$s. Kesto: %2$.2f sekuntia.',
	'MIGRATION_SCHEMA_IN_PROGRESS'		=> 'Asennetaan skeemaa: %1$s. Kesto: %2$.2f sekuntia.',
	'MIGRATION_SCHEMA_RUNNING'			=> 'Asennetaan skeemaa: %s.',

	'MIGRATION_REVERT_DATA_DONE'		=> 'Tiedot palautettiin: %1$s. Kesto: %2$.2f sekuntia.',
	'MIGRATION_REVERT_DATA_IN_PROGRESS'	=> 'Palautetaan tietoja: %1$s. Kesto: %2$.2f sekuntia.',
	'MIGRATION_REVERT_DATA_RUNNING'		=> 'Palautetaan tietoja: %s.',
	'MIGRATION_REVERT_SCHEMA_DONE'		=> 'Skeema palautettiin: %1$s. Kesto: %2$.2f sekuntia.',
	'MIGRATION_REVERT_SCHEMA_IN_PROGRESS'	=> 'Palautetaan skeema: %1$s. Kesto: %2$.2f sekuntia.',
	'MIGRATION_REVERT_SCHEMA_RUNNING'	=> 'Palautetaan skeema: %s.',

	'MIGRATION_INVALID_DATA_MISSING_CONDITION'		=> 'Siirros on virheellinen, koska jos-lauseesta puuttuu ehto.',
	'MIGRATION_INVALID_DATA_MISSING_STEP'			=> 'Siirros on virheellinen, koska jos-lauseesta puuttuu siirrosvaiheen kutsu.',
	'MIGRATION_INVALID_DATA_CUSTOM_NOT_CALLABLE'	=> 'Siirros on virheellinen, koska mukautetun funktion kutsu ei onnistu.',
	'MIGRATION_INVALID_DATA_UNKNOWN_TYPE'			=> 'Siirros on virheellinen, koska siirrostyökalun tyyppi on tuntematon.',
	'MIGRATION_INVALID_DATA_UNDEFINED_TOOL'			=> 'Siirros on virheellinen, koska siirrostyökalu on tuntematon.',
	'MIGRATION_INVALID_DATA_UNDEFINED_METHOD'		=> 'Siirros on virheellinen, koska siirrostyökalun metodi on tuntematon.',

	'MODULE_ERROR'						=> 'Moduulia luotaessa ilmeni virhe: %s',
	'MODULE_EXISTS'						=> 'Moduuli on jo olemassa: %s',
	'MODULE_EXIST_MULTIPLE'				=> 'Ylemmän tason moduulin kielivakiota vastaavia moduuleja on jo useita: %s. Kokeile tarkentaa moduulin sijaintia ennen/jälkeen-avainten avulla.',
	'MODULE_INFO_FILE_NOT_EXIST'		=> 'Tarvittava moduulin infotiedosto puuttuu: %2$s',
	'MODULE_NOT_EXIST'					=> 'Tarvittavaa moduulia ei ole olemassa: %s',
	'PARENT_MODULE_FIND_ERROR'			=> 'Ylemmän tason moduulin tunnisteen selvitys epäonnistui: %s',
	'PERMISSION_NOT_EXIST'				=> 'Oikeusasetusta ”%s” ei löydy.',
	'ROLE_NOT_EXIST'					=> 'Oikeusroolia ”%s” ei löydy.',
));
