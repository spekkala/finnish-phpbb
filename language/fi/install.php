<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'INSTALL_PANEL'	=> 'Asennus',
	'SELECT_LANG'	=> 'Valitse kieli',

	'STAGE_INSTALL'	=> 'Asennetaan phpBB:tä',

	// Introduction page
	'INTRODUCTION_TITLE'	=> 'Johdanto',
	'INTRODUCTION_BODY'	=> 'Tervetuloa phpBB3-ohjelmiston käyttäjäksi!<br /><br />phpBB® on maailman käytetyin avoimen lähdekoodin keskustelupalstaohjelmisto. PhpBB3 on vuonna 2000 aloitetun projektin uusin versio. Edeltäjiensä tavoin phpBB3 on monipuolinen, helppokäyttöinen ja täysin phpBB Teamin tukema. PhpBB3:ssa on parannettu ominaisuuksia, jotka tekivät phpBB2:sta suositun, ja sisältää usein pyydettyjä toimintoja, joita aikaisemmat versiot eivät sisältäneet. Toivomme, että phpBB3 ylittää odotuksesi.<br /><br />Tämä asennusohjelma opastaa sinua phpBB3:n asennuksessa, phpBB3:n uusimpaan versioon päivittämisessä ja siirtymisessä phpBB3:een muista keskustelupalstaohjelmistoista (mukaan lukien phpBB2:sta). Lisätietoa saat <a href="../docs/INSTALL.html">asennusoppaasta</a>.<br /><br />Näet phpBB3:n lisenssin sekä saat tietoa tuen hakemisesta ja asemastamme tukikysymyksissä valitsemalla sopivan vaihtoehdon sivupalkista. Voit jatkaa eteenpäin valitsemalla haluamasi toiminnon yllä olevista välilehdistä.',

	// Support page
	'SUPPORT_TITLE'		=> 'Tuki',
	'SUPPORT_BODY'	=> 'PhpBB3:n viimeisimmälle vakaalle versiolle on saatavilla kattavaa tuotetukea maksutta. Tukeen sisältyy</p><ul><li>asennus</li><li>asetusten muokkaus</li><li>tekniset kysymykset</li><li>mahdollisiin ohjelmistovirheisiin liittyvät ongelmat</li><li>päivittäminen Release Candidate -versioista viimeisimpään vakaaseen versioon</li><li>siirtyminen phpBB:n versiosta 2.0.x phpBB3:een</li><li>siirtyminen toisesta keskustelupalstaohjelmistosta phpBB3:een (ks. <a href="https://www.phpbb.com/community/viewforum.php?f=486">siirtymistä käsittelevä alue</a>)</li></ul><p>Suosittelemme phpBB3:n beta-versioiden käyttäjiä päivittämään ohjelmiston uusimpaan versioon.</p><h2>Laajennukset ja tyylit</h2><p>Laajennuksia koskevia kysymksiä voi esittää <a href="https://www.phpbb.com/community/viewforum.php?f=451">laajennuksia käsittelevällä alueella</a>.<br />Tyylejä, sivupohjia ja teemoja koskevia kysymyksiä voi puolestaan esittää <a href="https://www.phpbb.com/community/viewforum.php?f=471">tyylejä käsittelevällä alueella</a>.</p><p>Jos kysymyksesi koskee tiettyä pakettia, esitä kysymys suoraan pakettia käsittelevässä aiheessa.</p><h2>Tuen saaminen</h2><ul><li><a href="https://www.phpbb.com/community/viewtopic.php?f=14&amp;t=571070">PhpBB:n tervetuliaispaketti</a></li><li><a href="https://www.phpbb.com/support/">Tukialue</a></li><li><a href="https://www.phpbb.com/support/docs/en/3.1/ug/quickstart/">Pika-aloitusopas</a></li></ul><p>Voit myös <a href="https://www.phpbb.com/support/">liittyä postituslistallemme</a> pysyäksesi varmasti ajan tasalla uusimmista uutisista ja ohjelmistoversioista.',

	// License
	'LICENSE_TITLE'		=> 'Lisenssi',

	// Install page
	'INSTALL_INTRO'			=> 'Tervetuloa asennusohjelmaan',
	'INSTALL_INTRO_BODY'	=> 'Tässä voit asentaa phpBB3:n palvelimellesi.</p>

<p>Asentaminen edellyttää alla olevien tietokanta-asetusten ilmoittamisen. Jos et tiedä tietokantasi asetuksia, kysy niitä palveluntarjoajaltasi.</p>

<ul>
	<li>Tyyppi – käytössäsi olevan tietokannan tyyppi</li>
	<li>Isäntänimi tai DSN – tietokantapalvelimen osoite</li>
	<li>Portti – tietokantapalvelimen käyttämä portti (tavallisesti tätä tietoa ei tarvitse)</li>
	<li>Nimi – palvelimella olevan tietokannan nimi</li>
	<li>Käyttäjätunnus ja salasana – tietokantaan pääsemiseksi vaaditut kirjautumistiedot</li>
</ul>

<p><strong>Huomaa:</strong> Jos käytät asennuksessa SQLite-tietokantaa, syötä koko tietokantatiedostosi polku DSN-kenttään sekä jätä käyttäjätunnus- ja salasanakentät tyhjiksi. Tietoturvasyistä myös varmista, ettei tietokantatiedostoa ole tallennettu hakemistoon, johon pääsee käsiksi suoraan webin kautta.</p>

<p>PhpBB3 tukee seuraavia tietokantoja:</p>

<ul>
	<li>MySQL 3.23 tai uudempi (MySQLi on tuettu)</li>
	<li>PostgreSQL 8.3+</li>
	<li>SQLite 3.6.15+</li>
	<li>MS SQL Server 2000 tai uudempi (suoraan tai ODBC:n kautta)</li>
	<li>MS SQL Server 2005 tai uudempi (natiivi)</li>
	<li>Oracle</li>
</ul>

<p>Asennusohjelma luettelee vain ne tietokannat, joita palvelimellasi voi käyttää.',

	'ACP_LINK'	=> 'Siirry <a href="%1$s">ylläpidon hallintapaneeliin</a>',

	'INSTALL_PHPBB_INSTALLED'		=> 'PhpBB on jo asennettu.',
	'INSTALL_PHPBB_NOT_INSTALLED'	=> 'PhpBB:tä ei ole vielä asennettu.',
));

// Requirements translation
$lang = array_merge($lang, array(
	// Filesystem requirements
	'FILE_NOT_EXISTS'						=> 'Tiedostoa ei ole olemassa',
	'FILE_NOT_EXISTS_EXPLAIN'				=> 'PhpBB:n asennus vaatii, että tiedosto %1$s on olemassa.',
	'FILE_NOT_EXISTS_EXPLAIN_OPTIONAL'		=> 'Käyttökokemuksen parantamiseksi on suositeltavaa, että tiedosto %1$s on olemassa.',
	'FILE_NOT_WRITABLE'						=> 'Tiedostoon ei voi kirjoittaa',
	'FILE_NOT_WRITABLE_EXPLAIN'				=> 'PhpBB:n asennus vaatii, että tiedostoon %1$s voi kirjoittaa.',
	'FILE_NOT_WRITABLE_EXPLAIN_OPTIONAL'	=> 'Käyttökokemuksen parantamiseksi on suositeltavaa, että tiedostoon %1$s voi kirjoittaa.',

	'DIRECTORY_NOT_EXISTS'						=> 'Hakemistoa ei ole olemassa',
	'DIRECTORY_NOT_EXISTS_EXPLAIN'				=> 'PhpBB:n asennus vaatii, että hakemisto %1$s on olemassa.',
	'DIRECTORY_NOT_EXISTS_EXPLAIN_OPTIONAL'		=> 'Käyttökokemuksen parantamiseksi on suositeltavaa, että hakemisto %1$s on olemassa.',
	'DIRECTORY_NOT_WRITABLE'					=> 'Hakemistoon ei voi kirjoittaa',
	'DIRECTORY_NOT_WRITABLE_EXPLAIN'			=> 'PhpBB:n asennus vaatii, että hakemistoon %1$s voi kirjoittaa.',
	'DIRECTORY_NOT_WRITABLE_EXPLAIN_OPTIONAL'	=> 'Käyttökokemuksen parantamiseksi on suositeltavaa, että hakemistoon %1$s voi kirjoittaa.',

	// Server requirements
	'PHP_VERSION_REQD'					=> 'PHP:n versio',
	'PHP_VERSION_REQD_EXPLAIN'			=> 'PhpBB vaatii PHP 5.4.0:n tai sitä uudemman version.',
	'PHP_GETIMAGESIZE_SUPPORT'			=> 'PHP:n getimagesize()-funktio',
	'PHP_GETIMAGESIZE_SUPPORT_EXPLAIN'	=> 'PhpBB vaatii toimiakseen, että getimagesize()-funktio on käytettävissä.',
	'PCRE_UTF_SUPPORT'					=> 'PCRE:n UTF-8-tuki',
	'PCRE_UTF_SUPPORT_EXPLAIN'			=> 'PhpBB vaatii toimiakseen, että PHP-ohjelmistosi PCRE-laajennukseen on sisällytetty UTF-8-tuki.',
	'PHP_JSON_SUPPORT'					=> 'PHP:n JSON-tuki',
	'PHP_JSON_SUPPORT_EXPLAIN'			=> 'PhpBB vaatii toimiakseen, että PHP:n JSON-laajennus on käytettävissä.',
	'PHP_XML_SUPPORT'					=> 'PHP:n XML/DOM-tuki',
	'PHP_XML_SUPPORT_EXPLAIN'			=> 'PhpBB vaatii toimiakseen, että PHP:n XML/DOM-laajennus on käytettävissä.',
	'PHP_SUPPORTED_DB'					=> 'Tuetut tietokannat',
	'PHP_SUPPORTED_DB_EXPLAIN'			=> 'PHP-ohjelmistosi täytyy tukea ainakin yhtä yhteensopivaa tietokantaa. Jos ainuttakaan tietokantamoduulia ei ole käytettävissä, ota yhteyttä palveluntarjoajaasi tai katso ohjeita PHP:n asennusoppaasta.',

	'RETEST_REQUIREMENTS'	=> 'Tarkasta vaatimukset uudelleen',

	'STAGE_REQUIREMENTS'	=> 'Tarkasta vaatimukset',
));

// General error messages
$lang = array_merge($lang, array(
	'INST_ERR_MISSING_DATA'		=> 'Kaikki tämän kohdan kentät täytyy täyttää.',

	'TIMEOUT_DETECTED_TITLE'	=> 'Aikakatkaisu havaittu',
	'TIMEOUT_DETECTED_MESSAGE'	=> 'Asennusohjelma havaitsi aikakatkaisun. Voit kokeilla sivun lataamista uudelleen, mikä voi kuitenkin aiheuttaa tietojen vääristymistä. Suosittelemme, että joko muokkaat aikakatkaisun asetuksia tai jatkat asennusta komentorivin kautta.',
));

// Data obtaining translations
$lang = array_merge($lang, array(
	'STAGE_OBTAIN_DATA'	=> 'Asetukset',

	//
	// Admin data
	//
	'STAGE_ADMINISTRATOR'	=> 'Ylläpitäjä',

	// Form labels
	'ADMIN_CONFIG'				=> 'Ylläpitäjän asetukset',
	'ADMIN_PASSWORD'			=> 'Ylläpitäjän salasana',
	'ADMIN_PASSWORD_CONFIRM'	=> 'Vahvista ylläpitäjän salasana',
	'ADMIN_PASSWORD_EXPLAIN'	=> 'Valitse salasana, jonka pituus on 6–30 merkkiä.',
	'ADMIN_USERNAME'			=> 'Ylläpitäjän käyttäjätunnus',
	'ADMIN_USERNAME_EXPLAIN'	=> 'Valitse käyttäjätunnus, jonka pituus on 3–20 merkkiä.',

	// Errors
	'INST_ERR_EMAIL_INVALID'		=> 'Antamasi sähköpostiosoite ei kelpaa.',
	'INST_ERR_PASSWORD_MISMATCH'	=> 'Antamasi salasanat eivät täsmänneet.',
	'INST_ERR_PASSWORD_TOO_LONG'	=> 'Antamasi salasana oli liian pitkä. Salasana saa olla korkeintaan 30 merkin pituinen.',
	'INST_ERR_PASSWORD_TOO_SHORT'	=> 'Antamasi salasana oli liian lyhyt. Salasanan täytyy olla vähintään 6 merkin pituinen.',
	'INST_ERR_USER_TOO_LONG'		=> 'Antamasi käyttäjätunnus oli liian pitkä. Tunnus saa olla korkeintaan 20 merkin pituinen.',
	'INST_ERR_USER_TOO_SHORT'		=> 'Antamasi käyttäjätunnus oli liian lyhyt. Tunnuksen täytyy olla vähintään 3 merkin pituinen.',

	//
	// Board data
	//
	// Form labels
	'BOARD_CONFIG'		=> 'Keskustelupalstan asetukset',
	'DEFAULT_LANGUAGE'	=> 'Oletuskieli',
	'BOARD_NAME'		=> 'Keskustelupalstan otsikko',
	'BOARD_DESCRIPTION'	=> 'Lyhyt kuvaus keskustelupalstasta',

	//
	// Database data
	//
	'STAGE_DATABASE'	=> 'Tietokanta',

	// Form labels
	'DB_CONFIG'				=> 'Tietokanta-asetukset',
	'DBMS'					=> 'Tietokannan tyyppi',
	'DB_HOST'				=> 'Tietokantapalvelimen isäntänimi tai DSN',
	'DB_HOST_EXPLAIN'		=> 'DSN on lyhenne sanoista Data Source Name ja sitä tarvitaan vain ODBC-asennuksissa. Jos käytössäsi on PostgreSQL, syötä arvoksi localhost muodostaaksesi yhteyden paikalliselle palvelimelle UNIX-pistokkeen kautta tai 127.0.0.1 muodostaaksesi yhteyden TCP:n kautta. Jos käytössäsi on SQLite, syötä arvoksi koko tietokantatiedoston polku.',
	'DB_PORT'				=> 'Tietokantapalvelimen portti',
	'DB_PORT_EXPLAIN'		=> 'Jätä tämä tyhjäksi, ellet ole varma, että palvelin käyttää epästandardia porttia.',
	'DB_PASSWORD'			=> 'Tietokannan salasana',
	'DB_NAME'				=> 'Tietokannan nimi',
	'DB_USERNAME'			=> 'Tietokannan käyttäjätunnus',
	'DATABASE_VERSION'		=> 'Tietokannan versio',
	'TABLE_PREFIX'			=> 'Tietokannan taulujen etuliite',
	'TABLE_PREFIX_EXPLAIN'	=> 'Etuliitteen täytyy alkaa kirjaimella ja se saa sisältää vain kirjamia, numeroita ja alaviivoja.',

	// Database options
	'DB_OPTION_MSSQL_ODBC'	=> 'MSSQL Server 2000+ (ODBC:n kautta)',
	'DB_OPTION_MSSQLNATIVE'	=> 'MSSQL Server 2005+ (natiivi)',
	'DB_OPTION_MYSQL'		=> 'MySQL',
	'DB_OPTION_MYSQLI'		=> 'MySQL (MySQLi-laajennuksella)',
	'DB_OPTION_ORACLE'		=> 'Oracle',
	'DB_OPTION_POSTGRES'	=> 'PostgreSQL',
	'DB_OPTION_SQLITE3'		=> 'SQLite 3',

	// Errors
	'INST_ERR_NO_DB'				=> 'Valitun tietokantatyypin PHP-moduulin lataus epäonnistui.',
	'INST_ERR_DB_INVALID_PREFIX'	=> 'Antamasi etuliite ei kelpaa. Etuliitteen täytyy alkaa kirjaimella ja se saa sisältää vain kirjaimia, numeroita ja alaviivoja.',
	'INST_ERR_PREFIX_TOO_LONG'		=> 'Antamasi taulujen etuliite on liian pitkä. Suurin sallittu pituus on %d merkkiä.',
	'INST_ERR_DB_NO_NAME'			=> 'Tietokannan nimeä ei ole määritetty.',
	'INST_ERR_DB_FORUM_PATH'		=> 'Määrittämäsi tietokantatiedosto sijaitsee keskustelupalstasi hakemistopuussa. On suositeltavaa siirtää tiedosto hakemistoon, johon ei pääse käsiksi suoraan webin kautta.',
	'INST_ERR_DB_CONNECT'			=> 'Tietokantayhteyden muodostus epäonnistui. Virheilmoitus näkyy alla.',
	'INST_ERR_DB_NO_WRITABLE'		=> 'Tietokantaan tai sen sisältävään hakemistoon ei ole kirjoitusoikeutta.',
	'INST_ERR_DB_NO_ERROR'			=> 'Virheilmoitusta ei ole saatavilla.',
	'INST_ERR_PREFIX'				=> 'Antamasi taulujen etuliite on jo käytössä. Valitse jokin toinen etuliite.',
	'INST_ERR_DB_NO_MYSQLI'			=> 'Palvelimellesi asennettu MySQL-tietokanta ei ole yhteensopiva valitsemasi MySQL (MySQLi-laajennuksella) -vaihtoehdon kanssa. Kokeile MySQL-vaihtoehtoa sen sijaan.',
	'INST_ERR_DB_NO_SQLITE3'		=> 'Palvelimellesi asennettu SQLite-laajennus on liian vanha. Se täytyy päivittää vähintään versioon 3.6.15.',
	'INST_ERR_DB_NO_ORACLE'			=> 'Palvelimellesi asennettu Oracle-tietokanta vaatii, että <var>NLS_CHARACTERSET</var>-parametrin arvo on <var>UTF8</var>. Päivitä tietokanta vähintään versioon 9.2 tai muokkaa parametrin arvoa.',
	'INST_ERR_DB_NO_POSTGRES'		=> 'Valitsemasi tietokanta ei ole <var>UNICODE</var>- tai <var>UTF8</var>-koodattu. Käytä asennuksessa tietokantaa, joka on joko <var>UNICODE</var>- tai <var>UTF8</var>-koodattu.',
	'INST_SCHEMA_FILE_NOT_WRITABLE'	=> 'Skeematiedostoon ei voi kirjoittaa',

	//
	// Email data
	//
	'EMAIL_CONFIG'	=> 'Sähköpostin asetukset',

	// Package info
	'PACKAGE_VERSION'					=> 'Asennetun paketin versio',
	'UPDATE_INCOMPLETE'				=> 'PhpBB-ohjelmistosi päivitys ei ole valmis.',
	'UPDATE_INCOMPLETE_MORE'		=> 'Korjaa virhe alla olevien ohjeiden avulla.',
	'UPDATE_INCOMPLETE_EXPLAIN'		=> '<h1>Puuttellinen päivitys</h1>

		<p>Havaitsimme, että viimeisin phpBB-ohjelmistosi päivitys on jäänyt keskeneräiseksi. Siirry <a href="%1$s">päivitysohjelmaan</a> ja varmista, että <em>Päivitä vain tietokanta</em> -vaihtoehto on valittu. Viimeistele päivitys napsauttamalla <strong>Lähetä</strong>-painiketta ja poista lopuksi install-hakemisto tietokannan päivityksen jälkeen.</p>',

	//
	// Server data
	//
	// Form labels
	'UPGRADE_INSTRUCTIONS'			=> 'Uusia ominaisuuksia sisältävä päivitys <strong>%1$s</strong> on saatavilla. Tarkemmat tiedot ja asennusohjeet näet <a href="%2$s">julkaisutiedotteesta</a>.',
	'SERVER_CONFIG'				=> 'Palvelimen asetukset',
	'SCRIPT_PATH'	=> 'Ohjelmiston polku',
	'SCRIPT_PATH_EXPLAIN'	=> 'Polku phpBB:hen suhteessa verkkotunnukseen (esimerkiksi <samp>/phpBB3</samp>).',
));

// Default database schema entries...
$lang = array_merge($lang, array(
	'CONFIG_BOARD_EMAIL_SIG'	=> 'Ylläpito kiittää',
	'CONFIG_SITE_DESC'	=> 'Lyhyt kuvaus keskustelupalstastasi',
	'CONFIG_SITENAME'	=> 'verkkotunnuksesi.com',

	'DEFAULT_INSTALL_POST'	=> 'Tämä on phpBB3-ohjelmiston luoma esimerkkiviesti. Kaikki näyttäisi olevan kunnossa. Voit halutessasi poistaa tämän viestin ja jatkaa keskustelupalstasi perustamista. Asennuksen aikana ensimmäiselle kategoriallesi ja ensimmäiselle alueellesi asetettiin sopivat oikeudet ennalta määriteltyjä käyttäjäryhmiä varten (ylläpitäjät, botit, yleiset valvojat, vierailijat, rekisteröityneet käyttäjät ja rekisteröityneet COPPA-käyttäjät). Jos päätät poistaa myös ensimmäisen kategoriasi ja ensimmäisen alueesi, muista asettaa oikeudet kaikille näille käyttäjäryhmille, kun luot uusia kategorioita ja alueita. On suositeltavaa uudelleennimetä ensimmäinen kategoriasi ja ensimmäinen alueesi sekä kopioida oikeudet näistä, kun luot uusia kategorioita ja alueita. Pidä hauskaa!',

	'FORUMS_FIRST_CATEGORY'	=> 'Ensimmäinen kategoriasi',
	'FORUMS_TEST_FORUM_DESC'	=> 'Ensimmäisen alueesi kuvaus.',
	'FORUMS_TEST_FORUM_TITLE'	=> 'Ensimmäinen alueesi',

	'RANKS_SITE_ADMIN_TITLE'	=> 'Ylläpitäjä',
	'REPORT_WAREZ'	=> 'Viesti sisältää linkkejä laittomiin ohjelmistoihin tai piraattikopioihin.',
	'REPORT_SPAM'	=> 'Viestin ainoa tarkoitus on mainostaa nettisivua tai tuotetta.',
	'REPORT_OFF_TOPIC'	=> 'Viesti on harhautunut alkuperäisestä aiheesta.',
	'REPORT_OTHER'	=> 'Viesti ei sovi edellä mainittuihin luokkiin. Syötä ilmoituksen syy alla olevaan Lisätietoja-kenttään.',

	'SMILIES_ARROW'	=> 'Nuoli',
	'SMILIES_CONFUSED'	=> 'Hämmentynyt',
	'SMILIES_COOL'	=> 'Siisti',
	'SMILIES_CRYING'	=> 'Itkevä tai hyvin surullinen',
	'SMILIES_EMARRASSED'	=> 'Nolostunut',
	'SMILIES_EVIL'	=> 'Pahanilkinen tai hyvin vihainen',
	'SMILIES_EXCLAMATION'	=> 'Huutomerkki',
	'SMILIES_GEEK'	=> 'Nörtti',
	'SMILIES_IDEA'	=> 'Idea',
	'SMILIES_LAUGHING'	=> 'Nauraa',
	'SMILIES_MAD'	=> 'Vihainen',
	'SMILIES_MR_GREEN'	=> 'Leveä hymy',
	'SMILIES_NEUTRAL'	=> 'Neutraali',
	'SMILIES_QUESTION'	=> 'Kysymysmerkki',
	'SMILIES_RAZZ'	=> 'Härnäys',
	'SMILIES_ROLLING_EYES'	=> 'Pyörittää silmiä',
	'SMILIES_SAD'	=> 'Surullinen',
	'SMILIES_SHOCKED'	=> 'Järkyttynyt',
	'SMILIES_SMILE'	=> 'Hymy',
	'SMILIES_SURPRISED'	=> 'Yllättynyt',
	'SMILIES_TWISTED_EVIL'	=> 'Kieroutunut',
	'SMILIES_UBER_GEEK'	=> 'Supernörtti',
	'SMILIES_VERY_HAPPY'	=> 'Eritäin iloinen',
	'SMILIES_WINK'	=> 'Silmänisku',

	'TOPICS_TOPIC_TITLE'	=> 'Tervetuloa käyttämään phpBB3-ohjelmistoa',
));

// Common navigation items' translation
$lang = array_merge($lang, array(
	'MENU_OVERVIEW'		=> 'Yleiskatsaus',
	'MENU_INTRO'		=> 'Johdanto',
	'MENU_LICENSE'		=> 'Lisenssi',
	'MENU_SUPPORT'		=> 'Tuki',
));

// Task names
$lang = array_merge($lang, array(
	// Install filesystem
	'TASK_CREATE_CONFIG_FILE'	=> 'Luodaan asetustiedostoa',

	// Install database
	'TASK_ADD_CONFIG_SETTINGS'			=> 'Lisätään asetuksia',
	'TASK_ADD_DEFAULT_DATA'				=> 'Lisätään oletusasetukset tietokantaan',
	'TASK_CREATE_DATABASE_SCHEMA_FILE'	=> 'Luodaan tietokannan skeematiedostoa',
	'TASK_SETUP_DATABASE'				=> 'Valmistellaan tietokantaa',
	'TASK_CREATE_TABLES'				=> 'Luodaan tauluja',

	// Install data
	'TASK_ADD_BOTS'			=> 'Rekisteröidään botteja',
	'TASK_ADD_LANGUAGES'	=> 'Asennetaan käytettävissä olevat kielet',
	'TASK_ADD_MODULES'		=> 'Asennetaan moduuleja',
	'TASK_CREATE_SEARCH_INDEX'	=> 'Luodaan hakuindeksiä',

	// Install finish tasks
	'TASK_INSTALL_EXTENSIONS'	=> 'Asennetaan pakattuja laajennuksia',
	'TASK_NOTIFY_USER'			=> 'Lähetetään ilmoitus sähköpostitse',
	'TASK_POPULATE_MIGRATIONS'	=> 'Täydennetään siirroksia',

	// Installer general progress messages
	'INSTALLER_FINISHED'	=> 'Asennus on valmis',
));

// Installer's general messages
$lang = array_merge($lang, array(
	'MODULE_NOT_FOUND'				=> 'Moduulia ei ole',
	'MODULE_NOT_FOUND_DESCRIPTION'	=> 'Moduulia ei löytynyt, koska palvelua %s ei ole määritelty.',

	'TASK_NOT_FOUND'				=> 'Tehtävää ei ole',
	'TASK_NOT_FOUND_DESCRIPTION'	=> 'Tehtävää ei löytynyt, koska palvelua %s ei ole määritelty.',

	'SKIP_MODULE'	=> 'Ohita moduuli ”%s”',
	'SKIP_TASK'		=> 'Ohita tehtävä ”%s”',

	'TASK_SERVICE_INSTALLER_MISSING'	=> 'Kaikkien asennusohjelman tehtäväpalveluiden tulisi alkaa merkkijonolla ”installer”',
	'TASK_CLASS_NOT_FOUND'				=> 'Asennusohjelman tehtäväpalvelun määritys ei kelpaa. Palvelua ”%1$s” koskevan luokan nimiavaruuden tulisi olla ”%2$s”. Lisätietoja saat task_interface-rajapintaa käsittelevästä dokumentaatiosta.',

	'INSTALLER_CONFIG_NOT_WRITABLE'	=> 'Asennusohjelman asetustiedostoon ei voi kirjoittaa.',
));

// CLI messages
$lang = array_merge($lang, array(
	'CLI_INSTALL_BOARD'				=> 'Asenna phpBB',
	'CLI_UPDATE_BOARD'				=> 'Päivitä phpBB',
	'CLI_INSTALL_SHOW_CONFIG'		=> 'Näytä käytettävät asetukset',
	'CLI_INSTALL_VALIDATE_CONFIG'	=> 'Tarkasta asetustiedosto',
	'CLI_CONFIG_FILE'				=> 'Käytettävä asetustiedosto',
	'MISSING_FILE'					=> 'Tiedosto %1$s ei ole käytettävissä',
	'MISSING_DATA'					=> 'Asetustiedoston sisältö on puutteellinen tai virheellinen.',
	'INVALID_YAML_FILE'				=> 'YAML-tiedoston %1$s jäsennys epäonnistui',
	'CONFIGURATION_VALID'			=> 'Asetustiedosto on kelvollinen',
));

// Common updater messages
$lang = array_merge($lang, array(
	'UPDATE_INSTALLATION'			=> 'Päivitä phpBB-ohjelmisto',
	'UPDATE_INSTALLATION_EXPLAIN'	=> 'Tässä voit päivittää phpBB-ohjelmistosi uusimpaan versioon.<br />Päivityksen aikana kaikkien tiedostojesi eheys tarkastetaan. Voit tarkastella tiedostoja ja niiden eroavaisuuksia ennen päivitystä.<br /><br />Tiedostojen päivityksen voi suorittaa kahdella eri tavalla.</p>

<h2>Manuaalinen päivitys</h2>

<p>Tätä menetelmää käyttämällä sinun täytyy ainoastaan ladata muuttuneet tiedostosi, jotta et menetä niihin tekemiäsi muutoksia. Kun olet ladannut paketin, sinun täytyy siirtää tiedostot oikeisiin sijainteihin phpBB:n juurihakemistossa. Tämän jälkeen voit tarkastaa tiedostot uudelleen varmistaaksesi, että siirsit tiedostot oikein.</p>

<h2>Automaattinen päivitys FTP:n avulla</h2>

<p>Tämä menetelmä on samanlainen kuin ensimmäinen vaihtoehto sillä erotuksella, että sinun ei tarvitse ladata muuttuneita tiedostoja ja siirtää niitä takaisin itse, vaan tämä tehdään automaattisesti puolestasi. Tämän menetelmän käyttö vaatii, että tiedät FTP-kirjautumistietosi. Kun toiminto on valmis, sinut ohjataan takaisin tiedostojen tarkastukseen, jossa varmistetaan, että kaikki on päivitetty oikein.<br /><br />',
	'UPDATE_INSTRUCTIONS'			=> '
		<h1>Julkaisutiedote</h1>

		<p>Lue uusimman version julkaisutiedote ennen päivityksen jatkamista, sillä se saattaa sisältää tärkeää tietoa. Tiedote sisältää myös latauslinkit ja muutoslokin.</p>

		<h1>Ohjelmiston päivittäminen automaattisella päivityspaketilla</h1>

		<p>Tässä kuvattu suositeltu päivitystapa koskee vain automaattista päivityspakettia. Voit päivittää asennuksesi myös muilla INSTALL.html-tiedostossa luetelluilla menetelmillä. PhpBB3:n automaattinen päivitys koostuu seuraavista vaiheista:</p>

		<ul style="margin-left: 20px; font-size: 1.1em;">
			<li>Siirry <a href="https://www.phpbb.com/downloads/">phpBB:n lataussivulle</a> ja lataa Automatic Update -paketti.</li>
			<li>Pura paketti.</li>
			<li>Siirrä puretut install- ja vendor-hakemistot kokonaan phpBB:n juurihakemistoon (jossa config.php-tiedostosi sijaitsee).</li>
		</ul>

		<p>Kun siirto on valmis, keskustelupalstasi on suljettu tavallisilta käyttäjiltä install-hakemiston olemassaolon vuoksi.<br /><br /><strong><a href="%1$s">Aloita päivitys siirtymällä selaimellasi install-hakemistoon</a>.</strong><br /><br />Sinua opastetaan päivityksen aikana ja saat tiedon, kun päivitys on valmis.</p>
	',
));

// Updater forms
$lang = array_merge($lang, array(
	// Updater types
	'UPDATE_TYPE'			=> 'Suoritettavan päivityksen tyyppi',

	'UPDATE_TYPE_ALL'		=> 'Päivitä tiedostojärjestelmä ja tietokanta',
	'UPDATE_TYPE_DB_ONLY'	=> 'Päivitä vain tietokanta',

	// File updater methods
	'UPDATE_FILE_METHOD_TITLE'		=> 'Tiedostojen päivitys',

	'UPDATE_FILE_METHOD'			=> 'Tiedostojen päivitystapa',
	'UPDATE_FILE_METHOD_DOWNLOAD'	=> 'Lataa muuttuneet tiedostot pakettina',
	'UPDATE_FILE_METHOD_FTP'		=> 'Päivitä tiedostot FTP:n kautta (automaattisesti)',
	'UPDATE_FILE_METHOD_FILESYSTEM'	=> 'Päivitä tiedostot suoraan tiedostojärjestelmään (automaattisesti)',

	// File updater archives
	'SELECT_DOWNLOAD_FORMAT'	=> 'Valitse ladattavan paketin muoto',

	// FTP settings
	'FTP_SETTINGS'			=> 'FTP-asetukset',
));

// Requirements messages
$lang = array_merge($lang, array(
	'UPDATE_FILES_NOT_FOUND'	=> 'Kelvollista päivityshakemistoa ei löytynyt. Varmista, että olet siirtänyt tarvittavat tiedostot palvelimelle.',

	'NO_UPDATE_FILES_UP_TO_DATE'	=> 'Ohjelmistosi on ajan tasalla, joten päivitystä ei tarvitse suorittaa. Jos haluat tarkastaa tiedostojesi eheyden, varmista, että siirsit oikeat päivitystiedostot palvelimelle.',
	'OLD_UPDATE_FILES'				=> 'Päivitystiedostot ovat vanhentuneita. Löydetyt tiedostot on tarkoitettu phpBB:n version %1$s päivittämiseen versioon %2$s, kun taas viimeisin phpBB:n versio on %3$s.',
	'INCOMPATIBLE_UPDATE_FILES'		=> 'Löydetyt päivitystiedostot eivät ole yhteensopivia asennetun version kanssa. Käytössäsi oleva versio on %1$s, kun taas päivitystiedostot on tarkoitettu phpBB:n version %2$s päivittämiseen versioon %3$s.',
));

// Update files
$lang = array_merge($lang, array(
	'STAGE_UPDATE_FILES'		=> 'Tiedostojen päivitys',

	// Check files
	'UPDATE_CHECK_FILES'	=> 'Tarkasta tiedostot päivityksen jatkamiseksi',

	// Update file differ
	'FILE_DIFFER_ERROR_FILE_CANNOT_BE_READ'	=> 'Tiedoston %s avaus vertailua varten epäonnistui.',

	'UPDATE_FILE_DIFF'		=> 'Vertaillaan muuttuneita tiedostoja',
	'ALL_FILES_DIFFED'		=> 'Kaikkien muuttuneiden tiedostojen vertailu on valmis.',

	// File status
	'UPDATE_CONTINUE_FILE_UPDATE'	=> 'Päivitä tiedostot',

	'DOWNLOAD'							=> 'Lataa',
	'DOWNLOAD_CONFLICTS'				=> 'Lataa ristiriitaisia muutoksia sisältävät tiedostot',
	'DOWNLOAD_CONFLICTS_EXPLAIN'		=> 'Ristiriidat on merkitty merkkijonolla &lt;&lt;&lt;',
	'DOWNLOAD_UPDATE_METHOD'			=> 'Lataa muuttuneet tiedostot',
	'DOWNLOAD_UPDATE_METHOD_EXPLAIN'	=> 'Pura paketti lataamisen jälkeen. Paketti sisältää muuttuneet tiedostot, jotka sinun täytyy siirtää palvelimellesi phpBB:n juurihakemistoon. Voit jatkaa päivitystä, kun kaikki tiedostot on siirretty.',

	'FILE_ALREADY_UP_TO_DATE'		=> 'Tiedosto on jo ajan tasalla.',
	'FILE_DIFF_NOT_ALLOWED'			=> 'Tiedostoa ei voi vertailla.',
	'FILE_USED'						=> 'Tietojen lähde',			// Single file
	'FILES_CONFLICT'				=> 'Ristiriitaiset tiedostot',
	'FILES_CONFLICT_EXPLAIN'		=> 'Seuraavia tiedostoja on muokattu, eivätkä ne vastaa vanhan version alkuperäisiä tiedostoja. PhpBB on havainnut, että nämä tiedostot aiheuttavat ristiriitoja, jos niitä yritetään yhdistää. Tutki ristiriitoja ja yritä ratkaista ne itse, tai jatka päivitystä valitsemalla haluamasi yhdistämistapa. Jos ratkaiset ristiriidat itse, tarkasta tiedostot uudelleen muokkausten jälkeen. Voit valita haluamasi yhdistämistavan erikseen jokaiselle tiedostolle. Ensimmäinen vaihtoehto tuottaa tiedoston, josta on poistettu vanhan tiedoston ristiriitoja aiheuttavat rivit, kun taas toinen vaihtoehto tuottaa tiedoston, joka ei sisällä uuden tiedoston muutoksia.',
	'FILES_DELETED'					=> 'Poistetut tiedostot',
	'FILES_DELETED_EXPLAIN'			=> 'Uusi versio ei sisällä seuraavia tiedostoja, joten ne poistetaan.',
	'FILES_MODIFIED'				=> 'Muuttuneet tiedostot',
	'FILES_MODIFIED_EXPLAIN'		=> 'Seuraavia tiedostoja on muokattu, eivätkä ne vastaa vanhan version alkuperäisiä tiedostoja. Tekemäsi muutokset yhdistetään uuteen tiedostoon.',
	'FILES_NEW'						=> 'Uudet tiedostot',
	'FILES_NEW_EXPLAIN'				=> 'Seuraavia tiedostoja ei ole vanhassa versiossasi. Nämä tiedostot lisätään palvelimellesi.',
	'FILES_NEW_CONFLICT'			=> 'Uudet ristiriitaiset tiedostot',
	'FILES_NEW_CONFLICT_EXPLAIN'	=> 'Seuraavat tiedostot ovat uusia ohjelmiston viimeisimmässä versiossa, mutta asennusohjelma kuitenkin havaitsi, että samannimisiä tiedostoja on jo olemassa palvelimellasi. Nämä tiedostot korvataan uusilla tiedostoilla.',
	'FILES_NOT_MODIFIED'			=> 'Muuttumattomat tiedostot',
	'FILES_NOT_MODIFIED_EXPLAIN'	=> 'Seuraavia tiedostoja ei ole muokattu eli ne vastaavat nykyisen versiosi alkuperäisiä tiedostoja.',
	'FILES_UP_TO_DATE'				=> 'Aiemmin päivitetyt tiedostot',
	'FILES_UP_TO_DATE_EXPLAIN'		=> 'Seuraavat tiedostot ovat jo ajan tasalla, eikä niitä tarvitse päivittää.',
	'FILES_VERSION'					=> 'Tiedostojen versio',
	'TOGGLE_DISPLAY'				=> 'Näytä/piilota tiedostoluettelo',

	// File updater
	'UPDATE_UPDATING_FILES'	=> 'Päivitetään tiedostoja',

	'UPDATE_FILE_UPDATER_HAS_FAILED'	=> 'Tiedostojen päivitys menetelmällä ”%1$s” epäonnistui. Asennusohjelma kokeilee vaihtoehtoista menetelmää ”%2$s”.',
	'UPDATE_FILE_UPDATERS_HAVE_FAILED'	=> 'Tiedostojen päivitys epäonnistui. Muita vaihtoehtoisia menetelmiä ei ole käytettävissä.',

	'UPDATE_CONTINUE_UPDATE_PROCESS'	=> 'Jatka päivitystä',
	'UPDATE_RECHECK_UPDATE_FILES'		=> 'Tarkasta tiedostot uudelleen',
));

// Update database
$lang = array_merge($lang, array(
	'STAGE_UPDATE_DATABASE'		=> 'Päivitä tietokanta',

	'INLINE_UPDATE_SUCCESSFUL'		=> 'Tietokannan päivitys onnistui.',

	'TASK_UPDATE_EXTENSIONS'	=> 'Päivitetään laajennuksia',
));

// Converter
$lang = array_merge($lang, array(
	// Common converter messages
	'CONVERT_NOT_EXIST'			=> 'Määritettyä muunninta ei ole olemassa.',
	'DEV_NO_TEST_FILE'			=> 'Muunnin ei ole määrittänyt arvoa test_file-muuttujalle. Jos olet tämän muuntimen käyttäjä, sinun ei pitäisi nähdä tätä virhettä, joten ilmoita virheestä muuntimen tekijälle. Jos olet muuntimen tekijä, määritä jonkin lähdekeskustelupalstan tiedoston nimi, jotta sen polku voidaan tarkistaa.',
	'COULD_NOT_FIND_PATH'		=> 'Entisen keskustelupalstasi polkua ei löytynyt. Tarkista asetukset ja yritä uudelleen.<br />» Antamasi lähdepolku oli %s.',
	'CONFIG_PHPBB_EMPTY'		=> 'PhpBB3:n asetusmuuttuja kohteelle ”%s” on tyhjä.',

	'MAKE_FOLDER_WRITABLE'		=> 'Varmista, että hakemisto on olemassa ja että siihen voi kirjoittaa. Yritä sen jälkeen uudelleen.<br />»<strong>%s</strong>',
	'MAKE_FOLDERS_WRITABLE'		=> 'Varmista, että hakemistot ovat olemassa ja että niihin voi kirjoittaa. Yritä sen jälkeen uudelleen.<br />»<strong>%s</strong>',

	'INSTALL_TEST'				=> 'Testaa uudelleen',

	'NO_TABLES_FOUND'			=> 'Tauluja ei löytynyt.',
	'TABLES_MISSING'			=> 'Seuraavia tauluja ei löytynyt<br />» <strong>%s</strong>.',
	'CHECK_TABLE_PREFIX'		=> 'Tarkista taulujen etuliite ja yritä uudelleen.',

	// Conversion in progress
	'CONTINUE_CONVERT'			=> 'Jatka muunnosta',
	'CONTINUE_CONVERT_BODY'		=> 'Aiempi muunnosyritys on havaittu. Voit valita, aloitetaanko muunnos alusta vai jatketaanko edellistä muunnosta.
',
	'CONVERT_NEW_CONVERSION'	=> 'Uusi muunnos',
	'CONTINUE_OLD_CONVERSION'	=> 'Jatka aiempaa muunnosta',

	// Start conversion
	'SUB_INTRO'					=> 'Johdanto',
	'CONVERT_INTRO'				=> 'Tervetuloa phpBB Unified Convertor Framework -ohjelmaan',
	'CONVERT_INTRO_BODY'		=> 'Tässä voit tuoda tietoja muista (asennetuista) keskustelupalstaohjelmistoista. Käytettävissä olevat muunnosmoduulit näkyvät alla olevassa luettelossa. Jos luettelossa ei näy muunninta haluamallesi ohjelmistolle, selvitä, onko moduuli ladattavissa sivustoltamme.',
	'AVAILABLE_CONVERTORS'		=> 'Käytettävissä olevat muuntimet',
	'NO_CONVERTORS'				=> 'Käyttökelpoisia muuntimia ei ole.',
	'CONVERT_OPTIONS'			=> 'Asetukset',
	'SOFTWARE'					=> 'Keskustelupalstan ohjelmisto',
	'VERSION'					=> 'Versio',
	'CONVERT'					=> 'Muunna',

	// Settings
	'STAGE_SETTINGS'			=> 'Asetukset',
	'TABLE_PREFIX_SAME'			=> 'Taulujen etuliitteen täytyy olla sama kuin jota entinen keskustelupalstasi käyttää.<br />» Antamasi etuliite oli %s.',
	'DEFAULT_PREFIX_IS'			=> 'Muunnin ei löytänyt tauluja, joilla on antamasi etuliite. Varmista, että olet syöttänyt oikeat tiedot muunnettavalle keskustelupalstalle. %1$s käyttää etuliitettä <strong>%2$s</strong> oletusarvoisesti.',
	'SPECIFY_OPTIONS'			=> 'Määritä muunnoksen asetukset',
	'FORUM_PATH'				=> 'Keskustelupalstan polku',
	'FORUM_PATH_EXPLAIN'		=> '<strong>Suhteellinen</strong> tiedostojärjestelmän polku entiseen keskustelupalstaasi <strong>tämän phpBB3-ohjelmiston juurihakemistosta</strong>.',
	'REFRESH_PAGE'				=> 'Lataa sivu uudelleen jatkaaksesi muunnosta',
	'REFRESH_PAGE_EXPLAIN'		=> 'Jos valitset Kyllä-vaihtoehdon, muunnosohjelma lataa sivun uudelleen jatkaakseen muunnosta jokaisen suoritetun vaiheen jälkeen. Jos kokeilet muunnosta ensimmäistä kertaa ja haluat saada tietoja mahdollisista virheistä etukäteen, on suositeltavaa valita Ei-vaihtoehto.',

	// Conversion
	'STAGE_IN_PROGRESS'			=> 'Muunnos käynnissä',

	'AUTHOR_NOTES'				=> 'Tekijän huomautukset<br />» %s',
	'STARTING_CONVERT'			=> 'Aloitetaan muunnos',
	'CONFIG_CONVERT'			=> 'Muunnetaan asetuksia',
	'DONE'						=> 'Valmis',
	'PREPROCESS_STEP'			=> 'Suoritetaan esikäsittelytoimintoja ja -kyselyjä',
	'FILLING_TABLE'				=> 'Täytetään taulua <strong>%s</strong>',
	'FILLING_TABLES'			=> 'Täytetään tauluja',
	'DB_ERR_INSERT'				=> '<code>INSERT</code>-kyselyä suoritettaessa tapahtui virhe.',
	'DB_ERR_LAST'				=> '<var>query_last</var>-kyselyä suoritettaessa tapahtui virhe.',
	'DB_ERR_QUERY_FIRST'		=> '<var>query_first</var>-kyselyä suoritettaessa tapahtui virhe.',
	'DB_ERR_QUERY_FIRST_TABLE'	=> '<var>query_first</var>-kyselyä suoritettaessa tapahtui virhe: %s (”%s”).',
	'DB_ERR_SELECT'				=> '<code>SELECT</code>-kyselyä suoritettaessa tapahtui virhe.',
	'STEP_PERCENT_COMPLETED'	=> 'Vaihe <strong>%d</strong> / <strong>%d</strong>',
	'FINAL_STEP'				=> 'Suorita viimeinen vaihe',
	'SYNC_FORUMS'				=> 'Aloitetaan alueiden synkronointi',
	'SYNC_POST_COUNT'			=> 'Synkronoidaan viestien lukumääriä',
	'SYNC_POST_COUNT_ID'		=> 'Synkronoidaan viestien lukumääriä <var>kohdasta</var> %1$s kohtaan %2$s.',
	'SYNC_TOPICS'				=> 'Aloitetaan aiheiden synkronointi',
	'SYNC_TOPIC_ID'				=> 'Synkronoidaan aiheita %1$s–%2$s.',
	'PROCESS_LAST'					=> 'Suoritetaan viimeisiä lausekkeita',
	'UPDATE_TOPICS_POSTED'		=> 'Luodaan aloitettujen aiheiden tietoja',
	'UPDATE_TOPICS_POSTED_ERR'	=> 'Aloitettujen aiheiden tietoja luotaessa tapahtui virhe. Voit yrittää suorittaa tämän vaiheen uudelleen ylläpidon hallintapaneelissa, kun muunnos on valmis.',
	'CONTINUE_LAST'				=> 'Jatka viimeisiä lausekkeita',
	'CLEAN_VERIFY'				=> 'Siivotaan ja tarkastetaan lopullinen rakenne',
	'NOT_UNDERSTAND'			=> 'Kohtaa %s #%d taulussa %s ei ymmärretty (”%s”)',
	'NAMING_CONFLICT'			=> 'Nimeämisristiriita: %s ja %s ovat molemmat aliaksia<br /><br />%s',

	// Finish conversion
	'CONVERT_COMPLETE'			=> 'Muunnos on valmis',
	'CONVERT_COMPLETE_EXPLAIN'	=> 'Keskustelupalstasi muunnos phpBB 3.2:een on valmis. Voit nyt <a href="../">kirjautua sisään keskustelupalstallesi</a>. Varmista, että asetukset siirtyivät oikein, ennen kuin otat keskustelupalstasi käyttöön poistamalla install-hakemiston. Muista, että phpBB:n käyttöön saa apua <a href="https://www.phpbb.com/support/docs/en/3.2/ug/">käyttöoppaasta</a> ja <a href="https://www.phpbb.com/community/viewforum.php?f=466">tukialueelta</a>.',

	'CONV_ERROR_ATTACH_FTP_DIR'			=> 'Liitetiedostojen siirto FTP:n kautta on käytössä vanhalla keskustelupalstallasi. Poista FTP-siirto käytöstä ja kopioi kaikki liitetiedostot uuteen hakemistoon, johon on pääsy webin kautta. Tämän jälkeen aloita muunnos uudelleen.',
	'CONV_ERROR_CONFIG_EMPTY'			=> 'Muunnoksen asetustietoja ei ole saatavilla.',
	'CONV_ERROR_FORUM_ACCESS'			=> 'Alueiden käyttöoikeuksien haku epäonnistui.',
	'CONV_ERROR_GET_CATEGORIES'			=> 'Kategorioiden haku epäonnistui.',
	'CONV_ERROR_GET_CONFIG'				=> 'Keskustelupalstan asetusten haku epäonnistui.',
	'CONV_ERROR_COULD_NOT_READ'			=> 'Kohteeseen ”%s” pääsy tai sen luku epäonnistui.',
	'CONV_ERROR_GROUP_ACCESS'			=> 'Ryhmien tunnistautumistietojen haku epäonnistui.',
	'CONV_ERROR_INCONSISTENT_GROUPS'	=> 'Ryhmätaulussa havaittiin epäyhtenäisyyksiä add_bots()-funktiota suoritettaessa. Sinun täytyy lisätä kaikki erikoisryhmät, jos teet sen käsin.',
	'CONV_ERROR_INSERT_BOT'				=> 'Botin lisäys käyttäjätauluun epäonnistui.',
	'CONV_ERROR_INSERT_BOTGROUP'		=> 'Botin lisäys bottitauluun epäonnistui.',
	'CONV_ERROR_INSERT_USER_GROUP'		=> 'Käyttäjän lisäys user_group-tauluun epäonnistui.',
	'CONV_ERROR_MESSAGE_PARSER'			=> 'Viestin jäsentäjän virhe',
	'CONV_ERROR_NO_AVATAR_PATH'			=> 'Huomautus kehittäjälle: Sinun täytyy määrittää $convertor[\'avatar_path\'], jotta %s on käytettävissä.',
	'CONV_ERROR_NO_FORUM_PATH'			=> 'The relative path to the source board has not been specified.',
	'CONV_ERROR_NO_GALLERY_PATH'		=> 'Huomautus kehittäjälle: Sinun täytyy määrittää $convertor[\'avatar_gallery_path\'], jotta %s on käytettävissä.',
	'CONV_ERROR_NO_GROUP'				=> 'Ryhmää ”%1$s” ei löytynyt kohteesta %2$s.',
	'CONV_ERROR_NO_RANKS_PATH'			=> 'Huomautus kehittäjälle: Sinun täytyy määrittää $convertor[\'ranks_path\'], jotta %s on käytettävissä.',
	'CONV_ERROR_NO_SMILIES_PATH'		=> 'Huomautus kehittäjälle: Sinun täytyy määrittää $convertor[\'smilies_path\'], jotta %s on käytettävissä.',
	'CONV_ERROR_NO_UPLOAD_DIR'			=> 'Huomautus kehittäjälle: Sinun täytyy määrittää $convertor[\'upload_path\'], jotta %s on käytettävissä.',
	'CONV_ERROR_PERM_SETTING'			=> 'Oikeuksien lisäys tai päivitys epäonnistui.',
	'CONV_ERROR_PM_COUNT'				=> 'Kansioiden sisältämien yksityisviestien laskeminen epäonnistui.',
	'CONV_ERROR_REPLACE_CATEGORY'		=> 'Vanhan kategorian korvaaminen uudella alueella epäonnistui.',
	'CONV_ERROR_REPLACE_FORUM'			=> 'Vanhan alueen korvaaminen uudella alueella epäonnistui.',
	'CONV_ERROR_USER_ACCESS'			=> 'Käyttäjien tunnistautumistietojen haku epäonnistui.',
	'CONV_ERROR_WRONG_GROUP'			=> 'Väärä ryhmä ”%1$s” määritetty kohteessa %2$s.',
	'CONV_OPTIONS_BODY'					=> 'Tällä sivulla kysytään tietoja, joiden avulla saa yhteyden lähteenä käytettävään keskustelupalstaan. Syötä entisen keskustelupalstasi tietokanta-asetukset oheisiin kenttiin. Muunnosohjelma ei tee muutoksia alla mainittuun tietokantaan. Lähdekeskustelupalsta kannattaa poistaa käytöstä, jotta muunnos onnistuu yhdenmukaisesti.',
	'CONV_SAVED_MESSAGES'				=> 'Tallennetut viestit',

	'PRE_CONVERT_COMPLETE'			=> 'Kaikki alustavat toimenpiteet on suoritettu. Voit nyt aloittaa varsinaisen muunnoksen. Huomaa, että joudut ehkä tekemään ja säätämään useita asioita käsin. Muunnoksen jälkeen tarkista erityisesti myönnetyt oikeudet, rakenna hakuindeksi uudelleen ja varmista, että tiedostot (esim. avatarit ja hymiöt) on kopioitu oikein.',
));
