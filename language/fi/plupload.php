<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'PLUPLOAD_ADD_FILES'		=> 'Lisää tiedostoja',
	'PLUPLOAD_ADD_FILES_TO_QUEUE'	=> 'Lisää tiedostoja lähetysjonoon ja napsauta aloita-painiketta.',
	'PLUPLOAD_ALREADY_QUEUED'	=> '%s on jo jonossa.',
	'PLUPLOAD_CLOSE'			=> 'Sulje',
	'PLUPLOAD_DRAG'				=> 'Vedä tiedostot tähän.',
	'PLUPLOAD_DUPLICATE_ERROR'	=> 'Tiedosto on jo olemassa.',
	'PLUPLOAD_DRAG_TEXTAREA'	=> 'Tiedostoja voi liittää myös vetämällä ja pudottamalla niitä viestikenttään.',
	'PLUPLOAD_ERR_INPUT'		=> 'Lukuvirran avaus epäonnistui.',
	'PLUPLOAD_ERR_MOVE_UPLOADED'	=> 'Lähetetyn tiedoston siirto epäonnistui.',
	'PLUPLOAD_ERR_OUTPUT'		=> 'Tulostusvirran avaus epäonnistui.',
	'PLUPLOAD_ERR_FILE_TOO_LARGE'	=> 'Tiedoston on liian suuri:',
	'PLUPLOAD_ERR_FILE_COUNT'	=> 'Tiedostojen lukumäärä ei täsmää.',
	'PLUPLOAD_ERR_FILE_INVALID_EXT'	=> 'Tiedostopääte ei kelpaa:',
	'PLUPLOAD_ERR_RUNTIME_MEMORY'	=> 'Käytettävissä oleva muisti loppui kesken.',
	'PLUPLOAD_ERR_UPLOAD_URL'	=> 'Lähetyksen kohteen osoite on virheellinen tai sitä ei ole olemassa.',
	'PLUPLOAD_EXTENSION_ERROR'	=> 'Tiedostopäätevirhe.',
	'PLUPLOAD_FILE'				=> 'Tiedosto: %s',
	'PLUPLOAD_FILE_DETAILS'		=> 'Tiedosto: %s, koko: %d, suurin sallittu koko: %d',
	'PLUPLOAD_FILENAME'			=> 'Tiedoston nimi',
	'PLUPLOAD_FILES_QUEUED'		=> '%d tiedostoa jonossa',
	'PLUPLOAD_GENERIC_ERROR'	=> 'Yleinen virhe.',
	'PLUPLOAD_HTTP_ERROR'		=> 'HTTP-virhe.',
	'PLUPLOAD_IMAGE_FORMAT'		=> 'Kuvan tiedostomuoto on virheellinen tai se ei ole tuettu.',
	'PLUPLOAD_INIT_ERROR'		=> 'Alustusvirhe.',
	'PLUPLOAD_IO_ERROR'			=> 'IO-virhe.',
	'PLUPLOAD_NOT_APPLICABLE'	=> 'Ei käytössä',
	'PLUPLOAD_SECURITY_ERROR'	=> 'Tietoturvavirhe.',
	'PLUPLOAD_SELECT_FILES'		=> 'Valitse tiedostot',
	'PLUPLOAD_SIZE'				=> 'Koko',
	'PLUPLOAD_SIZE_ERROR'		=> 'Tiedoston koko ei täsmää.',
	'PLUPLOAD_STATUS'			=> 'Tila',
	'PLUPLOAD_START_UPLOAD'		=> 'Aloita lähetys',
	'PLUPLOAD_START_CURRENT_UPLOAD'	=> 'Aloita jonon lähetys',
	'PLUPLOAD_STOP_UPLOAD'		=> 'Keskeytä lähetys',
	'PLUPLOAD_STOP_CURRENT_UPLOAD'	=> 'Keskeytä meneillään oleva lähetys',
	// Note: This string is formatted independently by plupload and so does not
	// use the same formatting rules as normal phpBB translation strings
	'PLUPLOAD_UPLOADED'			=> 'Lähetetty %d/%d tiedostoa',
));
