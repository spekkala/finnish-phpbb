<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'CAPTCHA_QA'               => 'Kysymys ja vastaus',
	'CONFIRM_QUESTION_EXPLAIN' => 'Tämän kysymyksen tarkoituksena on estää roskapostibotteja lähettämästä viestejä keskustelupalstalle.',
	'CONFIRM_QUESTION_WRONG'   => 'Antamasi vastaus vahvistuskysymykseen oli väärä.',
	'CONFIRM_QUESTION_MISSING'	=> 'Vahvistuskysymysten haku epäonnistui. Ota yhteyttä keskustelupalstan ylläpitoon.',

	'QUESTION_ANSWERS'         => 'Vastaukset',
	'ANSWERS_EXPLAIN'          => 'Syötä kukin sallittu vastaus omalle rivilleen.',
	'CONFIRM_QUESTION'         => 'Kysymys',

	'ANSWER'                   => 'Vastaus',
	'EDIT_QUESTION'            => 'Muokkaa kysymystä',
	'QUESTIONS'                => 'Kysymykset',
  'QUESTIONS_EXPLAIN'        => 'Tässä osiossa voit määrittää kysymyksiä, joita käyttäjille esitetään, kun he lähettävät Kysymys ja vastaus -lisäosaa hyödyntäviä lomakkeita. Tämän lisäosan käyttö vaatii, että määrität ainakin yhden kysymyksen keskustelupalstan oletuskielellä. Näiden kysymysten pitäisi olla helppoja kohdeyleisöllesi, mutta liian vaikeita boteille, jotka kykenevät käyttämään Google™-hakua. Saavutat parhaimmat tulokset käyttämällä suurta määrää kysymyksiä, joita muutetaan säännöllisesti.',
	'QUESTION_DELETED'         => 'Kysymys poistettu',
	'QUESTION_LANG'            => 'Kieli',
	'QUESTION_LANG_EXPLAIN'    => 'Kieli, jolla tämä kysymys ja sen vastaukset on kirjoitettu.',
	'QUESTION_STRICT'          => 'Tarkka vastaus',
	'QUESTION_STRICT_EXPLAIN'  => 'Jos tämä asetus on käytössä, myös välimerkit, tyhjämerkit ja kirjainten koko otetaan huomioon.',

	'QUESTION_TEXT'            => 'Kysymys',
	'QUESTION_TEXT_EXPLAIN'    => 'Käyttäjälle esitettävä kysymys.',

	'QA_ERROR_MSG'             => 'Täytä kaikki kentät ja anna vähintään yksi vastaus.',
	'QA_LAST_QUESTION'         => 'Et voi poistaa kaikkia kysymyksiä, kun tämä lisäosa on aktiivinen.',
));
