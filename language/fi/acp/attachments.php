<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ACP_ATTACHMENT_SETTINGS_EXPLAIN'	=> 'Tässä voit muokata liitetiedostojen ja niihin liittyvien erityiskategorioiden asetuksia.',
	'ACP_EXTENSION_GROUPS_EXPLAIN'	=> 'Tässä voit lisätä, poistaa, muokata tai poistaa käytöstä pääteryhmiä. Lisäksi voit asettaa pääteryhmiä erityiskategorioihin, muuttaa niiden latausmekanismeja ja määrittää kuvakkeita, jotka näytetään kuhunkin ryhmään kuuluvien liitetiedostojen edessä.',
	'ACP_MANAGE_EXTENSIONS_EXPLAIN'	=> 'Tässä voit hallita sallittuja tiedostopäätteitä. Päätteitä voi aktivoida pääteryhmien hallintapaneelissa. On erittäin suositeltavaa estää skriptikielten päätteiden käyttö (esim. <code>php</code>, <code>php3</code>, <code>php4</code>, <code>phtml</code>, <code>pl</code>, <code>cgi</code>, <code>py</code>, <code>rb</code>, <code>asp</code>, <code>aspx</code>, …).',
	'ACP_ORPHAN_ATTACHMENTS_EXPLAIN'	=> 'Tässä näet irrallisiksi jääneet liitetiedostot. Tällaisia tiedostoja syntyy yleensä silloin, kun käyttäjät liittävät tiedostoja viesteihin, joita he eivät kuitenkaan lopulta lähetä. Voit poistaa nämä tiedostot tai liittää niitä jo lähetettyihin viesteihin. Viestiin liittämistä varten sinun täytyy selvittää sen viestin tunniste, johon haluat tiedoston yhdistää.',
	'ADD_EXTENSION'	=> 'Lisää pääte',
	'ADD_EXTENSION_GROUP'	=> 'Lisää pääteryhmä',
	'ADMIN_UPLOAD_ERROR'	=> 'Tiedostoa liitettäessä tapahtuneet virheet: ”%s”.',
	'ALLOWED_FORUMS'	=> 'Sallitut alueet',
	'ALLOWED_FORUMS_EXPLAIN'	=> 'Valitse alueet, joihin tähän pääteryhmään kuuluvia tiedostoja voi lähettää.',
	'ALLOWED_IN_PM_POST'	=> 'Sallittu',
	'ALLOW_ATTACHMENTS'	=> 'Salli liitetiedostot',
	'ALLOW_ALL_FORUMS'	=> 'Sallittu kaikilla alueilla',
	'ALLOW_IN_PM'	=> 'Sallittu yksityisviesteissä',
	'ALLOW_PM_ATTACHMENTS'	=> 'Salli liitetiedostot yksityisviesteissä',
	'ALLOW_SELECTED_FORUMS'	=> 'Sallittu vain alla valituilla alueilla',
	'ASSIGNED_EXTENSIONS'	=> 'Ryhmään kuuluvat päätteet',
	'ASSIGNED_GROUP'	=> 'Määritetty pääteryhmä',
	'ATTACH_EXTENSIONS_URL'	=> 'Päätteet',
	'ATTACH_EXT_GROUPS_URL'	=> 'Pääteryhmät',
	'ATTACH_ID'	=> 'Tunniste',
	'ATTACH_MAX_FILESIZE'	=> 'Tiedoston enimmäiskoko',
	'ATTACH_MAX_FILESIZE_EXPLAIN'	=> 'Yksittäisen tiedoston suurin sallittu koko. Jos arvo on 0, siirrettävän tiedoston kokoa rajoittavat ainoastaan PHP-asetuksesi.',
	'ATTACH_MAX_PM_FILESIZE'	=> 'Tiedoston enimmäiskoko yksityisviesteissä',
	'ATTACH_MAX_PM_FILESIZE_EXPLAIN'	=> 'Yksittäisen yksityisviestiin liitetyn tiedoston suurin sallittu koko. Anna arvoksi 0, jos et halua asettaa koolle ylärajaa.',
	'ATTACH_ORPHAN_URL'	=> 'Irralliset liitetiedostot',
	'ATTACH_POST_ID'	=> 'Viestin tunniste',
	'ATTACH_POST_TYPE'					=> 'Viestin tyyppi',
	'ATTACH_QUOTA'	=> 'Liitetiedostoille varattu tila',
	'ATTACH_QUOTA_EXPLAIN'	=> 'Kaikille keskustelupalstan liitetiedostoille varatun kiintolevytilan enimmäismäärä. Anna arvoksi 0, jos et halua asettaa tilalle ylärajaa.',
	'ATTACH_TO_POST'	=> 'Liitä tiedosto viestiin',

	'CAT_FLASH_FILES'	=> 'Flash-tiedostot',
	'CAT_IMAGES'	=> 'Kuvat',
	'CHECK_CONTENT'	=> 'Tarkasta liitetiedostot',
	'CHECK_CONTENT_EXPLAIN'	=> 'Joitakin selaimia voi huijata olettamaan virheellisen MIME-tyypin palvelimelle siirrettäville tiedostoille. Tämän asetuksen avulla voit varmistaa, että tällaiseen oletukseen todennäköisesti johtavat tiedostot hylätään.',
	'CREATE_GROUP'	=> 'Luo uusi ryhmä',
	'CREATE_THUMBNAIL'	=> 'Luo esikatselukuva',
	'CREATE_THUMBNAIL_EXPLAIN'	=> 'Luo esikatselukuva kaikissa mahdollisissa tilanteissa.',

	'DEFINE_ALLOWED_IPS'	=> 'Määritä sallitut IP-osoitteet/isäntänimet',
	'DEFINE_DISALLOWED_IPS'	=> 'Määritä kielletyt IP-osoitteet/isäntänimet',
	'DOWNLOAD_ADD_IPS_EXPLAIN'	=> 'Voit määrittää useita eri IP-osoitteita tai isäntänimiä asettamalla niistä jokaisen omalle rivilleen. IP-osoitealueita voi määrittää erottamalla alun ja lopun toisistaan yhdysviivalla (-). Lisäksi voit käyttää merkkiä * jokerimerkkinä.',
	'DOWNLOAD_REMOVE_IPS_EXPLAIN'	=> 'Voit poistaa (tai perua huomiotta jättämisen) useita IP-osoitteita samalla kertaa käyttämällä tietokoneesi ja selaimesi tukemia hiiri- ja näppäinyhdistelmiä. Huomioimattomat IP-osoitteet näkyvät korostettuina.',
	'DISPLAY_INLINED'	=> 'Näytä kuvat viesteissä',
	'DISPLAY_INLINED_EXPLAIN'	=> 'Jos valitset Ei-vaihtoehdon, kuvatiedostot näytetään linkkeinä.',
	'DISPLAY_ORDER'	=> 'Liitetiedostojen esitysjärjestys',
	'DISPLAY_ORDER_EXPLAIN'	=> 'Liitetiedostot näytetään aikajärjestyksessä.',

	'EDIT_EXTENSION_GROUP'	=> 'Muokkaa pääteryhmää',
	'EXCLUDE_ENTERED_IP'	=> 'Ota tämä asetus käyttöön jättääksesi antamasi IP-osoitteet/isäntänimet huomioimatta.',
	'EXCLUDE_FROM_ALLOWED_IP'	=> 'Jätä IP-osoite sallittujen IP-osoitteiden/isäntänimien ulkopuolelle',
	'EXCLUDE_FROM_DISALLOWED_IP'	=> 'Jätä IP-osoite kiellettyjen IP-osoitteiden/isäntänimien ulkopuolelle',
	'EXTENSIONS_UPDATED'	=> 'Päätteiden tiedot on päivitetty.',
	'EXTENSION_EXIST'	=> 'Pääte %s on jo olemassa.',
	'EXTENSION_GROUP'	=> 'Pääteryhmä',
	'EXTENSION_GROUPS'	=> 'Pääteryhmät',
	'EXTENSION_GROUP_DELETED'	=> 'Pääteryhmä on poistettu.',
	'EXTENSION_GROUP_EXIST'	=> 'Pääteryhmä %s on jo olemassa.',
	'EXT_GROUP_ARCHIVES'	=> 'Arkistot',
	'EXT_GROUP_DOCUMENTS'	=> 'Asiakirjat',
	'EXT_GROUP_DOWNLOADABLE_FILES'	=> 'Ladattavat tiedostot',
	'EXT_GROUP_FLASH_FILES'	=> 'Flash-tiedostot',
	'EXT_GROUP_IMAGES'	=> 'Kuvat',
	'EXT_GROUP_PLAIN_TEXT'	=> 'Tekstitiedostot',

	'FILES_GONE'			=> 'Osaa poistettavaksi valituista tiedostoista ei ole olemassa. On mahdollista, että ne on jo poistettu. Olemassa olevat tiedostot poistettiin.',
	'FILES_STATS_WRONG'		=> 'Tiedostojesi tilastot ovat todennäköisesti epätarkkoja ja vaativat päivittämistä. Todelliset arvot: liitetiedostojen lukumäärä = %1$d, liitetiedostojen yhteenlaskettu koko = %2$s.<br />%3$sPäivitä tilastot.%4$s',

	'GO_TO_EXTENSIONS'	=> 'Siirry päätteiden hallintaan',
	'GROUP_NAME'	=> 'Ryhmän nimi',

	'IMAGE_LINK_SIZE'	=> 'Kuvalinkkiin vaadittu koko',
	'IMAGE_LINK_SIZE_EXPLAIN'	=> 'Näytä kuvatiedosto linkkinä, jos kuva on tässä määritettyä kokoa suurempi. Voit poistaa tämän toiminnon käytöstä antamalla arvoksi 0 × 0 px.',
	'IMAGICK_PATH'	=> 'ImageMagick-polku',
	'IMAGICK_PATH_EXPLAIN'	=> 'Täysi polku ImageMagick-muunnosohjelmaan (esim. <samp>/usr/bin/</samp>).',

	'MAX_ATTACHMENTS'	=> 'Liitetiedostojen enimmäismäärä yhdessä viestissä',
	'MAX_ATTACHMENTS_PM'	=> 'Liitetiedostojen enimmäismäärä yhdessä yksityisviestissä',
	'MAX_EXTGROUP_FILESIZE'	=> 'Tiedoston enimmäiskoko',
	'MAX_IMAGE_SIZE'	=> 'Kuvan enimmäiskoko',
	'MAX_IMAGE_SIZE_EXPLAIN'	=> 'Kuvaliitteiden enimmäiskoko. Voit poistaa koon tarkastuksen käytöstä antamalla arvoksi 0 × 0 px.',
	'MAX_THUMB_WIDTH'	=> 'Esikatselukuvan enimmäisleveys tai -korkeus pikseleinä',
	'MAX_THUMB_WIDTH_EXPLAIN'	=> 'Luodut esikatselukuvat eivät ylitä tässä määritettyä leveyttä.',
	'MIN_THUMB_FILESIZE'	=> 'Esikatselukuvaan vaadittava vähimmäiskoko',
	'MIN_THUMB_FILESIZE_EXPLAIN'	=> 'Älä luo esikatselukuvaa tätä pienemmille kuville.',
	'MODE_INLINE'	=> 'Viestissä',
	'MODE_PHYSICAL'	=> 'Fyysinen',

	'NOT_ALLOWED_IN_PM'	=> 'Sallittu vain aiheissa',
	'NOT_ALLOWED_IN_PM_POST'	=> 'Ei sallittu',
	'NOT_ASSIGNED'	=> 'Ei määritetty',
	'NO_ATTACHMENTS'			=> 'Tältä aikaväliltä ei löytynyt liitetiedostoja.',
	'NO_EXT_GROUP'	=> 'Ei mitään',
	'NO_EXT_GROUP_NAME'	=> 'Ryhmän nimeä ei ole annettu',
	'NO_EXT_GROUP_SPECIFIED'	=> 'Pääteryhmää ei ole määritetty.',
	'NO_FILE_CAT'	=> 'Ei mitään',
	'NO_IMAGE'	=> 'Ei kuvaa',
	'NO_THUMBNAIL_SUPPORT'	=> 'Esikatselukuvien luonti ei ole käytössä. Tämä toiminto vaatii, että joko GD-laajennus on käytettävissä tai että ImageMagick on asennettu. Kumpaakaan ei löytynyt.',
	'NO_UPLOAD_DIR'	=> 'Määrittämääsi tallennushakemistoa ei ole olemassa.',
	'NO_WRITE_UPLOAD'	=> 'Määrittämääsi tallennushakemistoon ei voi kirjoittaa. Muuta hakemiston oikeuksia siten, että palvelin voi kirjoittaa siihen.',

	'ONLY_ALLOWED_IN_PM'	=> 'Sallittu vain yksityisviesteissä',
	'ORDER_ALLOW_DENY'	=> 'Salli',
	'ORDER_DENY_ALLOW'	=> 'Kiellä',

	'REMOVE_ALLOWED_IPS'	=> 'Poista tai ota huomioon <em>sallittuja</em> IP-osoitteita/isäntänimiä',
	'REMOVE_DISALLOWED_IPS'	=> 'Poista tai ota huomioon <em>kiellettyjä</em> IP-osoitteita/isäntänimiä',
	'RESYNC_FILES_STATS_CONFIRM'	=> 'Haluatko varmasti päivittää tiedostojen tilastot?',

	'SEARCH_IMAGICK'	=> 'Etsi ImageMagick-ohjelmaa',
	'SECURE_ALLOW_DENY'	=> 'Sallittujen/kiellettyjen luettelo',
	'SECURE_ALLOW_DENY_EXPLAIN'	=> 'Valitse, onko suojattuihin latauksiin liittyvä sallittujen/kiellettyjen luettelo <strong>valkoinen lista</strong> (salli) vai <strong>musta lista</strong> (kiellä).',
	'SECURE_DOWNLOADS'	=> 'Suojatut lataukset käytössä',
	'SECURE_DOWNLOADS_EXPLAIN'	=> 'Jos tämä asetus on käytössä, lataaminen on mahdollista vain määrittämistäsi IP-osoitteista/isäntänimistä.',
	'SECURE_DOWNLOAD_NOTICE'	=> 'Suojatut lataukset eivät ole käytössä. Alla olevilla asetuksilla on merkitystä vain, jos otat suojatut lataukset käyttöön.',
	'SECURE_DOWNLOAD_UPDATE_SUCCESS'	=> 'IP-osoitteiden luettelo on päivitetty.',
	'SECURE_EMPTY_REFERRER'	=> 'Salli tyhjä viittaaja',
	'SECURE_EMPTY_REFERRER_EXPLAIN'	=> 'Suojatut lataukset perustuvat viittaajiin. Haluatko sallia lataukset niille, jotka eivät ilmoita viittaajaa?',
	'SETTINGS_CAT_IMAGES'	=> 'Kuvakategorian asetukset',
	'SPECIAL_CATEGORY'	=> 'Erityiskategoria',
	'SPECIAL_CATEGORY_EXPLAIN'	=> 'Erityiskategoriat vaikuttavat siihen, miten liitetiedostot näytetään viesteissä.',
	'SUCCESSFULLY_UPLOADED'	=> 'Tiedosto on siirretty.',
	'SUCCESS_EXTENSION_GROUP_ADD'	=> 'Pääteryhmä on lisätty.',
	'SUCCESS_EXTENSION_GROUP_EDIT'	=> 'Pääteryhmä on päivitetty.',

	'UPLOADING_FILES'	=> 'Siirretään tiedostoja',
	'UPLOADING_FILE_TO'	=> 'Siirretään tiedostoa ”%1$s” viestiin numero %2$d…',
	'UPLOAD_DENIED_FORUM'	=> 'Sinulla ei ole oikeuksia lisätä liitetiedostoja alueelle ”%s”.',
	'UPLOAD_DIR'	=> 'Tallennushakemisto',
	'UPLOAD_DIR_EXPLAIN'	=> 'Hakemisto, johon liitetiedostot tallennetaan. Huomaa, että jos muutat tätä hakemistoa sen jälkeen, kun liitetiedostoja on jo lisätty, sinun täytyy itse kopioida tiedostot valitsemaasi uuteen sijaintiin.',
	'UPLOAD_ICON'	=> 'Liitetiedoston kuvake',
	'UPLOAD_NOT_DIR'	=> 'Määrittämäsi tallennushakemisto ei näytä olevan hakemisto.',
));
