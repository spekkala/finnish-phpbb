<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ACP_SEARCH_INDEX_EXPLAIN'	=> 'Tässä voit hallita hakujärjestelmien indeksejä. Koska tavallisesti käytössä on vain yksi järjestelmä, sinun tulisi poistaa kaikki indeksit, joita et käytä. Jos muutat haun asetuksia (esim. merkkien vähimmäis- ja enimmäismääriä), voi olla kannattavaa rakentaa indeksi uudestaan, jotta muutoksilla on vaikutusta.',
	'ACP_SEARCH_SETTINGS_EXPLAIN'	=> 'Tässä voit valita, mitä hakujärjestelmää käytetään viestien indeksointiin ja hakujen suorittamiseen. Voit muokata useita asetuksia, jotka vaikuttavat siihen, kuinka paljon prosessointia nämä toimenpiteet vaativat. Osa asetuksista on yhteisiä kaikille hakujärjestelmille.',

	'COMMON_WORD_THRESHOLD'	=> 'Yleisen sanan raja',
	'COMMON_WORD_THRESHOLD_EXPLAIN'	=> 'Sanoja, jotka esiintyvät tässä antamaasi prosenttilukua suuremmassa osassa kaikista viesteistä, pidetään yleisinä. Yleiset sanat jätetään huomiotta hakuja suoritettaessa. Anna arvoksi 0, jos haluat poistaa tämän toiminnon käytöstä. Tällä asetuksella on vaikutusta vain, jos viestejä on yli 100. Jos haluat sisällyttää indeksiin sanoja, jotka ovat yleisiä tämänhetkisen asetuksen perusteella, sinun täytyy rakentaa indeksi uudelleen.',
	'CONFIRM_SEARCH_BACKEND'	=> 'Haluatko varmasti siirtyä käyttämään toista hakujärjestelmää? Hakujärjestelmän vaihdon jälkeen sinun täytyy luoda indeksi uudelle hakujärjestelmälle. Jos et aio siirtyä takaisin vanhaan hakujärjestelmään, voit poistaa vanhan järjestelmän indeksin resursseja vapauttaaksesi.',
	'CONTINUE_DELETING_INDEX'	=> 'Jatka aikaisempaa indeksin poistoa',
	'CONTINUE_DELETING_INDEX_EXPLAIN'	=> 'Indeksin poisto on aloitettu. Et voi siirtyä hakuindeksisivulle, ennen kuin poisto on valmis tai peruutettu.',
	'CONTINUE_INDEXING'	=> 'Jatka aikaisempaa indeksointia',
	'CONTINUE_INDEXING_EXPLAIN'	=> 'Indeksointi on aloitettu. Et voi siirtyä hakuindeksisivulle, ennen kuin indeksointi on valmis tai peruutettu.',
	'CREATE_INDEX'	=> 'Luo indeksi',

	'DELETE_INDEX'	=> 'Poista indeksi',
	'DELETING_INDEX_IN_PROGRESS'	=> 'Indeksin poisto käynnissä',
	'DELETING_INDEX_IN_PROGRESS_EXPLAIN'	=> 'Hakujärjestelmä tyhjentää indeksiään parhaillaan. Tämä voi kestää muutaman minuutin.',

	'FULLTEXT_MYSQL_INCOMPATIBLE_DATABASE'	=> 'MySQL Fulltext -hakujärjestelmää voi käyttää vain MySQL 4:n ja sitä uudempien tietokantaversioiden kanssa.',
	'FULLTEXT_MYSQL_NOT_SUPPORTED'	=> 'MySQL Fulltext -indeksejä voi käyttää vain MyISAM- ja InnoDB-tauluissa. InnoDB-taulujen fulltext-indeksit vaativat toimiakseen MySQL 5.6.8:n tai sitä uudemman version.',
	'FULLTEXT_MYSQL_TOTAL_POSTS'	=> 'Indeksoitujen viestien kokonaismäärä',
	'FULLTEXT_MYSQL_MIN_SEARCH_CHARS_EXPLAIN'	=> 'Sanat, jotka sisältävät vähintään näin monta merkkiä, indeksoidaan hakua varten. Sinä tai palveluntarjoajasi voi muuttaa tätä asetusta vain muokkamalla MySQL:n asetuksia.',
	'FULLTEXT_MYSQL_MAX_SEARCH_CHARS_EXPLAIN'	=> 'Sanat, jotka sisältävät enintään näin monta merkkiä, indeksoidaan hakua varten. Sinä tai palveluntarjoajasi voi muuttaa tätä asetusta vain muokkaamalla MySQL:n asetuksia.',

	'FULLTEXT_POSTGRES_INCOMPATIBLE_DATABASE'	=> 'PostgreSQL Fulltext -hakujärjestelmää voi käyttää vain PostgreSQL-tietokantojen kanssa.',
	'FULLTEXT_POSTGRES_TOTAL_POSTS'			=> 'Indeksoituja viestejä yhteensä',
	'FULLTEXT_POSTGRES_VERSION_CHECK'		=> 'PostgreSQL:n versio',
	'FULLTEXT_POSTGRES_TS_NAME'				=> 'Tekstihaun konfigurointiprofiili:',
	'FULLTEXT_POSTGRES_MIN_WORD_LEN'			=> 'Avainsanojen vähimmäispituus',
	'FULLTEXT_POSTGRES_MAX_WORD_LEN'			=> 'Avainsanojen enimmäispituus',
	'FULLTEXT_POSTGRES_VERSION_CHECK_EXPLAIN'		=> 'Tämä hakujärjestelmä vaatii PostgreSQL 8.3:n tai sitä uudemman version.',
	'FULLTEXT_POSTGRES_TS_NAME_EXPLAIN'				=> 'Tekstihaun konfigurointiprofiili, jonka perusteella jäsentäjä ja sanasto valitaan.',
	'FULLTEXT_POSTGRES_MIN_WORD_LEN_EXPLAIN'			=> 'Tietokantakyselyihin sisällytetään sanat, joissa on vähintään tämän verran merkkejä.',
	'FULLTEXT_POSTGRES_MAX_WORD_LEN_EXPLAIN'			=> 'Tietokantakyselyihin sisällytetään sanat, joissa on enintään tämän verran merkkejä.',

	'FULLTEXT_SPHINX_CONFIGURE'				=> 'Muokkaa seuraavia asetuksia luodaksesi Sphinx-asetustiedoston.',
	'FULLTEXT_SPHINX_DATA_PATH'				=> 'Datahakemiston polku',
	'FULLTEXT_SPHINX_DATA_PATH_EXPLAIN'		=> 'Hakemisto, johon tallennetaan indeksit ja lokitiedostot. Tämän hakemiston tulisi sijaita julkisten web-hakemistojen ulkopuolella. Polun täytyy päättyä kauttaviivaan.',
	'FULLTEXT_SPHINX_DELTA_POSTS'			=> 'Tiheään päivitettässä delta-indeksissä olevien viestien lukumäärä',
	'FULLTEXT_SPHINX_HOST'					=> 'Sphinx-hakuprosessin isäntänimi',
	'FULLTEXT_SPHINX_HOST_EXPLAIN'			=> 'Isäntänimi, jolla Sphinx-hakuprosessi (searchd) kuuntelee. Jätä kenttä tyhjäksi käyttääksesi localhost-oletusarvoa.',
	'FULLTEXT_SPHINX_INDEXER_MEM_LIMIT'		=> 'Indeksoijan enimmäismuisti',
	'FULLTEXT_SPHINX_INDEXER_MEM_LIMIT_EXPLAIN'	=> 'Tämän arvon tulisi aina olla pienempi kuin käytettävissä olevan RAM-muistin määrä. Jos havaitset ajoittaisia ongelmia suorituskyvyn kanssa, syynä voi olla indeksoijan liiallisen suuri resurssienkulutus. Indeksoijan käytettävissä olevan muistin määrän pienentäminen saattaa auttaa ongelmaan.',
	'FULLTEXT_SPHINX_MAIN_POSTS'			=> 'Pääindeksissä olevien viestien lukumäärä',
	'FULLTEXT_SPHINX_PORT'					=> 'Sphinx-hakuprosessin portti',
	'FULLTEXT_SPHINX_PORT_EXPLAIN'			=> 'Portti, jolla Sphinx-hakuprosessi (searchd) kuuntelee. Jätä kenttä tyhjäksi käyttääksesi Sphinx-rajapinnan oletusporttia 9312.',
	'FULLTEXT_SPHINX_WRONG_DATABASE'		=> 'PhpBB:n Sphinx-haku tukee ainoastaan MySQL- ja PostgreSQL-tietokantoja.',
	'FULLTEXT_SPHINX_CONFIG_FILE'			=> 'Sphinx-asetustiedosto',
	'FULLTEXT_SPHINX_CONFIG_FILE_EXPLAIN'	=> 'Sphinx-asetustiedostoa varten luotu sisältö. Nämä tiedot täytyy kopioida sphinx.conf-tiedostoon, jota Sphinx-hakuprosessi käyttää. Korvaa mallimuuttujat [dbuser] ja [dbpassword] omilla tietokantatunnuksillasi.',
	'FULLTEXT_SPHINX_NO_CONFIG_DATA'		=> 'Sphinx-datahakemiston polkua ei ole määritetty. Määritä polku ja tallenna muutokset luodaksesi asetustiedoston.',

	'GENERAL_SEARCH_SETTINGS'	=> 'Yleiset haun asetukset',
	'GO_TO_SEARCH_INDEX'	=> 'Siirry hakuindeksisivulle',

	'INDEX_STATS'	=> 'Indeksin tilastot',
	'INDEXING_IN_PROGRESS'	=> 'Indeksointi käynnissä',
	'INDEXING_IN_PROGRESS_EXPLAIN'	=> 'Hakujärjestelmä indeksoi parhaillaan kaikkia keskustelupalstan viestejä. Tämä saattaa kestää muutamasta minuutista muutamaan tuntiin keskustelupalstasi koosta riippuen.',

	'LIMIT_SEARCH_LOAD'	=> 'Hakusivun kuormituksen rajoitus',
	'LIMIT_SEARCH_LOAD_EXPLAIN'	=> 'Jos järjestelmän yhden minuutin kuormitus ylittää tämän arvon, hakusivu poistuu käytöstä. Arvo 1.0 vastaa yhden prosessorin noin 100-prosenttista käyttöä. Tämä toimii vain UNIX-pohjaisilla palvelimilla.',

	'MAX_SEARCH_CHARS'	=> 'Indeksoitavien merkkien enimmäismäärä',
	'MAX_SEARCH_CHARS_EXPLAIN'	=> 'Sanat, jotka sisältävät enintään näin monta merkkiä, indeksoidaan hakua varten.',
	'MAX_NUM_SEARCH_KEYWORDS'	=> 'Sallittujen avainsanojen enimmäismäärä',
	'MAX_NUM_SEARCH_KEYWORDS_EXPLAIN'	=> 'Määrittää, kuinka montaa sanaa käyttäjä voi korkeintaan etsiä. Anna arvoksi 0, jos et halua rajoittaa sanojen lukumäärää.',
	'MIN_SEARCH_CHARS'	=> 'Indeksoitavien merkkien vähimmäismäärä',
	'MIN_SEARCH_CHARS_EXPLAIN'	=> 'Sanat, jotka sisältävät vähintään näin monta merkkiä, indeksoidaan hakua varten.',
	'MIN_SEARCH_AUTHOR_CHARS'	=> 'Merkkien vähimmäismäärä kirjoittajan nimessä',
	'MIN_SEARCH_AUTHOR_CHARS_EXPLAIN'	=> 'Kirjoittajan nimestä täytyy antaa vähintään näin monta merkkiä, kun hakua suoritetaan kirjoittajan nimen perusteella jokerimerkkejä käytettäessä. Jos kirjoittajan käyttäjätunnus on tätä lyhyempi, voit silti hakea kirjoittajan viestejä antamalla koko käyttäjätunnuksen.',

	'PROGRESS_BAR'	=> 'Etenemispalkki',

	'SEARCH_GUEST_INTERVAL'	=> 'Vierailijoiden hakujen rajoitus',
	'SEARCH_GUEST_INTERVAL_EXPLAIN'	=> 'Määrittää, kuinka monta sekuntia vierailijoiden on odotettava hakujen välillä. Jos yksi vierailija suorittaa haun, kaikkien muiden täytyy odottaa, kunnes aikaraja on täyttynyt.',
	'SEARCH_INDEX_CREATE_REDIRECT'			=> array(
		2	=> 'Kaikki viestit viestiin %2$d asti on indeksoitu. Näistä viesteistä %1$d indeksoitiin tässä vaiheessa.<br />',
	),
	'SEARCH_INDEX_CREATE_REDIRECT_RATE'		=> array(
		2	=> 'Tämänhetkinen indeksointinopeus on noin %1$.1f viestiä sekunnissa.<br />Indeksointi on käynnissä…',
	),
	'SEARCH_INDEX_DELETE_REDIRECT'			=> array(
		2	=> 'Kaikki viestit viestiin %2$d asti on poistettu hakuindeksistä.<br />Poisto on käynnissä…',
	),
	'SEARCH_INDEX_CREATED'	=> 'Kaikki keskustelupalstan viestit on indeksoitu.',
	'SEARCH_INDEX_REMOVED'	=> 'Tämän hakujärjestelmän indeksi on poistettu.',
	'SEARCH_INTERVAL'	=> 'Käyttäjien hakujen rajoitus',
	'SEARCH_INTERVAL_EXPLAIN'	=> 'Määrittää, kuinka monta sekuntia käyttäjien täytyy odottaa hakujen välillä. Tämä rajoitus on käyttäjäkohtainen.',
	'SEARCH_STORE_RESULTS'	=> 'Hakutulosten välimuistin kesto',
	'SEARCH_STORE_RESULTS_EXPLAIN'	=> 'Välimuistissa olevat hakutulokset vanhentuvat tämän ajan kuluttua. Anna arvoksi 0, jos haluat poistaa hakujen välimuistin käytöstä.',
	'SEARCH_TYPE'	=> 'Hakujärjestelmä',
	'SEARCH_TYPE_EXPLAIN'	=> 'PhpBB antaa sinun valita hakujärjestelmän, jota käytetään viestien tekstisisällön etsimiseen. Oletusarvoisesti käytössä on phpBB:n oma fulltext-haku.',
	'SWITCHED_SEARCH_BACKEND'	=> 'Vaihdoit hakujärjestelmän. Uutta hakujärjestelmää käyttääksesi varmista, että sille on luotu indeksi.',

	'TOTAL_WORDS'	=> 'Indeksoitujen sanojen kokonaismäärä',
	'TOTAL_MATCHES'	=> 'Indeksoitujen sana-viestisuhteiden kokonaismäärä',

	'YES_SEARCH'	=> 'Hakutoiminto käytössä',
	'YES_SEARCH_EXPLAIN'	=> 'Ottaa hakutoiminnon käyttöön käyttäjähaun mukaan lukien.',
	'YES_SEARCH_UPDATE'	=> 'Fulltext-päivitys käytössä',
	'YES_SEARCH_UPDATE_EXPLAIN'	=> 'Ottaa käyttöön fulltext-indeksien päivityksen viestejä lähetettäessä. Tällä asetuksella ei ole vaikutusta, jos hakutoiminto ei ole käytössä.',
));
