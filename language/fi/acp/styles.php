<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ACP_STYLES_EXPLAIN'	=> 'Tässä voit hallita keskustelupalstan käytössä olevia tyylejä. Voit muokata, poistaa, aktivoida ja asentaa tyylejä. Esikatselutoiminnon avulla voit selvittää, miltä tietty tyyli näyttää. Luettelo sisältää myös tiedon siitä, kuinka monta käyttäjää kullakin tyylillä on. Huomaa, ettei käyttäjien valitsemien tyylien ohitusta ole huomioitu lukumäärissä.',

	'CANNOT_BE_INSTALLED'			=> 'Asentaminen ei ole mahdollista',
	'CONFIRM_UNINSTALL_STYLES'		=> 'Haluatko varmasti poistaa valitsemasi tyylit?',
	'COPYRIGHT'						=> 'Tekijänoikeudet',

	'DEACTIVATE_DEFAULT'		=> 'Et voi poistaa oletustyyliä käytöstä.',
	'DELETE_FROM_FS'			=> 'Poista tiedostojärjestelmästä',
	'DELETE_STYLE_FILES_FAILED'	=> 'Tyylin ”%s” tiedostoja poistettaessa tapahtui virhe.',
	'DELETE_STYLE_FILES_SUCCESS'	=> 'Tyylin ”%s” tiedostot on poistettu.',
	'DETAILS'					=> 'Lisätiedot',

	'INHERITING_FROM'			=> 'On riippuvainen tyylistä',
	'INSTALL_STYLE'				=> 'Asenna tyyli',
	'INSTALL_STYLES'			=> 'Asenna tyylit',
	'INSTALL_STYLES_EXPLAIN'	=> 'Tässä voit asentaa uusia tyylejä.<br />Jos et näe tiettyä tyyliä alla olevassa luettelossa, varmista, ettei tyyliä ole jo asennettu. Jos tyyliä ei ole asennettu, tarkista, onko se siirretty palvelimelle oikein.',
	'INVALID_STYLE_ID'			=> 'Tyylin tunniste ei ole kelvollinen.',

	'NO_MATCHING_STYLES_FOUND'	=> 'Hakuasi vastaavia tyylejä ei löytynyt.',
	'NO_UNINSTALLED_STYLE'		=> 'Asentamattomia tyylejä ei löytynyt.',

	'PURGED_CACHE'				=> 'Välimuisti on tyhjennetty.',

	'REQUIRES_STYLE'			=> 'Tämä tyylin käyttö vaatii, että tyyli ”%s” on asennettu.',

	'STYLE_ACTIVATE'			=> 'Aktivoi',
	'STYLE_ACTIVE'				=> 'Aktiivinen',
	'STYLE_DEACTIVATE'			=> 'Poista käytöstä',
	'STYLE_DEFAULT'				=> 'Aseta oletustyyliksi',
	'STYLE_DEFAULT_CHANGE_INACTIVE'	=> 'Tyyli täytyy aktivoida ennen kuin sen voi asettaa oletustyyliksi.',
	'STYLE_ERR_INVALID_PARENT'	=> 'Tyylin riippuvuus ei ole kelvollinen.',
	'STYLE_ERR_NAME_EXIST'		=> 'Samanniminen tyyli on jo olemassa.',
	'STYLE_ERR_STYLE_NAME'		=> 'Anna tyylille nimi.',
	'STYLE_INSTALLED'			=> 'Tyyli ”%s” on asennettu.',
	'STYLE_INSTALLED_RETURN_INSTALLED_STYLES'	=> 'Palaa asennettujen tyylien luetteloon',
	'STYLE_INSTALLED_RETURN_UNINSTALLED_STYLES'	=> 'Asenna lisää tyylejä',
	'STYLE_NAME'				=> 'Tyylin nimi',
	'STYLE_NAME_RESERVED'		=> 'Tyyliä ”%s” ei voi asentaa, koska nimi on varattu.',
	'STYLE_NOT_INSTALLED'		=> 'Tyyliä ”%s” ei asennettu.',
	'STYLE_PATH'				=> 'Tyylin polku',
	'STYLE_UNINSTALL'			=> 'Poista',
	'STYLE_UNINSTALL_DEPENDENT'	=> 'Tyyliä ”%s” ei voi poistaa, koska ainakin yksi muu tyyli on riippuvainen siitä.',
	'STYLE_UNINSTALLED'			=> 'Tyyli ”%s” on poistettu.',
	'STYLE_USED_BY'				=> 'Käyttäjien lukumäärä (botit mukaan lukien)',
	'STYLE_VERSION'				=> 'Tyylin versio',

	'UNINSTALL_DEFAULT'		=> 'Et voi poistaa oletustyyliä.',

	'BROWSE_STYLES_DATABASE'	=> 'Selaa tyylien tietokantaa',
));
