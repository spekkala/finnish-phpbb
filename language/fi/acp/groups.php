<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ACP_GROUPS_MANAGE_EXPLAIN'	=> 'Tässä voit hallita kaikkia käyttäjäryhmiäsi. Voit poistaa, luoda tai muokata olemassa olevia ryhmiä. Lisäksi voit valita ryhmänjohtajia, muuttaa ryhmiä avoimiksi/piilotetuiksi/suljetuiksi sekä muokata ryhmien nimiä ja kuvauksia.',
	'ADD_GROUP_CATEGORY'			=> 'Lisää kategoria',
	'ADD_USERS'	=> 'Lisää käyttäjiä',
	'ADD_USERS_EXPLAIN'	=> 'Tässä voit lisätä uusia käyttäjiä ryhmään. Voit päättää, tuleeko tästä ryhmästä uusi oletusryhmä valitsemillesi käyttäjille. Halutessasi voit myös tehdä heistä ryhmänjohtajia. Ole hyvä ja aseta kukin käyttäjätunnus omalle rivilleen.',

	'COPY_PERMISSIONS'	=> 'Kopioi oikeudet ryhmältä',
	'COPY_PERMISSIONS_EXPLAIN'	=> 'Luotavalla ryhmällä on samat oikeudet kuin tässä valitsemallasi ryhmällä.',
	'CREATE_GROUP'	=> 'Luo uusi ryhmä',

	'GROUPS_NO_MEMBERS'	=> 'Tässä ryhmässä ei ole jäseniä',
	'GROUPS_NO_MODS'	=> 'Ryhmänjohtajia ei ole määritetty',
	'GROUP_APPROVE'	=> 'Hyväksy jäseneksi',
	'GROUP_APPROVED'	=> 'Hyväksytyt jäsenet',
	'GROUP_AVATAR'	=> 'Ryhmän avatar',
	'GROUP_AVATAR_EXPLAIN'	=> 'Tämä kuva näytetään ryhmän hallintapaneelissa.',
	'GROUP_CATEGORY_NAME'			=> 'Kategorian nimi',
	'GROUP_CLOSED'	=> 'Suljettu',
	'GROUP_COLOR'	=> 'Ryhmän väri',
	'GROUP_COLOR_EXPLAIN'	=> 'Määrittää ryhmän jäsenten käyttäjätunnusten värin. Jätä kenttä tyhjäksi, jos haluat käyttää käyttäjän oletusväriä.',
	'GROUP_CONFIRM_ADD_USERS'		=> array(
		1	=> 'Haluatko varmasti lisätä käyttäjän %2$s tähän ryhmään?',
		2	=> 'Haluatko varmasti lisätä käyttäjät %2$s tähän ryhmään?',
	),
	'GROUP_CREATED'	=> 'Ryhmä on luotu.',
	'GROUP_DEFAULT'	=> 'Aseta oletusryhmäksi',
	'GROUP_DEFS_UPDATED'	=> 'Oletusryhmä on asetettu kaikille valituille jäsenille.',
	'GROUP_DELETE'	=> 'Poista jäsen ryhmästä',
	'GROUP_DELETED'	=> 'Ryhmä on poistettu ja käyttäjien oletusryhmät on asetettu.',
	'GROUP_DEMOTE'	=> 'Alenna ryhmänjohtajasta',
	'GROUP_DESC'	=> 'Ryhmän kuvaus',
	'GROUP_DETAILS'	=> 'Ryhmän tiedot',
	'GROUP_EDIT_EXPLAIN'	=> 'Tässä voit muokata jo olemassa olevaa ryhmää. Voit vaihtaa sen nimen, kuvauksen ja tyypin (avoin, suljettu, jne.). Voit myös säätää tiettyjä ryhmän jäsenille yhteisiä asetuksia, kuten esimerkiksi väriä ja arvonimeä. Tässä tehdyt muutokset ohittavat käyttäjien nykyiset asetukset. Huomaa, että ryhmän jäsenet voivat ohittaa ryhmän avatar-asetukset, ellet estä tätä muokkaamalla käyttäjien oikeuksia.',
	'GROUP_ERR_USERS_EXIST'	=> 'Määrittämäsi käyttäjät ovat jo tämän ryhmän jäseniä.',
	'GROUP_FOUNDER_MANAGE'	=> 'Vain perustajat voivat hallita',
	'GROUP_FOUNDER_MANAGE_EXPLAIN'	=> 'Salli tämän ryhmän hallinta vain perustajille. Muilla käyttäjillä voi silti olla oikeus nähdä tämä ryhmä ja sen jäsenet.',
	'GROUP_HIDDEN'	=> 'Piilotettu',
	'GROUP_LANG'	=> 'Ryhmän kieli',
	'GROUP_LEAD'	=> 'Ryhmänjohtajat',
	'GROUP_LEADERS_ADDED'	=> 'Uudet johtajat on lisätty ryhmään.',
	'GROUP_LEGEND'	=> 'Näytä ryhmä selitteessä',
	'GROUP_LIST'	=> 'Nykyiset jäsenet',
	'GROUP_LIST_EXPLAIN'	=> 'Tässä on luettelo kaikista tämän ryhmän jäsenistä. Voit poistaa jäseniä (tietyistä erityisryhmistä lukuun ottamatta) tai lisätä uusia halutessasi.',
	'GROUP_MEMBERS'	=> 'Ryhmän jäsenet',
	'GROUP_MEMBERS_EXPLAIN'	=> 'Tässä on luettelo kaikista tämän ryhmän jäsenistä. Luettelo sisältää erilliset osiot ryhmänjohtajille, hyväksymistä odottaville käyttäjille ja nykyisille käyttäjille. Voit päättää, ketkä ovat jäseniä tässä ryhmässä ja mitkä heidän roolinsa ovat. Jos haluat poistaa ryhmänjohtajan mutta pitää hänet silti ryhmässä, valitse Alenna ryhmänjohtajasta -toiminto Poista-vaihtoehdon sijaan. Vastaavasti voit käyttää Ylennä ryhmänjohtajaksi -toimintoa tehdäksesi tavallisesta jäsenestä ryhmänjohtajan.',
	'GROUP_MESSAGE_LIMIT'	=> 'Ryhmäkohtainen yksityisviestien enimmäismäärä per kansio',
	'GROUP_MESSAGE_LIMIT_EXPLAIN'	=> 'Tämä asetus ohittaa käyttäjäkohtaisen yksityisviestien enimmäismäärän. Lopullinen arvo riippuu käyttäjän kaikkien ryhmien enimmäisarvosta.<br /> Anna arvoksi 0, jos haluat käyttää keskustelupalstan oletusrajoitusta kaikille tämän ryhmän käyttäjille.',
	'GROUP_MODS_ADDED'	=> 'Uudet ryhmänjohtajat on lisätty.',
	'GROUP_MODS_DEMOTED'	=> 'Ryhmänjohtajat on alennettu.',
	'GROUP_MODS_PROMOTED'	=> 'Ryhmän jäsenet on ylennetty.',
	'GROUP_NAME'	=> 'Ryhmän nimi',
	'GROUP_NAME_TAKEN'	=> 'Antamasi ryhmän nimi on jo käytössä. Ole hyvä ja valitse toinen nimi.',
	'GROUP_OPEN'	=> 'Avoin',
	'GROUP_PENDING'	=> 'Hyväksymistä odottavat jäsenet',
	'GROUP_MAX_RECIPIENTS'	=> 'Sallittujen vastaanottajien enimmäismäärä yksityisviestissä',
	'GROUP_MAX_RECIPIENTS_EXPLAIN'	=> 'Sallittujen vastaanottajien enimmäismäärä yhtä yksityisviestiä kohden. Lopullinen arvo riippuu käyttäjän kaikkien ryhmien enimmäisarvosta.<br /> Anna arvoksi 0, jos haluat käyttää keskustelupalstan oletusrajoitusta kaikille tämän ryhmän käyttäjille.',
	'GROUP_OPTIONS_SAVE'	=> 'Ryhmäkohtaiset valinnat',
	'GROUP_PROMOTE'	=> 'Ylennä ryhmänjohtajaksi',
	'GROUP_RANK'	=> 'Ryhmän arvonimi',
	'GROUP_RECEIVE_PM'	=> 'Ryhmälle voi lähettää yksityisviestejä',
	'GROUP_RECEIVE_PM_EXPLAIN'	=> 'Huomaa, että piilotetuille ryhmille ei voi lähettää yksityisviestejä, vaikka tämä asetus sen sallisikin.',
	'GROUP_REQUEST'	=> 'Hyväksymisen vaativa',
	'GROUP_SETTINGS_SAVE'	=> 'Ryhmäkohtaiset asetukset',
	'GROUP_TYPE'	=> 'Ryhmän tyyppi',
	'GROUP_TYPE_EXPLAIN'	=> 'Tämä asetus ratkaisee, mitkä käyttäjät voivat liittyä tähän ryhmään tai nähdä sen jäsenet.',
	'GROUP_SKIP_AUTH'	=> 'Vapauta ryhmänjohtajat oikeuksista',
	'GROUP_SKIP_AUTH_EXPLAIN'	=> 'Jos tämä asetus on valittu, ryhmänjohtajat eivät peri oikeuksia tältä ryhmältä.',
	'GROUP_SPECIAL'					=> 'Ennalta määritelty',
	'GROUP_TEAMPAGE'				=> 'Näytä ryhmä ryhmäsivulla',
	'GROUP_UPDATED'	=> 'Ryhmän asetukset on päivitetty.',
	'GROUP_USERS_ADDED'	=> 'Uudet käyttäjät on lisätty ryhmään.',
	'GROUP_USERS_EXIST'	=> 'Valitut käyttäjät ovat jo jäseniä.',
	'GROUP_USERS_REMOVE'	=> 'Käyttäjät on poistettu ryhmästä ja uudet oletukset on asetettu.',

	'LEGEND_EXPLAIN'				=> 'Nämä ryhmät näkyvät selitteessä:',
	'LEGEND_SETTINGS'				=> 'Selitteen asetukset',
	'LEGEND_SORT_GROUPNAME'			=> 'Järjestä selite ryhmän nimen mukaan',
	'LEGEND_SORT_GROUPNAME_EXPLAIN'	=> 'Alla oleva järjestys jätetään huomiotta, jos tämä asetus on käytössä.',

	'MANAGE_LEGEND'			=> 'Muokkaa ryhmän selitettä',
	'MANAGE_TEAMPAGE'		=> 'Muokkaa ryhmäsivua',
	'MAKE_DEFAULT_FOR_ALL'	=> 'Aseta tämä ryhmä kaikkien jäsenten oletusryhmäksi',
	'MEMBERS'	=> 'Jäsenet',

	'NO_GROUP'	=> 'Ryhmää ei ole määritetty.',
	'NO_GROUPS_ADDED'			=> 'Ryhmiä ei ole vielä lisätty.',
	'NO_GROUPS_CREATED'	=> 'Ryhmiä ei ole vielä luotu.',
	'NO_PERMISSIONS'	=> 'Älä kopioi oikeuksia',
	'NO_USERS'	=> 'Et antanut ainuttakaan käyttäjää.',
	'NO_USERS_ADDED'	=> 'Ainuttakaan käyttäjää ei lisätty ryhmään.',
	'NO_VALID_USERS'	=> 'Et antanut ainuttakaan käyttäjää, joihin tämän toimenpiteen voisi kohdistaa.',

	'PENDING_MEMBERS'			=> 'Odottavat',

	'SELECT_GROUP'				=> 'Valitse ryhmä',
	'SPECIAL_GROUPS'	=> 'Ennalta määritellyt ryhmät',
	'SPECIAL_GROUPS_EXPLAIN'	=> 'Ennalta määritellyt ryhmät ovat erityisryhmiä, joita ei voi poistaa tai muokata suoraan. Voit kuitenkin lisätä niihin jäseniä ja muokata niiden perusasetuksia.',

	'TEAMPAGE'					=> 'Ryhmäsivu',
	'TEAMPAGE_DISP_ALL'			=> 'Kaikki jäsenyydet',
	'TEAMPAGE_DISP_DEFAULT'		=> 'Vain käyttäjän oletusryhmä',
	'TEAMPAGE_DISP_FIRST'		=> 'Vain ensimmäinen jäsenyys',
	'TEAMPAGE_EXPLAIN'			=> 'Nämä ryhmät näkyvät ryhmäsivulla:',
	'TEAMPAGE_FORUMS'			=> 'Näytä valvotut alueet',
	'TEAMPAGE_FORUMS_EXPLAIN'	=> 'Jos tämä toiminto on käytössä, kunkin valvojan rivillä näkyy luettelo alueista, joilla heillä on valvojan oikeudet. Tämä saattaa kuormittaa tietokantaa merkittävästi suurilla keskustelupalstoilla.',
	'TEAMPAGE_MEMBERSHIPS'		=> 'Näytä käyttäjien jäsenyydet',
	'TEAMPAGE_SETTINGS'			=> 'Ryhmäsivun asetukset',
	'TOTAL_MEMBERS'	=> 'Jäseniä',

	'USERS_APPROVED'	=> 'Käyttäjät on hyväksytty.',
	'USER_DEFAULT'	=> 'Käyttäjän oletusarvo',
	'USER_DEF_GROUPS'	=> 'Käyttäjän määrittelemät ryhmät',
	'USER_DEF_GROUPS_EXPLAIN'	=> 'Nämä ovat sinun tai jonkun toisen ylläpitäjän luomia ryhmiä. Voit hallita jäsenyyksiä ja muokata ryhmien asetuksia tai jopa poistaa ryhmiä.',
	'USER_GROUP_DEFAULT'	=> 'Aseta oletusryhmäksi',
	'USER_GROUP_DEFAULT_EXPLAIN'	=> 'Jos valitset Kyllä-vaihtoehdon, tämä ryhmä asetetaan kaikkien lisättyjen käyttäjien oletusryhmäksi.',
	'USER_GROUP_LEADER'	=> 'Aseta ryhmänjohtajiksi',
));
