<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

// Custom profile fields
$lang = array_merge($lang, array(
	'ADDED_PROFILE_FIELD'	=> 'Mukautettu profiilikenttä on lisätty.',
	'ALPHA_DOTS'			=> 'Alfanumeeriset ja pisteet',
	'ALPHA_ONLY'	=> 'Vain alfanumeeriset',
	'ALPHA_SPACERS'	=> 'Alfanumeeriset ja tyhjämerkit',
	'ALPHA_UNDERSCORE'		=> 'Alfanumeeriset ja alaviivat',
	'ALPHA_PUNCTUATION'		=> 'Alfanumeerisista, pilkuista, pisteistä, alaviivoista ja yhdysviivoista koostuvat, jotka alkavat kirjaimella',
	'ALWAYS_TODAY'	=> 'Aina nykyinen päivämäärä',

	'BOOL_ENTRIES_EXPLAIN'	=> 'Syötä vaihtoehdot tähän.',
	'BOOL_TYPE_EXPLAIN'	=> 'Valitse tyypiksi joko valintaruutu tai valintanappi. Valintaruutu näkyy vain, jos käyttäjä on merkinnyt sen. Tällöin käytetään <strong>toista</strong> kieleen sidottua vaihtoehtoa. Valintanapit näkyvät aina valitusta arvosta riippumatta.',

	'CHANGED_PROFILE_FIELD'	=> 'Profiilikenttää on muutettu.',
	'CHARS_ANY'	=> 'Mikä tahansa merkki',
	'CHECKBOX'	=> 'Valintaruutu',
	'COLUMNS'	=> 'Sarakkeet',
	'CP_LANG_DEFAULT_VALUE'	=> 'Oletusarvo',
	'CP_LANG_EXPLAIN'	=> 'Kentän kuvaus',
	'CP_LANG_EXPLAIN_EXPLAIN'	=> 'Käyttäjälle näkyvä kentän kuvaus.',
	'CP_LANG_NAME'	=> 'Käyttäjälle näkyvä kentän nimi/otsikko',
	'CP_LANG_OPTIONS'	=> 'Vaihtoehdot',
	'CREATE_NEW_FIELD'	=> 'Luo uusi kenttä',
	'CUSTOM_FIELDS_NOT_TRANSLATED'	=> 'Ainakin yksi mukautetuista profiilikentistä on kääntämättä. Ole hyvä ja syötä puuttuvat tiedot napsauttamalla Käännä-linkkiä.',

	'DEFAULT_ISO_LANGUAGE'	=> 'Oletuskieli (%s)',
	'DEFAULT_LANGUAGE_NOT_FILLED'	=> 'Oletuskielen käyttämän kielitiedoston muuttujia ei ole täytetty tämän profiilikentän osalta.',
	'DEFAULT_VALUE'	=> 'Oletusarvo',
	'DELETE_PROFILE_FIELD'	=> 'Poista profilikenttä',
	'DELETE_PROFILE_FIELD_CONFIRM'	=> 'Haluatko varmasti poistaa tämän profiilikentän?',
	'DISPLAY_AT_PROFILE'	=> 'Näytä käyttäjän hallintapaneelissa',
	'DISPLAY_AT_PROFILE_EXPLAIN'	=> 'Käyttäjä voi muuttaa tätä profiilikenttää käyttäjän hallintapaneelissa.',
	'DISPLAY_AT_REGISTER'	=> 'Näytä rekisteröitymisen yhteydessä',
	'DISPLAY_AT_REGISTER_EXPLAIN'	=> 'Jos tämä asetus on valittu, kenttä näytetään rekisteröitymisen yhteydessä.',
	'DISPLAY_ON_MEMBERLIST'			=> 'Näytä jäsenten luettelossa',
	'DISPLAY_ON_MEMBERLIST_EXPLAIN'	=> 'Jos tämä asetus on valittu, kenttä näkyy käyttäjän rivillä jäsenluettelossa.',
	'DISPLAY_ON_PM'					=> 'Näytä yksityisviestinäkymässä',
	'DISPLAY_ON_PM_EXPLAIN'			=> 'Jos tämä asetus on valittu, kenttä näkyy pienoisprofiilissa yksityisviestejä kirjoitettaessa.',
	'DISPLAY_ON_VT'	=> 'Näytä aiheita luettaessa',
	'DISPLAY_ON_VT_EXPLAIN'	=> 'Jos tämä asetus on valittu, kenttä näytetään pienoisprofiilissa aiheita luettaessa.',
	'DISPLAY_PROFILE_FIELD'	=> 'Näytä profiilikenttä julkisesti',
	'DISPLAY_PROFILE_FIELD_EXPLAIN'	=> 'Profiilikenttä näytetään kaikissa kuormitusasetusten sallimissa kohdissa. Tämän asetuksen valitsematta jättäminen piilottaa kentän aiheista, profiileista ja käyttäjäluettelosta.',
	'DROPDOWN_ENTRIES_EXPLAIN'	=> 'Syötä kukin vaihtoehto omalle rivilleen.',

	'EDIT_DROPDOWN_LANG_EXPLAIN'	=> 'Huomaa, että voit muuttaa vaihtoehtojen tekstejä ja lisätä uusia vaihtoehtoja loppuun. Ei ole suositeltavaa lisätä uusia vaihtoehtoja jo olemassa olevien vaihtoehtojen väliin, sillä tämän seurauksena käyttäjille saatetaan asettaa vääriä vaihtoehtoja. Näin voi käydä myös, jos poistat välissä olevia vaihtoehtoja. Viimeisen vaihtoehdon poistaminen johtaa siihen, tämän vaihtoehdon valinneille käyttäjille asetetaan kentän oletusarvo.',
	'EMPTY_FIELD_IDENT'	=> 'Kentän tunniste on tyhjä',
	'EMPTY_USER_FIELD_NAME'	=> 'Syötä kentän nimi/otsikko',
	'ENTRIES'	=> 'Vaihtoehdot',
	'EVERYTHING_OK'	=> 'Kaikki kunnossa',

	'FIELD_BOOL'	=> 'Boolean (kyllä/ei)',
	'FIELD_CONTACT_DESC'		=> 'Yhteydenoton kuvaus',
	'FIELD_CONTACT_URL'			=> 'Yhteydenottolinkki',
	'FIELD_DATE'	=> 'Päivämäärä',
	'FIELD_DESCRIPTION'	=> 'Kentän kuvaus',
	'FIELD_DESCRIPTION_EXPLAIN'	=> 'Käyttäjälle näytettävä kentän kuvaus.',
	'FIELD_DROPDOWN'	=> 'Pudotusvalikko',
	'FIELD_GOOGLEPLUS'			=> 'Google+',
	'FIELD_IDENT'	=> 'Kentän tunniste',
	'FIELD_IDENT_ALREADY_EXIST'	=> 'Valitsemasi kentän tunniste on jo olemassa. Ole hyvä ja valitse toinen nimi.',
	'FIELD_IDENT_EXPLAIN'	=> 'Kentän tunniste on nimi, jonka avulla profiilikenttä voidaan tunnistaa tietokannassa ja sivupohjissa.',
	'FIELD_INT'	=> 'Numerot',
	'FIELD_IS_CONTACT'			=> 'Näytä kenttä yhteystietona',
	'FIELD_IS_CONTACT_EXPLAIN'	=> 'Yhteystiedot näkyvät käyttäjän profiilissa sekä viesteihin ja yksityisviesteihin yhdistetyssä pienoisprofiilissa. Voit käyttää syötettä <samp>%s</samp> mallimuuttujana, joka korvataan käyttäjän antamalla arvolla.',
	'FIELD_LENGTH'	=> 'Tekstikentän koko',
	'FIELD_NOT_FOUND'	=> 'Profiilikenttää ei löytynyt.',
	'FIELD_STRING'	=> 'Yksittäinen tekstikenttä',
	'FIELD_TEXT'	=> 'Tekstialue',
	'FIELD_TYPE'	=> 'Kentän tyyppi',
	'FIELD_TYPE_EXPLAIN'	=> 'Kentän tyyppiä ei voi muuttaa jälkikäteen.',
	'FIELD_URL'					=> 'URL (linkki)',
	'FIELD_VALIDATION'	=> 'Kentän hyväksyminen',
	'FIRST_OPTION'	=> 'Ensimmäinen vaihtoehto',

	'HIDE_PROFILE_FIELD'	=> 'Piilota profiilikenttä',
	'HIDE_PROFILE_FIELD_EXPLAIN'	=> 'Piilota profiilikenttä kaikilta paitsi käyttäjältä itseltään, ylläpitäjiltä ja valvojilta. Jos Näytä käyttäjän hallintapaneelissa -asetusta ei ole valittu, käyttäjä ei pysty näkemään tai muuttamaan kentän arvoa, vaan muutoksia voivat tehdä ainoastaan ylläpitäjät.',

	'INVALID_CHARS_FIELD_IDENT'	=> 'Kentän tunniste voi sisältää vain pieniä kirjaimia (a–z) ja alaviivoja (_)',
	'INVALID_FIELD_IDENT_LEN'	=> 'Kentän tunniste saa olla enintään 17 merkin pituinen',
	'ISO_LANGUAGE'	=> 'Kieli (%s)',

	'LANG_SPECIFIC_OPTIONS'	=> 'Kielikohtaiset asetukset (<strong>%s</strong>)',
	'LETTER_NUM_DOTS'			=> 'Kirjaimet, numerot ja pisteet',
	'LETTER_NUM_ONLY'			=> 'Kirjaimet ja numerot',
	'LETTER_NUM_PUNCTUATION'	=> 'Kirjaimista, numeroista, pilkuista, pisteistä, alaviivoista ja yhdysviivoista koostuvat, jotka alkavat kirjaimella',
	'LETTER_NUM_SPACERS'		=> 'Kirjaimet, numerot ja tyhjämerkit',
	'LETTER_NUM_UNDERSCORE'		=> 'Kirjaimet, numerot ja alaviivat',

	'MAX_FIELD_CHARS'	=> 'Merkkien enimmäismäärä',
	'MAX_FIELD_NUMBER'	=> 'Suurin sallittu luku',
	'MIN_FIELD_CHARS'	=> 'Merkkien vähimmäismäärä',
	'MIN_FIELD_NUMBER'	=> 'Pienin sallittu luku',

	'NO_FIELD_ENTRIES'	=> 'Vaihtoehtoja ei ole määritetty',
	'NO_FIELD_ID'	=> 'Kentän tunnistetta ei ole määritetty',
	'NO_FIELD_TYPE'	=> 'Kentän tyyppiä ei ole määritetty',
	'NO_VALUE_OPTION'	=> 'Vaihtoehto on sama kuin tyhjän kentän arvo',
	'NO_VALUE_OPTION_EXPLAIN'	=> 'Tyhjän kentän arvo. Jos kenttä on pakollinen, käyttäjä saa virheilmoituksen, mikäli hän valitsee tässä määritetyn vaihtoehdon.',
	'NUMBERS_ONLY'	=> 'Vain numerot (0–9)',

	'PROFILE_BASIC_OPTIONS'	=> 'Perusasetukset',
	'PROFILE_FIELD_ACTIVATED'	=> 'Profiilikenttä on aktivoitu.',
	'PROFILE_FIELD_DEACTIVATED'	=> 'Profiilikenttä on poistettu käytöstä.',
	'PROFILE_LANG_OPTIONS'	=> 'Kielikohtaiset asetukset',
	'PROFILE_TYPE_OPTIONS'	=> 'Kentän tyyppiin sidotut asetukset',

	'RADIO_BUTTONS'	=> 'Valintanapit',
	'REMOVED_PROFILE_FIELD'	=> 'Profiilikenttä on poistettu.',
	'REQUIRED_FIELD'	=> 'Pakollinen kenttä',
	'REQUIRED_FIELD_EXPLAIN'	=> 'Pakota käyttäjä tai ylläpitäjä täyttämään kenttä tai valitsemaan sen arvo. Jos Näytä rekisteröitymisen yhteydessä -asetusta ei ole valittu, kentän täyttäminen on pakollista vain, kun käyttäjä muokkaa profiiliaan.',
	'ROWS'	=> 'Rivit',

	'SAVE'	=> 'Tallenna',
	'SECOND_OPTION'	=> 'Toinen vaihtoehto',
	'SHOW_NOVALUE_FIELD' => 'Näytä kenttä, vaikka mitään arvoa ei valittu',
	'SHOW_NOVALUE_FIELD_EXPLAIN' => 'Määrittää, näytetäänkö profiilikenttä, vaikka mitään arvoa ei valittu vapaavalintaiselle kentälle tai mitään arvoa ei ole vielä valittu pakolliselle kentälle.',
	'STEP_1_EXPLAIN_CREATE'	=> 'Tässä voit määrittää uuden profiilikenttäsi perusasetukset. Näitä tietoja tarvitaan toisessa vaiheessa, jossa voit määrittää jäljelle jäävät asetukset ja muokata profiilikenttääsi enemmän.',
	'STEP_1_EXPLAIN_EDIT'	=> 'Tässä voit muokata profiilikenttäsi perusasetuksia. Kenttään liittyvät vaihtoehdot tarkastetaan uudelleen seuraavassa vaiheessa.',
	'STEP_1_TITLE_CREATE'	=> 'Lisää profiilikenttä',
	'STEP_1_TITLE_EDIT'	=> 'Muokkaa profiilikenttää',
	'STEP_2_EXPLAIN_CREATE'	=> 'Tässä voit määrittää yhteisiä asetuksia, joita haluat ehkä muokata.',
	'STEP_2_EXPLAIN_EDIT'	=> 'Tässä voit muokata yhteisiä asetuksia.<br /><strong>Huomaa, että profiilikenttiin tehdyt muutokset eivät vaikuta arvoihin, joita käyttäjät ovat jo syöttäneet.</strong>',
	'STEP_2_TITLE_CREATE'	=> 'Kentän tyyppiin sidotut asetukset',
	'STEP_2_TITLE_EDIT'	=> 'Kentän tyyppiin sidotut asetukset',
	'STEP_3_EXPLAIN_CREATE'	=> 'Koska keskustelupalstallasi on käytössä useampi kuin yksi kieli, sinun täytyy määrittää myös muihin kieliin liittyvät asetukset. Profiilikenttä toimii myös pelkillä oletuskielen asetuksilla, joten halutessasi voit määrittää muita kieliä koskevat asetukset myöhemmin.',
	'STEP_3_EXPLAIN_EDIT'	=> 'Koska keskustelupalstallasi on käytössä useampi kuin yksi kieli, voit nyt muokata tai lisätä muita kieliä koskevia vaihtoehtoja. Profiilikenttä toimii myös pelkillä oletuskielen asetuksilla.',
	'STEP_3_TITLE_CREATE'	=> 'Jäljellä olevat kielet',
	'STEP_3_TITLE_EDIT'	=> 'Kielimääritykset',
	'STRING_DEFAULT_VALUE_EXPLAIN'	=> 'Syötä kentässä näkyvä oletusarvo. Jätä arvo tyhjäksi, jos haluat kentän näkyvän tyhjänä aluksi.',

	'TEXT_DEFAULT_VALUE_EXPLAIN'	=> 'Syötä kentässä näkyvä oletusarvo. Jätä arvo tyhjäksi, jos haluat kentän näkyvän tyhjänä aluksi.',
	'TRANSLATE'	=> 'Käännä',

	'USER_FIELD_NAME'	=> 'Käyttäjälle näytettävä kentän nimi/otsikko',

	'VISIBILITY_OPTION'	=> 'Näkyvyysasetukset',
));
