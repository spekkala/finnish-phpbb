<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'EXTENSION'					=> 'Laajennus',
	'EXTENSIONS'				=> 'Laajennukset',
	'EXTENSIONS_ADMIN'			=> 'Laajennusten hallinta',
	'EXTENSIONS_EXPLAIN'		=> 'Tässä voit hallita kaikkia keskustelupalstasi laajennuksia ja katsoa niiden lisätietoja.',
	'EXTENSION_INVALID_LIST'	=> 'Laajennus ”%s” ei ole kelvollinen.<br />%s<br /><br />',
	'EXTENSION_NOT_AVAILABLE'	=> 'Valittu laajennus ei ole käytettävissä tällä keskustelupalstalla. Tarkasta, että käyttämäsi phpBB:n ja PHP:n versiot ovat tuettuja (ks. lisätiedot-sivu).',
	'EXTENSION_DIR_INVALID'		=> 'Valittua laajennusta ei voi ottaa käyttöön, koska laajennuksen hakemistorakenne ei ole kelvollinen.',
	'EXTENSION_NOT_ENABLEABLE'	=> 'Valittua laajennusta ei voi ottaa käyttöön. Tarkasta laajennuksen vaatimukset.',
	'EXTENSION_NOT_INSTALLED'	=> 'Laajennus ”%s” ei ole käytettävissä. Tarkista, että olet asentanut sen oikein.',

	'DETAILS'				=> 'Lisätiedot',

	'EXTENSIONS_DISABLED'	=> 'Käytöstä poistetut laajennukset',
	'EXTENSIONS_ENABLED'	=> 'Käytössä olevat laajennukset',

	'EXTENSION_DELETE_DATA'	=> 'Poista data',
	'EXTENSION_DISABLE'		=> 'Poista käytöstä',
	'EXTENSION_ENABLE'		=> 'Ota käyttöön',

	'EXTENSION_DELETE_DATA_EXPLAIN'	=> 'Laajennuksen datan poistaminen poistaa kaikki sen tiedot ja asetukset. Laajennuksen tiedostot säilytetään, jotta laajennuksen voi ottaa käyttöön uudelleen.',
	'EXTENSION_DISABLE_EXPLAIN'		=> 'Laajennuksen poistaminen käytöstä säilyttää kaikki sen tiedostot, tiedot ja asetukset, mutta poistaa kaikki laajennuksen tarjoamat toiminnot.',
	'EXTENSION_ENABLE_EXPLAIN'		=> 'Ota laajennus käyttöön hyödyntääksesi sitä keskustelupalstallasi.',

	'EXTENSION_DELETE_DATA_IN_PROGRESS'	=> 'Laajennuksen dataa poistetaan parhaillaan. Älä poistu tältä sivulta tai päivitä sivua ennen kuin toimenpide on valmis.',
	'EXTENSION_DISABLE_IN_PROGRESS'	=> 'Laajennusta poistetaan käytöstä parhaillaan. Älä poistu tältä sivulta tai päivitä sivua ennen kuin toimenpide on valmis.',
	'EXTENSION_ENABLE_IN_PROGRESS'	=> 'Laajennusta otetaan käyttöön parhaillaan. Älä poistu tältä sivulta tai päivitä sivua ennen kuin toimenpide on valmis.',

	'EXTENSION_DELETE_DATA_SUCCESS'	=> 'Laajennuksen data on poistettu.',
	'EXTENSION_DISABLE_SUCCESS'		=> 'Laajennus on poistettu käytöstä.',
	'EXTENSION_ENABLE_SUCCESS'		=> 'Laajennus on otettu käyttöön.',

	'EXTENSION_NAME'			=> 'Laajennuksen nimi',
	'EXTENSION_ACTIONS'			=> 'Toiminnot',
	'EXTENSION_OPTIONS'			=> 'Asetukset',
	'EXTENSION_INSTALL_HEADLINE'=> 'Laajennuksen asentaminen',
	'EXTENSION_INSTALL_EXPLAIN'	=> '<ol>
			<li>Lataa laajennus phpBB:n laajennusten tietokannasta</li>
			<li>Pura laajennus ja siirrä sen sisältö phpBB-keskustelupalstasi <samp>ext/</samp>-hakemistoon</li>
			<li>Ota laajennus käyttöön laajennusten hallinnassa</li>
		</ol>',
	'EXTENSION_UPDATE_HEADLINE'	=> 'Laajennuksen päivittäminen',
	'EXTENSION_UPDATE_EXPLAIN'	=> '<ol>
			<li>Poista laajennus käytöstä</li>
			<li>Poista laajennuksen tiedostot tiedostojärjestelmästä</li>
			<li>Siirrä uudet tiedostot palvelimelle</li>
			<li>Ota laajennus käyttöön</li>
		</ol>',
	'EXTENSION_REMOVE_HEADLINE'	=> 'Laajennuksen poistaminen keskustelupalstalta kokonaan',
	'EXTENSION_REMOVE_EXPLAIN'	=> '<ol>
			<li>Poista laajennus käytöstä</li>
			<li>Poista laajennuksen data</li>
			<li>Poista laajennuksen tiedostot tiedostojärjestelmästä</li>
		</ol>',

	'EXTENSION_DELETE_DATA_CONFIRM'	=> 'Haluatko varmasti poistaa laajennuksen ”%s” datan?<br /><br />Poistettuja tietoja ja asetuksia ei voi palauttaa!',
	'EXTENSION_DISABLE_CONFIRM'		=> 'Haluatko varmasti poistaa laajennuksen ”%s” käytöstä?',
	'EXTENSION_ENABLE_CONFIRM'		=> 'Haluatko varmasti ottaa laajennuksen ”%s” käyttöön?',
	'EXTENSION_FORCE_UNSTABLE_CONFIRM'	=> 'Haluatko varmasti ottaa epävakaan version käyttöön?',

	'RETURN_TO_EXTENSION_LIST'	=> 'Palaa laajennusten luetteloon',

	'EXT_DETAILS'			=> 'Laajennuksen lisätiedot',
	'DISPLAY_NAME'			=> 'Näytettävä nimi',
	'CLEAN_NAME'			=> 'Puhdistettu nimi',
	'TYPE'					=> 'Tyyppi',
	'DESCRIPTION'			=> 'Kuvaus',
	'VERSION'				=> 'Versio',
	'HOMEPAGE'				=> 'Kotisivu',
	'PATH'					=> 'Tiedostopolku',
	'TIME'					=> 'Julkaisuaika',
	'LICENSE'				=> 'Lisenssi',

	'REQUIREMENTS'			=> 'Vaatimukset',
	'PHPBB_VERSION'			=> 'PhpBB:n versio',
	'PHP_VERSION'			=> 'PHP:n versio',
	'AUTHOR_INFORMATION'	=> 'Tekijän tiedot',
	'AUTHOR_NAME'			=> 'Nimi',
	'AUTHOR_EMAIL'			=> 'Sähköposti',
	'AUTHOR_HOMEPAGE'		=> 'Kotisivu',
	'AUTHOR_ROLE'			=> 'Rooli',

	'NOT_UP_TO_DATE'		=> '%s ei ole ajan tasalla',
	'UP_TO_DATE'			=> '%s on ajan tasalla',
	'ANNOUNCEMENT_TOPIC'	=> 'Julkaisutiedote',
	'DOWNLOAD_LATEST'		=> 'Lataa versio',
	'NO_VERSIONCHECK'		=> 'Version tarkastamisen tietoja ei ole määritetty.',

	'VERSIONCHECK_FORCE_UPDATE_ALL'		=> 'Tarkasta kaikki versiot',
	'FORCE_UNSTABLE'					=> 'Tarkasta, onko epävakaita versioita saatavilla',
	'EXTENSIONS_VERSION_CHECK_SETTINGS'	=> 'Versioiden tarkastuksen asetukset',

	'BROWSE_EXTENSIONS_DATABASE'		=> 'Selaa laajennusten tietokantaa',

	'META_FIELD_NOT_SET'	=> 'Vaadittua metakenttää %s ei ole asetettu.',
	'META_FIELD_INVALID'	=> 'Metakenttä %s ei ole kelvollinen.',
));
