<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

// BBCodes
// Note to translators: you can translate everything but what's between { and }
$lang = array_merge($lang, array(
	'ACP_BBCODES_EXPLAIN'	=> 'BBCode on HTML:n erityistoteutus, joka tarjoaa paremman mahdollisuuden hallita lopputuloksen ulkoasua. Tällä sivulla voit lisätä, poistaa ja muokata omia BBCode-tageja.',
	'ADD_BBCODE'	=> 'Lisää uusi BBCode-tagi',

	'BBCODE_DANGER'	=> 'BBCode-tagi, jota olet luomassa, näyttää käyttävän {TEXT}-tyyppiä HTML-attribuutissa. Tämä voi johtaa XSS-haavoittuvuuteen. Kokeile mieluummin rajoittavampia {SIMPLETEXT}- ja {INTTEXT}-tyyppejä. Jatka vain, jos ymmärrät riskit ja {TEXT}-tyypin käyttö on mielestäsi täysin välttämätöntä.',
	'BBCODE_DANGER_PROCEED'	=> 'Jatka',	//'I understand the risk',
	'BBCODE_ADDED'	=> 'BBCode-tagi on lisätty.',
	'BBCODE_EDITED'	=> 'BBCode-tagia on muokattu.',
	'BBCODE_DELETED'			=> 'BBCode-tagi on poistettu.',
	'BBCODE_NOT_EXIST'	=> 'Antamaasi BBCode-tagia ei ole olemassa.',
	'BBCODE_HELPLINE'	=> 'Ohjeteksti',
	'BBCODE_HELPLINE_EXPLAIN'	=> 'Tämä teksti näytetään, kun hiiren osoitin on BBCode-tagin päällä.',
	'BBCODE_HELPLINE_TEXT'	=> 'Ohjeteksti',
	'BBCODE_HELPLINE_TOO_LONG'	=> 'Antamasi ohjeteksti on liian pitkä.',
	'BBCODE_INVALID_TAG_NAME'	=> 'Antamasi BBCode-tagin nimi on jo olemassa.',
	'BBCODE_INVALID'	=> 'Antamasi BBCoden rakenne ei ole kelvollinen.',
	'BBCODE_OPEN_ENDED_TAG'	=> 'Antamasi BBCoden täytyy sisältää sekä alku- että lopputagit.',
	'BBCODE_TAG'	=> 'Tagi',
	'BBCODE_TAG_TOO_LONG'	=> 'Tagille antamasi nimi on liian pitkä.',
	'BBCODE_TAG_DEF_TOO_LONG'	=> 'Tagille antamasi määritelmä on liian pitkä.',
	'BBCODE_USAGE'	=> 'BBCode-tagin käyttö',
	'BBCODE_USAGE_EXAMPLE'	=> '[korostus={COLOR}]{TEXT}[/korostus]<br /><br />[fontti={SIMPLETEXT1}]{SIMPLETEXT2}[/fontti]',
	'BBCODE_USAGE_EXPLAIN'	=> 'Määritä, kuinka BBCode-tagia käytetään. Korvaa muuttujat niitä vastaavilla tyypeillä (%sks. alla%s).',

	'EXAMPLE'	=> 'Esimerkki:',
	'EXAMPLES'	=> 'Esimerkkejä:',

	'HTML_REPLACEMENT'	=> 'Korvaava HTML',
	'HTML_REPLACEMENT_EXAMPLE'	=> '&lt;span style="background-color: {COLOR};"&gt;{TEXT}&lt;/span&gt;<br /><br />&lt;span style="font-family: {SIMPLETEXT1};"&gt;{SIMPLETEXT2}&lt;/span&gt;',
	'HTML_REPLACEMENT_EXPLAIN'	=> 'Määritä korvaava HTML, jota käytetään oletusarvoisesti. Muista sisällyttää korvaukseen tyypit, joita käytit yllä!',

	'TOKEN'	=> 'Tyyppi',
	'TOKENS'	=> 'Tyypit',
	'TOKENS_EXPLAIN'	=> 'Tyypit kuvastavat käyttäjän syötteitä. Syötteet hyväksytään vain, jos ne sopivat niitä vastaavien tyyppien määritelmiin. Tarvittaessa voit numeroida tyyppejä lisäämällä numeron viimeiseksi merkiksi sulkujen väliin (esim. {TEXT1}, {TEXT2}).<br /><br />HTML-korvaus voi sisältää myös mitä tahansa language/-hakemistossa sijaitsevien kielipakettien merkkijonoja seuraavasti: {L_<em>&lt;MERKKIJONO&gt;</em>}, missä <em>&lt;MERKKIJONO&gt;</em> on nimi käännetylle merkkijonolle, jonka haluat lisätä. Esimerkiksi {L_WROTE} korvataan tekstillä ”Kirjoitti” tai sitä vastaavalla käännöksellä käyttäjän kielivalinnasta riippuen.<br /><br /><strong>Huomaa, että vain alla lueteltuja tyyppejä voi käyttää mukautettujen BBCode-tagien kanssa.</strong>',
	'TOKEN_DEFINITION'	=> 'Määritelmä',
	'TOO_MANY_BBCODES'	=> 'Et voi luoda enempää BBCode-tageja. Poista yksi tai useampi tagi ja yritä sen jälkeen uudelleen.',

	'tokens'	=> array(
		'TEXT'	=> 'Mikä tahansa teksti mukaan lukien ulkomaalaiset merkit, numerot jne. Tätä tyyppiä ei kannata käyttää HTML-tageissa. Sen sijaan kokeile IDENTIFIER-, INTTEXT- ja SIMPLETEXT-tyyppejä.',
		'SIMPLETEXT'	=> 'Latinalaisen aakkoston merkit (A–Z), numerot, välilyönnit, pilkut, pisteet, miinusmerkki, plusmerkki, yhdysviiva ja alaviiva.',
		'INTTEXT'	=> 'Unicode-kirjaimet, numerot, välilyönnit, pilkut, pisteet, miinusmerkki, plusmerkki, yhdysviiva, alaviiva ja tyhjämerkit.',
		'IDENTIFIER' => 'Latinalaisen aakkoston kirjaimet (A–Z), numerot, yhdysviiva ja alaviiva.',
		'NUMBER'	=> 'Mikä tahansa numerojono.',
		'EMAIL'	=> 'Kelvollinen sähköpostiosoite.',
		'URL'	=> 'Mitä tahansa protokollaa käyttävä kelvollinen URL (protokollat http, ftp jne. eivät ole alttiita JavaScript-hyökkäyksille). Jos protokollaa ei ole annettu, ”http://” lisätään merkkijonon eteen.',
		'LOCAL_URL'	=> 'Paikallinen URL. URL:n täytyy olla suhteellinen aihesivuun nähden eikä se saa sisältää palvelimen nimeä tai protokollaa, sillä linkkien alkuun lisätään automaattisesti ”%s”.',
		'RELATIVE_URL' => 'Suhteellinen URL. Voit käyttää tätä täsmätäksesi osia URL:sta, mutta ole varovainen: täysi URL on toimiva suhteellinen URL. Jos haluat käyttää keskustelupalstaasi nähden suhteellisia URL:eja, valitse LOCAL_URL-tyyppi.',
		'COLOR'	=> 'HTML-väri joko numeerisessa muodossa (esim. <samp>#FF1234</samp>) tai <a href="http://www.w3.org/TR/CSS21/syndata.html#value-def-color">CSS-avainsanana</a> (esim. <samp>fuchsia</samp> tai <samp>InactiveBorder</samp>).',
	),
));

// Smilies and topic icons
$lang = array_merge($lang, array(
	'ACP_ICONS_EXPLAIN'	=> 'Tässä voit lisätä, poistaa ja muokata kuvakkeita, joita käyttäjät voivat lisätä aiheisiinsa tai viesteihinsä. Nämä kuvakkeet näkyvät tavallisesti aiheiden otsikoiden vieressä aluetta luettaessa tai viestien otsikoiden vieressä aiheita luettaessa. Voit myös asentaa ja luoda uusia kuvakepaketteja.',
	'ACP_SMILIES_EXPLAIN'	=> 'Hymiöt ovat tavallisesti pieniä ja joskus myös animoituja kuvia, joiden avulla voi ilmaista tunnetiloja. Tällä sivulla voit lisätä, poistaa ja muokata hymiöitä, joita käyttäjät voivat lisätä viesteihinsä ja yksityisviesteihinsä. Voit myös asentaa ja luoda uusia hymiöpaketteja.',
	'ADD_SMILIES'	=> 'Lisää useita hymiöitä',
	'ADD_SMILEY_CODE'	=> 'Lisää hymiökoodi',
	'ADD_ICONS'	=> 'Lisää useita kuvakkeita',
	'AFTER_ICONS'	=> 'Kuvakkeen %s jälkeen',
	'AFTER_SMILIES'	=> 'Hymiön %s jälkeen',

	'CODE'	=> 'Koodi',
	'CURRENT_ICONS'	=> 'Nykyiset kuvakkeet',
	'CURRENT_ICONS_EXPLAIN'	=> 'Valitse, mitä tällä hetkellä asennetuille kuvakkeille tehdään.',
	'CURRENT_SMILIES'	=> 'Nykyiset hymiöt',
	'CURRENT_SMILIES_EXPLAIN'	=> 'Valitse, mitä tällä hetkellä asennetuille hymiöille tehdään.',

	'DISPLAY_ON_POSTING'	=> 'Näytä viestien kirjoitussivulla',
	'DISPLAY_POSTING'	=> 'Näytetään viestien kirjoitussivulla',
	'DISPLAY_POSTING_NO'	=> 'Ei näytetä viestien kirjoitussivulla',

	'EDIT_ICONS'	=> 'Muokkaa kuvakkeita',
	'EDIT_SMILIES'	=> 'Muokkaa hymiöitä',
	'EMOTION'	=> 'Tunnetila',
	'EXPORT_ICONS'	=> 'Lataa kuvakkeet',
	'EXPORT_ICONS_EXPLAIN'	=> '%sTätä linkkiä napsauttamalla asentamiesi kuvakkeiden asetukset pakataan tiedostoon <samp>icons.pak</samp>. Tämän tiedoston avulla voit latauksen jälkeen luoda <samp>.zip</samp>- tai <samp>.tgz</samp>-tiedoston, joka sisältää kaikki kuvakkeesi ja tämän <samp>icons.pak</samp>-asetustiedoston.%s',
	'EXPORT_SMILIES'	=> 'Lataa hymiöt',
	'EXPORT_SMILIES_EXPLAIN'	=> '%sTätä linkkiä napsauttamalla asentamiesi hymiöiden asetukset pakataan tiedostoon <samp>smilies.pak</samp>. Tämän tiedoston avulla voit latauksen jälkeen luoda <samp>.zip</samp>- tai <samp>.tgz</samp>-tiedoston, joka sisältää kaikki hymiösi ja tämän <samp>smilies.pak</samp>-asetustiedoston.%s',

	'FIRST'	=> 'Ensimmäinen',

	'ICONS_ADD'	=> 'Lisää uusi kuvake',
	'ICONS_ADDED'			=> array(
		0	=> 'Kuvakkeita ei lisätty.',
		1	=> 'Kuvake on lisätty.',
		2	=> 'Kuvakkeet on lisätty.',
	),
	'ICONS_CONFIG'	=> 'Kuvakkeen asetukset',
	'ICONS_DELETED'	=> 'Kuvake on poistettu.',
	'ICONS_EDIT'	=> 'Muokkaa kuvaketta',
	'ICONS_EDITED'			=> array(
		0	=> 'Kuvakkeita ei päivitetty.',
		1	=> 'Kuvake on päivitetty.',
		2	=> 'Kuvakkeet on päivitetty.',
	),
	'ICONS_HEIGHT'	=> 'Kuvakkeen korkeus',
	'ICONS_IMAGE'	=> 'Kuvakkeen kuva',
	'ICONS_IMPORTED'	=> 'Kuvakepaketti on asennettu.',
	'ICONS_IMPORT_SUCCESS'	=> 'Kuvakepaketin tuonti onnistui.',
	'ICONS_LOCATION'	=> 'Kuvakkeen sijainti',
	'ICONS_NOT_DISPLAYED'	=> 'Seuraavia kuvakkeita ei näytetä viestien kirjoitussivulla',
	'ICONS_ORDER'	=> 'Kuvakkeen järjestys',
	'ICONS_URL'	=> 'Kuvakkeen kuvatiedosto',
	'ICONS_WIDTH'	=> 'Kuvakkeen leveys',
	'IMPORT_ICONS'	=> 'Asenna kuvakepaketti',
	'IMPORT_SMILIES'	=> 'Asenna hymiöpaketti',

	'KEEP_ALL'	=> 'Säilytä kaikki',

	'MASS_ADD_SMILIES'	=> 'Lisää useita hymiöitä',

	'NO_ICONS_ADD'	=> 'Lisättäviä kuvakkeita ei ole.',
	'NO_ICONS_EDIT'	=> 'Muokattavia kuvakkeita ei ole.',
	'NO_ICONS_EXPORT'	=> 'Sinulla ei ole kuvakkeita, joista voisi luoda paketin.',
	'NO_ICONS_PAK'	=> 'Kuvakepaketteja ei löytynyt.',
	'NO_SMILIES_ADD'	=> 'Lisättäviä hymiöitä ei ole.',
	'NO_SMILIES_EDIT'	=> 'Muokattavia hymiöitä ei ole.',
	'NO_SMILIES_EXPORT'	=> 'Sinulla ei ole hymiöitä, joista voisi luoda paketin.',
	'NO_SMILIES_PAK'	=> 'Hymiöpaketteja ei löytynyt.',

	'PAK_FILE_NOT_READABLE'	=> '<samp>.pak</samp>-tiedoston luku epäonnistui.',

	'REPLACE_MATCHES'	=> 'Korvaa vastaavuudet',

	'SELECT_PACKAGE'	=> 'Valitse pakettitiedosto',
	'SMILIES_ADD'	=> 'Lisää uusi hymiö',
	'SMILIES_ADDED'				=> array(
		0	=> 'Hymiöitä ei lisätty.',
		1	=> 'Hymiö on lisätty.',
		2	=> 'Hymiöt on lisätty.',
	),
	'SMILIES_CODE'	=> 'Hymiön koodi',
	'SMILIES_CONFIG'	=> 'Hymiön asetukset',
	'SMILIES_DELETED'	=> 'Hymiö on poistettu.',
	'SMILIES_EDIT'	=> 'Muokkaa hymiötä',
	'SMILIE_NO_CODE'	=> 'Hymiötä ”%s” ei hyväksytty, koska sille ei ollut annettu koodia.',
	'SMILIE_NO_EMOTION'	=> 'Hymiötä ”%s” ei hyväksytty, koska sille ei ollut annettu tunnetilaa.',
	'SMILIE_NO_FILE'	=> 'Hymiötä ”%s” ei hyväksytty, koska tiedostoa ei ole olemassa.',
	'SMILIES_EDITED'			=> array(
		0	=> 'Hymiöitä ei päivitetty.',
		1	=> 'Hymiö on päivitetty.',
		2	=> 'Hymiöt on päivitetty.',
	),
	'SMILIES_EMOTION'	=> 'Tunnetila',
	'SMILIES_HEIGHT'	=> 'Hymiön korkeus',
	'SMILIES_IMAGE'	=> 'Hymiön kuva',
	'SMILIES_IMPORTED'	=> 'Hymiöpaketti on asennettu.',
	'SMILIES_IMPORT_SUCCESS'	=> 'Hymiöpaketin tuonti onnistui.',
	'SMILIES_LOCATION'	=> 'Hymiön sijainti',
	'SMILIES_NOT_DISPLAYED'	=> 'Seuraavia hymiöitä ei näytetä viestien kirjoitussivulla',
	'SMILIES_ORDER'	=> 'Hymiön järjestys',
	'SMILIES_URL'	=> 'Hymiön kuvatiedosto',
	'SMILIES_WIDTH'	=> 'Hymiön leveys',

	'TOO_MANY_SMILIES'			=> array(
		1	=> 'Hymiöitä voi olla käytössä enintään %d.',
		2	=> 'Hymiöitä voi olla käytössä enintään %d.',
	),

	'WRONG_PAK_TYPE'	=> 'Määrittämäsi paketti ei sisällä kelvollista tietoa.',
));

// Word censors
$lang = array_merge($lang, array(
	'ACP_WORDS_EXPLAIN'	=> 'Tässä hallintapaneelissa voit lisätä, poistaa ja muokata sanoja, jotka sensuroidaan automaattisesti keskustelupalstallasi. Näitä sanoja sisältävien käyttäjätunnusten rekisteröinti on silti mahdollista. Jokerimerkkien (*) käyttö on sallittua. Esimerkiksi *malli* sopii sanaan ”kummallinen”, malli* sanaan ”mallikas” ja *malli sanaan ”toimintamalli”.',
	'ADD_WORD'	=> 'Lisää uusi sana',

	'EDIT_WORD'	=> 'Muokkaa sanan sensurointia',
	'ENTER_WORD'	=> 'Sinun täytyy antaa sana ja sen korvaus.',

	'NO_WORD'	=> 'Muokattavaa sanaa ei ole valittu.',

	'REPLACEMENT'	=> 'Korvaus',

	'UPDATE_WORD'	=> 'Päivitä sanan sensurointi',

	'WORD'	=> 'Sana',
	'WORD_ADDED'	=> 'Sensuroitava sana on lisätty.',
	'WORD_REMOVED'	=> 'Valitsemasi sanan sensurointi on poistettu.',
	'WORD_UPDATED'	=> 'Valitsemasi sanan sensurointi on päivitetty.',
));

// Ranks
$lang = array_merge($lang, array(
	'ACP_RANKS_EXPLAIN'	=> 'Tässä voit lisätä, muokata ja poistaa arvonimiä. Voit luoda myös erityisiä arvonimiä, joita voi antaa käyttäjille käyttäjien hallinnassa.',
	'ADD_RANK'	=> 'Lisää uusi arvonimi',

	'MUST_SELECT_RANK'	=> 'Et valinnut arvonimeä.',

	'NO_ASSIGNED_RANK'	=> 'Erityistä arvonimeä ei ole.',
	'NO_RANK_TITLE'	=> 'Et antanut arvonimelle kuvausta.',
	'NO_UPDATE_RANKS'	=> 'Arvonimi on poistettu. Sen sijaan tätä arvonimeä käyttävien käyttäjien tietoja ei päivitetty. Sinun täytyy muuttaa näiden käyttäjien arvonimet käsin.',

	'RANK_ADDED'	=> 'Arvonimi on lisätty.',
	'RANK_IMAGE'	=> 'Arvonimen kuva',
	'RANK_IMAGE_EXPLAIN'	=> 'Tämän avulla voit yhdistää arvonimeen pienen kuvan. Polku on suhteessa phpBB:n juurihakemistoon.',
	'RANK_IMAGE_IN_USE'	=> '(Käytössä)',
	'RANK_MINIMUM'	=> 'Viestien vähimmäismäärä',
	'RANK_REMOVED'	=> 'Arvonimi on poistettu.',
	'RANK_SPECIAL'	=> 'Aseta erityiseksi arvonimeksi',
	'RANK_TITLE'	=> 'Arvonimen kuvaus',
	'RANK_UPDATED'	=> 'Arvonimi on päivitetty.',
));

// Disallow Usernames
$lang = array_merge($lang, array(
	'ACP_DISALLOW_EXPLAIN'	=> 'Tässä voit hallita, minkälaisten käyttäjätunnusten käyttö ei ole sallittu. Kiellettyjen käyttäjätunnusten määrittämisessä voi käyttää jokerimerkkiä *.',
	'ADD_DISALLOW_EXPLAIN'	=> 'Jokerimerkki * vastaa mitä tahansa merkkejä.',
	'ADD_DISALLOW_TITLE'	=> 'Lisää kielletty käyttäjätunnus',

	'DELETE_DISALLOW_EXPLAIN'	=> 'Voit poistaa kielletyn käyttäjätunnuksen valitsemalla sen tästä luettelosta ja napsauttamalla Lähetä-painiketta.',
	'DELETE_DISALLOW_TITLE'	=> 'Poista kielletty käyttäjätunnus',
	'DISALLOWED_ALREADY'	=> 'Antamasi nimi on jo kielletty.',
	'DISALLOWED_DELETED'	=> 'Kielletty käyttäjätunnus on poistettu.',
	'DISALLOW_SUCCESSFUL'	=> 'Kielletty käyttäjätunnus on lisätty.',

	'NO_DISALLOWED'	=> 'Kiellettyjä käyttäjätunnuksia ei ole',
	'NO_USERNAME_SPECIFIED'	=> 'Et ole valinnut tai syöttänyt käyttäjätunnusta käsiteltäväksi.',
));

// Reasons
$lang = array_merge($lang, array(
	'ACP_REASONS_EXPLAIN'	=> 'Tässä voit hallita syitä, joiden perusteella viestejä voi ilmiantaa tai hylätä. Tähdellä (*) merkittyä oletussyytä ei voi poistaa, koska tätä syytä käytetään tavallisesti mukautettujen perustelujen antamiseen, kun mikään muu syy ei päde.',
	'ADD_NEW_REASON'	=> 'Lisää uusi syy',
	'AVAILABLE_TITLES'	=> 'Käytettävissä olevat lokalisoidut syiden otsikot',

	'IS_NOT_TRANSLATED'	=> 'Tätä syytä <strong>ei</strong> ole lokalisoitu.',
	'IS_NOT_TRANSLATED_EXPLAIN'	=> 'Tätä syytä <strong>ei</strong> ole lokalisoitu. Jos haluat määrittää lokalisoidun muodon, syötä kielitiedostojen ilmiantojen syiden osiossa käytetty avain.',
	'IS_TRANSLATED'	=> 'Tämä syy on lokalisoitu.',
	'IS_TRANSLATED_EXPLAIN'	=> 'Tämä syy on lokalisoitu. Jos tässä antamasi otsikko on määritetty kielitiedostojen ilmiantojen syiden osiossa, otsikon ja kuvauksen lokalisoidut muodot otetaan käyttöön.',

	'NO_REASON'	=> 'Syytä ei löytynyt.',
	'NO_REASON_INFO'	=> 'Tälle syylle täytyy antaa otsikko ja kuvaus.',
	'NO_REMOVE_DEFAULT_REASON'	=> 'Et voi poistaa Muu syy -oletussyytä.',

	'REASON_ADD'	=> 'Lisää ilmiannon/hylkäyksen syy',
	'REASON_ADDED'	=> 'Ilmiannon/hylkäyksen syy on lisätty.',
	'REASON_ALREADY_EXIST'	=> 'Tätä otsikkoa käyttävä syy on jo olemassa. Anna tälle syylle jokin toinen otsikko.',
	'REASON_DESCRIPTION'	=> 'Syyn kuvaus',
	'REASON_DESC_TRANSLATED'	=> 'Näytettävä syyn kuvaus',
	'REASON_EDIT'	=> 'Muokkaa ilmiannon/hylkäyksen syytä',
	'REASON_EDIT_EXPLAIN'	=> 'Tässä voit lisätä tai muokata syytä. Jos syy on käännetty toiselle kielelle, sen lokalisoitua versiota käytetään tässä annetun kuvauksen sijaan.',
	'REASON_REMOVED'	=> 'Ilmiannon/hylkäyksen syy on poistettu.',
	'REASON_TITLE'	=> 'Syyn otsikko',
	'REASON_TITLE_TRANSLATED'	=> 'Näytettävä syyn otsikko',
	'REASON_UPDATED'	=> 'Ilmiannon/hylkäyksen syy on päivitetty.',

	'USED_IN_REPORTS'	=> 'Ilmiantojen lukumäärä',
));
