<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ADMIN_SIG_PREVIEW'	=> 'Allekirjoituksen esikatselu',
	'AT_LEAST_ONE_FOUNDER'	=> 'Et voi muuttaa tätä perustajaa tavalliseksi käyttäjäksi, sillä keskustelupalstalla täytyy olla vähintään yksi perustaja. Jos haluat muuttaa tämän käyttäjän tavalliseksi käyttäjäksi, ylennä jokin toinen käyttäjä perustajaksi ensin.',

	'BAN_ALREADY_ENTERED'	=> 'Tämä porttikielto on jo annettu. Porttikieltojen luetteloa ei päivitetty.',
	'BAN_SUCCESSFUL'	=> 'Porttikielto on annettu.',

	'CANNOT_BAN_ANONYMOUS'	=> 'Et voi antaa porttikieltoa vierailijoille. Vierailijoiden oikeuksia voi muokata Oikeudet-välilehdessä.',
	'CANNOT_BAN_FOUNDER'	=> 'Et voi antaa porttikieltoa perustajille.',
	'CANNOT_BAN_YOURSELF'	=> 'Et voi antaa porttikieltoa itsellesi.',
	'CANNOT_DEACTIVATE_BOT'	=> 'Et voi sulkea bottien käyttäjätilejä. Ole hyvä ja poista botti käytöstä bottien hallintasivulla.',
	'CANNOT_DEACTIVATE_FOUNDER'	=> 'Et voi sulkea perustajien käyttäjätilejä.',
	'CANNOT_DEACTIVATE_YOURSELF'	=> 'Et voi sulkea omaa käyttäjätiliäsi.',
	'CANNOT_FORCE_REACT_BOT'	=> 'Et voi pakottaa botteja aktivoimaan käyttäjätilejään uudelleen. Ole hyvä ja ota botti käyttöön bottien hallintasivulla.',
	'CANNOT_FORCE_REACT_FOUNDER'	=> 'Et voi pakottaa perustajia aktivoimaan käyttäjätilejään uudelleen.',
	'CANNOT_FORCE_REACT_YOURSELF'	=> 'Et voi pakottaa itseäsi aktivoimaan käyttäjätiliäsi uudelleen.',
	'CANNOT_REMOVE_ANONYMOUS'	=> 'Et voi poistaa vierailijoiden käyttäjätiliä.',
	'CANNOT_REMOVE_FOUNDER' => 'Et voi poistaa perustajien käyttäjätilejä.',
	'CANNOT_REMOVE_YOURSELF'	=> 'Et voi poistaa omaa käyttäjätiliäsi.',
	'CANNOT_SET_FOUNDER_IGNORED'	=> 'Et voi ylentää huomioimattomia käyttäjiä perustajiksi.',
	'CANNOT_SET_FOUNDER_INACTIVE'	=> 'Käyttäjän tili täytyy aktivoida, ennen kuin hänet voi ylentää perustajaksi.',
	'CONFIRM_EMAIL_EXPLAIN'	=> 'Tätä vaaditaan vain, jos haluat vaihtaa käyttäjän sähköpostiosoitteen.',

	'DELETE_POSTS'	=> 'Poista viestit',
	'DELETE_USER'	=> 'Poista käyttäjä',
	'DELETE_USER_EXPLAIN'	=> 'Huomaa, että poistettua käyttäjää ei voi palauttaa. Tämän käyttäjän lähettämät yksityisviestit poistetaan myös, eivätkä ne enää näy viestien vastaanottajille.',

	'FORCE_REACTIVATION_SUCCESS'	=> 'Uudelleenaktivointi on pakotettu.',
	'FOUNDER'	=> 'Perustaja',
	'FOUNDER_EXPLAIN'	=> 'Perustajilla on kaikki ylläpidolliset oikeudet eikä heitä voi poistaa, muokata tai asettaa porttikieltoon muiden kuin toisten perustajien toimesta.',

	'GROUP_APPROVE'	=> 'Hyväksy jäsen',
	'GROUP_DEFAULT'	=> 'Aseta oletusryhmäksi jäsenelle',
	'GROUP_DELETE'	=> 'Poista jäsen ryhmästä',
	'GROUP_DEMOTE'	=> 'Alenna ryhmänjohtajasta',
	'GROUP_PROMOTE'	=> 'Ylennä ryhmänjohtajaksi',

	'IP_WHOIS_FOR'	=> 'Whois-tiedot osoitteelle %s',

	'LAST_ACTIVE'	=> 'Viimeksi ollut aktiivinen',

	'MOVE_POSTS_EXPLAIN'	=> 'Valitse alue, johon haluat siirtää kaikki käyttäjän kirjoittamat viestit.',

	'NO_SPECIAL_RANK'	=> 'Ei erityistä arvonimeä',
	'NO_WARNINGS'	=> 'Ei varoituksia.',
	'NOT_MANAGE_FOUNDER'	=> 'Yritit muuttaa perustajan tietoja. Vain perustajat voivat hallita muita perustajia.',

	'QUICK_TOOLS'	=> 'Pikatyökalut',

	'REGISTERED'	=> 'Rekisteröitynyt',
	'REGISTERED_IP'	=> 'Rekisteröityi IP-osoitteesta',
	'RETAIN_POSTS'	=> 'Säilytä viestit',

	'SELECT_FORM'	=> 'Valitse lomake',
	'SELECT_USER'	=> 'Valitse käyttäjä',

	'USER_ADMIN'	=> 'Käyttäjän hallinta',
	'USER_ADMIN_ACTIVATE'	=> 'Aktivoi käyttäjätili',
	'USER_ADMIN_ACTIVATED'	=> 'Käyttäjätili on aktivoitu.',
	'USER_ADMIN_AVATAR_REMOVED'	=> 'Käyttäjän avatar on poistettu.',
	'USER_ADMIN_BAN_EMAIL'	=> 'Anna porttikielto sähköpostiosoitteelle',
	'USER_ADMIN_BAN_EMAIL_REASON'	=> 'Sähköpostiosoite on asetettu porttikieltoon käyttäjän hallinnan kautta',
	'USER_ADMIN_BAN_IP'	=> 'Anna porttikielto IP-osoitteelle',
	'USER_ADMIN_BAN_IP_REASON'	=> 'IP-osoite on asetettu porttikieltoon käyttäjän hallinnan kautta',
	'USER_ADMIN_BAN_NAME_REASON'	=> 'Käyttäjätunnus on asetettu porttikieltoon käyttäjän hallinnan kautta',
	'USER_ADMIN_BAN_USER'	=> 'Anna porttikielto käyttäjätunnukselle',
	'USER_ADMIN_DEACTIVATE'	=> 'Sulje käyttäjätili',
	'USER_ADMIN_DEACTIVED'	=> 'Käyttäjätili on suljettu.',
	'USER_ADMIN_DEL_ATTACH'	=> 'Poista kaikki liitetiedostot',
	'USER_ADMIN_DEL_AVATAR'	=> 'Poista avatar',
	'USER_ADMIN_DEL_OUTBOX'	=> 'Tyhjennä lähtevien yksityisviestien kansio',
	'USER_ADMIN_DEL_POSTS'	=> 'Poista kaikki viestit',
	'USER_ADMIN_DEL_SIG'	=> 'Poista allekirjoitus',
	'USER_ADMIN_EXPLAIN'	=> 'Tässä voit muokata käyttäjiesi tietoja ja tiettyjä asetuksia.',
	'USER_ADMIN_FORCE'	=> 'Pakota käyttäjätilin uudelleenaktivointi',
	'USER_ADMIN_LEAVE_NR'	=> 'Poista uusien käyttäjien ryhmästä',
	'USER_ADMIN_MOVE_POSTS'	=> 'Siirrä kaikki viestit',
	'USER_ADMIN_SIG_REMOVED'	=> 'Käyttäjän allekirjoitus on poistettu.',
	'USER_ATTACHMENTS_REMOVED'	=> 'Kaikki käyttäjän lähettämät liitetiedostot on poistettu.',
	'USER_AVATAR_NOT_ALLOWED'	=> 'Avataria ei voi näyttää, koska niiden käyttö on kielletty.',
	'USER_AVATAR_UPDATED'	=> 'Käyttäjän avatar on päivitetty.',
	'USER_AVATAR_TYPE_NOT_ALLOWED'	=> 'Nykyistä avataria ei voi näyttää, koska sen tyyppiä ei ole sallittu.',
	'USER_CUSTOM_PROFILE_FIELDS'	=> 'Mukautetut profiilikentät',
	'USER_DELETED'	=> 'Käyttäjä on poistettu.',
	'USER_GROUP_ADD'	=> 'Lisää käyttäjä ryhmään',
	'USER_GROUP_NORMAL'	=> 'Käyttäjien määrittelemät ryhmät, joissa käyttäjä on jäsenenä',
	'USER_GROUP_PENDING'	=> 'Ryhmät, joihin käyttäjä odottaa pääsyä',
	'USER_GROUP_SPECIAL'	=> 'Erityisryhmät, joissa käyttäjä on jäsenenä',
	'USER_LIFTED_NR'	=> 'Käyttäjä on poistettu uusien käyttäjien ryhmästä.',
	'USER_NO_ATTACHMENTS'	=> 'Liitetiedostoja ei ole.',
	'USER_NO_POSTS_TO_DELETE' => 'Käyttäjä ei ole kirjoittanut viestejä, jotka voisi säilyttää tai poistaa.',
	'USER_OUTBOX_EMPTIED'	=> 'Käyttäjän lähtevien yksityisviestien kansio on tyhjennetty.',
	'USER_OUTBOX_EMPTY'	=> 'Käyttäjän lähtevien yksityisviestien kansio oli jo tyhjä.',
	'USER_OVERVIEW_UPDATED'	=> 'Käyttäjän tiedot on päivitetty.',
	'USER_POSTS_DELETED'	=> 'Kaikki käyttäjän kirjoittamat viestit on poistettu.',
	'USER_POSTS_MOVED'	=> 'Käyttäjän kirjoittamat viestit on siirretty kohdealueelle.',
	'USER_PREFS_UPDATED'	=> 'Käyttäjän asetukset on päivitetty.',
	'USER_PROFILE'	=> 'Käyttäjän profiili',
	'USER_PROFILE_UPDATED'	=> 'Käyttäjän profiili on päivitetty.',
	'USER_RANK'	=> 'Käyttäjän arvonimi',
	'USER_RANK_UPDATED'	=> 'Käyttäjän arvonimi on päivitetty.',
	'USER_SIG_UPDATED'	=> 'Käyttäjän allekirjoitus on päivitetty',
	'USER_WARNING_LOG_DELETED'	=> 'Tietoja ei ole saatavilla. Lokimerkintä on mahdollisesti poistettu.',
	'USER_TOOLS'	=> 'Perustyökalut',
));
