<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

// Database Backup/Restore
$lang = array_merge($lang, array(
	'ACP_BACKUP_EXPLAIN'	=> 'Tässä voit ottaa varmuuskopion kaikista phpBB:hen liittyvistä tiedoista. Voit tallentaa tuloksena olevan tiedoston palvelimen <samp>store/</samp>-hakemistoon tai ladata sen suoraan omalle tietokoneellesi. Palvelimen asetuksista riippuen voit ehkä pakata tiedoston eri tavoilla.',
	'ACP_RESTORE_EXPLAIN'	=> 'Tässä voit palauttaa kaikki phpBB:n käyttämät taulut tallennetusta tiedostosta. Palvelimen asetuksista riippuen voit ehkä käyttää gzip- tai bzip2-pakattua tekstitiedostoa, joka puretaan automaattisesti. <strong>VAROITUS:</strong> Tämä toimenpide ylikirjoittaa kaiken olemassa olevan tiedon. Palautus saattaa kestää pitkään, joten älä siirry pois tältä sivulta, ennen kuin palautus on valmis. Varmuuskopiot sijaitsevat <samp>store/</samp>-hakemistossa ja niiden oletetaan olevan phpBB:n varmuuskopiointitoiminnolla luotuja. Muulla tavalla luotujen varmuuskopioiden palautus ei välttämättä onnistu.',

	'BACKUP_DELETE'		=> 'Varmuuskopion sisältävä tiedosto on poistettu.',
	'BACKUP_INVALID'	=> 'Varmuuskopioitava tiedosto ei kelpaa.',
	'BACKUP_OPTIONS'	=> 'Varmuuskopioinnin asetukset',
	'BACKUP_SUCCESS'	=> 'Varmuuskopion sisältävä tiedosto on luotu.',
	'BACKUP_TYPE'		=> 'Varmuuskopion tyyppi',

	'DATABASE' 			=> 'Tietokantatyökalut',
	'DATA_ONLY'			=> 'Vain data',
	'DELETE_BACKUP'		=> 'Poista varmuuskopio',
	'DELETE_SELECTED_BACKUP'	=> 'Haluatko varmasti poistaa valitsemasi varmuuskopion?',
	'DESELECT_ALL'		=> 'Poista valinta kaikista',
	'DOWNLOAD_BACKUP'	=> 'Lataa varmuuskopio omalle tietokoneellesi',

	'FILE_TYPE'			=> 'Tiedoston tyyppi',
	'FILE_WRITE_FAIL'	=> 'Säilytyshakemistoon kirjoittaminen epäonnistui.',
	'FULL_BACKUP'		=> 'Täysi',

	'RESTORE_FAILURE'		=> 'Varmuuskopion sisältävä tiedosto saattaa olla vioittunut.',
	'RESTORE_OPTIONS'		=> 'Palautuksen asetukset',
	'RESTORE_SELECTED_BACKUP' => 'Haluatko varmasti palauttaa valitsemasi varmuuskopion?',
	'RESTORE_SUCCESS'		=> 'Tietokannan palautus onnistui.<br /><br />Keskustelupalstasi pitäisi olla nyt siinä tilassa, jossa se oli varmuuskopiota luotaessa.',

	'SELECT_ALL'			=> 'Valitse kaikki',
	'SELECT_FILE'			=> 'Valitse tiedosto',
	'START_BACKUP'			=> 'Aloita varmuuskopiointi',
	'START_RESTORE'			=> 'Aloita palautus',
	'STORE_AND_DOWNLOAD'	=> 'Tallenna ja lataa',
	'STORE_LOCAL'			=> 'Tallenna tiedosto paikallisesti',
	'STRUCTURE_ONLY'		=> 'Vain rakenne',

	'TABLE_SELECT'		=> 'Taulujen valinta',
	'TABLE_SELECT_ERROR'=> 'Sinun täytyy valita ainakin yksi taulu.',
));
