<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

/**
*	EXTENSION-DEVELOPERS PLEASE NOTE
*
*	You are able to put your permission sets into your extension.
*	The permissions logic should be added via the 'core.permissions' event.
*	You can easily add new permission categories, types and permissions, by
*	simply merging them into the respective arrays.
*	The respective language strings should be added into a language file, that
*	start with 'permissions_', so they are automatically loaded within the ACP.
*/

$lang = array_merge($lang, array(
	'ACL_CAT_ACTIONS'		=> 'Toimenpiteet',
	'ACL_CAT_CONTENT'		=> 'Sisältö',
	'ACL_CAT_FORUMS'		=> 'Alueet',
	'ACL_CAT_MISC'			=> 'Sekalaiset',
	'ACL_CAT_PERMISSIONS'	=> 'Oikeudet',
	'ACL_CAT_PM'			=> 'Yksityisviestit',
	'ACL_CAT_POLLS'			=> 'Äänestykset',
	'ACL_CAT_POST'			=> 'Viesti',
	'ACL_CAT_POST_ACTIONS'	=> 'Viestien toimenpiteet',
	'ACL_CAT_POSTING'		=> 'Viestien lähetys',
	'ACL_CAT_PROFILE'		=> 'Profiili',
	'ACL_CAT_SETTINGS'		=> 'Asetukset',
	'ACL_CAT_TOPIC_ACTIONS'	=> 'Aiheiden toimenpiteet',
	'ACL_CAT_USER_GROUP'	=> 'Käyttäjät ja ryhmät',
));

// User Permissions
$lang = array_merge($lang, array(
	'ACL_U_VIEWPROFILE'	=> 'Voi katsoa profiileja, jäsenluetteloa ja läsnäolijoiden luetteloa',
	'ACL_U_CHGNAME'		=> 'Voi vaihtaa käyttäjätunnuksen',
	'ACL_U_CHGPASSWD'	=> 'Voi vaihtaa salasanan',
	'ACL_U_CHGEMAIL'	=> 'Voi vaihtaa sähköpostiosoitteen',
	'ACL_U_CHGAVATAR'	=> 'Voi vaihtaa avatarin',
	'ACL_U_CHGGRP'		=> 'Voi vaihtaa oletusryhmän',
	'ACL_U_CHGPROFILEINFO'	=> 'Voi muokata profiilikenttiä',

	'ACL_U_ATTACH'		=> 'Voi lisätä liitetiedostoja',
	'ACL_U_DOWNLOAD'	=> 'Voi ladata tiedostoja',
	'ACL_U_SAVEDRAFTS'	=> 'Voi tallentaa luonnoksia',
	'ACL_U_CHGCENSORS'	=> 'Voi ohittaa sanojen sensuroinnin',
	'ACL_U_SIG'			=> 'Voi lisätä allekirjoituksen',

	'ACL_U_SENDPM'		=> 'Voi lähettää yksityisviestejä',
	'ACL_U_MASSPM'		=> 'Voi lähettää viestejä useille käyttäjille',
	'ACL_U_MASSPM_GROUP'=> 'Voi lähettää viestejä ryhmille',
	'ACL_U_READPM'		=> 'Voi lukea yksityisviestejä',
	'ACL_U_PM_EDIT'		=> 'Voi muokata omia yksityisviestejään',
	'ACL_U_PM_DELETE'	=> 'Voi poistaa yksityisviestejä omasta kansiostaan',
	'ACL_U_PM_FORWARD'	=> 'Voi lähettää yksityisviestejä eteenpäin',
	'ACL_U_PM_EMAILPM'	=> 'Voi lähettää yksityisviestejä sähköpostitse',
	'ACL_U_PM_PRINTPM'	=> 'Voi tulostaa yksityisviestejä',
	'ACL_U_PM_ATTACH'	=> 'Voi lisätä liitetiedostoja yksityisviesteihin',
	'ACL_U_PM_DOWNLOAD'	=> 'Voi ladata yksityisviestien sisältämiä tiedostoja',
	'ACL_U_PM_BBCODE'	=> 'Voi käyttää BBCode-tageja yksityisviesteissä',
	'ACL_U_PM_SMILIES'	=> 'Voi lisätä hymiöitä yksityisviesteihin',
	'ACL_U_PM_IMG'		=> 'Voi käyttää BBCode-tagia [img] yksityisviesteissä',
	'ACL_U_PM_FLASH'	=> 'Voi käyttää BBCode-tagia [flash] yksityisviesteissä',

	'ACL_U_SENDEMAIL'	=> 'Voi lähettää sähköpostia',
	'ACL_U_SENDIM'		=> 'Voi lähettää pikaviestejä',
	'ACL_U_IGNOREFLOOD'	=> 'Voi ohittaa viestien lähetyksen aikarajan',
	'ACL_U_HIDEONLINE'	=> 'Voi piilottaa paikallaolonsa',
	'ACL_U_VIEWONLINE'	=> 'Voi nähdä piilotetut paikallaolijat',
	'ACL_U_SEARCH'		=> 'Voi käyttää hakutoimintoa',
));

// Forum Permissions
$lang = array_merge($lang, array(
	'ACL_F_LIST'		=> 'Voi nähdä alueen',
	'ACL_F_READ'		=> 'Voi lukea aluetta',
	'ACL_F_SEARCH'		=> 'Voi hakea alueelta',
	'ACL_F_SUBSCRIBE'	=> 'Voi asettaa alueen seurantaan',
	'ACL_F_PRINT'		=> 'Voi tulostaa aiheita',
	'ACL_F_EMAIL'		=> 'Voi lähettää aiheita sähköpostitse',
	'ACL_F_BUMP'		=> 'Voi tönäistä aiheita',
	'ACL_F_USER_LOCK'	=> 'Voi lukita omia aiheitaan',
	'ACL_F_DOWNLOAD'	=> 'Voi ladata tiedostoja',
	'ACL_F_REPORT'		=> 'Voi ilmiantaa viestejä',

	'ACL_F_POST'		=> 'Voi aloitaa uusia aiheita',
	'ACL_F_STICKY'		=> 'Voi kirjoittaa pysyviä aiheita',
	'ACL_F_ANNOUNCE'	=> 'Voi kirjoittaa tiedotteita',
	'ACL_F_ANNOUNCE_GLOBAL'	=> 'Voi kirjoittaa yleistiedotteita',
	'ACL_F_REPLY'		=> 'Voi vastata aiheisiin',
	'ACL_F_EDIT'		=> 'Voi muokata omia viestejään',
	'ACL_F_DELETE'		=> 'Voi poistaa omia viestejään lopullisesti',
	'ACL_F_SOFTDELETE'	=> 'Voi poistaa omia viestejään näkyvistä<br /><em>Valvojat, joilla on oikeus hyväksyä viestejä, voivat palauttaa näkyvistä poistettuja viestejä.</em>',
	'ACL_F_IGNOREFLOOD' => 'Voi ohittaa viestien lähetyksen aikarajan',
	'ACL_F_POSTCOUNT'	=> 'Kasvattaa viestilaskurin lukemaa<br /><em>Huomaa, että tämä asetus vaikuttaa vain uusiin viesteihin.</em>',
	'ACL_F_NOAPPROVE'	=> 'Voi kirjoittaa viestejä ilman hyväksymistä',

	'ACL_F_ATTACH'		=> 'Voi lisätä liitetiedostoja',
	'ACL_F_ICONS'		=> 'Voi käyttää aiheiden ja viestien kuvakkeita',
	'ACL_F_BBCODE'		=> 'Voi käyttää BBCode-tageja',
	'ACL_F_FLASH'		=> 'Voi käyttää BBCode-tagia [flash]',
	'ACL_F_IMG'			=> 'Voi käyttää BBCode-tagia [img]',
	'ACL_F_SIGS'		=> 'Voi lisätä allekirjoituksen',
	'ACL_F_SMILIES'		=> 'Voi käyttää hymiöitä',

	'ACL_F_POLL'		=> 'Voi luoda äänestyksiä',
	'ACL_F_VOTE'		=> 'Voi osallistua äänestyksiin',
	'ACL_F_VOTECHG'		=> 'Voi muuttaa annettua ääntä',
));

// Moderator Permissions
$lang = array_merge($lang, array(
	'ACL_M_EDIT'		=> 'Voi muokata viestejä',
	'ACL_M_DELETE'		=> 'Voi poistaa viestejä lopullisesti',
	'ACL_M_SOFTDELETE'	=> 'Voi poistaa viestejä näkyvistä<br /><em>Valvojat, joilla on oikeus hyväksyä viestejä, voivat palauttaa näkyvistä poistettuja viestejä.</em>',
	'ACL_M_APPROVE'		=> 'Voi hyväksyä ja palauttaa viestejä',
	'ACL_M_REPORT'		=> 'Voi sulkea ja poistaa viesteistä tehtyjä ilmoituksia',
	'ACL_M_CHGPOSTER'	=> 'Voi vaihtaa viestin kirjoittajan',

	'ACL_M_MOVE'	=> 'Voi siirtää aiheita',
	'ACL_M_LOCK'	=> 'Voi lukita aiheita',
	'ACL_M_SPLIT'	=> 'Voi jakaa aiheita',
	'ACL_M_MERGE'	=> 'Voi yhdistää aiheita',

	'ACL_M_INFO'	=> 'Voi katsoa viestien tietoja',
	'ACL_M_WARN'	=> 'Voi antaa varoituksia<br /><em>Tämä asetus koskee koko keskustelupalstaa. Se ei ole aluekohtainen.</em>', // This moderator setting is only global (and not local)
	'ACL_M_PM_REPORT' => 'Voi sulkea ja poistaa yksityisviesteistä tehtyjä ilmoituksia<br /><em>Tämä asetus koskee koko keskustelupalstaa. Se ei ole aluekohtainen.</em>', // This moderator setting is only global (and not local)
	'ACL_M_BAN'		=> 'Voi hallita porttikieltoja<br /><em>Tämä asetus koskee koko keskustelupalstaa. Se ei ole aluekohtainen.</em>', // This moderator setting is only global (and not local)
));

// Admin Permissions
$lang = array_merge($lang, array(
	'ACL_A_BOARD'		=> 'Voi muuttaa keskustelupalstan asetuksia ja tarkastaa päivityksiä',
	'ACL_A_SERVER'		=> 'Voi muuttaa palvelimen/kommunikoinnin asetuksia',
	'ACL_A_JABBER'		=> 'Voi muuttaa Jabber-asetuksia',
	'ACL_A_PHPINFO'		=> 'Voi katsoa PHP-asetuksia',

	'ACL_A_FORUM'		=> 'Voi hallita alueita',
	'ACL_A_FORUMADD'	=> 'Voi luoda alueita',
	'ACL_A_FORUMDEL'	=> 'Voi poistaa alueita',
	'ACL_A_PRUNE'		=> 'Voi siivota alueita',

	'ACL_A_ICONS'		=> 'Voi muokata aiheiden/viestien kuvakkeita ja hymiöitä',
	'ACL_A_WORDS'		=> 'Voi muokata sanojen sensurointia',
	'ACL_A_BBCODE'		=> 'Voi määritellä BBCode-tageja',
	'ACL_A_ATTACH'		=> 'Voi muuttaa liitetiedostojen asetuksia',

	'ACL_A_USER'		=> 'Voi hallita käyttäjiä<br /><em>Tämä sallii myös käyttäjien selainten näkemisen paikallaolijoiden luettelossa.</em>',
	'ACL_A_USERDEL'		=> 'Voi poistaa/siivota käyttäjiä',
	'ACL_A_GROUP'		=> 'Voi hallita ryhmiä',
	'ACL_A_GROUPADD'	=> 'Voi luoda ryhmiä',
	'ACL_A_GROUPDEL'	=> 'Voi poistaa ryhmiä',
	'ACL_A_RANKS'		=> 'Voi hallita arvonimiä',
	'ACL_A_PROFILE'		=> 'Voi hallita mukautettuja profiilikenttiä',
	'ACL_A_NAMES'		=> 'Voi hallita nimien kieltoja',
	'ACL_A_BAN'			=> 'Voi hallita porttikieltoja',

	'ACL_A_VIEWAUTH'	=> 'Voi katsoa oikeuksien maskeja',
	'ACL_A_AUTHGROUPS'	=> 'Voi muuttaa yksittäisten ryhmien oikeuksia',
	'ACL_A_AUTHUSERS'	=> 'Voi muuttaa yksittäisten käyttäjien oikeuksia',
	'ACL_A_FAUTH'		=> 'Voi muuttaa alueen oikeuksien luokkaa',
	'ACL_A_MAUTH'		=> 'Voi muuttaa valvojan oikeuksien luokkaa',
	'ACL_A_AAUTH'		=> 'Voi muuttaa ylläpitäjän oikeuksien luokkaa',
	'ACL_A_UAUTH'		=> 'Voi muuttaa käyttäjän oikeuksien luokkaa',
	'ACL_A_ROLES'		=> 'Voi hallita rooleja',
	'ACL_A_SWITCHPERM'	=> 'Voi käyttää muiden käyttäjien oikeuksia',

	'ACL_A_STYLES'		=> 'Voi hallita tyylejä',
	'ACL_A_EXTENSIONS'	=> 'Voi hallita laajennuksia',
	'ACL_A_VIEWLOGS'	=> 'Voi katsoa lokitietoja',
	'ACL_A_CLEARLOGS'	=> 'Voi poistaa lokitietoja',
	'ACL_A_MODULES'		=> 'Voi hallita moduuleja',
	'ACL_A_LANGUAGE'	=> 'Voi hallita kielipaketteja',
	'ACL_A_EMAIL'		=> 'Voi lähettää joukkopostia',
	'ACL_A_BOTS'		=> 'Voi hallita botteja',
	'ACL_A_REASONS'		=> 'Voi hallita ilmoitusten/hylkäysten syitä',
	'ACL_A_BACKUP'		=> 'Voi luoda varmuuskopion ja palauttaa tietokannan',
	'ACL_A_SEARCH'		=> 'Voi hallita hakujärjestelmiä ja niiden asetuksia',
));
