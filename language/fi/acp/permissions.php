<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ACP_PERMISSIONS_EXPLAIN'	=> '
		<p>Oikeudet ovat hyvin yksityiskohtaisia ja ne on ryhmitelty neljään pääluokkaan, joita ovat:</p>

		<h2>Yleiset oikeudet</h2>
		<p>Näiden avulla hallitaan käyttöä yleisellä tasolla ja ne koskevat koko keskustelupalstaa. Oikeudet on jaoteltu erikseen käyttäjille, ryhmille, ylläpitäjille ja yleisille valvojille.</p>

		<h2>Alueita koskevat oikeudet</h2>
		<p>Näiden avulla hallitaan yksittäisten alueiden käyttöä. Ne on jaoteltu alueiden oikeuksiin, alueiden valvojiin, käyttäjien aluekohtaisiin oikeuksiin ja ryhmien aluekohtaisiin oikeuksiin.</p>

		<h2>Oikeuksien roolit</h2>
		<p>Näiden avulla voit luoda oikeuksien kokonaisuuksia, joita voi myöhemmin asettaa roolikohtaisesti. Oletusroolien pitäisi riittää sekä suurien että pienien keskustelupalstojen ylläpitoon, joskin voit luoda/muokata/poistaa rooleja tarpeidesi mukaan.</p>

		<h2>Oikeuksien maskit</h2>
		<p>Näiden avulla voit katsoa käyttäjien, valvojien (paikallisten ja yleisten), ylläpitäjien ja alueiden todellisia oikeuksia.</p>

		<br />

		<p>Lisätietoa phpBB3-keskustelupalstan oikeuksien asettamisesta ja hallitsemisesta saat <a href="https://www.phpbb.com/support/docs/en/3.1/ug/quickstart/permissions/">pika-aloitusoppaasta</a>.</p>
	',

	'ACL_NEVER'				=> 'Ei koskaan',
	'ACL_SET'				=> 'Oikeuksien asettaminen',
	'ACL_SET_EXPLAIN'		=> 'Oikeudet perustuvat yksinkertaiseen <strong>KYLLÄ</strong>/<strong>EI</strong>-järjestelmään. Käyttäjän tai ryhmän oikeuden asettaminen arvoon <strong>EI KOSKAAN</strong> syrjäyttää kaikki muut oikeudelle asetetut arvot. Jos et halua asettaa arvoa jollekin tämän käyttäjän tai ryhmän oikeudelle, valitse <strong>EI</strong>. Jos oikeudelle on asetettu arvoja muualla, muodostuu lopullinen arvo niiden perusteella. Muussa tapauksessa oletusarvo on <strong>EI KOSKAAN</strong>. Määrittämäsi oikeudet kopioidaan kaikille valintaruuduilla merkityille kohteille.',
	'ACL_SETTING'			=> 'Asetus',

	'ACL_TYPE_A_'			=> 'Ylläpitäjän oikeudet',
	'ACL_TYPE_F_'			=> 'Aluekohtaiset oikeudet',
	'ACL_TYPE_M_'			=> 'Valvojan oikeudet',
	'ACL_TYPE_U_'			=> 'Käyttäjän oikeudet',

	'ACL_TYPE_GLOBAL_A_'	=> 'Ylläpitäjän oikeudet',
	'ACL_TYPE_GLOBAL_U_'	=> 'Käyttäjän oikeudet',
	'ACL_TYPE_GLOBAL_M_'	=> 'Yleisen valvojan oikeudet',
	'ACL_TYPE_LOCAL_M_'		=> 'Alueen valvojan oikeudet',
	'ACL_TYPE_LOCAL_F_'		=> 'Aluekohtaiset oikeudet',

	'ACL_NO'				=> 'Ei',
	'ACL_VIEW'				=> 'Näytä oikeudet',
	'ACL_VIEW_EXPLAIN'		=> 'Tässä näet käyttäjän/ryhmän todelliset oikeudet. Punainen suorakulmio tarkoittaa, että käyttäjälle/ryhmälle ei ole myönnetty oikeutta, kun taas vihreä suorakulmio tarkoittaa, että oikeus on myönnetty.',
	'ACL_YES'				=> 'Kyllä',

	'ACP_ADMINISTRATORS_EXPLAIN'				=> 'Tässä voit asettaa ylläpitäjän oikeuksia käyttäjille tai ryhmille. Käyttäjillä, joille on myönnetty ylläpitäjän oikeuksia, on pääsy ylläpidon hallintapaneeliin.',
	'ACP_FORUM_MODERATORS_EXPLAIN'				=> 'Tässä voi asettaa käyttäjiä ja ryhmiä alueiden valvojiksi.',
	'ACP_FORUM_PERMISSIONS_EXPLAIN'				=> 'Tässä voit määrittää, millä käyttäjillä ja ryhmillä on pääsy kullekin alueelle.',
	'ACP_FORUM_PERMISSIONS_COPY_EXPLAIN'		=> 'Tässä voit kopioida oikeuksia alueelta toiselle.',
	'ACP_GLOBAL_MODERATORS_EXPLAIN'				=> 'Tässä voit asettaa yleisen valvojan oikeuksia käyttäjille ja ryhmille. Yleiset valvojat eroavat tavallisista valvojista siten, että heillä on pääsy kaikille keskustelupalstan alueille.',
	'ACP_GROUPS_FORUM_PERMISSIONS_EXPLAIN'		=> 'Tässä voit asettaa aluekohtaisia oikeuksia ryhmille.',
	'ACP_GROUPS_PERMISSIONS_EXPLAIN'			=> 'Tässä voit asettaa yleisiä oikeuksia ryhmille. Oikeudet on jaoteltu käyttäjän oikeuksiin, yleisen valvojan oikeuksiin ja ylläpitäjän oikeuksiin. Käyttäjän oikeuksiin sisältyvät mm. avatarien käyttö ja yksityisviestien lähetys. Yleisen valvojan oikeudet kattavat mm. viestien hyväksymisen sekä aiheiden ja porttikieltojen hallitsemisen. Ylläpitäjän oikeudet koskevat mm. oikeuksien muuuttamista, mukautettujen BBCode-tagien luomista ja alueiden hallintaa. Yksittäisten käyttäjien oikeuksia tulisi muuttaa vain harvoin. On suositeltavampaa laittaa käyttäjät ryhmiin ja muuttaa ryhmien oikeuksia.',
	'ACP_ADMIN_ROLES_EXPLAIN'					=> 'Tässä voit hallita ylläpitäjän oikeuksien rooleja. Roolit vaikuttavat todellisin oikeuksiin, joten jos muokkaat roolia, rooliin nimitettyjen kohteiden oikeudet muuttuvat myös.',
	'ACP_FORUM_ROLES_EXPLAIN'					=> 'Tässä voit hallita aluekohtaisten oikeuksien rooleja. Roolit vaikuttavat todellisin oikeuksiin, joten jos muokkaat roolia, rooliin nimitettyjen kohteiden oikeudet muuttuvat myös.',
	'ACP_MOD_ROLES_EXPLAIN'						=> 'Tässä voit hallita valvojan oikeuksien rooleja. Roolit vaikuttavat todellisin oikeuksiin, joten jos muokkaat roolia, rooliin nimitettyjen kohteiden oikeudet muuttuvat myös.',
	'ACP_USER_ROLES_EXPLAIN'					=> 'Tässä voit hallita käyttäjän oikeuksien rooleja. Roolit vaikuttavat todellisin oikeuksiin, joten jos muokkaat roolia, rooliin nimitettyjen kohteiden oikeudet muuttuvat myös.',
	'ACP_USERS_FORUM_PERMISSIONS_EXPLAIN'		=> 'Tässä voit asettaa aluekohtaisia oikeuksia käyttäjille.',
	'ACP_USERS_PERMISSIONS_EXPLAIN'				=> 'Tässä voit asettaa yleisiä oikeuksia käyttäjille. Oikeudet on jaoteltu käyttäjätason oikeuksiin, yleisen valvojan oikeuksiin ja ylläpitäjän oikeuksiin. Käyttäjätason oikeuksiin sisältyvät mm. avatarien käyttö ja yksityisviestien lähetys. Yleisen valvojan oikeudet kattavat mm. viestien hyväksymisen sekä aiheiden ja porttikieltojen hallitsemisen. Ylläpitäjän oikeudet koskevat mm. oikeuksien muuuttamista, mukautettujen BBCode-tagien luomista ja alueiden hallintaa. Yksittäisten käyttäjien oikeuksia tulisi muuttaa vain harvoin. On suositeltavampaa laittaa käyttäjät ryhmiin ja muuttaa ryhmien oikeuksia.',
	'ACP_VIEW_ADMIN_PERMISSIONS_EXPLAIN'		=> 'Tässä voit katsoa valittujen käyttäjien/ryhmien todellisia ylläpitäjän oikeuksia.',
	'ACP_VIEW_GLOBAL_MOD_PERMISSIONS_EXPLAIN'	=> 'Tässä voit katsoa valituille käyttäjille/ryhmille asetettuja yleisen valvojan oikeuksia.',
	'ACP_VIEW_FORUM_PERMISSIONS_EXPLAIN'		=> 'Tässä voit katsoa valituille käyttäjille/ryhmille ja alueille asetettuja aluekohtaisia oikeuksia.',
	'ACP_VIEW_FORUM_MOD_PERMISSIONS_EXPLAIN'	=> 'Tässä voit katsoa valituille käyttäjille/ryhmille ja alueille asetettuja alueen valvojan oikeuksia.',
	'ACP_VIEW_USER_PERMISSIONS_EXPLAIN'			=> 'Tässä voit katsoa valittujen käyttäjien/ryhmien todellisia käyttäjätason oikeuksia.',

	'ADD_GROUPS'				=> 'Lisää ryhmiä',
	'ADD_PERMISSIONS'			=> 'Lisää oikeuksia',
	'ADD_USERS'					=> 'Lisää käyttäjiä',
	'ADVANCED_PERMISSIONS'		=> 'Tarkemmat oikeudet',
	'ALL_GROUPS'				=> 'Valitse kaikki ryhmät',
	'ALL_NEVER'					=> 'Kaikkiin <strong>EI KOSKAAN</strong>',
	'ALL_NO'					=> 'Kaikkiin <strong>EI</strong>',
	'ALL_USERS'					=> 'Valitse kaikki käyttäjät',
	'ALL_YES'					=> 'Kaikkiin <strong>KYLLÄ</strong>',
	'APPLY_ALL_PERMISSIONS'		=> 'Aseta kaikki oikeudet',
	'APPLY_PERMISSIONS'			=> 'Aseta oikeudet',
	'APPLY_PERMISSIONS_EXPLAIN'	=> 'Määritetyt oikeudet ja rooli asetetaan vain tälle kohteelle ja kaikille valituille kohteille.',
	'AUTH_UPDATED'				=> 'Oikeudet on päivitetty.',

	'COPY_PERMISSIONS_CONFIRM'				=> 'Haluatko varmasti suorittaa tämän toimenpiteen? Huomaa, että kaikki valittujen kohteiden nykyiset oikeudet ylikirjoitetaan.',
	'COPY_PERMISSIONS_FORUM_FROM_EXPLAIN'	=> 'Alue, jonka oikeudet kopioidaan.',
	'COPY_PERMISSIONS_FORUM_TO_EXPLAIN'		=> 'Alueet, joille oikeudet kopioidaan.',
	'COPY_PERMISSIONS_FROM'					=> 'Lähdealue',
	'COPY_PERMISSIONS_TO'					=> 'Kohdealueet',

	'CREATE_ROLE'				=> 'Luo rooli',
	'CREATE_ROLE_FROM'			=> 'Kopioi asetukset roolista…',
	'CUSTOM'					=> 'Mukautettu…',

	'DEFAULT'					=> 'Oletusarvo',
	'DELETE_ROLE'				=> 'Poista rooli',
	'DELETE_ROLE_CONFIRM'		=> 'Haluatko varmasti poistaa tämän roolin? Tähän rooliin nimitetyt kohteet <strong>eivät</strong> menetä oikeuksiaan.',
	'DISPLAY_ROLE_ITEMS'		=> 'Näytä tähän rooliin nimitetyt kohteet',

	'EDIT_PERMISSIONS'			=> 'Muokkaa oikeuksia',
	'EDIT_ROLE'					=> 'Muokkaa roolia',

	'GROUPS_NOT_ASSIGNED'		=> 'Tähän rooliin ei ole nimitetty ryhmiä',

	'LOOK_UP_GROUP'				=> 'Etsi ryhmä',
	'LOOK_UP_USER'				=> 'Etsi käyttäjä',

	'MANAGE_GROUPS'		=> 'Hallitse ryhmiä',
	'MANAGE_USERS'		=> 'Hallitse käyttäjiä',

	'NO_AUTH_SETTING_FOUND'		=> 'Oikeusasetuksia ei ole määritetty.',
	'NO_ROLE_ASSIGNED'			=> 'Roolia ei ole valittu…',
	'NO_ROLE_ASSIGNED_EXPLAIN'	=> 'Tähän rooliin nimittäminen ei muuta oikealla olevia oikeuksia. Jos haluat poistaa kaikki oikeudet, käytä ”Kaikkiin <strong>EI</strong>” -linkkiä.',
	'NO_ROLE_AVAILABLE'			=> 'Käytettävissä olevia rooleja ei ole',
	'NO_ROLE_NAME_SPECIFIED'	=> 'Anna roolille nimi.',
	'NO_ROLE_SELECTED'			=> 'Roolia ei löytynyt.',
	'NO_USER_GROUP_SELECTED'	=> 'Et valinnut käyttäjiä tai ryhmiä.',

	'ONLY_FORUM_DEFINED'	=> 'Valitse alueiden lisäksi myös ainakin yksi käyttäjä tai ryhmä.',

	'PERMISSION_APPLIED_TO_ALL'		=> 'Oikeudet ja rooli asetetaan myös kaikille valituille kohteille',
	'PLUS_SUBFORUMS'				=> '+ sisäalueet',

	'REMOVE_PERMISSIONS'			=> 'Poista oikeudet',
	'REMOVE_ROLE'					=> 'Poista rooli',
	'RESULTING_PERMISSION'			=> 'Lopullinen oikeus',
	'ROLE'							=> 'Rooli',
	'ROLE_ADD_SUCCESS'				=> 'Rooli on lisätty.',
	'ROLE_ASSIGNED_TO'				=> 'Rooliin ”%s” nimitetyt käyttäjät/ryhmät',
	'ROLE_DELETED'					=> 'Rooli on poistettu.',
	'ROLE_DESCRIPTION'				=> 'Roolin kuvaus',

	'ROLE_ADMIN_FORUM'			=> 'Alueen ylläpitäjä',
	'ROLE_ADMIN_FULL'			=> 'Täysivaltainen ylläpitäjä',
	'ROLE_ADMIN_STANDARD'		=> 'Tavallinen ylläpitäjä',
	'ROLE_ADMIN_USERGROUP'		=> 'Käyttäjien ja ryhmien ylläpitäjä',
	'ROLE_FORUM_BOT'			=> 'Botin oikeudet',
	'ROLE_FORUM_FULL'			=> 'Kaikki oikeudet',
	'ROLE_FORUM_LIMITED'		=> 'Rajoitetut oikeudet',
	'ROLE_FORUM_LIMITED_POLLS'	=> 'Rajoitetut oikeudet + äänestykset',
	'ROLE_FORUM_NOACCESS'		=> 'Ei oikeuksia',
	'ROLE_FORUM_ONQUEUE'		=> 'Valvontajonossa',
	'ROLE_FORUM_POLLS'			=> 'Tavalliset oikeudet + äänestykset',
	'ROLE_FORUM_READONLY'		=> 'Vain lukuoikeudet',
	'ROLE_FORUM_STANDARD'		=> 'Tavalliset oikeudet',
	'ROLE_FORUM_NEW_MEMBER'		=> 'Uuden käyttäjän oikeudet',
	'ROLE_MOD_FULL'				=> 'Täysivaltainen valvoja',
	'ROLE_MOD_QUEUE'			=> 'Jonon valvoja',
	'ROLE_MOD_SIMPLE'			=> 'Rajoitettu valvoja',
	'ROLE_MOD_STANDARD'			=> 'Tavallinen valvoja',
	'ROLE_USER_FULL'			=> 'Kaikki toiminnot',
	'ROLE_USER_LIMITED'			=> 'Rajoitetut toiminnot',
	'ROLE_USER_NOAVATAR'		=> 'Ei avataria',
	'ROLE_USER_NOPM'			=> 'Ei yksityisviestejä',
	'ROLE_USER_STANDARD'		=> 'Tavalliset toiminnot',
	'ROLE_USER_NEW_MEMBER'		=> 'Uuden käyttäjän toiminnot',

	'ROLE_DESCRIPTION_ADMIN_FORUM'			=> 'Pääsee alueiden hallintaan ja alueiden oikeuksien asetuksiin.',
	'ROLE_DESCRIPTION_ADMIN_FULL'			=> 'Voi käyttää kaikkia keskustelupalstan ylläpidon toimintoja.<br />Ei suositella.',
	'ROLE_DESCRIPTION_ADMIN_STANDARD'		=> 'Voi käyttää suurinta osaa ylläpidon toiminnoista, mutta ei palvelimeen tai järjestelmään liittyviä työkaluja.',
	'ROLE_DESCRIPTION_ADMIN_USERGROUP'		=> 'Voi hallita ryhmiä ja käyttäjiä: Voi muuttaa oikeuksia ja asetuksia sekä hallita porttikieltoja ja arvonimiä.',
	'ROLE_DESCRIPTION_FORUM_BOT'			=> 'Tätä roolia suositellaan boteille ja hakukoneille.',
	'ROLE_DESCRIPTION_FORUM_FULL'			=> 'Voi käyttää kaikkia alueen toimintoja mukaan lukien tiedotteiden ja pysyvien viestien kirjoittamista. Voi myös ohittaa viestien lähetyksen aikarajan.<br />Ei suositella tavallisille käyttäjille.',
	'ROLE_DESCRIPTION_FORUM_LIMITED'		=> 'Voi käyttää joitakin alueen toimintoja, mutta ei voi lisätä liitetiedostoja tai käyttää viestien kuvakkeita.',
	'ROLE_DESCRIPTION_FORUM_LIMITED_POLLS'	=> 'Sama kuin Rajoitetut oikeudet, mutta voi luoda äänestyksiä.',
	'ROLE_DESCRIPTION_FORUM_NOACCESS'		=> 'Ei voi nähdä aluetta tai päästä alueelle.',
	'ROLE_DESCRIPTION_FORUM_ONQUEUE'		=> 'Voi käyttää suurinta osaa alueen toiminnoista mukaan lukien liitetiedostojen lisäystä, mutta viestit ja aiheet täytyy hyväksyttää valvojilla.',
	'ROLE_DESCRIPTION_FORUM_POLLS'			=> 'Sama kuin Tavalliset oikeudet, mutta voi luoda äänestyksiä.',
	'ROLE_DESCRIPTION_FORUM_READONLY'		=> 'Voi lukea aluetta, mutta ei voi aloittaa uusia aiheita tai vastata viesteihin.',
	'ROLE_DESCRIPTION_FORUM_STANDARD'		=> 'Voi käyttää suurinta osaa alueen toiminnoista mukaan lukien liitetiedostojen lisäystä ja omien aiheiden poistoa, mutta ei voi lukita omia aiheita tai luoda äänestyksiä.',
	'ROLE_DESCRIPTION_FORUM_NEW_MEMBER'		=> 'Uusien käyttäjien ryhmän jäsenille tarkoitettu rooli. Sisältää <strong>EI KOSKAAN</strong> -oikeuksia, joilla estetään tiettyjen toimintojen käyttö.',
	'ROLE_DESCRIPTION_MOD_FULL'				=> 'Voi käyttää kaikkia valvontatyökaluja, mukaan lukien porttikieltoja.',
	'ROLE_DESCRIPTION_MOD_QUEUE'			=> 'Voi ainoastaan hyväksyä/hylätä ja muokata valvontajonossa olevia viestejä.',
	'ROLE_DESCRIPTION_MOD_SIMPLE'			=> 'Voi käyttää vain aiheiden perustoimintoja. Ei voi antaa varoituksia tai käyttää valvontajonoa.',
	'ROLE_DESCRIPTION_MOD_STANDARD'			=> 'Voi käyttää suurinta osaa valvojan työkaluista, mutta ei voi antaa porttikieltoja tai vaihtaa viestien kirjoittajia.',
	'ROLE_DESCRIPTION_USER_FULL'			=> 'Voi käyttää kaikkia keskustelupalstan käyttäjän toimintoja mukaan lukien käyttäjätunnuksen vaihtoa ja viestien lähetyksen aikarajan ohitusta.<br />Ei suositella.',
	'ROLE_DESCRIPTION_USER_LIMITED'			=> 'Voi käyttää joitakin käyttäjän toimintoja. Liitetiedostojen, sähköpostin tai pikaviestien käyttö ei ole sallittu.',
	'ROLE_DESCRIPTION_USER_NOAVATAR'		=> 'Voi käyttää pientä osaa toiminnoista, mutta ei voi valita avataria.',
	'ROLE_DESCRIPTION_USER_NOPM'			=> 'Voi käyttää pientä osaa toiminnoista, mutta ei voi käyttää yksityisviestejä.',
	'ROLE_DESCRIPTION_USER_STANDARD'		=> 'Voi käyttää lähes kaikkia käyttäjän toimintoja, mutta ei voi esimerkiksi vaihtaa käyttäjätunnusta tai ohittaa viestien lähetyksen aikarajaa.',
	'ROLE_DESCRIPTION_USER_NEW_MEMBER'		=> 'Uusien käyttäjien ryhmän jäsenille tarkoitettu rooli. Sisältää <strong>EI KOSKAAN</strong> -oikeuksia, joilla estetään tiettyjen toimintojen käyttö.',

	'ROLE_DESCRIPTION_EXPLAIN'		=> 'Voit syöttää lyhyen kuvauksen roolin käyttötarkoituksesta. Syöttämäsi teksti näytetään myös oikeuksia asetettaessa.',
	'ROLE_DESCRIPTION_LONG'			=> 'Roolin kuvaus voi olla enintään 4000 merkkiä pitkä.',
	'ROLE_DETAILS'					=> 'Roolin lisätiedot',
	'ROLE_EDIT_SUCCESS'				=> 'Roolia on muokattu.',
	'ROLE_NAME'						=> 'Roolin nimi',
	'ROLE_NAME_ALREADY_EXIST'		=> 'Rooli nimeltä <strong>%s</strong> on jo olemassa valitulla oikeuksien tyypillä.',
	'ROLE_NOT_ASSIGNED'				=> 'Roolia ei ole vielä asetettu.',

	'SELECTED_FORUM_NOT_EXIST'		=> 'Valittuja alueita ei ole olemassa.',
	'SELECTED_GROUP_NOT_EXIST'		=> 'Valittuja ryhmiä ei ole olemassa.',
	'SELECTED_USER_NOT_EXIST'		=> 'Valittuja käyttäjiä ei ole olemassa.',
	'SELECT_FORUM_SUBFORUM_EXPLAIN'	=> 'Kaikki valitsemasi alueen sisäalueet sisällytetään valintaan.',
	'SELECT_ROLE'					=> 'Valitse rooli…',
	'SELECT_TYPE'					=> 'Valitse tyyppi',
	'SET_PERMISSIONS'				=> 'Aseta oikeudet',
	'SET_ROLE_PERMISSIONS'			=> 'Aseta roolin oikeudet',
	'SET_USERS_PERMISSIONS'			=> 'Aseta käyttäjän oikeudet',
	'SET_USERS_FORUM_PERMISSIONS'	=> 'Aseta aluekohtaiset käyttäjän oikeudet',

	'TRACE_DEFAULT'					=> 'Jokaisen oikeuden oletusarvo on <strong>EI</strong>, jotta oikeuden voi ylikirjoittaa muilla asetuksilla.',
	'TRACE_FOR'						=> 'Jäljitys',
	'TRACE_GLOBAL_SETTING'			=> '%s (yleinen)',
	'TRACE_GROUP_NEVER_TOTAL_NEVER'	=> 'Ryhmän oikeuden arvo on <strong>EI KOSKAAN</strong>, joka vastaa voimassa olevaa arvoa, joten aikaisempi arvo säilytetään.',
	'TRACE_GROUP_NEVER_TOTAL_NEVER_LOCAL'	=> 'Ryhmän oikeuden arvo tällä alueella on <strong>EI KOSKAAN</strong>, joka vastaa voimassa olevaa arvoa, joten aikaisempi arvo säilytetään.',
	'TRACE_GROUP_NEVER_TOTAL_NO'	=> 'Ryhmän oikeuden arvo on <strong>EI KOSKAAN</strong>, josta tulee uusi voimassa oleva arvo, koska arvoa ei ollut vielä asetettu (arvo oli <strong>EI</strong>).',
	'TRACE_GROUP_NEVER_TOTAL_NO_LOCAL'	=> 'Ryhmän oikeuden arvo tällä alueella on <strong>EI KOSKAAN</strong>, josta tulee uusi voimassa oleva arvo, koska arvoa ei ollut vielä asetettu (arvo oli <strong>EI</strong>).',
	'TRACE_GROUP_NEVER_TOTAL_YES'	=> 'Ryhmän oikeuden arvo on <strong>EI KOSKAAN</strong>, joka syrjäyttää tällä käyttäjällä voimassa olleen <strong>KYLLÄ</strong>-arvon.',
	'TRACE_GROUP_NEVER_TOTAL_YES_LOCAL'	=> 'Ryhmän oikeuden arvo tällä alueella on <strong>EI KOSKAAN</strong>, joka syrjäyttää tällä käyttäjällä voimassa olleen <strong>KYLLÄ</strong>-arvon.',
	'TRACE_GROUP_NO'				=> 'Ryhmän oikeuden arvo on <strong>EI</strong>, joten voimassa oleva arvo säilytetään.',
	'TRACE_GROUP_NO_LOCAL'			=> 'Ryhmän oikeuden arvo tällä alueella on <strong>EI</strong>, joten voimassa oleva arvo säilytetään.',
	'TRACE_GROUP_YES_TOTAL_NEVER'	=> 'Ryhmän oikeuden arvo on <strong>KYLLÄ</strong>, mutta voimassa olevaa <strong>EI KOSKAAN</strong> -arvoa ei voi syrjäyttää.',
	'TRACE_GROUP_YES_TOTAL_NEVER_LOCAL'	=> 'Ryhmän oikeuden arvo tällä alueella on <strong>KYLLÄ</strong>, mutta voimassa olevaa <strong>EI KOSKAAN</strong> -arvoa ei voi syrjäyttää.',
	'TRACE_GROUP_YES_TOTAL_NO'		=> 'Ryhmän oikeuden arvo on <strong>KYLLÄ</strong>, josta tulee uusi voimassa oleva arvo, koska arvoa ei ollut vielä asetettu (arvo oli <strong>EI</strong>).',
	'TRACE_GROUP_YES_TOTAL_NO_LOCAL'	=> 'Ryhmän oikeuden arvo tällä alueella on <strong>KYLLÄ</strong>, josta tulee uusi voimassa oleva arvo, koska arvoa ei ollut vielä asetettu (arvo oli <strong>EI</strong>).',
	'TRACE_GROUP_YES_TOTAL_YES'		=> 'Ryhmän oikeuden arvo on <strong>KYLLÄ</strong> ja voimassa oleva arvo on jo <strong>KYLLÄ</strong>, joten voimassa oleva arvo säilytetään.',
	'TRACE_GROUP_YES_TOTAL_YES_LOCAL'	=> 'Ryhmän oikeuden arvo tällä alueella on <strong>KYLLÄ</strong> ja voimassa oleva arvo on jo <strong>KYLLÄ</strong>, joten voimassa oleva arvo säilytetään.',
	'TRACE_PERMISSION'				=> 'Jäljitä oikeus – %s',
	'TRACE_RESULT'					=> 'Jäljityksen tulos',
	'TRACE_SETTING'					=> 'Jäljitä oikeus',

	'TRACE_USER_GLOBAL_YES_TOTAL_YES'		=> 'Alueesta riippumattoman käyttäjän oikeuden arvo on <strong>KYLLÄ</strong>, mutta voimassa oleva arvo on jo <strong>KYLLÄ</strong>, joten voimassa oleva arvo säilytetään. %sJäljitä yleinen oikeus%s',
	'TRACE_USER_GLOBAL_YES_TOTAL_NEVER'		=> 'Alueesta riippumattoman käyttäjän oikeuden arvo on <strong>KYLLÄ</strong>, joka syrjäyttää tämänhetkisen paikallisen <strong>EI KOSKAAN</strong> -arvon. %sJäljitä yleinen oikeus%s',
	'TRACE_USER_GLOBAL_NEVER_TOTAL_KEPT'	=> 'Alueesta riippumattoman käyttäjän oikeuden arvo on <strong>EI KOSKAAN</strong>, joka ei vaikuta paikalliseen oikeuteen. %sJäljitä yleinen oikeus%s',

	'TRACE_USER_FOUNDER'					=> 'Käyttäjä on perustaja, joten ylläpitäjän oikeuksien arvo on aina <strong>KYLLÄ</strong>.',
	'TRACE_USER_KEPT'						=> 'Käyttäjän oikeuden arvo on <strong>EI</strong>, joten voimassa oleva arvo säilytetään.',
	'TRACE_USER_KEPT_LOCAL'					=> 'Käyttäjän oikeuden arvo tällä alueella on <strong>EI</strong>, joten voimassa oleva arvo säilytetään.',
	'TRACE_USER_NEVER_TOTAL_NEVER'			=> 'Käyttäjän oikeuden arvo on <strong>EI KOSKAAN</strong> ja voimassa oleva arvo on <strong>EI KOSKAAN</strong>, joten muutosta ei tule.',
	'TRACE_USER_NEVER_TOTAL_NEVER_LOCAL'	=> 'Käyttäjän oikeuden arvo tällä alueella on <strong>EI KOSKAAN</strong> ja voimassa oleva arvo on <strong>EI KOSKAAN</strong>, joten muutosta ei tule.',
	'TRACE_USER_NEVER_TOTAL_NO'				=> 'Käyttäjän oikeuden arvo on <strong>EI KOSKAAN</strong>, josta tulee uusi voimassa oleva arvo, koska aikaisempi arvo oli EI.',
	'TRACE_USER_NEVER_TOTAL_NO_LOCAL'		=> 'Käyttäjän oikeuden arvo tällä alueella on <strong>EI KOSKAAN</strong>, josta tulee uusi voimassa oleva arvo, koska aikaisempi arvo oli EI.',
	'TRACE_USER_NEVER_TOTAL_YES'			=> 'Käyttäjän oikeuden arvo on <strong>EI KOSKAAN</strong>, joka syrjäyttää aikaisemman <strong>KYLLÄ</strong>-arvon.',
	'TRACE_USER_NEVER_TOTAL_YES_LOCAL'		=> 'Käyttäjän oikeuden arvo tällä alueella on <strong>EI KOSKAAN</strong>, joka syrjäyttää aikaisemman <strong>KYLLÄ</strong>-arvon.',
	'TRACE_USER_NO_TOTAL_NO'				=> 'Käyttäjän oikeuden arvo on <strong>EI</strong> ja voimassa oleva arvo oli EI, joten oletusarvo <strong>EI KOSKAAN</strong> tulee voimaan.',
	'TRACE_USER_NO_TOTAL_NO_LOCAL'			=> 'Käyttäjän oikeuden arvo tällä alueella on <strong>EI</strong> ja voimassa oleva arvo oli EI, joten oletusarvo <strong>EI KOSKAAN</strong> tulee voimaan.',
	'TRACE_USER_YES_TOTAL_NEVER'			=> 'Käyttäjän oikeuden arvo on <strong>KYLLÄ</strong>, mutta voimassa olevaa <strong>EI KOSKAAN</strong> -arvoa ei voi syrjäyttää.',
	'TRACE_USER_YES_TOTAL_NEVER_LOCAL'		=> 'Käyttäjän oikeuden arvo tällä alueella on <strong>KYLLÄ</strong>, mutta voimassa olevaa <strong>EI KOSKAAN</strong> -arvoa ei voi syrjäyttää.',
	'TRACE_USER_YES_TOTAL_NO'				=> 'Käyttäjän oikeuden arvo on <strong>KYLLÄ</strong>, josta tulee uusi voimassa oleva arvo, koska aikaisempi arvo oli <strong>EI</strong>.',
	'TRACE_USER_YES_TOTAL_NO_LOCAL'			=> 'Käyttäjän oikeuden arvo tällä alueella on <strong>KYLLÄ</strong>, josta tulee uusi voimassa oleva arvo, koska aikaisempi arvo oli <strong>EI</strong>.',
	'TRACE_USER_YES_TOTAL_YES'				=> 'Käyttäjän oikeuden arvo on <strong>KYLLÄ</strong> ja voimassa oleva arvo on <strong>KYLLÄ</strong>, joten muutosta ei tule.',
	'TRACE_USER_YES_TOTAL_YES_LOCAL'		=> 'Käyttäjän oikeuden arvo tällä alueella on <strong>KYLLÄ</strong> ja voimassa oleva arvo on <strong>KYLLÄ</strong>, joten muutosta ei tule.',
	'TRACE_WHO'								=> 'Kuka',
	'TRACE_TOTAL'							=> 'Voimassa oleva',

	'USERS_NOT_ASSIGNED'			=> 'Tähän rooliin ei ole nimitetty käyttäjiä',
	'USER_IS_MEMBER_OF_DEFAULT'		=> 'on jäsenenä seuraavissa ennalta määritellyissä ryhmissä',
	'USER_IS_MEMBER_OF_CUSTOM'		=> 'on jäsenenä seuraavissa käyttäjän määrittelemissä ryhmissä',

	'VIEW_ASSIGNED_ITEMS'	=> 'Näytä nimitetyt kohteet',
	'VIEW_LOCAL_PERMS'		=> 'Paikalliset oikeudet',
	'VIEW_GLOBAL_PERMS'		=> 'Yleiset oikeudet',
	'VIEW_PERMISSIONS'		=> 'Näytä oikeudet',

	'WRONG_PERMISSION_TYPE'				=> 'Valittu oikeuksien tyyppi on väärä.',
	'WRONG_PERMISSION_SETTING_FORMAT'	=> 'Oikeusasetukset ovat väärässä muodossa, minkä vuoksi phpBB ei pysty käsittelemään niitä oikein.',
));
