<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ACP_FILES'						=> 'Ylläpidon kielitiedostot',
	'ACP_LANGUAGE_PACKS_EXPLAIN'	=> 'Tässä voit asentaa ja poistaa kielipaketteja. Oletuskieli on merkitty tähdellä (*).',

	'DELETE_LANGUAGE_CONFIRM'		=> 'Haluatko varmasti poistaa kielen ”%s”?',

	'INSTALLED_LANGUAGE_PACKS'		=> 'Asennetut kielipaketit',

	'LANGUAGE_DETAILS_UPDATED'			=> 'Kielen tiedot on päivitetty',
	'LANGUAGE_PACK_ALREADY_INSTALLED'	=> 'Tämä kielipaketti on jo asennettu.',
	'LANGUAGE_PACK_DELETED'				=> 'Kielipaketti <strong>%s</strong> on poistettu. Kaikki tätä kieltä käyttäneet käyttäjät on asetettu käyttämään keskustelupalstan oletuskieltä.',
	'LANGUAGE_PACK_DETAILS'				=> 'Kielipaketin tiedot',
	'LANGUAGE_PACK_INSTALLED'			=> 'Kielipaketti <strong>%s</strong> on asennettu.',
	'LANGUAGE_PACK_CPF_UPDATE'			=> 'Mukautettujen profiilikenttien merkkijonot kopioitiin oletuskielestä. Muokkaa niitä tarvittaessa.',
	'LANGUAGE_PACK_ISO'					=> 'ISO-koodi',
	'LANGUAGE_PACK_LOCALNAME'			=> 'Paikallinen nimi',
	'LANGUAGE_PACK_NAME'				=> 'Nimi',
	'LANGUAGE_PACK_NOT_EXIST'			=> 'Valitsemaasi kielipakettia ei ole olemassa.',
	'LANGUAGE_PACK_USED_BY'				=> 'Käyttäjiä (botit mukaan lukien)',
	'LANGUAGE_VARIABLE'					=> 'Kielen muuttuja',
	'LANG_AUTHOR'						=> 'Kielipaketin tekijä',
	'LANG_ENGLISH_NAME'					=> 'Englanninkielinen nimi',
	'LANG_ISO_CODE'						=> 'ISO-koodi',
	'LANG_LOCAL_NAME'					=> 'Paikallinen nimi',

	'MISSING_LANG_FILES'		=> 'Puuttuvat kielitiedostot',
	'MISSING_LANG_VARIABLES'	=> 'Puuttuvat kielen muuttujat',

	'NO_FILE_SELECTED'				=> 'Et ole valinnut kielitiedostoa.',
	'NO_LANG_ID'					=> 'Et ole valinnut kielipakettia.',
	'NO_REMOVE_DEFAULT_LANG'		=> 'Et voi poistaa oletuskieltä.<br />Jos haluat poistaa tämän kielipaketin, vaihda keskustelupalstasi oletuskieli ensin.',
	'NO_UNINSTALLED_LANGUAGE_PACKS'	=> 'Asentamattomia kielipaketteja ei ole',

	'THOSE_MISSING_LANG_FILES'			=> 'Seuraavat tiedostot puuttuvat kielen ”%s” hakemistosta',
	'THOSE_MISSING_LANG_VARIABLES'		=> 'Seuraavat muuttujat puuttuvat kielipaketista ”%s”',

	'UNINSTALLED_LANGUAGE_PACKS'	=> 'Asentamattomat kielipaketit',

	'BROWSE_LANGUAGE_PACKS_DATABASE'	=> 'Selaa kielipakettien tietokantaa',
));
