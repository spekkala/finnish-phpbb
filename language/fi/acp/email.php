<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

// Email settings
$lang = array_merge($lang, array(
	'ACP_MASS_EMAIL_EXPLAIN'	=> 'Tässä voit lähettää sähköpostia joko kaikille keskustelupalstan käyttäjille tai kaikille yksittäisen ryhmän jäsenille, <strong>jotka ovat sallineet joukkopostin vastaanoton</strong>. Tämä toteutetaan lähettämällä antamaasi ylläpidon osoitteeseen sähköpostiviesti, jonka piilokopiokenttä sisältää vastaanottajien osoitteet. Oletusarvoisesti yksi viesti lähetetään korkeintaan 20 vastaanottajalle. Jos vastaanottajia on useampia, viestejä lähetetään enemmän. Jos lähetät sähköpostia suurelle joukolle käyttäjiä, odota kärsivällisesti viestin lähetyksen jälkeen äläkä sulje sivua kesken kaiken. On tavallista, että joukkopostitus kestää kauan. Saat tiedon, kun kaikki viestit on lähetetty.',
	'ALL_USERS'	=> 'Kaikki käyttäjät',

	'COMPOSE'	=> 'Kirjoita viesti',

	'EMAIL_SEND_ERROR'	=> 'Viestiä lähetettäessä tapahtui yksi tai useampi virhe. Näet tarkemmat virheilmoitukset %svirhelokista%s.',
	'EMAIL_SENT'	=> 'Viesti on lähetetty.',
	'EMAIL_SENT_QUEUE'	=> 'Viesti odottaa lähetystä.',

	'LOG_SESSION'	=> 'Kirjaa sähköpostin tapahtumat kriittiseen lokiin',

	'SEND_IMMEDIATELY'	=> 'Lähetä heti',
	'SEND_TO_GROUP'	=> 'Lähetä ryhmälle',
	'SEND_TO_USERS'	=> 'Lähetä käyttäjille',
	'SEND_TO_USERS_EXPLAIN'	=> 'Jos syötät käyttäjätunnuksia tähän, yllä valittua ryhmää ei oteta huomioon. Syötä kukin käyttäjätunnus omalle rivilleen.',

	'MAIL_BANNED'	=> 'Lähetä viesti porttikiellossa oleville käyttäjille',
	'MAIL_BANNED_EXPLAIN'	=> 'Jos lähetät joukkopostia ryhmälle, voit valita, saavatko myös porttikiellossa olevat käyttäjät viestin.',
	'MAIL_HIGH_PRIORITY'	=> 'Korkea',
	'MAIL_LOW_PRIORITY'	=> 'Pieni',
	'MAIL_NORMAL_PRIORITY'	=> 'Normaali',
	'MAIL_PRIORITY'	=> 'Viestin prioriteetti',
	'MASS_MESSAGE'	=> 'Viesti',
	'MASS_MESSAGE_EXPLAIN'	=> 'Voit käyttää vain tavallista tekstiä. Kaikki muotoilut poistetaan ennen lähetystä.',

	'NO_EMAIL_MESSAGE'	=> 'Kirjoita viesti.',
	'NO_EMAIL_SUBJECT'	=> 'Anna viestille otsikko.',
));
