<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

// Bot settings
$lang = array_merge($lang, array(
	'BOTS'	=> 'Bottien hallinta',
	'BOTS_EXPLAIN'	=> 'Botit ovat automaattisia ohjelmia, joita käytetään tavallisesti hakukoneiden tietokantojen päivittämiseen. Koska botit harvoin käyttävät istuntoja oikein, ne saattavat vääristää vierailijoiden määriä, kuormittaa palvelinta ja joskus myös epäonnistua sivuston indeksoimisessa. Tässä voit määrittää boteille erityisen käyttäjätyypin näiden ongelmien ratkaisemiseksi.',
	'BOT_ACTIVATE'	=> 'Aktivoi',
	'BOT_ACTIVE'	=> 'Botti on aktiivinen',
	'BOT_ADD'	=> 'Lisää botti',
	'BOT_ADDED'	=> 'Uusi botti on lisätty.',
	'BOT_AGENT'	=> 'Botin agenttimerkkijono',
	'BOT_AGENT_EXPLAIN'	=> 'Botin agenttia vastaava merkkijono. Osittaiset vastaavuudet ovat sallittuja.',
	'BOT_DEACTIVATE'	=> 'Poista käytöstä',
	'BOT_DELETED'	=> 'Botti on poistettu.',
	'BOT_EDIT'	=> 'Muokkaa bottia',
	'BOT_EDIT_EXPLAIN'	=> 'Tässä voit lisätä tai muokata olemassa olevan botin tietoja. Voit määrittää agenttimerkkijonon ja/tai yhden tai useamman IP-osoitteen (tai osoitealueen), joiden avulla botti tunnistetaan. Ole huolellinen, kun määrität agenttimerkkijonoa tai osoitteita. Voit myös valita tyylin ja kielen, jotka botille näytetään. Tällä tavalla voit ehkä pienentää siirrettävän tiedon määrä, jos valitset boteille yksinkertaisen tyylin. Muista myös asettaa sopivat oikeudet erityiselle bottien käyttäjäryhmälle.',
	'BOT_LANG'	=> 'Botin kieli',
	'BOT_LANG_EXPLAIN'	=> 'Botillle näytettävä kieli.',
	'BOT_LAST_VISIT'	=> 'Viimeisin vierailu',
	'BOT_IP'	=> 'Botin IP-osoite',
	'BOT_IP_EXPLAIN'	=> 'Osittaiset osoitteet ovat sallittuja. Erota osoitteet toisistaan pilkuilla.',
	'BOT_NAME'	=> 'Botin nimi',
	'BOT_NAME_EXPLAIN'	=> 'Nimeä käytetään vain omaksi tiedoksesi.',
	'BOT_NAME_TAKEN'	=> 'Nimi on jo käytössä keskustelupalstallasi, joten sitä ei voi antaa botille.',
	'BOT_NEVER'	=> 'Ei koskaan',
	'BOT_STYLE'	=> 'Botin tyyli',
	'BOT_STYLE_EXPLAIN'	=> 'Botille näytettävä keskustelupalstan tyyli.',
	'BOT_UPDATED'	=> 'Botin tiedot on päivitetty.',

	'ERR_BOT_AGENT_MATCHES_UA'	=> 'Antamasi botin agentti on samanlainen kuin jokin jo käytössä oleva. Ole hyvä ja muokkaa tämän botin agenttia.',
	'ERR_BOT_NO_IP'	=> 'Antamasi IP-osoitteet olivat virheellisiä tai isäntänimien selvitys epäonnistui.',
	'ERR_BOT_NO_MATCHES'	=> 'Anna ainakin yksi IP-osoite tai merkkijono, jonka avulla botti voidaan tunnistaa.',

	'NO_BOT'	=> 'Antamallasi tunnisteella ei löytynyt botteja.',
	'NO_BOT_GROUP'	=> 'Bottien käyttäjäryhmää ei löytynyt.',
));
