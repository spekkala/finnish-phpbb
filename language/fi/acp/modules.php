<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ACP_MODULE_MANAGEMENT_EXPLAIN'	=> 'Tässä voit hallita moduuleja. Huomaa, että ylläpidon hallintapaneelilla on kolmitasoinen valikkorakenne (Kategoria -&gt; Kategoria -&gt; Moduuli), kun taas muualla käytetään kaksitasoista valikkorakennetta (Kategoria -&gt; Moduuli). Nämä rakenteet täytyy säilyttää. Huomaa myös, että saatat lukita itsesi järjestelmän ulkopuolelle, jos poistat tai kytket pois käytöstä moduulit, jotka vastaavat moduulien hallinnasta.',
	'ADD_MODULE'					=> 'Lisää moduuli',
	'ADD_MODULE_CONFIRM'			=> 'Haluatko varmasti lisätä valitun moduulin valitussa moodissa?',
	'ADD_MODULE_TITLE'				=> 'Lisää moduuli',

	'CANNOT_REMOVE_MODULE'	=> 'Moduulia ei voi poistaa, koska sillä on alamoduuleja. Poista tai siirrä kaikki alamoduulit ennen kuin suoritat tämän toimenpiteen.',
	'CATEGORY'				=> 'Kategoria',
	'CHOOSE_MODE'			=> 'Valitse moduulin moodi',
	'CHOOSE_MODE_EXPLAIN'	=> 'Valitse moduulissa käytettävä moodi.',
	'CHOOSE_MODULE'			=> 'Valitse moduuli',
	'CHOOSE_MODULE_EXPLAIN'	=> 'Valitse tämän moduulin kutsuma tiedosto.',
	'CREATE_MODULE'			=> 'Luo uusi moduuli',

	'DEACTIVATED_MODULE'	=> 'Käytöstä poistettu moduuli',
	'DELETE_MODULE'			=> 'Poista moduuli',
	'DELETE_MODULE_CONFIRM'	=> 'Haluatko varmasti poistaa tämän moduulin?',

	'EDIT_MODULE'			=> 'Muokkaa moduulia',
	'EDIT_MODULE_EXPLAIN'	=> 'Tässä voit muokata moduulin asetuksia.',

	'HIDDEN_MODULE'			=> 'Piilotettu moduuli',

	'MODULE'					=> 'Moduuli',
	'MODULE_ADDED'				=> 'Moduuli on lisätty.',
	'MODULE_DELETED'			=> 'Moduuli on poistettu.',
	'MODULE_DISPLAYED'			=> 'Moduuli näkyvissä',
	'MODULE_DISPLAYED_EXPLAIN'	=> 'Jos et halua moduulin olevan näkyvissä mutta silti haluat käyttää sitä, valitse <em>Ei</em>-vaihtoehto.',
	'MODULE_EDITED'				=> 'Moduulia on muokattu.',
	'MODULE_ENABLED'			=> 'Moduuli käytössä',
	'MODULE_LANGNAME'			=> 'Moduulin kielen nimi',
	'MODULE_LANGNAME_EXPLAIN'	=> 'Syötä näytettävä moduulin nimi. Käytä kielivakiota, jos nimi haetaan kielitiedostosta.',
	'MODULE_TYPE'				=> 'Moduulin tyyppi',

	'NO_CATEGORY_TO_MODULE'	=> 'Kategoriaa ei voi muuttaa moduuliksi. Poista tai siirrä kaikki alamoduulit ennen kuin suoritat tämän toimenpiteen.',
	'NO_MODULE'				=> 'Moduulia ei löytynyt.',
	'NO_MODULE_ID'			=> 'Moduulin tunnistetta ei ole määritetty.',
	'NO_MODULE_LANGNAME'	=> 'Moduulin kielen nimeä ei ole määritetty.',
	'NO_PARENT'				=> 'Ei moduulia',

	'PARENT'				=> 'Ylemmän tason moduuli',
	'PARENT_NO_EXIST'		=> 'Ylemmän tason moduulia ei ole olemassa.',

	'SELECT_MODULE'			=> 'Valitse moduuli',
));
