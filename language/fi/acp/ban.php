<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

// Banning
$lang = array_merge($lang, array(
	'1_HOUR'	=> '1 tunti',
	'30_MINS'	=> '30 minuuttia',
	'6_HOURS'	=> '6 tuntia',

	'ACP_BAN_EXPLAIN'	=> 'Tässä voit hallita käyttäjätunnusten, IP-osoitteiden ja sähköpostiosoitteiden perusteella annettuja porttikieltoja. Porttikiellon saaneilla käyttäjillä ei ole pääsyä millekään keskustelupalstan osa-alueelle. Voit ilmoittaa porttikiellolle lyhyen (enintään 3000 merkin pituisen) syyn, joka kirjataan ylläpidon lokitietoihin. Voit myös asettaa porttikiellolle keston. Jos haluat porttikiellon kestävän tiettyyn päivämäärään asti, valitse Päivämäärään asti -vaihtoehto porttikiellon kestoksi ja syötä päivämäärä muodossa <kbd>VVVV-KK-PP</kbd>.',

	'BAN_EXCLUDE'	=> 'Vapauta porttikielloista',
	'BAN_LENGTH'	=> 'Porttikiellon kesto',
	'BAN_REASON'	=> 'Porttikiellon syy',
	'BAN_GIVE_REASON'	=> 'Porttikiellon saaneille näytettävä syy',
	'BAN_UPDATE_SUCCESSFUL'	=> 'Porttikieltojen luettelo on päivitetty.',
	'BANNED_UNTIL_DATE'	=> '%s asti',	// Example: "until Mon 13.Jul.2009, 14:44"
	'BANNED_UNTIL_DURATION'	=> '%1$s (%2$s asti)',	// Example: "7 days (until Tue 14.Jul.2009, 14:44)"

	'EMAIL_BAN'	=> 'Anna porttikielto yhdelle tai useammalle sähköpostiosoitteelle',
	'EMAIL_BAN_EXCLUDE_EXPLAIN'	=> 'Vapauta annetut sähköpostiosoitteet kaikista voimassa olevista porttikielloista.',
	'EMAIL_BAN_EXPLAIN'	=> 'Voit syöttää useamman kuin yhden sähköpostiosoitteen lisäämällä kunkin osoitteen omalle rivilleen. Osittaisia osoitteita voi määrittää käyttämällä merkkiä * jokerimerkkinä, esim. <samp>*@hotmail.com</samp> tai <samp>*@*.domain.tld</samp>.',
	'EMAIL_NO_BANNED'	=> 'Porttikieltoon asetettuja sähköpostiosoitteita ei ole',
	'EMAIL_UNBAN'	=> 'Poista porttikielto tai vapautus porttikielloista',
	'EMAIL_UNBAN_EXPLAIN'	=> 'Voit poistaa porttikiellon (tai vapautuksen porttikielloista) usealta sähköpostiosoitteelta käyttämällä tietokoneesi ja selaimesi tukemia hiiri- ja näppäinyhdistelmiä. Porttikielloista vapautetut sähköpostiosoitteet näkyvät korostettuina.',

	'IP_BAN'	=> 'Anna porttikielto yhdelle tai useammalle IP-osoitteelle',
	'IP_BAN_EXCLUDE_EXPLAIN'	=> 'Vapauta annetut IP-osoitteet kaikista voimassa olevista porttikielloista.',
	'IP_BAN_EXPLAIN'	=> 'Voit syöttää useamman kuin yhden IP-osoitteen tai isäntänimen lisäämällä niistä jokaisen omalle rivilleen. IP-osoitealueita voi määrittää erottamalla alun ja lopun toisistaan yhdysviivalla (-). Lisäksi voit käyttää merkkiä * jokerimerkkinä.',
	'IP_HOSTNAME'	=> 'IP-osoitteet tai isäntänimet',
	'IP_NO_BANNED'	=> 'Porttikieltoon asetettuja IP-osoitteita ei ole',
	'IP_UNBAN'	=> 'Poista porttikielto tai vapautus porttikielloista',
	'IP_UNBAN_EXPLAIN'	=> 'Voit poistaa porttikiellon (tai vapautuksen porttikielloista) usealta IP-osoitteelta käyttämällä tietokoneesi ja selaimesi tukemia hiiri- ja näppäinyhdistelmiä. Porttikielloista vapautetut IP-osoitteet näkyvät korostettuina.',

	'LENGTH_BAN_INVALID'	=> 'Päivämäärä täytyy antaa muodossa <kbd>VVVV-KK-PP</kbd>.',

	'OPTIONS_BANNED' => 'Porttikiellossa',
	'OPTIONS_EXCLUDED' => 'Vapautettu porttikielloista',

	'PERMANENT'	=> 'Pysyvä',

	'UNTIL'	=> 'Päivämäärään asti',
	'USER_BAN'	=> 'Anna porttikielto käyttäjätunnuksen perusteella',
	'USER_BAN_EXCLUDE_EXPLAIN'	=> 'Vapauta annetut käyttäjät kaikista voimassa olevista porttikielloista.',
	'USER_BAN_EXPLAIN'	=> 'Voit antaa porttikiellon useammalle kuin yhdelle käyttäjälle syöttämällä kunkin käyttäjätunnuksen omalle rivilleen. Käytä Etsi käyttäjä -toimintoa hakeaksesi ja lisätäksesi käyttäjiä automaattisesti.',
	'USER_NO_BANNED'	=> 'Porttikieltoon asetettuja käyttäjätunnuksia ei ole',
	'USER_UNBAN'	=> 'Poista porttikielto tai vapautus porttikielloista',
	'USER_UNBAN_EXPLAIN'	=> 'Voit poistaa porttikiellon (tai vapautuksen porttikielloista) usealta käyttäjältä käyttämällä tietokoneesi ja selaimesi tukemia hiiri- ja näppäinyhdistelmiä. Porttikielloista vapautetut käyttäjät näkyvät korostettuina.',
));
