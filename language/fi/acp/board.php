<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

// Board Settings
$lang = array_merge($lang, array(
	'ACP_BOARD_SETTINGS_EXPLAIN'	=> 'Tässä voit vaikuttaa keskustelupalstasi perustoimintoihin, antaa sille sopivan nimen ja kuvauksen sekä säätää muita asetuksia kuten esimerkiksi aikavyöhykkeen ja kielen oletusarvoja.',

	'BOARD_INDEX_TEXT'				=> 'Keskustelupalstan etusivun kuvaus',
	'BOARD_INDEX_TEXT_EXPLAIN'		=> 'Tämä teksti näkyy keskustelupalstan etusivun kohdalla navigointipolussa. Tekstin oletusarvo on ”Keskustelupalstan etusivu”.',
	'BOARD_STYLE'					=> 'Keskustelupalstan tyyli',

	'CUSTOM_DATEFORMAT'	=> 'Mukautettu…',

	'DEFAULT_DATE_FORMAT'	=> 'Päivämäärän muoto',
	'DEFAULT_DATE_FORMAT_EXPLAIN'	=> 'Päiväyksen muoto on sama kuin PHP:n <code>date</code>-funktiossa.',
	'DEFAULT_LANGUAGE'	=> 'Oletuskieli',
	'DEFAULT_STYLE'	=> 'Oletustyyli',
	'DEFAULT_STYLE_EXPLAIN'			=> 'Oletusarvoinen tyyli uusille käyttäjille.',
	'DISABLE_BOARD'	=> 'Poista keskustelupalsta käytöstä',
	'DISABLE_BOARD_EXPLAIN'	=> 'Tämä sulkee keskustelupalstan käyttäjiltä, jotka eivät ole joko ylläpitäjiä tai valvojia. Halutessasi voit myös antaa lyhyen (enintään 255 merkin pituisen) viestin, joka näytetään käyttäjille.',
	'DISPLAY_LAST_SUBJECT'			=> 'Näytä uusimman viestin otsikko alueiden luettelossa',
	'DISPLAY_LAST_SUBJECT_EXPLAIN'	=> 'Viimeisimmän lähetetyn viestin otsikko sekä linkki viestiin näytetään keskustelualueiden luettelossa. Otsikoita ei näytetä alueilta, jotka ovat salasanalla suojattuja tai joille käyttäjillä ei ole lukuoikeutta.',

	'GUEST_STYLE'					=> 'Vierailijoiden tyyli',
	'GUEST_STYLE_EXPLAIN'			=> 'Vierailijoille näkyvä keskustelupalstan tyyli.',

	'OVERRIDE_STYLE'	=> 'Ohita käyttäjien valitsemat tyylit',
	'OVERRIDE_STYLE_EXPLAIN'	=> 'Korvaa käyttäjien (ja vierailijoiden) tyylit oletustyyliksi asetetulla tyylillä.',

	'SITE_DESC'	=> 'Sivuston kuvaus',
	'SITE_HOME_TEXT'				=> 'Pääsivuston kuvaus',
	'SITE_HOME_TEXT_EXPLAIN'		=> 'Tämä teksti näkyy linkkinä sivustosi etusivulle keskustelupalstan navigointipolussa. Tekstin oletusarvo on ”Pääsivusto”.',
	'SITE_HOME_URL'					=> 'Pääsivuston osoite',
	'SITE_HOME_URL_EXPLAIN'			=> 'Jos täytät tämän kentän, linkki antamaasi osoitteeseen lisätään keskustelupalstan navigointipolun alkuun. Lisäksi keskustelupalstan logo toimii linkkinä antamaasi osoitteeseen keskustelupalstan etusivun sijaan. Osoitteen täytyy olla absoluuttinen (esim. <samp>http://www.phpbb.com</samp>).',
	'SITE_NAME'	=> 'Sivuston nimi',
	'SYSTEM_TIMEZONE'	=> 'Vierailijoiden aikavyöhyke',
	'SYSTEM_TIMEZONE_EXPLAIN'	=> 'Tämä aikavyöhyke on käytössä niillä, jotka eivät ole kirjautuneet sisään (vierailijat ja botit). Sisäänkirjautuneet käyttäjät valitsevat aikavyöhykkeensä rekisteröitymisen yhteydessä ja voivat muuttaa sitä käyttäjän hallintapaneelissa.',

	'WARNINGS_EXPIRE'	=> 'Varoitusten kesto',
	'WARNINGS_EXPIRE_EXPLAIN'	=> 'Aikamäärä, jonka täytyy kulua, ennen kuin varoitus poistetaan automaattisesti käyttäjän tiedoista. Anna arvoksi 0, jos haluat tehdä varoituksesta pysyvän.',
));

// Board Features
$lang = array_merge($lang, array(
	'ACP_BOARD_FEATURES_EXPLAIN'	=> 'Tässä voit ottaa käyttöön tai poistaa käytöstä useita keskustelupalstan toimintoja.',
	'ALLOW_ATTACHMENTS'	=> 'Salli liitetiedostot',
	'ALLOW_BIRTHDAYS'	=> 'Salli syntymäpäivät',
	'ALLOW_BIRTHDAYS_EXPLAIN'	=> 'Salli syntymäpäivän syöttäminen ja iän näkyminen profiilissa. Huomaa, että keskustelupalstan etusivulla näkyvää syntymäpäiväluetteloa hallitaan erikseen kuormituksen asetuksissa.',
	'ALLOW_BOOKMARKS'	=> 'Salli kirjanmerkkien lisääminen aiheisiin',
	'ALLOW_BOOKMARKS_EXPLAIN'	=> 'Käyttäjät voivat lisätä henkilökohtaisia kirjanmerkkejä.',
	'ALLOW_BBCODE'	=> 'Salli BBCode',
	'ALLOW_FORUM_NOTIFY'	=> 'Salli alueiden seuranta',
	'ALLOW_NAME_CHANGE'	=> 'Salli käyttäjätunnuksen vaihto',
	'ALLOW_NO_CENSORS'	=> 'Salli sensuroinnin ohittaminen',
	'ALLOW_NO_CENSORS_EXPLAIN'	=> 'Käyttäjät voivat poistaa automaattisen sanojen sensuroinnin käytöstä aiheissa ja yksityisviesteissä.',
	'ALLOW_PM_ATTACHMENTS'	=> 'Salli liitetiedostot yksityisviesteissä',
	'ALLOW_PM_REPORT'	=> 'Salli yksityisviestien ilmiantaminen',
	'ALLOW_PM_REPORT_EXPLAIN'	=> 'Jos tämä asetus on käytössä, käyttäjät voivat tehdä ilmoituksia vastaanottamistaan ja lähettämistään yksityisviesteistä keskustelupalstan valvojille. Nämä yksityisviestit tulevat näkyviin valvojien hallintapaneeliin.',
	'ALLOW_QUICK_REPLY'	=> 'Salli pikavastaus',
	'ALLOW_QUICK_REPLY_EXPLAIN'	=> 'Tämän asetuksen avulla voit poistaa pikavastauksen käytöstä koko keskustelupalstalta. Jos pikavastaus on käytössä, aluekohtaiset asetukset ratkaisevat, onko pikavastaus käytettävissä kullakin alueella.',
	'ALLOW_QUICK_REPLY_BUTTON'	=> 'Tallenna asetukset ja salli pikavastauksen käyttö kaikilla alueilla',
	'ALLOW_SIG'	=> 'Salli allekirjoitukset',
	'ALLOW_SIG_BBCODE'	=> 'Salli BBCoden käyttö allekirjoituksissa',
	'ALLOW_SIG_FLASH'	=> 'Salli BBCode-tagin <code>[flash]</code> käyttö allekirjoituksissa',
	'ALLOW_SIG_IMG'	=> 'Salli BBCode-tagin <code>[img]</code> käyttö allekirjoituksissa',
	'ALLOW_SIG_LINKS'	=> 'Salli linkkien käyttö allekirjoituksissa',
	'ALLOW_SIG_LINKS_EXPLAIN'	=> 'Jos tämä asetus ei ole käytössä, BBCode-tagi <code>[url]</code> ja automaattinen linkitys eivät ole käytettävissä.',
	'ALLOW_SIG_SMILIES'	=> 'Salli hymiöiden käyttö allekirjoituksissa',
	'ALLOW_SMILIES'	=> 'Salli hymiöt',
	'ALLOW_TOPIC_NOTIFY'	=> 'Salli aiheiden seuranta',

	'BOARD_PM'	=> 'Yksityisviestit',
	'BOARD_PM_EXPLAIN'	=> 'Salli yksityisviestien käyttö kaikille käyttäjille.',
	'ALLOW_BOARD_NOTIFICATIONS' => 'Salli keskustelupalstan ilmoitukset',
));

// Avatar Settings
$lang = array_merge($lang, array(
	'ACP_AVATAR_SETTINGS_EXPLAIN'	=> 'Avatarit ovat yleensä pieniä ja yksilöllisiä kuvia, joita käyttäjät voivat valita itselleen. Tyylistä riippuen ne näkyvät tavallisesti käyttäjätunnusten alapuolella aiheita luettaessa. Tässä voit päättää, millaiset avatarit ovat sallittuja. Huomaa, että avatarien palvelimelle siirtämistä varten sinun täytyy luoda hakemisto, jonka nimen voit valita alla, ja varmistaa, että palvelin voi kirjoittaa siihen. Huomaa myös, että tiedostojen kokorajoitukset koskevat vain palvelimelle siirrettyjä avatareja. Ne eivät päde muilta sivustoilta linkitettyihin kuviin.',
	'ALLOW_AVATARS'	=> 'Salli avatarit',
	'ALLOW_AVATARS_EXPLAIN'	=> 'Salli avatarien yleinen käyttö.<br />Jos estät kaikkien tai tietynlaisten avatarien käytön, käytöstä poistetut avatarit eivät enää näy keskustelupalstalla, mutta käyttäjät voivat silti ladata omat avatarinsa tietokoneelleen käyttäjän hallintapaneelissa.',
	'ALLOW_GRAVATAR'				=> 'Salli Gravatar-avatarit',
	'ALLOW_LOCAL'	=> 'Avatar-galleria käytössä',
	'ALLOW_REMOTE'	=> 'Salli linkitetyt avatarit',
	'ALLOW_REMOTE_EXPLAIN'	=> 'Salli muilta sivustoilta linkitettyjen avatarien käyttö.',
	'ALLOW_REMOTE_UPLOAD'	=> 'Salli ulkoisten avatarien siirto',
	'ALLOW_REMOTE_UPLOAD_EXPLAIN'	=> 'Salli avatarien siirto muilta sivustoilta.',
	'ALLOW_UPLOAD'	=> 'Salli avatarien siirto palvelimelle',
	'AVATAR_GALLERY_PATH'	=> 'Avatar-gallerian polku',
	'AVATAR_GALLERY_PATH_EXPLAIN'	=> 'Polku phpBB:n juurihakemistosta esiasennettuihin kuviin (esim. <samp>images/avatars/gallery</samp>).<br />Ylähakemistoviittaukset kuten <samp>../</samp> poistetaan polusta tietoturvasyistä.',
	'AVATAR_STORAGE_PATH'	=> 'Avatarien tallennuspolku',
	'AVATAR_STORAGE_PATH_EXPLAIN'	=> 'Polku phpBB:n juurihakemistosta (esim. <samp>images/avatars/upload</samp>).<br />Avatarien tallennus <strong>ei ole mahdollista</strong>, jos tähän hakemistoon ei voi kirjoittaa.',

	'MAX_AVATAR_SIZE'	=> 'Avatarien suurin sallittu koko',
	'MAX_AVATAR_SIZE_EXPLAIN'	=> 'Leveys × korkeus pikseleinä.',
	'MAX_FILESIZE'	=> 'Avatar-tiedostojen suurin sallittu koko',
	'MAX_FILESIZE_EXPLAIN'	=> 'Koskee palvelimelle siirrettyjä avatareja. Jos arvo on 0, siirrettävien tiedostojen kokoa rajoittavat ainoastaan PHP-asetuksesi.',
	'MIN_AVATAR_SIZE'	=> 'Avatarien pienin sallittu koko',
	'MIN_AVATAR_SIZE_EXPLAIN'	=> 'Leveys × korkeus pikseleinä.',
));

// Message Settings
$lang = array_merge($lang, array(
	'ACP_MESSAGE_SETTINGS_EXPLAIN'	=> 'Tässä voit määrittää kaikki yksityisviestien oletusasetukset.',
	'ALLOW_BBCODE_PM'	=> 'Salli BBCoden käyttö yksityisviesteissä',
	'ALLOW_FLASH_PM'	=> 'Salli BBCode-tagin <code>[flash]</code> käyttö',
	'ALLOW_FLASH_PM_EXPLAIN'	=> 'Huomaa, että mahdollisuus käyttää Flashia yksityisviesteissä riippuu myös oikeuksista.',
	'ALLOW_FORWARD_PM'	=> 'Salli yksityisviestien lähettäminen eteenpäin',
	'ALLOW_IMG_PM'	=> 'Salli BBCode-tagin <code>[img]</code> käyttö',
	'ALLOW_MASS_PM'	=> 'Salli yksityisviestien lähettäminen useille käyttäjille ja ryhmille',
	'ALLOW_MASS_PM_EXPLAIN'	=> 'Ryhmille lähettämistä voi säätää ryhmäkohtaisesti ryhmien asetuksissa.',
	'ALLOW_PRINT_PM'	=> 'Salli tulostusnäkymä yksityisviesteissä',
	'ALLOW_QUOTE_PM'	=> 'Salli viestien lainaaminen yksityisviesteissä',
	'ALLOW_SIG_PM'	=> 'Salli allekirjoitukset yksityisviesteissä',
	'ALLOW_SMILIES_PM'	=> 'Salli hymiöiden käyttö yksityisviesteissä',

	'BOXES_LIMIT'	=> 'Yksityisviestien enimmäismäärä yhtä kansiota kohden',
	'BOXES_LIMIT_EXPLAIN'	=> 'Käyttäjät voivat vastaanottaa korkeintaan tämän verran viestejä kuhunkin yksityisviestikansioon. Aseta arvoksi 0, jos et halua rajoittaa viestien määrää.',
	'BOXES_MAX'	=> 'Yksityisviestikansioiden enimmäismäärä',
	'BOXES_MAX_EXPLAIN'	=> 'Oletusarvoisesti käyttäjät voivat luoda tämän verran henkilökohtaisia kansiota yksityisviestejä varten.',

	'ENABLE_PM_ICONS'	=> 'Salli aiheiden kuvakkeiden käyttö yksityisviesteissä',

	'FULL_FOLDER_ACTION'	=> 'Oletustoimenpide kansion ollessa täynnä',
	'FULL_FOLDER_ACTION_EXPLAIN'	=> 'Toimenpide, joka suoritetaan oletusarvoisesti, jos käyttäjän kansio on täynnä ja käyttäjän valitsema toimenpide ei ole käytettävissä. Ainoa poikkeus on Lähetetyt-kansio, jonka oletustoimenpide on aina vanhojen viestien poistaminen.',

	'HOLD_NEW_MESSAGES'	=> 'Pidätä uudet viestit',

	'PM_EDIT_TIME'	=> 'Rajoita muokkausaikaa',
	'PM_EDIT_TIME_EXPLAIN'	=> 'Rajoittaa aikaa, jonka kuluessa yksityisviestiä voi muokata, jos viestiä ei ole vielä toimitettu perille. Aseta arvoksi 0, jos haluat poistaa rajoituksen käytöstä.',
	'PM_MAX_RECIPIENTS'	=> 'Sallittujen vastaanottajien enimmäismäärä',
	'PM_MAX_RECIPIENTS_EXPLAIN'	=> 'Sallittujen vastaanottajien enimmäismäärä yhtä yksityisviestiä kohden. Jos arvo on 0, vastaanottajien määrää ei ole rajoitettu. Tätä asetusta voi säätää ryhmäkohtaisesti ryhmien asetuksissa.',
));

// Post Settings
$lang = array_merge($lang, array(
	'ACP_POST_SETTINGS_EXPLAIN'	=> 'Tässä voit muokata viestien lähetyksen oletusasetuksia.',
	'ALLOW_POST_LINKS'	=> 'Salli linkkien käyttö viesteissä ja yksityisviesteissä',
	'ALLOW_POST_LINKS_EXPLAIN'	=> 'Jos tämä asetus ei ole käytössä, BBCode-tagi <code>[url]</code> ja automaattinen linkitys poistetaan käytöstä.',
	'ALLOWED_SCHEMES_LINKS'				=> 'Linkeissä sallitut protokollat',
	'ALLOWED_SCHEMES_LINKS_EXPLAIN'		=> 'Käyttäjien lähettämien linkkien URL-osoitteiden täytyy olla protokollattomia tai protokollan täytyy kuulua oheiseen pilkuilla eroteltuun luetteloon.',
	'ALLOW_POST_FLASH'	=> 'Salli BBCode-tagin <code>[flash]</code> käyttö viesteissä',
	'ALLOW_POST_FLASH_EXPLAIN'	=> 'Jos tämä asetus ei ole käytössä, BBCode-tagin <code>[flash]</code> käyttö viesteissä on estetty. Muussa tapauksessa voimassa olevat oikeudet ratkaisevat, mitkä käyttäjät voivat käyttää BBCode-tagia <code>[flash]</code>.',

	'BUMP_INTERVAL'	=> 'Tönäisyn aikaraja',
	'BUMP_INTERVAL_EXPLAIN'	=> 'Minuuttien, tuntien tai päivien lukumäärä, jonka täytyy kulua aiheen viimeisimmän viestin lähetyksestä, ennen kuin aihetta voi tönäistä. Anna arvoksi 0, jos haluat poistaa tönäisyn käytöstä kokonaan.',

	'CHAR_LIMIT'	=> 'Merkkien enimmäismäärä viestissä/yksityisviestissä',
	'CHAR_LIMIT_EXPLAIN'	=> 'Yhdessä viestissä/yksityisviestissä sallittujen merkkien enimmäismäärä. Anna arvoksi 0, jos et halua rajoittaa merkkien lukumäärää.',

	'DELETE_TIME'	=> 'Poiston aikaraja',
	'DELETE_TIME_EXPLAIN'	=> 'Rajoittaa aikaa, jonka kuluessa uuden viestin voi poistaa. Anna arvoksi 0, jos haluat poistaa rajoituksen käytöstä.',
	'DISPLAY_LAST_EDITED'	=> 'Näytä viimeisimmän muokkauksen tiedot',
	'DISPLAY_LAST_EDITED_EXPLAIN'	=> 'Valitse, näytetäänkö viimeisimmästä muokkauksesta tietoja viestien yhteydessä.',

	'EDIT_TIME'	=> 'Muokkauksen aikaraja',
	'EDIT_TIME_EXPLAIN'	=> 'Rajoittaa aikaa, jonka kuluessa uutta viestiä voi muokata. Anna arvoksi 0, jos haluat poistaa rajoituksen käytöstä.',

	'FLOOD_INTERVAL'	=> 'Viestien lähetyksen rajoitus',
	'FLOOD_INTERVAL_EXPLAIN'	=> 'Aika (sekunteina), jonka käyttäjän täytyy odottaa uusien viestien lähetysten välillä. Voit sallia käyttäjien ohittaa tämän rajoituksen muokkaamalla heidän oikeuksiaan.',

	'HOT_THRESHOLD'	=> 'Suositun aiheen raja',
	'HOT_THRESHOLD_EXPLAIN'	=> 'Yhdessä aiheessa vaadittavien viestien vähimmäismäärä, ennen kuin aihe merkitään suosituksi. Anna arvoksi 0, jos haluat poistaa tämän toiminnon käytöstä.',

	'MAX_POLL_OPTIONS'	=> 'Äänestysten vaihtoehtojen enimmäismäärä',
	'MAX_POST_FONT_SIZE'	=> 'Fontin enimmäiskoko viesteissä',
	'MAX_POST_FONT_SIZE_EXPLAIN'	=> 'Suurin sallittu fontin koko viesteissä. Anna arvoksi 0, jos et halua rajoittaa kokoa.',
	'MAX_POST_IMG_HEIGHT'	=> 'Kuvien enimmäiskorkeus viesteissä',
	'MAX_POST_IMG_HEIGHT_EXPLAIN'	=> 'Viesteihin sisältyvien kuvien tai Flash-tiedostojen enimmäiskorkeus. Anna arvoksi 0, jos et halua rajoittaa korkeutta.',
	'MAX_POST_IMG_WIDTH'	=> 'Kuvien enimmäisleveys viesteissä',
	'MAX_POST_IMG_WIDTH_EXPLAIN'	=> 'Viesteihin sisältyvien kuvien tai Flash-tiedostojen enimmäisleveys. Anna arvoksi 0, jos et halua rajoittaa leveyttä.',
	'MAX_POST_URLS'	=> 'Linkkien enimmäismäärä viestissä',
	'MAX_POST_URLS_EXPLAIN'	=> 'Yhden viestin sisältämien linkkien enimmäismäärä. Anna arvoksi 0, jos et halua rajoittaa lukumäärää.',
	'MIN_CHAR_LIMIT'	=> 'Merkkien vähimmäismäärä viestissä/yksityisviestissä',
	'MIN_CHAR_LIMIT_EXPLAIN'	=> 'Yhdessä viestissä/yksityisviestissä vaadittavien merkkien vähimmäismäärä. Pienin sallittu arvo tälle asetukselle on 1.',

	'POSTING'	=> 'Viestien lähetys',
	'POSTS_PER_PAGE'	=> 'Viestejä sivua kohti',

	'QUOTE_DEPTH_LIMIT'	=> 'Sisäkkäisten lainausten enimmäissyvyys',
	'QUOTE_DEPTH_LIMIT_EXPLAIN'	=> 'Viesteissä olevien sisäkkäisten lainausten suurin sallittu syvyys. Anna arvoksi 0, jos et halua rajoittaa syvyyttä.',

	'SMILIES_LIMIT'	=> 'Hymiöiden enimmäismäärä viestissä',
	'SMILIES_LIMIT_EXPLAIN'	=> 'Yhdessä viestissä sallittujen hymiöiden enimmäismäärä. Anna arvoksi 0, jos et halua rajoittaa lukumäärää.',
	'SMILIES_PER_PAGE'	=> 'Hymiöitä sivua kohti',

	'TOPICS_PER_PAGE'	=> 'Aiheita sivua kohti',
));

// Signature Settings
$lang = array_merge($lang, array(
	'ACP_SIGNATURE_SETTINGS_EXPLAIN'	=> 'Tässä voit muokata allekirjoitusten oletusasetuksia.',

	'MAX_SIG_FONT_SIZE'	=> 'Fontin enimmäiskoko allekirjoituksissa',
	'MAX_SIG_FONT_SIZE_EXPLAIN'	=> 'Suurin sallittu fontin koko käyttäjien allekirjoituksissa. Anna arvoksi 0, jos et halua rajoittaa kokoa.',
	'MAX_SIG_IMG_HEIGHT'	=> 'Kuvien enimmäiskorkeus allekirjoituksissa',
	'MAX_SIG_IMG_HEIGHT_EXPLAIN'	=> 'Käyttäjien allekirjoituksiin sisältyvien kuvien tai Flash-tiedostojen enimmäiskorkeus. Anna arvoksi 0, jos et halua rajoittaa korkeutta.',
	'MAX_SIG_IMG_WIDTH'	=> 'Kuvien enimmäisleveys allekirjoituksissa',
	'MAX_SIG_IMG_WIDTH_EXPLAIN'	=> 'Käyttäjien allekirjoituksiin sisältyvien kuvien tai Flash-tiedostojen enimmäisleveys. Anna arvoksi 0, jos et halua rajoittaa leveyttä.',
	'MAX_SIG_LENGTH'	=> 'Allekirjoituksen enimmäispituus',
	'MAX_SIG_LENGTH_EXPLAIN'	=> 'Yhden käyttäjän allekirjoituksessa sallittujen merkkien enimmäismäärä.',
	'MAX_SIG_SMILIES'	=> 'Hymiöiden enimmäismäärä allekirjoituksessa',
	'MAX_SIG_SMILIES_EXPLAIN'	=> 'Yhden käyttäjän allekirjoituksessa sallittujen hymiöiden enimmäismäärä. Anna arvoksi 0, jos et halua rajoittaa lukumäärää.',
	'MAX_SIG_URLS'	=> 'Linkkien enimmäismäärä allekirjoituksessa',
	'MAX_SIG_URLS_EXPLAIN'	=> 'Yhden käyttäjän allekirjoituksessa sallittujen linkkien enimmäismäärä. Anna arvoksi 0, jos et halua rajoittaa lukumäärää.',
));

// Registration Settings
$lang = array_merge($lang, array(
	'ACP_REGISTER_SETTINGS_EXPLAIN'	=> 'Tässä voit muokata rekisteröitymiseen ja profiileihin liittyviä asetuksia.',
	'ACC_ACTIVATION'	=> 'Käyttäjätilien aktivointi',
	'ACC_ACTIVATION_EXPLAIN'	=> 'Tämä asetus määrittää, pääsevätkö käyttäjät keskustelupalstalle välittömästi vai täytyykö heidän käyttäjätilinsä aktivoida ensin. Voit myös poistaa rekisteröitymisen käytöstä kokonaan. <em>Sähköpostitoimintojen täytyy olla käytössä, jos haluat käyttää käyttäjän tai ylläpidon suorittamaa aktivointia.</em>',
	'ACC_ACTIVATION_WARNING'		=> 'Huomaa, että valittu aktivointitapa vaatii sähköpostitoimintojen olevan käytössä. Muussa tapauksessa uusien käyttäjätilien rekisteröiminen estetään. Suosittelemme joko valitsemaan toisen aktivointitavan tai ottamaan sähköpostitoiminnot käyttöön.',
	'NEW_MEMBER_POST_LIMIT'	=> 'Uuden käyttäjän viestiraja',
	'NEW_MEMBER_POST_LIMIT_EXPLAIN'	=> 'Uudet käyttäjät pysyvät <em>Uudet käyttäjät</em> -ryhmässä, kunnes he ovat kirjoittaneet tässä määritetyn verran viestejä. Tämän ryhmän avulla voit estää heitä lähettämästä yksityisviestejä tai asettaa heidän kirjoittamansa viestit tarkastettavaksi ennen julkaisua. <strong>Anna arvoksi 0, jos haluat poistaa tämän toiminnon käytöstä.</strong>',
	'NEW_MEMBER_GROUP_DEFAULT'	=> 'Aseta Uudet käyttäjät -ryhmä oletusryhmäksi',
	'NEW_MEMBER_GROUP_DEFAULT_EXPLAIN'	=> 'Jos tämä asetus on käytössä ja uuden käyttäjän viestiraja on määritetty, <em>Uudet käyttäjät</em> -ryhmä asetetaan uusien käyttäjien oletusryhmäksi. Tästä voi olla hyötyä, jos haluat asettaa ryhmälle oletusarvonimen tai -avatarin, joka periytyy käyttäjälle.',

	'ACC_ADMIN'	=> 'Ylläpito aktivoi',
	'ACC_DISABLE'	=> 'Poista rekisteröityminen käytöstä',
	'ACC_NONE'	=> 'Ei aktivointia (välitön pääsy)',
	'ACC_USER'	=> 'Käyttäjä aktivoi (sähköpostivahvistus)',
	'ALLOW_EMAIL_REUSE'	=> 'Salli sähköpostiosoitteen uudelleenkäyttö',
	'ALLOW_EMAIL_REUSE_EXPLAIN'	=> 'Salli eri käyttäjien rekisteröityä samalla sähköpostiosoitteella.',

	'COPPA'	=> 'Coppa',
	'COPPA_FAX'	=> 'COPPA-faksinumero',
	'COPPA_MAIL'	=> 'COPPA-postiosoite',
	'COPPA_MAIL_EXPLAIN'	=> 'Postiosoite, johon vanhemmat voivat lähettää COPPA-rekisteröitymislomakkeet.',

	'ENABLE_COPPA'	=> 'COPPA käytössä',
	'ENABLE_COPPA_EXPLAIN'	=> 'Vaadi käyttäjiä ilmoittamaan, ovatko he vähintään 13-vuotiaita Yhdysvaltain COPPA-lain noudattamiseksi. Jos tämä asetus ei ole käytössä, COPPA-ryhmät poistetaan näkyvistä.',

	'MAX_CHARS'	=> 'Enintään',
	'MIN_CHARS'	=> 'Vähintään',

	'NO_AUTH_PLUGIN'	=> 'Sopivaa tunnistautumislisäosaa ei löytynyt.',

	'PASSWORD_LENGTH'	=> 'Salasanan pituus',
	'PASSWORD_LENGTH_EXPLAIN'	=> 'Salasanojen sisältämien merkkien vähimmäis- ja enimmäismäärät.',

	'REG_LIMIT'	=> 'Rekisteröitymisyritykset',
	'REG_LIMIT_EXPLAIN'	=> 'Määrittää, kuinka monta kertaa käyttäjä voi yrittää ratkaista roskapostin torjuntaan tarkoitettua tehtävää, ennen kuin hänen istuntonsa lukitaan.',

	'USERNAME_ALPHA_ONLY'	=> 'Vain kirjaimet ja numerot',
	'USERNAME_ALPHA_SPACERS'	=> 'Vain kirjaimet, numerot ja välimerkit',
	'USERNAME_ASCII'	=> 'ASCII-merkit (ei kansainvälisiä Unicode-merkkejä)',
	'USERNAME_LETTER_NUM'	=> 'Mitkä tahansa kirjaimet ja numerot',
	'USERNAME_LETTER_NUM_SPACERS'	=> 'Mitkä tahansa kirjaimet, numerot ja välimerkit',
	'USERNAME_CHARS'	=> 'Käyttäjätunnuksissa sallitut merkit',
	'USERNAME_CHARS_ANY'	=> 'Mitkä tahansa merkit',
	'USERNAME_CHARS_EXPLAIN'	=> 'Aseta rajoituksia käyttäjätunnuksissa sallituille merkeille. Välimerkkejä ovat välilyönti, -, +, _, [ ja ].',
	'USERNAME_LENGTH'	=> 'Käyttäjätunnuksen pituus',
	'USERNAME_LENGTH_EXPLAIN'	=> 'Käyttäjätunnusten sisältämien merkkien vähimmäis- ja enimmäismäärät.',
));

// Feeds
$lang = array_merge($lang, array(
	'ACP_FEED_MANAGEMENT'	=> 'Syötteiden asetukset',
	'ACP_FEED_MANAGEMENT_EXPLAIN'	=> 'Tämä moduuli tuo saataville erilaisia ATOM-syötteitä. Moduuli käsittelee viestien sisältämät BBCode-tagit siten, että viestit ovat lukukelpoisia ulkoisissa syötteissä.',
	'ACP_FEED_GENERAL'	=> 'Yleiset syötteiden asetukset',
	'ACP_FEED_POST_BASED'	=> 'Viesteihin liittyvät syötteiden asetukset',
	'ACP_FEED_TOPIC_BASED'	=> 'Aiheisiin liittyvät syötteiden asetukset',
	'ACP_FEED_SETTINGS_OTHER'	=> 'Muut syötteiden asetukset',
	'ACP_FEED_ENABLE'	=> 'Syötteet käytössä',
	'ACP_FEED_ENABLE_EXPLAIN'	=> 'Ota ATOM-syötteet käyttöön tai poista ne käytöstä koko keskustelupalstalta.<br />Jos valitset Pois käytöstä -vaihtoehdon, kaikki syötteet poistetaan käytöstä alla olevista asetuksista riippumatta.',
	'ACP_FEED_LIMIT'	=> 'Kohteiden lukumäärä',
	'ACP_FEED_LIMIT_EXPLAIN'	=> 'Syötteissä näytettävien kohteiden enimmäismäärä.',
	'ACP_FEED_OVERALL'	=> 'Koko keskustelupalstan kattava syöte käytössä',
	'ACP_FEED_OVERALL_EXPLAIN'	=> 'Kaikki keskustelupalstan uudet viestit.',
	'ACP_FEED_FORUM'	=> 'Aluekohtaiset syötteet käytössä',
	'ACP_FEED_FORUM_EXPLAIN'	=> 'Uudet viestit yksittäisillä alueilla ja sisäalueilla.',
	'ACP_FEED_TOPIC'	=> 'Aihekohtaiset syötteet käytössä',
	'ACP_FEED_TOPIC_EXPLAIN'	=> 'Uudet viestit yksittäisissä aiheissa.',
	'ACP_FEED_TOPICS_NEW'	=> 'Uusien aiheiden syöte käytössä',
	'ACP_FEED_TOPICS_NEW_EXPLAIN'	=> 'Ota käyttöön ”Uudet aiheet” -syöte, joka sisältää viimeisimmät aiheet ja niiden ensimmäiset viestit.',
	'ACP_FEED_TOPICS_ACTIVE'	=> 'Aktiivisten aiheiden syöte käytössä',
	'ACP_FEED_TOPICS_ACTIVE_EXPLAIN'	=> 'Ota käyttöön ”Aktiiviset aiheet” -syöte, joka sisältää viimeisimmät aktiiviset aiheet ja niiden uusimmat viestit.',
	'ACP_FEED_NEWS'	=> 'Uutissyöte',
	'ACP_FEED_NEWS_EXPLAIN'	=> 'Hae alla valittujen alueiden ensimmäiset viestit. Älä valitse ainuttakaan aluetta, jos haluat poistaa uutissyötteen käytöstä.<br />Voit valita useita alueita pitämällä <samp>CTRL</samp>-näppäintä pohjassa ja napsauttamalla haluamiasi alueita.',
	'ACP_FEED_OVERALL_FORUMS'	=> 'Alueiden syöte käytössä',
	'ACP_FEED_OVERALL_FORUMS_EXPLAIN'	=> 'Ota käyttöön ”Kaikki alueet” -syöte, joka sisältää luettelon alueista.',
	'ACP_FEED_HTTP_AUTH'	=> 'Salli HTTP-tunnistautuminen',
	'ACP_FEED_HTTP_AUTH_EXPLAIN'	=> 'Ota käyttöön HTTP-tunnistautuminen, joka antaa käyttäjille mahdollisuuden vastaanottaa vierailijoilta piilotettua sisältöä lisäämällä <samp>auth=http</samp>-parametrin syötteen osoitteeseen. Huomaa, että jotkin PHP-ympäristöt vaativat lisämuutoksia .htaccess-tiedostoon. Ohjeita saat kyseisestä tiedostosta.',
	'ACP_FEED_ITEM_STATISTICS'	=> 'Kohteiden tilastot',
	'ACP_FEED_ITEM_STATISTICS_EXPLAIN'	=> 'Näytä tilastoja syötteiden kohteista<br />(esim. kirjoittaja, päivämäärä ja aika, vastausten lukumäärä, lukukertojen lukumäärä)',
	'ACP_FEED_EXCLUDE_ID'	=> 'Huomiotta jätetyt alueet',
	'ACP_FEED_EXCLUDE_ID_EXPLAIN'	=> 'Näiden alueiden sisältöä <strong>ei sisällytetä syötteisiin</strong>. Älä valitse ainuttakaan aluetta, jos haluat hakea tietoa kaikilta alueilta.<br />Voit valita useita alueita painamalla <samp>CTRL</samp>-näppäintä pohjassa ja napsauttamalla haluamiasi alueita.',
));

// Visual Confirmation Settings
$lang = array_merge($lang, array(
	'ACP_VC_SETTINGS_EXPLAIN'	=> 'Tässä voit valita ja säätää lisäosia, joiden tehtävänä on estää roskapostibotteja lähettämästä lomakkeita automaattisesti. Näiden lisäosien toiminta perustuu yleensä käyttäjälle esitettyyn <em>CAPTCHA</em>-haasteeseen eli testiin, joka on tehty vaikeasti ratkaistavaksi tietokoneelle.',
	'ACP_VC_EXT_GET_MORE'					=> 'Vaihtoehtoisia (ja mahdollisesti parempia) roskapostin torjuntaan tarkoitettuja lisäosia voi etsiä <a href="https://www.phpbb.com/go/anti-spam-ext"><strong>phpBB:n laajennustietokannasta</strong></a>. Lisätietoja roskapostin estämisestä saat <a href="https://www.phpbb.com/go/anti-spam"><strong>phpBB:n käyttöohjeista</strong></a>.',
	'AVAILABLE_CAPTCHAS'	=> 'Käytettävissä olevat lisäosat',

	'CAPTCHA_UNAVAILABLE'	=> 'Tätä lisäosaa ei voi valita, koska sen vaatimukset eivät täyty.',
	'CAPTCHA_GD'	=> 'GD-kuva',
	'CAPTCHA_GD_3D'	=> 'GD 3D -kuva',
	'CAPTCHA_GD_FOREGROUND_NOISE'	=> 'Edustan kohina',
	'CAPTCHA_GD_EXPLAIN'	=> 'Käyttää GD:tä luodakseen kehittyneemmän roskapostibotteja vastustavan kuvan.',
	'CAPTCHA_GD_FOREGROUND_NOISE_EXPLAIN'	=> 'Käytä edustan kohinaa tehdäksesi kuvasta vaikeammin luettavan.',
	'CAPTCHA_GD_X_GRID'	=> 'Taustan kohina x-akselilla',
	'CAPTCHA_GD_X_GRID_EXPLAIN'	=> 'Käytä pienempää arvoa tehdäksesi kuvasta vaikeammin luettavan. Anna arvoksi 0, jos haluat poistaa taustakohinan x-akselilta.',
	'CAPTCHA_GD_Y_GRID'	=> 'Taustan kohina y-akselilla',
	'CAPTCHA_GD_Y_GRID_EXPLAIN'	=> 'Käytä pienempää arvoa tehdäksesi kuvasta vaikeammin luettavan. Anna arvoksi 0, jos haluat poistaa taustakohinan y-akselilta.',
	'CAPTCHA_GD_WAVE'	=> 'Aaltovääristymä',
	'CAPTCHA_GD_WAVE_EXPLAIN'	=> 'Lisää aaltovääristymä kuvaan.',
	'CAPTCHA_GD_3D_NOISE'	=> 'Lisää 3D-kohinaobjekteja',
	'CAPTCHA_GD_3D_NOISE_EXPLAIN'	=> 'Lisää enemmän objekteja kuvaan kirjainten päälle.',
	'CAPTCHA_GD_FONTS'	=> 'Käytä erilaisia fontteja',
	'CAPTCHA_GD_FONTS_EXPLAIN'	=> 'Valitse, kuinka montaa erilaista kirjainmuotoa käytetään. Voit käyttää joko oletusmuotoja tai muokattuja kirjaimia. On mahdollista ottaa mukaan myös pieniä kirjaimia.',
	'CAPTCHA_FONT_DEFAULT'	=> 'Oletusmuodot',
	'CAPTCHA_FONT_NEW'	=> 'Uudet muodot',
	'CAPTCHA_FONT_LOWER'	=> 'Käytä myös pieniä kirjaimia',
	'CAPTCHA_NO_GD'	=> 'Yksinkertainen kuva',
	'CAPTCHA_PREVIEW_MSG'	=> 'Tekemiäsi muutoksia ei ole tallennettu, sillä tämä on vain esikatselu.',
	'CAPTCHA_PREVIEW_EXPLAIN'	=> 'Lisäosan esikatselu nykyisillä asetuksilla.',
	'CAPTCHA_SELECT'	=> 'Asennetut lisäosat',
	'CAPTCHA_SELECT_EXPLAIN'	=> 'Oheinen pudotusvalikko sisältää keskustelupalstan tunnistamat lisäosat. Harmaana näkyvät vaihtoehdot eivät ole käytettävissä tällä hetkellä, vaan ne vaativat ehkä asetusten muokkaamista ennen käyttöönottoa.',
	'CAPTCHA_CONFIGURE'	=> 'Lisäosan asetukset',
	'CAPTCHA_CONFIGURE_EXPLAIN'	=> 'Muokkaa valitsemasi lisäosan asetuksia.',
	'CONFIGURE'	=> 'Muokkaa asetuksia',
	'CAPTCHA_NO_OPTIONS'	=> 'Tällä lisäosalla ei ole muokattavia asetuksia.',

	'VISUAL_CONFIRM_POST'	=> 'Roskapostin torjunta käytössä vierailijoiden lähettäessä viestejä',
	'VISUAL_CONFIRM_POST_EXPLAIN'	=> 'Vaadi vierailijoita läpäisemään roskapostin torjuntaan tarkoitettu tehtävä automaattisesti lähetettyjen viestien vähentämiseksi.',
	'VISUAL_CONFIRM_REG'	=> 'Roskapostin torjunta käytössä rekisteröitymisen yhteydessä',
	'VISUAL_CONFIRM_REG_EXPLAIN'	=> 'Vaadi uusia käyttäjiä läpäisemään roskapostin torjuntaan tarkoitettu tehtävä automaattisten rekisteröitymisten vähentämiseksi.',
	'VISUAL_CONFIRM_REFRESH'	=> 'Salli roskapostin torjuntaan tarkoitetun tehtävän vaihto',
	'VISUAL_CONFIRM_REFRESH_EXPLAIN'	=> 'Salli käyttäjien pyytää uutta roskapostin torjuntaan tarkoitettua tehtävää rekisteröitymisen yhteydessä, jos he eivät pysty ratkaisemaan ensimmäisenä esitettyä tehtävää. Kaikki lisäosat eivät välttämättä tue tätä asetusta.',
));

// Cookie Settings
$lang = array_merge($lang, array(
	'ACP_COOKIE_SETTINGS_EXPLAIN'	=> 'Nämä asetukset määrittävät, millaisia evästeitä lähetetään käyttäjiesi selaimille. Tavallisesti evästeiden asetusten oletusarvojen pitäisi olla riittäviä. Tee mahdolliset muutokset varoen, sillä väärät asetukset saattavat estää käyttäjiä kirjautumasta sisään. Jos keskustelupalstasi käyttäjillä on vaikeuksia pysyä sisäänkirjautuneina, katso <b><a href="https://www.phpbb.com/support/go/cookie-settings/">phpBB:n evästeitä käsitteleviä ohjeita</a></b>.',

	'COOKIE_DOMAIN'	=> 'Evästeiden verkkotunnus',
	'COOKIE_DOMAIN_EXPLAIN'		=> 'Tavallisesti evästeiden verkkotunnusta ei tarvitse asettaa. Jätä kenttä tyhjäksi, jos olet epävarma.<br /><br /> Jos käytössäsi on keskustelupalstan lisäksi muita ohjelmistoja tai useita verkkotunnuksia, määritä evästeiden verkkotunnus seuraavasti: Jos käytössäsi on esimerkiksi verkkotunnukset <i>example.com</i> ja <i>forums.example.com</i> tai kenties <i>forums.example.com</i> ja <i>blog.example.com</i>, poista aliverkkotunnukset, jotta saat selville yhteisen verkkotunnuksen (<i>example.com</i>). Lisää yhteisen verkkotunnuksen alkuun piste ja syötä lopullinen arvo evästeiden verkkotunnukseksi (<i>.example.com</i>).',
	'COOKIE_NAME'	=> 'Evästeiden nimi',
	'COOKIE_NAME_EXPLAIN'		=> 'Evästeiden nimeksi voi asettaa mitä tahansa, joten käytä mielikuvitustasi. Aina kun evästeiden asetuksia muutetaan, tulisi myös evästeiden nimeä muuttaa.',
	'COOKIE_NOTICE'				=> 'Ilmoita evästeistä',
	'COOKIE_NOTICE_EXPLAIN'		=> 'Jos tämä asetus on käytössä, keskustelupalstasi käyttäjille näytetään ilmoitus evästeistä. Voimassa olevat lait saattavat vaatia tämän ilmoituksen riippuen keskustelupalstasi sisällöstä ja laajennuksista.',
	'COOKIE_PATH'	=> 'Evästeiden polku',
	'COOKIE_PATH_EXPLAIN'		=> 'Tämän kentän arvo on aina kauttaviiva riippumatta keskustelupalstasi URL-osoitteesta.',
	'COOKIE_SECURE'	=> 'Suojatut evästeet',
	'COOKIE_SECURE_EXPLAIN'	=> 'Jos SSL on käytössä palvelimellasi, ota tämä asetus käyttöön. Muussa tapauksessa jätä tämä asetus pois käytöstä. Jos tämä asetus on käytössä ja SSL ei ole käytössä, uudelleenohjaukset eivät toimi ongelmitta.',

	'ONLINE_LENGTH'	=> 'Paikallaolon kesto',
	'ONLINE_LENGTH_EXPLAIN'	=> 'Aika, jonka kuluttua passiiviset käyttäjät eivät enää näy paikallaolijoiden luettelossa. Mitä suurempi tämä arvo on, sitä enemmän prosessointia luettelon luominen vaatii.',

	'SESSION_LENGTH'	=> 'Istunnon kesto',
	'SESSION_LENGTH_EXPLAIN'	=> 'Aika, jonka kuluttua istunnot umpeutuvat.',
));

// Contact Settings
$lang = array_merge($lang, array(
	'ACP_CONTACT_SETTINGS_EXPLAIN'		=> 'Tässä voit ottaa yhteydenottosivun käyttöön tai poistaa sen käytöstä sekä määrittää tekstin, joka sivulla näkyy.',

	'CONTACT_US_ENABLE'				=> 'Yhteydenottosivu käytössä',
	'CONTACT_US_ENABLE_EXPLAIN'		=> 'Käyttäjät voivat lähettää sähköpostia keskustelupalstan ylläpidolle tämän sivun kautta.',
	'CONTACT_US_INFO'				=> 'Yhteystiedot',
	'CONTACT_US_INFO_EXPLAIN'		=> 'Yhteydenottosivulla näkyvä viesti.',
	'CONTACT_US_INFO_PREVIEW'		=> 'Yhteydenottosivu – Esikatselu',
	'CONTACT_US_INFO_UPDATED'		=> 'Yhteydenottosivun tiedot on päivitetty.',
));

// Load Settings
$lang = array_merge($lang, array(
	'ACP_LOAD_SETTINGS_EXPLAIN'	=> 'Tässä voit ottaa käyttöön ja poistaa käytöstä tiettyjä keskustelupalstan toimintoja vähentääksesi kuormituksen määrää. Useimmilla palvelimilla ei ole tarvetta poistaa käytöstä mitään toimintoja. Joissakin järjestelmissä tai jaetuissa ympäristöissä voi kuitenkin olla hyödyllistä poistaa käytöstä toimintoja, joille ei ole todellista tarvetta. Voit myös määrittää järjestelmän kuormitukselle ja aktiivisille istunnoille rajoja, joiden ylityttyä keskustelupalsta sulkeutuu.',
	'ALLOW_CDN'						=> 'Salli ulkopuolisten sisällönjakeluverkostojen käyttö',
	'ALLOW_CDN_EXPLAIN'				=> 'Jos tämä asetus on käytössä, jotkin tiedostot ladataan ulkopuolisilta palvelimilta käyttämäsi palvelimen sijaan. Tämä vähentää palvelimesi vaatimaa kaistanleveyttä, mutta saattaa aiheuttaa yksityisyysdensuojaan liittyviä ongelmia joidenkin keskustelupalstojen ylläpitäjille. Oletusarvoista phpBB-asennusta käytettäessä jQuery-kirjasto ja Open Sans -fontti ladataan Googlen sisällönjakeluverkostoista.',
	'ALLOW_LIVE_SEARCHES'			=> 'Salli välittömät haut',
	'ALLOW_LIVE_SEARCHES_EXPLAIN'	=> 'Jos tämä asetus on käytössä, käyttäjille näytetään avainsanaehdotuksia, kun he täyttävät tiettyjä hakukenttiä keskustelupalstalla.',

	'CUSTOM_PROFILE_FIELDS'	=> 'Mukautetut profiilikentät',

	'LIMIT_LOAD'	=> 'Järjestelmän kuormituksen rajoitus',
	'LIMIT_LOAD_EXPLAIN'	=> 'Jos järjestelmän yhden minuutin keskimääräinen kuormitus ylittää tämän arvon, keskustelupalsta sulkeutuu automaattisesti. Arvo 1.0 vastaa yhden prosessorin noin 100-prosenttista käyttöä. Tämä toimii vain sellaisilla UNIX-palvelimilla, joissa tämä tieto on saatavilla. Tämä asetus palautuu takaisin arvoon 0, jos phpBB ei pysty selvittämään kuormituksen määrää.',
	'LIMIT_SESSIONS'	=> 'Istuntojen rajoitus',
	'LIMIT_SESSIONS_EXPLAIN'	=> 'Jos istuntojen lukumäärä ylittää tässä määritetyn arvon yhden minuutin aikana, keskustelupalsta sulkeutuu. Anna arvoksi 0, jos et halua rajoittaa lukumäärää.',
	'LOAD_CPF_MEMBERLIST'	=> 'Salli tyylien näyttää mukautetut profiilikentät jäsenluettelossa',
	'LOAD_CPF_PM'					=> 'Näytä mukautetut profiilikentät yksityisviesteissä',
	'LOAD_CPF_VIEWPROFILE'	=> 'Näytä mukautetut profiilikentät käyttäjien profiileissa',
	'LOAD_CPF_VIEWTOPIC'	=> 'Näytä mukautetut profiilikentät aiheita luettaessa',
	'LOAD_USER_ACTIVITY'	=> 'Näytä käyttäjän aktiivisuus',
	'LOAD_USER_ACTIVITY_EXPLAIN'	=> 'Näytä aktiivinen aihe/alue käyttäjien profiileissa ja käyttäjän hallintapaneelissa. On suositeltavaa poistaa tämä toiminto käytöstä sellaisilla keskustelupalstoilla, joissa on yli miljoona viestiä.',
	'LOAD_USER_ACTIVITY_LIMIT'		=> 'Käyttäjien aktiivisuuden viestiraja',
	'LOAD_USER_ACTIVITY_LIMIT_EXPLAIN'	=> ' Aktiivinen aihe/alue näytetään ainoastaan niiden käyttäjien kohdalla, joilla on korkeintaan tämän verran viestejä. Syötä arvoksi 0, jos et halua asettaa rajaa.',

	'READ_NOTIFICATION_EXPIRE_DAYS'	=> 'Luetun ilmoituksen vanhentuminen',
	'READ_NOTIFICATION_EXPIRE_DAYS_EXPLAIN' => 'Aika (päivinä), jonka kuluttua luettu ilmoitus poistetaan automaattisesti. Anna arvoksi 0 tehdäksesi ilmoituksista pysyviä.',
	'RECOMPILE_STYLES'	=> 'Rakenna vanhentuneet tyylien osat uudelleen',
	'RECOMPILE_STYLES_EXPLAIN'	=> 'Tarkasta, ovatko tyylien osat päivittyneet tiedostojärjestelmässä, ja rakenna ne tarvittaessa uudelleen.',

	'YES_ANON_READ_MARKING'	=> 'Aiheiden merkintä käytössä vierailijoilla',
	'YES_ANON_READ_MARKING_EXPLAIN'	=> 'Tallenna tiedot vierailijoiden lukemista aiheista. Jos tämä asetus ei ole käytössä, viestit on aina merkitty luetuiksi vierailijoille.',
	'YES_BIRTHDAYS'	=> 'Syntymäpäiväluettelo käytössä',
	'YES_BIRTHDAYS_EXPLAIN'	=> 'Jos tämä asetus on pois käytöstä, syntymäpäiväluetteloa ei näytetä. Myös syntymäpäivätoiminnon täytyy olla käytössä, jotta tällä asetuksella olisi merkitystä.',
	'YES_JUMPBOX'	=> 'Näytä siirtymisvalikot',
	'YES_MODERATORS'	=> 'Näytä valvojat',
	'YES_ONLINE'	=> 'Näytä paikallaolijat',
	'YES_ONLINE_EXPLAIN'	=> 'Näytä luettelo paikallaolijoista etusivulla sekä alueita ja aiheita luettaessa.',
	'YES_ONLINE_GUESTS'	=> 'Näytä vierailijat paikallaolijoiden luettelossa',
	'YES_ONLINE_GUESTS_EXPLAIN'	=> 'Salli vierailijoiden tietojen näyttäminen paikallaolijoiden luettelossa.',
	'YES_ONLINE_TRACK'	=> 'Näytä tiedot käyttäjien paikalla-/poissaolosta',
	'YES_ONLINE_TRACK_EXPLAIN'	=> 'Näytä tiedot käyttäjien paikalla-/poissaolosta profiileissa ja aiheita luettaessa.',
	'YES_POST_MARKING'	=> 'Merkityt aiheet käytössä',
	'YES_POST_MARKING_EXPLAIN'	=> 'Merkityt aiheet ovat aiheita, joihin käyttäjä on kirjoittanut.',
	'YES_READ_MARKING'	=> 'Aiheiden merkintä käytössä palvelimella',
	'YES_READ_MARKING_EXPLAIN'	=> 'Tallentaa tiedot luetuista/lukemattomista aiheista tietokantaan evästeen sijaan.',
	'YES_UNREAD_SEARCH'	=> 'Salli lukemattomien viestien haku',
));

// Auth settings
$lang = array_merge($lang, array(
	'ACP_AUTH_SETTINGS_EXPLAIN'	=> 'PhpBB tukee tunnistautumisen lisäosia (tai moduuleja). Näiden avulla voit määrittää, kuinka käyttäjät tunnistetaan heidän kirjautuessaan sisään keskustelupalstalle. Oletusarvoisesti käytettävissä on neljä lisäosaa: DB, LDAP, Apache ja OAuth. Kaikki menetelmät eivät vaadi lisätietoja, joten täytä vain kentät, jotka liittyvät valitsemaasi menetelmään.',
	'AUTH_METHOD'	=> 'Valitse tunnistautumismenetelmä',
	'AUTH_PROVIDER_OAUTH_ERROR_ELEMENT_MISSING'	=> 'Jokaiselle käytössä olevalle OAuth-palveluntarjoajalle täytyy syöttää sekä avain että salaisuus. Syötit niistä vain toisen.',
	'AUTH_PROVIDER_OAUTH_EXPLAIN'				=> 'Jokainen OAuth-palveluntarjoaja vaatii yksilöivän salaisuuden ja avaimen tunnistautuakseen ulkoiselle palvelimelle. Syötä tiedot juuri sellaisessa muodossa kuin OAuth-palvelu tarjosi ne rekisteröidessäsi sivustosi heille.<br />Palvelut eivät ole keskustelupalstan käyttäjien käytettävissä, mikäli sekä avainta että salaisuutta ei ole syötetty. Huomaa myös, että käyttäjät voivat silti rekisteröityä ja kirjautua sisään käyttäen DB-tunnistautumismenetelmää.',
	'AUTH_PROVIDER_OAUTH_KEY'					=> 'Avain',
	'AUTH_PROVIDER_OAUTH_TITLE'					=> 'OAuth',
	'AUTH_PROVIDER_OAUTH_SECRET'				=> 'Salaisuus',
	'APACHE_SETUP_BEFORE_USE'	=> 'Sinun täytyy määrittää Apachen tunnistautumisen asetukset ensin, ennen kuin voit vaihtaa phpBB:n käyttämään tätä tunnistautumismenetelmää. Muista, että Apachen tunnistautumisen käyttäjätunnuksen täytyy olla sama kuin phpBB:n käyttäjätunnuksesi. Apachen tunnistautumista voi käyttää vain mod_php:n kanssa (CGI-versio ei kelpaa) ja kun safe_mode on pois päältä.',

	'LDAP'							=> 'LDAP',
	'LDAP_DN'	=> 'LDAP:n juuri-<var>DN</var>',
	'LDAP_DN_EXPLAIN'	=> 'Tämä on yksilöivä nimi, jolla käyttäjän tiedot paikallistetaan (esim. <samp>o=My Company,c=US</samp>).',
	'LDAP_EMAIL'	=> 'LDAP:n sähköpostiattribuutti',
	'LDAP_EMAIL_EXPLAIN'	=> 'Anna käyttäjätietojesi sähköpostiattribuutin nimi (jos olemassa), jotta uusien käyttäjien sähköpostiosoitteet voidaan määrittää automaattisesti. Jos jätät tämän kentän tyhjäksi, ensimmäistä kertaa kirjautuvien käyttäjien sähköpostiosoitteet jäävät myös tyhjiksi.',
	'LDAP_INCORRECT_USER_PASSWORD'	=> 'LDAP-palvelimeen yhdistäminen epäonnistui antamallasi käyttäjätunnuksella/salasanalla.',
	'LDAP_NO_EMAIL'	=> 'Määrittämääsi sähköpostiattribuuttia ei ole olemassa.',
	'LDAP_NO_IDENTITY'	=> 'Kohteelle %s ei löytynyt kirjautumisidentiteettiä.',
	'LDAP_PASSWORD'	=> 'LDAP:n salasana',
	'LDAP_PASSWORD_EXPLAIN'	=> 'Jätä tyhjäksi käyttääksesi nimetöntä yhdistämistä. Muussa tapauksessa anna yllä olevan käyttäjän salasana. Tätä tietoa tarvitaan Active Directory -palvelimia varten.<br /><em><strong>Varoitus:</strong> Tämä salasana tallennetaan tietokantaan salaamattomana ja se näkyy kaikille, joilla on pääsy tietokantaasi tai jotka näkevät tämän asetussivun.</em>',
	'LDAP_PORT'	=> 'LDAP-palvelimen portti',
	'LDAP_PORT_EXPLAIN'	=> 'Halutessasi voi määrittää portin, jota käytetään oletusportin 389 sijaan LDAP-palvelimeen yhdistäessä.',
	'LDAP_SERVER'	=> 'LDAP-palvelimen nimi',
	'LDAP_SERVER_EXPLAIN'	=> 'Jos LDAP on käytössä, tämä on LDAP-palvelimen isäntänimi tai IP-osoite. Voit antaa myös muodossa ldap://isäntänimi:portti/ olevan URL:n.',
	'LDAP_UID'	=> 'LDAP:n <var>UID</var>',
	'LDAP_UID_EXPLAIN'	=> 'Tämä on avain, jolla etsitään annettua kirjautumisidentiteettiä (esim. <var>uid</var> tai <var>sn</var>).',
	'LDAP_USER'	=> 'LDAP:n käyttäjä-<var>DN</var>',
	'LDAP_USER_EXPLAIN'	=> 'Jätä tyhjäksi käyttääksesi nimetöntä yhdistämistä. Jos täytät tämän kentän, phpBB käyttää antamaasi yksilöivää nimeä löytääksen oikean käyttäjän kirjautumisyritysten yhteydessä. Esim. <samp>uid=Username,ou=MyUnit,o=MyCompany,c=US</samp>. Tätä tietoa tarvitaan Active Directory -palvelimia varten.',
	'LDAP_USER_FILTER'	=> 'LDAP:n käyttäjien suodatus',
	'LDAP_USER_FILTER_EXPLAIN'	=> 'Halutessasi voit rajoittaa tutkittavia objekteja lisäsuodattimilla. Esimerkiksi <samp>objectClass=posixGroup</samp> asettaa käyttöön suodattimen <samp>(&amp;(uid=$username)(objectClass=posixGroup))</samp>.',
));

// Server Settings
$lang = array_merge($lang, array(
	'ACP_SERVER_SETTINGS_EXPLAIN'	=> 'Tässä voit muokata palvelimesta ja verkkotunnuksesta riippuvia asetuksia. Varmista, että antamasi tiedot ovat täsmällisiä, sillä muuten sähköpostiviestit sisältävät virheellisiä tietoja. Verkkotunnusta antaessasi muista, että siihen täytyy sisältyä merkkijono http:// tai jonkin toisen protokollan nimi. Muuta portin numeroa vain, jos tiedät palvelimesi käyttävän toista porttia. Portti 80 on oikea useimmissa tapauksissa.',

	'ENABLE_GZIP'	=> 'Gzip-pakkaus käytössä',
	'ENABLE_GZIP_EXPLAIN'	=> 'Luotu sisältö pakataan ennen käyttäjälle lähettämistä. Tämä voi pienentää verkossa siirrettävän tiedon määrää, mutta se myös nostaa CPU:n käyttöä sekä palvelimen että asiakkaan puolella. Vaatii, että PHP:n zlib-laajennus on ladattu.',

	'FORCE_SERVER_VARS'	=> 'Pakota palvelimen URL-asetukset',
	'FORCE_SERVER_VARS_EXPLAIN'	=> 'Jos tämä asetus on käytössä, tässä määritettyjä palvelimen asetuksia käytetään automaattisesti selvitettyjen asetusten sijaan.',

	'ICONS_PATH'	=> 'Viestien kuvakkeiden tallennuspolku',
	'ICONS_PATH_EXPLAIN'	=> 'Polku phpBB:n juurihakemistosta (esim. <samp>images/icons</samp>).',

	'MOD_REWRITE_ENABLE'		=> 'Osoitteiden uudelleenkirjoitus käytössä',
	'MOD_REWRITE_ENABLE_EXPLAIN' => 'Jos tämä toiminto on käytössä, tekstin ”app.php” sisältäviä osoitteita muokataan siten, että niistä poistetaan tiedostonimi (esim. osoitteesta app.php/foo tulee /foo). <strong>Tämä toiminto vaatii, että Apache-palvelimen mod_rewrite-moduuli on käytettävissä. Jos toiminto otetaan käyttöön ilman mod_rewrite-tukea, keskustelupalstasi osoitteet eivät välttämättä toimi.</strong>',
	'MOD_REWRITE_DISABLED'		=> 'Apache-palvelimesi <strong>mod_rewrite</strong>-moduuli ei ole käytettävissä. Ota moduuli käyttöön tai ota yhteyttä webhotelliisi, jos haluat käyttää tätä toimintoa.',
	'MOD_REWRITE_INFORMATION_UNAVAILABLE' => 'Emme pystyneet selvittämään, tukeeko palvelimesi osoitteiden uudelleenkirjoitusta. Tämän toiminnon voi ottaa käyttöön, mutta jos osoitteiden uudelleenkirjoitus ei ole käytettävissä, keskustelupalstan muodostamat polut (kuten esimerkiksi linkit) eivät välttämättä toimi. Ota yhteyttä webhotelliisi, jos et ole varma, voitko käyttää tätä toimintoa turvallisesti.',

	'PATH_SETTINGS'	=> 'Polkujen asetukset',

	'RANKS_PATH'	=> 'Arvonimien kuvakkeiden tallennuspolku',
	'RANKS_PATH_EXPLAIN'	=> 'Polku phpBB:n juurihakemistosta (esim. <samp>images/ranks</samp>).',

	'SCRIPT_PATH'	=> 'Ohjelmiston polku',
	'SCRIPT_PATH_EXPLAIN'	=> 'Polku, jossa phpBB sijaitsee suhteessa verkkotunnukseen (esim. <samp>/phpBB3</samp>).',
	'SERVER_NAME'	=> 'Verkkotunnus',
	'SERVER_NAME_EXPLAIN'	=> 'Verkkotunnus, jolla tämä keskustelupalsta sijaitsee (esim. <samp>www.example.com</samp>).',
	'SERVER_PORT'	=> 'Palvelimen portti',
	'SERVER_PORT_EXPLAIN'	=> 'Palvelimen käyttämä portti. Muuta tätä asetusta vain, jos portti on eri kuin 80.',
	'SERVER_PROTOCOL'	=> 'Palvelimen protokolla',
	'SERVER_PROTOCOL_EXPLAIN'	=> 'Tätä protokollaa käytetään, jos palvelimen URL-asetukset on pakotettu. Jos arvo on tyhjä tai asetuksia ei ole pakotettu, protokolla valitaan suojattujen evästeiden asetuksen perusteella (<samp>http://</samp> tai <samp>https://</samp>).',
	'SERVER_URL_SETTINGS'	=> 'Palvelimen URL-asetukset',
	'SMILIES_PATH'	=> 'Hymiöiden tallennuspolku',
	'SMILIES_PATH_EXPLAIN'	=> 'Polku phpBB:n juurihakemistosta (esim. <samp>images/smilies</samp>).',

	'UPLOAD_ICONS_PATH'	=> 'Tunnisteryhmien ikonien tallennuspolku',
	'UPLOAD_ICONS_PATH_EXPLAIN'	=> 'Polku phpBB:n juurihakemistosta (esim. <samp>images/upload_icons</samp>).',
	'USE_SYSTEM_CRON'		=> 'Käytä järjestelmän ajastinta',
	'USE_SYSTEM_CRON_EXPLAIN'		=> 'Jos toiminto ei ole käytössä, phpBB huolehtii säännöllisesti toistuvien tehtävien suorittamisesta automaattisesti. Jos toiminto on käytössä, phpBB ei ajoita tehtäviä itse, vaan järjestelmän ylläpitäjän täytyy asettaa komento <code>bin/phpbbcli.php cron:run</code> suoritettavaksi järjestelmän ajastimella säännöllisin väliajoin (esim. 5 minuutin välein).',
));

// Security Settings
$lang = array_merge($lang, array(
	'ACP_SECURITY_SETTINGS_EXPLAIN'	=> 'Tässä voit muokata istuntoihin ja kirjautumiseen liittyviä asetuksia.',
	'ALL'	=> 'Kaikki',
	'ALLOW_AUTOLOGIN'	=> 'Salli automaattiset kirjautumiset',
	'ALLOW_AUTOLOGIN_EXPLAIN'	=> 'Määrittää, näytetäänkö ”Muista minut” -vaihtoehto käyttäjille, kun he vierailevat keskustelupalstalla.',
	'ALLOW_PASSWORD_RESET'			=> 'Salli salasanan nollaus (”Unohdin salasanani”)',
	'ALLOW_PASSWORD_RESET_EXPLAIN'	=> 'Määrittää, voivatko käyttäjät käyttää ”Unohdin salasanani” -linkkiä kirjautumissivulla saadakseen tilinsä takaisin hallintaansa. Tämän toiminnon poistaminen käytöstä voi olla aiheellista ulkoisia tunnistautumismenetelmiä käytettäessä.',
	'AUTOLOGIN_LENGTH'	=> 'Automaattisen kirjautumisen avaimen kelpoisuusaika',
	'AUTOLOGIN_LENGTH_EXPLAIN'	=> 'Aika (päivinä), jonka kuluttua automaattisten kirjautumisten avaimet poistetaan. Anna arvoksi 0, jos haluat poistaa tämän toiminnon käytöstä.',

	'BROWSER_VALID'	=> 'Tarkasta selain',
	'BROWSER_VALID_EXPLAIN'	=> 'Ota selaimen tarkastus käyttöön jokaiselle istunnolle. Tämä parantaa tietoturvaa.',

	'CHECK_DNSBL'	=> 'Tarkasta IP-osoite käyttämällä DNS Blackhole -luetteloa',
	'CHECK_DNSBL_EXPLAIN'	=> 'Jos tämä toiminto on käytössä, käyttäjän IP-osoite tarkastetaan seuraavia DNSBL-palveluja käyttäen rekisteröitymisen ja viestien lähettämisen yhteydessä: <a href="http://spamcop.net">spamcop.net</a> ja <a href="http://www.spamhaus.org">www.spamhaus.org</a>. Tämä tarkastus saattaa kestää hetken palvelimen asetuksista riippuen. Jos tarkastus aiheuttaa hidastumista tai tuottaa liikaa virheellisiä tuloksia, kannattaa kytkeä tämä tarkastus pois päältä.',
	'CLASS_B'	=> 'A.B',
	'CLASS_C'	=> 'A.B.C',

	'EMAIL_CHECK_MX'	=> 'Tarkasta, onko sähköpostiosoitteen verkkotunnuksella kelvollinen MX-tietue',
	'EMAIL_CHECK_MX_EXPLAIN'	=> 'Jos tämä toiminto on käytössä, rekisteröitymisten ja profiilimuutosten yhteydessä tarkastetaan, onko annetun sähköpostiosoitteen verkkotunnuksella kelvollista MX-tietuetta.',

	'FORCE_PASS_CHANGE'	=> 'Pakota salasanan vaihto',
	'FORCE_PASS_CHANGE_EXPLAIN'	=> 'Pakota käyttäjät vaihtamaan salasanansa tietyn ajan välein. Anna arvoksi 0, jos haluat poistaa tämän toiminnon käytöstä.',
	'FORM_TIME_MAX'	=> 'Enimmäisaika lomakkeiden lähettämiseen',
	'FORM_TIME_MAX_EXPLAIN'	=> 'Aika, joka käyttäjällä on käytettävissä lomakkeen lähettämiseen. Anna arvoksi -1, jos et halua asettaa ajalle ylärajaa. Huomaa, että lomake voi mitätöityä istunnon vanhentuessa tästä asetuksesta huolimatta.',
	'FORM_SID_GUESTS'	=> 'Sido lomakkeet vierailijoiden istuntoihin',
	'FORM_SID_GUESTS_EXPLAIN'	=> 'Jos tämä toiminto on käytössä, vierailijoille annetut lomakeavaimet ovat istuntokohtaisia. Tämä saattaa aiheuttaa ongelmia joidenkin Internet-palveluntarjoajien kanssa.',
	'FORWARDED_FOR_VALID'	=> 'Tarkasta <var>X_FORWARDED_FOR</var>-otsake',
	'FORWARDED_FOR_VALID_EXPLAIN'	=> 'Istuntoja jatketaan vain, jos lähetetty <var>X_FORWARDED_FOR</var>-otsake on sama kuin aikaisempien pyyntöjen yhteydessä lähetetty otsake. Lisäksi tarkastetaan, onko <var>X_FORWARDED_FOR</var>-otsakkeen sisältämä IP-osoite porttikiellossa.',

	'IP_VALID'	=> 'Tarkasta istunnon IP-osoite',
	'IP_VALID_EXPLAIN'	=> 'Määrittää, kuinka suurta osaa käyttäjän IP-osoitteesta käytetään istunnon tarkastamiseen. <samp>Kaikki</samp>-vaihtoehto vertaa koko osoitetta, <samp>A.B.C</samp> kolmea ensimmäistä oktettia (x.x.x) ja <samp>A.B</samp> kahta ensimmäistä oktettia (x.x). <samp>Ei tarkastusta</samp> -vaihtoehto poistaa tarkastuksen käytöstä. IPv6-osoitteita käytettäessä <samp>A.B.C</samp>-vaihtoehto vertaa neljää ensimmäistä lohkoa ja <samp>A.B</samp> kolmea ensimmäistä lohkoa.',
	'IP_LOGIN_LIMIT_MAX'	=> 'Sallittujen kirjautumisyritysten enimmäismäärä IP-osoitetta kohti',
	'IP_LOGIN_LIMIT_MAX_EXPLAIN'	=> 'Määrittää, kuinka monta kertaa yhdestä IP-osoitteesta voi yrittää kirjautumista, ennen kuin roskapostin torjuntaan tarkoitettu tehtävä otetaan käyttöön. Anna arvoksi 0, jos haluat poistaa tämän toiminnon käytöstä.',
	'IP_LOGIN_LIMIT_TIME'	=> 'IP-osoitekohtaisten kirjautumisyritysten vanhentumisaika',
	'IP_LOGIN_LIMIT_TIME_EXPLAIN'	=> 'Kirjautumisyritykset vanhentuvat tämän ajan umpeuduttua.',
	'IP_LOGIN_LIMIT_USE_FORWARDED'	=> 'Rajoita kirjautumisyrityksiä <var>X_FORWARDED_FOR</var>-otsakkeen perusteella',
	'IP_LOGIN_LIMIT_USE_FORWARDED_EXPLAIN'	=> 'Rajoita kirjautumisyrityksiä <var>X_FORWARDED_FOR</var>-otsakkeen sisältämän arvon perusteella IP-osoitteen sijaan.<br /><em><strong>Varoitus:</strong> Ota tämä toiminto käyttöön vain, jos käytät välityspalvelinta, joka antaa <var>X_FORWARDED_FOR</var>-otsakkeelle luotettavia arvoja.</em>',

	'MAX_LOGIN_ATTEMPTS'	=> 'Sallittujen kirjautumisyritysten enimmäismäärä käyttäjätunnusta kohti',
	'MAX_LOGIN_ATTEMPTS_EXPLAIN'	=> 'Määrittää, kuinka monta kertaa yhdelle käyttäjätilille voi yrittää kirjautua, ennen kuin roskapostin torjuntaan tarkoitettu tehtävä otetaan käyttöön. Anna arvoksi 0, jos haluat poistaa tämän toiminnon käytöstä.',

	'NO_IP_VALIDATION'	=> 'Ei tarkastusta',
	'NO_REF_VALIDATION'	=> 'Ei tarkastusta',

	'PASSWORD_TYPE'	=> 'Salasanan monimutkaisuus',
	'PASSWORD_TYPE_EXPLAIN'	=> 'Määrittää, kuinka monimutkaisen salasanan täytyy olla, kun sitä asetetaan tai muutetaan. Oheisessa pudotusvalikossa olevat vaihtoehdot sisältyvät valikossa alempana oleviin vaihtoehtoihin.',
	'PASS_TYPE_ALPHA'	=> 'Täytyy sisältää kirjaimia ja numeroita',
	'PASS_TYPE_ANY'	=> 'Ei vaatimuksia',
	'PASS_TYPE_CASE'	=> 'Täytyy sisältää sekä pieniä että isoja kirjaimia',
	'PASS_TYPE_SYMBOL'	=> 'Täytyy sisältää symboleja',

	'REF_HOST'	=> 'Tarkasta vain isäntä',
	'REF_PATH'	=> 'Tarkasta myös polku',
	'REFERRER_VALID'	=> 'Tarkasta viittaaja',
	'REFERRER_VALID_EXPLAIN'	=> 'Jos tämä toiminto on käytössä, POST-pyyntöjen viittaajia verrataan isännän ja ohjelmiston polun asetuksiin. Tämä saattaa aiheuttaa ongelmia keskustelupalstoilla, jotka käyttävät useita verkkotunnuksia ja/tai ulkoisia kirjautumisia.',

	'TPL_ALLOW_PHP'	=> 'Salli PHP:n käyttö sivupohjissa',
	'TPL_ALLOW_PHP_EXPLAIN'	=> 'Jos tämä toiminto on käytössä, <code>PHP</code>- ja <code>INCLUDEPHP</code>-lausekkeet tunnistetaan ja suoritetaan sivupohjissa.',
	'UPLOAD_CERT_VALID'				=> 'Vahvista siirron sertifikaatti',
	'UPLOAD_CERT_VALID_EXPLAIN'		=> 'Jos tämä asetus on käytössä, ulkoisten siirtojen sertifikaatit vahvistetaan. Tämä toiminto vaatii, että sertifikaattien myöntäjätiedot on määritetty php.ini-tiedoston <samp>openssl.cafile</samp>- tai <samp>curl.cainfo</samp>-asetuksissa.',
));

// Email Settings
$lang = array_merge($lang, array(
	'ACP_EMAIL_SETTINGS_EXPLAIN'	=> 'Näitä tietoja käytetään, kun keskustelupalsta lähettää sähköpostiviestejä käyttäjillesi. Varmista, että antamasi sähköpostiosoite on kelvollinen, sillä palautuvat ja toimittamatta jääneet viestit lähetetään todennäköisesti tähän osoitteeseen. Jos palveluntarjoajallasi ei ole käytettävissä PHP-pohjaista sähköpostipalvelua, voit vaihtoehtoisesti lähettää viestejä suoraan SMTP:n avulla. Tätä varten tarvitaan sopivan palvelimen osoite (kysy tietoa palveluntarjoajaltasi tarvittaessa). Jos (ja vain jos) palvelin vaatii tunnistautumista, anna tarvittava käyttäjätunnus, salasana ja tunnistautumismenetelmä.',
	'ADMIN_EMAIL'	=> 'Sähköpostin palautusosoite',
	'ADMIN_EMAIL_EXPLAIN'	=> 'Tämä on palautusosoite eli teknisen yhteyshenkilön sähköpostiosoite. Sitä käytetään aina <samp>Return-Path</samp>- ja <samp>Sender</samp>-osoitteina sähköpostiviesteissä.',

	'BOARD_EMAIL_FORM'	=> 'Sähköpostin lähetys keskustelupalstan kautta',
	'BOARD_EMAIL_FORM_EXPLAIN'	=> 'Salli käyttäjien lähettää sähköpostia keskustelupalstan kautta sen sijaan, että heille näytetään muiden käyttäjien sähköpostiosoitteet.',
	'BOARD_HIDE_EMAILS'	=> 'Piilota sähköpostiosoitteet',
	'BOARD_HIDE_EMAILS_EXPLAIN'	=> 'Pidä sähköpostiosoitteet täysin yksityisinä.',

	'CONTACT_EMAIL'	=> 'Yhteyshenkilön sähköpostiosoite',
	'CONTACT_EMAIL_EXPLAIN'	=> 'Tätä osoitetta käytetään aina, kun tarvitaan tieto yhteyshenkilöstä esimerkiksi roskapostin tai virheiden tapauksissa. Sitä käytetään aina <samp>From</samp>- ja <samp>Reply-To</samp>-osoitteina sähköpostiviesteissä.',
	'CONTACT_EMAIL_NAME'			=> 'Yhteyshenkilön nimi',
	'CONTACT_EMAIL_NAME_EXPLAIN'	=> 'Tämä nimi näkyy sähköpostin vastaanottajille. Jos et halua määrittää yhteyshenkilön nimeä, jätä kenttä tyhjäksi.',

	'EMAIL_FUNCTION_NAME'	=> 'Sähköpostifunktion nimi',
	'EMAIL_FUNCTION_NAME_EXPLAIN'	=> 'Sähköpostifunktio, jota käytetään, kun viestejä lähetetään PHP:n kautta.',
	'EMAIL_PACKAGE_SIZE'	=> 'Sähköpostipaketin koko',
	'EMAIL_PACKAGE_SIZE_EXPLAIN'	=> 'Tämä on yhdessä paketissa lähetettävien sähköpostiviestien enimmäismäärä. Tämä asetus koskee sisäistä viestijonoa. Anna arvoksi 0, jos sinulla on ongelmia toimittamatta jääneiden viestien kanssa.',
	'EMAIL_SIG'	=> 'Sähköpostin allekirjoitus',
	'EMAIL_SIG_EXPLAIN'	=> 'Tämä teksti liitetään kaikkiin keskustelupalstan lähettämiin sähköpostiviesteihin.',
	'ENABLE_EMAIL'	=> 'Sähköpostitoiminto käytössä',
	'ENABLE_EMAIL_EXPLAIN'	=> 'Jos tämä toiminto ei ole käytössä, keskustelupalsta ei lähetä ainuttakaan sähköpostiviestiä. <em>Huomaa, että käyttäjätilien aktivointi käyttäjien tai ylläpidon toimesta vaatii, että tämä toiminto on käytössä. Jos aktivointi käyttäjien tai ylläpidon toimesta on tällä hetkellä valittuna aktivointiasetuksissa, tämän toiminnon poistaminen käytöstä estää uusien käyttäjätilien rekisteröimisen.</em>',
	'SEND_TEST_EMAIL'				=> 'Lähetä testiviesti',
	'SEND_TEST_EMAIL_EXPLAIN'		=> 'Lähettää testiviestin tilillesi määritettyyn sähköpostiosoitteeseen.',
	'SMTP_ALLOW_SELF_SIGNED'		=> 'Salli itseallekirjoitetut SSL-sertifikaatit',
	'SMTP_ALLOW_SELF_SIGNED_EXPLAIN'=> 'ävät itseallekirjoitettuja SSL-sertifikaatteja. <strong>Varoitus:</strong> <em>Itseallekirjoitettujen SSL-sertifikaattien salliminen voi aiheuttaa turvallisuusriskejä.</em>',
	'SMTP_AUTH_METHOD'	=> 'SMTP-tunnistautumismenetelmä',
	'SMTP_AUTH_METHOD_EXPLAIN'	=> 'Tätä käytetään vain, jos käyttäjätunnus tai salasana on asetettu. Pyydä apua palveluntarjoajaltasi, jos et ole varma, mitä menetelmää tulisi käyttää.',
	'SMTP_CRAM_MD5'	=> 'CRAM-MD5',
	'SMTP_DIGEST_MD5'	=> 'DIGEST-MD5',
	'SMTP_LOGIN'	=> 'LOGIN',
	'SMTP_PASSWORD'	=> 'SMTP-salasana',
	'SMTP_PASSWORD_EXPLAIN'	=> 'Anna salasana vain, jos SMTP-palvelimesi vaatii sitä.<br /><em><strong>Varoitus:</strong> Tämä salasana tallennetaan tietokantaan salaamattomana ja se näkyy kaikille, joilla on pääsy tietokantaan tai jotka näkevät tämän asetussivun.</em>',
	'SMTP_PLAIN'	=> 'PLAIN',
	'SMTP_POP_BEFORE_SMTP'	=> 'POP-BEFORE-SMTP',
	'SMTP_PORT'	=> 'SMTP-palvelimen portti',
	'SMTP_PORT_EXPLAIN'	=> 'Muuta tätä vain, jos tiedät SMTP-palvelimesi käyttävän toista porttia.',
	'SMTP_SERVER'	=> 'SMTP-palvelimen osoite',
	'SMTP_SERVER_EXPLAIN'			=> 'Syötä myös palvelimesi käyttämä protokolla. Jos käytät SSL:ää, osoitteen täytyy olla muodossa ”ssl://oma.postipalvelimesi.com”.',
	'SMTP_SETTINGS'	=> 'SMTP-asetukset',
	'SMTP_USERNAME'	=> 'SMTP-käyttäjätunnus',
	'SMTP_USERNAME_EXPLAIN'	=> 'Anna käyttäjätunnus vain, jos SMTP-palvelimesi vaatii sitä.',
	'SMTP_VERIFY_PEER'				=> 'Tarkasta SSL-sertifikaatti',
	'SMTP_VERIFY_PEER_EXPLAIN'		=> 'Tarkasta SMTP-palvelimen käyttämä SSL-sertifikaatti. <strong>Varoitus:</strong> <em>Yhteyden muodostaminen vertaiskoneeseen, jonka SSL-sertifikaattia ei ole tarkastettu, voi aiheuttaa turvallisuusriskejä.</em>',
	'SMTP_VERIFY_PEER_NAME'			=> 'Tarkasta SMTP-vertaisnimi',
	'SMTP_VERIFY_PEER_NAME_EXPLAIN'	=> 'Tarkasta SSL- tai TLS-yhteyksiä käyttävien SMTP-palvelinten vertaisnimet. <strong>Varoitus:</strong> <em>Yhteyden muodostaminen tarkastamattomaan vertaiskoneeseen voi aiheuttaa turvallisuusriskejä.</em>',
	'TEST_EMAIL_SENT'				=> 'Testiviesti on lähetetty.<br />Jos et saa viestiä, tarkista sähköpostiasetuksesi.<br /><br />Jos tarvitset apua, käy <a href="https://www.phpbb.com/community/">phpBB:n tukialueella</a>.',

	'USE_SMTP'	=> 'Käytä SMTP-palvelinta sähköpostin lähettämiseen',
	'USE_SMTP_EXPLAIN'	=> 'Valitse Kyllä-vaihtoehto, jos haluat tai sinun on pakko lähettää sähköpostia nimetyn palvelimen kautta paikallisen sähköpostifunktion sijaan.',
));

// Jabber settings
$lang = array_merge($lang, array(
	'ACP_JABBER_SETTINGS_EXPLAIN'	=> 'Tässä voit ottaa Jabberin käyttöön ja hallita, kuinka sitä käytetään pikaviestien ja keskustelupalstan ilmoitusten lähettämiseen. Jabber on avoimen lähdekoodin protokolla ja siten kenen tahansa käytettävissä. Jotkin Jabber-palvelimet sisältävät yhdyskäytäviä tai siirtopalveluja, jotka mahdollistavat yhteydenpidon muissa verkoissa oleviin käyttäjiin. Kaikki palvelimet eivät tarjoa kaikkia siirtopalveluja, minkä lisäksi protokollamuutokset voivat estää siirtopalveluja toimimasta. Varmista, että syötät jo rekisteröidyn tilin tiedot, sillä phpBB käyttää antamiasi tietoja sellaisenaan.',

	'JAB_ALLOW_SELF_SIGNED'			=> 'Salli itseallekirjoitetut SSL-sertifikaatit',
	'JAB_ALLOW_SELF_SIGNED_EXPLAIN'	=> 'Salli yhteydet Jabber-palvelimiin, jotka käyttävät itseallekirjoitettuja SSL-sertifikaatteja. <strong>Varoitus:</strong> <em>Itseallekirjoitettujen SSL-sertifikaattien salliminen voi aiheuttaa turvallisuusriskejä.</em>',
	'JAB_ENABLE'	=> 'Jabber käytössä',
	'JAB_ENABLE_EXPLAIN'	=> 'Ota Jabber-viestintä ja -ilmoitukset käyttöön.',
	'JAB_GTALK_NOTE'	=> 'Huomaa, ettei GTalk toimi, koska <samp>dns_get_record</samp>-funktiota ei löytynyt. Tämä funktio ei ole käytettävissä PHP4-ympäristössä tai Windows-alustoilla. Tällä hetkellä se ei myöskään toimi BSD-pohjaisissa järjestelmissä kuten Mac OS:ssä.',
	'JAB_PACKAGE_SIZE'	=> 'Jabber-paketin koko',
	'JAB_PACKAGE_SIZE_EXPLAIN'	=> 'Määritä yhdessä paketissa lähetettävien viestien lukumäärä. Jos arvo on 0, viestit lähetetään välittömästi sen sijaan, että ne asetettaisiin jonoon myöhempää lähetystä varten.',
	'JAB_PASSWORD'	=> 'Jabber-salasana',
	'JAB_PASSWORD_EXPLAIN'	=> '<em><strong>Varoitus:</strong> Tämä salasana tallennetaan tietokantaan salaamattomana ja se näkyy kaikille, joilla on pääsy tietokantaasi tai jotka näkevät tämän asetussivun.</em>',
	'JAB_PORT'	=> 'Jabber-portti',
	'JAB_PORT_EXPLAIN'	=> 'Jätä kenttä tyhjäksi, ellet ole varma, että portti on muu kuin 5222.',
	'JAB_SERVER'	=> 'Jabber-palvelin',
	'JAB_SERVER_EXPLAIN'	=> 'Näet luettelon palvelimista osoitteessa %sjabber.org%s.',
	'JAB_SETTINGS_CHANGED'	=> 'Jabber-asetukset on muutettu.',
	'JAB_USE_SSL'	=> 'Käytä SSL-yhteyttä',
	'JAB_USE_SSL_EXPLAIN'	=> 'Jos tämä toiminto on käytössä, yhteys yritetään muodostaa suojattuna. Jabber-portiksi vaihdetaan 5223, jos portti 5222 on määritetty.',
	'JAB_USERNAME'	=> 'Jabber-käyttäjätunnus tai JID',
	'JAB_USERNAME_EXPLAIN'	=> 'Anna rekisteröity käyttäjätunnus tai kelvollinen JID. Käyttäjätunnuksen kelvollisuutta ei tarkasteta. Jos annat vain käyttäjätunnuksen, JID:nä käytetään antamaasi käyttäjätunnusta ja palvelimena yllä antamaasi tietoa. Muussa tapauksessa anna kelvollinen JID (esim. user@jabber.org).',
	'JAB_VERIFY_PEER'				=> 'Tarkasta SSL-sertifikaatti',
	'JAB_VERIFY_PEER_EXPLAIN'		=> 'Tarkasta Jabber-palvelimen käyttämä SSL-sertifikaatti. <strong>Varoitus:</strong> <em>Yhteyden muodostaminen vertaiskoneeseen, jonka SSL-sertifikaattia ei ole tarkastettu, voi aiheuttaa turvallisuusriskejä.</em>',
	'JAB_VERIFY_PEER_NAME'			=> 'Tarkasta Jabber-vertaisnimi',
	'JAB_VERIFY_PEER_NAME_EXPLAIN'	=> 'Tarkasta SSL- tai TLS-yhteyksiä käyttävien Jabber-palvelinten vertaisnimet. <strong>Varoitus:</strong> <em>Yhteyden muodostaminen tarkastamattomaan vertaiskoneeseen voi aiheuttaa turvallisuusriskejä.</em>',
));
