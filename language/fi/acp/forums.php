<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

// Forum Admin
$lang = array_merge($lang, array(
	'AUTO_PRUNE_DAYS'	=> 'Automaattisesti siivottavien viestien ikä',
	'AUTO_PRUNE_DAYS_EXPLAIN'	=> 'Määrittää, kuinka monen päivän kuluttua aihe poistetaan viimeisimmän vastauksen jälkeen.',
	'AUTO_PRUNE_FREQ'	=> 'Automaattisen siivouksen tiheys',
	'AUTO_PRUNE_FREQ_EXPLAIN'	=> 'Päivien lukumäärä siivousten välissä.',
	'AUTO_PRUNE_VIEWED'	=> 'Automaattinen siivous viestien lukemisen perusteella',
	'AUTO_PRUNE_VIEWED_EXPLAIN'	=> 'Määrittää, kuinka monen päivän kuluttua aihe poistetaan viimeisimmän lukukerran jälkeen. ',
	'AUTO_PRUNE_SHADOW_FREQ'	=> 'Varjoaiheiden automaattisen siivouksen tiheys',
	'AUTO_PRUNE_SHADOW_DAYS'	=> 'Automaattisesti siivottavien varjoaiheiden ikä',
	'AUTO_PRUNE_SHADOW_DAYS_EXPLAIN'	=> 'Määritä, kuinka monen päivän kuluttua varjoaihe poistetaan.',
	'AUTO_PRUNE_SHADOW_FREQ_EXPLAIN'	=> 'Päivien lukumäärä siivousten välissä.',

	'CONTINUE'	=> 'Jatka',
	'COPY_PERMISSIONS'	=> 'Kopioi oikeudet alueelta',
	'COPY_PERMISSIONS_EXPLAIN'	=> 'Voit helpottaa uuden alueen oikeuksien asettamista kopioimalla oikeudet jo olemassa olevalta alueelta.',
	'COPY_PERMISSIONS_ADD_EXPLAIN'	=> 'Luotava alue saa samat oikeudet kuin jotka on asetettu tässä valitsemallesi alueelle. Jos et valitse aluetta, uusi alue ei näy, ennen kuin sille on asetettu oikeudet.',
	'COPY_PERMISSIONS_EDIT_EXPLAIN'	=> 'Jos kopioit oikeudet, tämä alue saa samat oikeudet kuin jotka on asetettu valitsemallesi alueelle. Tämä korvaa kaikki tälle alueelle aikaisemmin asetetut oikeudet valitsemasi alueen oikeuksilla. Jos et valitse aluetta, nykyiset oikeudet pysyvät voimassa.',
	'COPY_TO_ACL'	=> 'Voit myös %sasettaa uudet oikeudet%s käsin tälle alueelle.',
	'CREATE_FORUM'	=> 'Luo uusi alue',

	'DECIDE_MOVE_DELETE_CONTENT'	=> 'Poista sisältö tai siirrä alueelle',
	'DECIDE_MOVE_DELETE_SUBFORUMS'	=> 'Poista sisäalueet tai siirrä alueelle',
	'DEFAULT_STYLE'	=> 'Oletustyyli',
	'DELETE_ALL_POSTS'	=> 'Poista viestit',
	'DELETE_SUBFORUMS'	=> 'Poista sisäalueet ja viestit',
	'DISPLAY_ACTIVE_TOPICS'	=> 'Aktiiviset aiheet käytössä',
	'DISPLAY_ACTIVE_TOPICS_EXPLAIN'	=> 'Jos valitset Kyllä-vaihtoehdon, valittujen sisäalueiden aktiiviset aiheet näkyvät tässä kategoriassa.',

	'EDIT_FORUM'	=> 'Muokkaa aluetta',
	'ENABLE_INDEXING'	=> 'Hakuindeksointi käytössä',
	'ENABLE_INDEXING_EXPLAIN'	=> 'Jos tämä toiminto on käytössä, tälle alueelle lähetetyt viestit indeksoidaan hakemista varten.',
	'ENABLE_POST_REVIEW'	=> 'Viestien tarkistus käytössä',
	'ENABLE_POST_REVIEW_EXPLAIN'	=> 'Antaa käyttäjille mahdollisuuden korjata kirjoittamiaan viestejä, jos muut käyttäjät lähettivät vastauksia samaan aiheeseen samanaikaisesti. Tämä kannattaa poistaa käytöstä chat-alueilla.',
	'ENABLE_QUICK_REPLY'	=> 'Pikavastaus käytössä',
	'ENABLE_QUICK_REPLY_EXPLAIN'	=> 'Mahdollistaa pikavastauksen käytön tällä alueella. Tällä asetuksella ei ole vaikutusta, jos pikavastaus on poistettu käytöstä koko keskustelupalstalta. Pikavastaus näkyy vain käyttäjille, joille on oikeus kirjoittaa tälle alueelle.',
	'ENABLE_RECENT'	=> 'Näytä aktiiviset aiheet',
	'ENABLE_RECENT_EXPLAIN'	=> 'Jos tämä toiminto on käytössä, tämän alueen aiheet näkyvät aktiivisten aiheiden luettelossa.',
	'ENABLE_TOPIC_ICONS'	=> 'Aiheiden kuvakkeet käytössä',

	'FORUM_ADMIN'	=> 'Alueen ylläpito',
	'FORUM_ADMIN_EXPLAIN'	=> 'PhpBB3:n toiminta perustuu alueisiin. Kategoria on vain alueen erityistyyppi. Jokaisella alueella voi olla rajaton määrä sisäalueita, ja voit valita, mille alueille voi kirjoittaa (ts. mitkä alueet toimivat kuten vanhat kategoriat). Tässä voit lisätä, muokata, poistaa, lukita ja avata yksittäisiä alueita sekä säätää tiettyjä lisäasetuksia. Jos viestisi ja aiheesi ovat epäjärjestyksessä, voit myös synkronoida alueita uudelleen. <strong>Uusille alueille täytyy kopioida tai asettaa sopivat oikeudet, ennen kuin alueet tulevat näkyviin.</strong>',
	'FORUM_AUTO_PRUNE'	=> 'Automaattinen siivous käytössä',
	'FORUM_AUTO_PRUNE_EXPLAIN'	=> 'Siivoaa alueen aiheita. Säädä tiheyden ja aikarajojen asetuksia alla.',
	'FORUM_CREATED'	=> 'Alue on luotu.',
	'FORUM_DATA_NEGATIVE'	=> 'Siivouksen asetukset eivät voi olla negatiivisia.',
	'FORUM_DESC_TOO_LONG'	=> 'Alueen kuvaus on liian pitkä. Kuvauksen täytyy olla alle 4000 merkin pituinen.',
	'FORUM_DELETE'	=> 'Poista alue',
	'FORUM_DELETE_EXPLAIN'	=> 'Voit poistaa alueen alla olevalla lomakkeella. Jos alueelle voi kirjoittaa, voit valita, mihin alueen sisältämät aiheet (tai alueet) siirretään.',
	'FORUM_DELETED'	=> 'Alue on poistettu.',
	'FORUM_DESC'	=> 'Kuvaus',
	'FORUM_DESC_EXPLAIN'	=> 'Kaikki tähän syötetyt HTML-muotoilut näytetään sellaisenaan.',
	'FORUM_EDIT_EXPLAIN'	=> 'Voit muokata tätä aluetta alla olevalla lomakkeella. Huomaa, että valvonnan ja viestien lukumäärän asetukset asetetaan alueen oikeuksien kautta jokaiselle käyttäjälle tai käyttäjäryhmälle.',
	'FORUM_IMAGE'	=> 'Alueen kuva',
	'FORUM_IMAGE_EXPLAIN'	=> 'Tähän alueeseen yhdistetyn lisäkuvan sijainti suhteessa phpBB:n juurihakemistoon.',
	'FORUM_IMAGE_NO_EXIST'	=> 'Määrittämääsi alueen kuvaa ei ole olemassa',
	'FORUM_LINK_EXPLAIN'	=> 'Koko kohdesijainnin URL (protokolla mukaan lukien, esim. <samp>http://</samp>), johon käyttäjä ohjataan tätä aluetta napsauttaessaan. Esim. <samp>http://www.phpbb.com/</samp>.',
	'FORUM_LINK_TRACK'	=> 'Pidä kirjaa linkin uudelleenohjauksista',
	'FORUM_LINK_TRACK_EXPLAIN'	=> 'Seuraa, kuinka monta kertaa aluelinkkiä on napsautettu.',
	'FORUM_NAME'	=> 'Alueen nimi',
	'FORUM_NAME_EMPTY'	=> 'Tälle alueelle täytyy antaa nimi.',
	'FORUM_PARENT'	=> 'Yläalue',
	'FORUM_PASSWORD'	=> 'Alueen salasana',
	'FORUM_PASSWORD_CONFIRM'	=> 'Vahvista alueen salasana',
	'FORUM_PASSWORD_CONFIRM_EXPLAIN'	=> 'Tätä tarvitaan vain, jos asetat alueelle salasanan.',
	'FORUM_PASSWORD_EXPLAIN'	=> 'Määrittää salasanan tälle alueelle. On suositeltavampaa käyttää oikeuksien hallintaa tähän tarkoitukseen.',
	'FORUM_PASSWORD_UNSET'	=> 'Poista alueen salasana',
	'FORUM_PASSWORD_UNSET_EXPLAIN'	=> 'Valitse tämä, jos haluat poistaa alueen salasanan.',
	'FORUM_PASSWORD_OLD'	=> 'Alueen salasanalle on laskettu tiiviste vanhaa menetelmää käyttäen, minkä takia salasana tulisi vaihtaa.',
	'FORUM_PASSWORD_MISMATCH'	=> 'Antamasi salasanat eivät täsmää.',
	'FORUM_PRUNE_SETTINGS'	=> 'Automaattisen siivouksen asetukset',
	'FORUM_PRUNE_SHADOW'				=> 'Varjoaiheiden automaattinen siivous käytössä',
	'FORUM_PRUNE_SHADOW_EXPLAIN'			=> 'Siivoaa varjoaiheet alueelta alla olevan tiheyden ja iän mukaisesti.',
	'FORUM_RESYNCED'	=> 'Alue ”%s” on synkronoitu uudelleen',
	'FORUM_RULES_EXPLAIN'	=> 'Alueen säännöt näkyvät jokaisella alueen sisältämällä sivulla.',
	'FORUM_RULES_LINK'	=> 'Linkki alueen sääntöihin',
	'FORUM_RULES_LINK_EXPLAIN'	=> 'Voit antaa URL:n sivuun tai viestiin, joka sisältää tämän alueen säännöt. Tämä asetus ohittaa alla määritetyt alueen säännöt.',
	'FORUM_RULES_PREVIEW'	=> 'Alueen sääntöjen esikatselu',
	'FORUM_RULES_TOO_LONG'	=> 'Alueen sääntöjen täytyy olla alle 4000 merkin pituiset.',
	'FORUM_SETTINGS'	=> 'Alueen asetukset',
	'FORUM_STATUS'	=> 'Alueen tila',
	'FORUM_STYLE'	=> 'Alueen tyyli',
	'FORUM_TOPICS_PAGE'	=> 'Aiheita yhtä sivua kohti',
	'FORUM_TOPICS_PAGE_EXPLAIN'	=> 'Jos arvo on eri kuin 0, tämä asetus ohittaa yhdellä sivulla näytettävien aiheiden lukumäärän oletusasetuksen.',
	'FORUM_TYPE'	=> 'Alueen tyyppi',
	'FORUM_UPDATED'	=> 'Alueen tiedot on päivitetty.',
	'FORUM_WITH_SUBFORUMS_NOT_TO_LINK'	=> 'Olet muuttamassa linkiksi aluetta, jolle voi kirjoittaa ja joka sisältää sisäalueita. Ennen jatkamista siirrä kaikki sisäalueet pois tältä alueelta, koska linkiksi muuttamisen jälkeen et enää ne tähän alueeseen yhdistettyjä sisäalueita.',

	'GENERAL_FORUM_SETTINGS'	=> 'Yleiset alueen asetukset',

	'LINK'	=> 'Linkki',
	'LIST_INDEX'	=> 'Näytä sisäalue yläalueen selitteessä',
	'LIST_INDEX_EXPLAIN'	=> 'Näyttää tämän alueen etusivulla ja muualla linkkinä yläalueen selitteessä, jos yläalueen Näytä sisäalueet selitteessä -vaihtoehto on valittu.',
	'LIST_SUBFORUMS'	=> 'Näytä sisäalueet selitteessä',
	'LIST_SUBFORUMS_EXPLAIN'	=> 'Näyttää tämän alueen sisäalueet etusivulla ja muualla linkkinä selitteessä, jos sisäalueiden Näytä sisäalue yläalueen selitteessä -vaihtoehto on valittu.',
	'LOCKED'	=> 'Lukittu',

	'MOVE_POSTS_NO_POSTABLE_FORUM'	=> 'Alueelle, jolle yrität siirtää viestejä, ei voi kirjoittaa. Valitse alue, jolle voi kirjoittaa.',
	'MOVE_POSTS_TO'	=> 'Siirrä viestit alueelle',
	'MOVE_SUBFORUMS_TO'	=> 'Siirrä sisäalueet alueelle',

	'NO_DESTINATION_FORUM'	=> 'Et ole valinnut aluetta, jolle sisältö siirretään.',
	'NO_FORUM_ACTION'	=> 'Alueen sisällölle ei ole valittu toimenpidettä.',
	'NO_PARENT'	=> 'Yläaluetta ei ole',
	'NO_PERMISSIONS'	=> 'Älä kopioi oikeuksia',
	'NO_PERMISSION_FORUM_ADD'	=> 'Sinulla ei ole alueiden lisäämiseen vaadittavia oikeuksia.',
	'NO_PERMISSION_FORUM_DELETE'	=> 'Sinulla ei ole alueiden poistamiseen vaadittavia oikeuksia.',

	'PARENT_IS_LINK_FORUM'	=> 'Valitsemasi yläalue on linkki. Linkit eivät voi sisältää muita alueita. Valitse joko kategoria tai alue yläalueeksi.',
	'PARENT_NOT_EXIST'	=> 'Yläaluetta ei ole olemassa.',
	'PRUNE_ANNOUNCEMENTS'	=> 'Siivoa tiedotteet',
	'PRUNE_STICKY'	=> 'Siivoa pysyvät aiheet',
	'PRUNE_OLD_POLLS'	=> 'Siivoa vanhat äänestykset',
	'PRUNE_OLD_POLLS_EXPLAIN'	=> 'Poistaa aiheet, joiden sisältämiin äänestyksiin ei ole osallistuttu Automaattinen siivous viestien iän perusteella -kohdassa määritetyn ajan kuluessa.',

	'REDIRECT_ACL'	=> 'Voit nyt %sasettaa oikeudet%s tälle alueelle.',

	'SYNC_IN_PROGRESS'	=> 'Synkronoidaan aluetta',
	'SYNC_IN_PROGRESS_EXPLAIN'	=> 'Synkronoidaan aiheita väliltä %1$d / %2$d.',

	'TYPE_CAT'	=> 'Kategoria',
	'TYPE_FORUM'	=> 'Alue',
	'TYPE_LINK'	=> 'Linkki',

	'UNLOCKED'	=> 'Avoin',
));
