<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

// User pruning
$lang = array_merge($lang, array(
	'ACP_PRUNE_USERS_EXPLAIN'	=> 'Tässä voit poistaa tai sulkea keskustelupalstasi käyttäjätilejä. Käyttäjiä voi rajata useilla eri tavoilla, kuten esimerkiksi viestien lukumäärän tai aktiivisuuden perusteella. Kriteerejä voi yhdistää rajauksen tarkentamiseksi. Voit esimerkiksi poistaa käyttäjät, joilla on alle kymmenen viestiä ja jotka eivät ole käyneet keskustelupalstalla 1.1.2002 jälkeen. Käytä merkkiä * jokerimerkkinä tekstikentissä. Voit myös ohittaa kriteerien valitsemisen kokonaan syöttämällä luettelon käyttäjistä (kukin omalle rivilleen) tekstikenttään. Käytä tätä toimintoa varoen, sillä poistettuja käyttäjiä ei voi palauttaa!',

	'CRITERIA'				=> 'Kriteerit',

	'DEACTIVATE_DELETE'	=> 'Sulje tai poista',
	'DEACTIVATE_DELETE_EXPLAIN'	=> 'Valitse, haluatko sulkea käyttäjätilit vai poistaa ne kokonaan. Huomaa, että poistettuja käyttäjiä ei voi palauttaa!',
	'DELETE_USERS'	=> 'Poista',
	'DELETE_USER_POSTS'	=> 'Poista siivottujen käyttäjien viestit',
	'DELETE_USER_POSTS_EXPLAIN'	=> 'Poistaa poistettujen käyttäjien lähettämät viestit. Tällä asetuksella ei ole vaikutusta, jos käyttäjätilit ainoastaan suljetaan.',

	'JOINED_EXPLAIN'	=> 'Anna päivämäärä muodossa <kbd>VVVV-KK-PP</kbd>. Täytä molemmat kentät määrittääksesi rajatun aikavälin tai jätä toinen tyhjäksi käyttääksesi avointa aikaväliä.',

	'LAST_ACTIVE_EXPLAIN'	=> 'Anna päivämäärä muodossa <kbd>VVVV-KK-PP</kbd>. Käytä arvoa <kbd>0000-00-00</kbd> siivotaksesi käyttäjät, jotka eivät ole koskaan kirjautuneet sisään. Tällöin <em>Ennen</em>- ja <em>Jälkeen</em>-ehdot jätetään huomioimatta.',

	'POSTS_ON_QUEUE'			=> 'Hyväksymistä odottavat viestit',
	'PRUNE_USERS_GROUP_EXPLAIN'	=> 'Kohdista vain valitun ryhmän käyttäjiin.',
	'PRUNE_USERS_GROUP_NONE'	=> 'Kaikki ryhmät',
	'PRUNE_USERS_LIST'	=> 'Siivottavat käyttäjät',
	'PRUNE_USERS_LIST_DELETE'	=> 'Seuraavat käyttäjätilit poistetaan valitsemiesi kriteerien perusteella. Voit estää yksittäisten tilien poistamisen poistamalla merkinnän käyttäjätunnuksen vieressä olevasta valintaruudusta.',
	'PRUNE_USERS_LIST_DEACTIVATE'	=> 'Seuraavat käyttäjätilit suljetaan valitsemiesi kriteerien perusteella. Voit estää yksittäisten tilien sulkemisen poistamalla merkinnän käyttäjätunnuksen vieressä olevasta valintaruudusta.',

	'SELECT_USERS_EXPLAIN'	=> 'Syötä yksittäiset käyttäjätunnukset tähän. Niitä käytetään yllä olevien kriteerien sijaan. Perustajia ei voi poistaa.',

	'USER_DEACTIVATE_SUCCESS'	=> 'Valitut käyttäjätilit on suljettu.',
	'USER_DELETE_SUCCESS'	=> 'Valitut käyttäjätilit on poistettu.',
	'USER_PRUNE_FAILURE'	=> 'Antamillasi kriteereillä ei löytynyt käyttäjiä.',

	'WRONG_ACTIVE_JOINED_DATE'	=> 'Antamasi päivämäärä on virheellinen. Sen täytyy olla muodossa <kbd>VVVV-KK-PP</kbd>.',
));

// Forum Pruning
$lang = array_merge($lang, array(
	'ACP_PRUNE_FORUMS_EXPLAIN'	=> 'Tässä voit poistaa aiheita, joihin ei ole vastattu tai joita ei ole luettu valitsemasi ajan sisällä. Jos et syötä lukua, kaikki aiheet poistetaan. Käynnissä olevia äänestyksiä sisältäviä aiheita sekä pysyviä aiheita ja tiedotteita ei poisteta oletusarvoisesti.',

	'FORUM_PRUNE'	=> 'Alueiden siivous',

	'NO_PRUNE'	=> 'Ainuttakaan aluetta ei siivottu.',

	'SELECTED_FORUM'	=> 'Valittu alue',
	'SELECTED_FORUMS'	=> 'Valitut alueet',

	'POSTS_PRUNED'	=> 'Siivotut viestit',
	'PRUNE_ANNOUNCEMENTS'	=> 'Siivoa tiedotteet',
	'PRUNE_FINISHED_POLLS'	=> 'Siivoa päättyneet äänestykset',
	'PRUNE_FINISHED_POLLS_EXPLAIN'	=> 'Poistaa aiheet, joiden sisältämät äänestykset ovat päättyneet.',
	'PRUNE_FORUM_CONFIRM'	=> 'Haluatko varmasti siivota valitsemasi alueet määritetyillä asetuksilla? Poistettuja viestejä ja aiheita ei voi palauttaa.',
	'PRUNE_NOT_POSTED'	=> 'Päivää viimeisimmän vastauksen jälkeen',
	'PRUNE_NOT_VIEWED'	=> 'Päivää viimeisimmän lukukerran jälkeen',
	'PRUNE_OLD_POLLS'	=> 'Siivoa vanhat äänestykset',
	'PRUNE_OLD_POLLS_EXPLAIN'	=> 'Poistaa aiheet, joiden sisältämiin äänestyksiin ei ole osallistuttu annetun aikavälin sisällä.',
	'PRUNE_STICKY'	=> 'Siivoa pysyvät aiheet',
	'PRUNE_SUCCESS'	=> 'Alueet on siivottu.',

	'TOPICS_PRUNED'	=> 'Siivotut aiheet',
));
