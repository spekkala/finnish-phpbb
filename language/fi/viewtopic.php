<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'APPROVE'								=> 'Hyväksy',
	'ATTACHMENT'	=> 'Liite',
	'ATTACHMENT_FUNCTIONALITY_DISABLED'	=> 'Liitetiedostot eivät ole käytettävissä.',

	'BOOKMARK_ADDED'	=> 'Kirjanmerkki on lisätty.',
	'BOOKMARK_ERR'	=> 'Kirjanmerkin lisääminen epäonnistui. Ole hyvä ja yritä uudelleen.',
	'BOOKMARK_REMOVED'	=> 'Kirjanmerkki on poistettu.',
	'BOOKMARK_TOPIC'	=> 'Lisää kirjanmerkki',
	'BOOKMARK_TOPIC_REMOVE'	=> 'Poista kirjanmerkki',
	'BUMPED_BY'	=> '%1$s on tönäissyt aihetta viimeksi %2$s.',
	'BUMP_TOPIC'	=> 'Tönäise aihetta',

	'CODE'	=> 'Koodi',

	'DELETE_TOPIC'	=> 'Poista aihe',
	'DELETED_INFORMATION'	=> '%1$s poisti aiheen %2$s',
	'DISAPPROVE'					=> 'Hylkää',
	'DOWNLOAD_NOTICE'	=> 'Sinulla ei ole liitetiedostojen näkemiseen vaadittavia oikeuksia.',

	'EDITED_TIMES_TOTAL'	=> array(
		1	=> 'Viestiä on viimeksi muokannut %2$s (%3$s). Muokattu yhteensä %1$d kerran.',
		2	=> 'Viestiä on viimeksi muokannut %2$s (%3$s). Muokattu yhteensä %1$d kertaa.',
	),
	'EMAIL_TOPIC'	=> 'Lähetä kaverille',
	'ERROR_NO_ATTACHMENT'	=> 'Valitsemaasi liitetiedostoa ei ole enää olemassa.',

	'FILE_NOT_FOUND_404'	=> 'Tiedostoa <strong>%s</strong> ei ole olemassa.',
	'FORK_TOPIC'	=> 'Kopioi aihe',
	'FULL_EDITOR'	=> 'Täysi editori ja esikatselu',

	'LINKAGE_FORBIDDEN'	=> 'Sinulla ei ole oikeuksia nähdä, ladata tai linkittää tältä sivustolta tai tälle sivustolle.',
	'LOGIN_NOTIFY_TOPIC'	=> 'Sinulle on ilmoitettu tästä aiheesta. Kirjaudu sisään katsoaksesi sen sisältöä.',
	'LOGIN_VIEWTOPIC'	=> 'Sinun täytyy olla rekisteröitynyt ja kirjautunut sisään nähdäksesi tämän aiheen.',

	'MAKE_ANNOUNCE'	=> 'Muuta tiedotteeksi',
	'MAKE_GLOBAL'	=> 'Muuta yleistiedotteeksi',
	'MAKE_NORMAL'	=> 'Muuta tavalliseksi aiheeksi',
	'MAKE_STICKY'	=> 'Muuta pysyväksi',
	'MAX_OPTIONS_SELECT'		=> array(
		1	=> 'Voit valita vain <strong>1</strong> vaihtoehdon',
		2	=> 'Voit valita enintään <strong>%d</strong> vaihtoehtoa',
	),
	'MISSING_INLINE_ATTACHMENT'	=> 'Liitetiedosto <strong>%s</strong> ei ole enää saatavilla',
	'MOVE_TOPIC'	=> 'Siirrä aihe',

	'NO_ATTACHMENT_SELECTED'	=> 'Et ole valinnut liitetiedostoa ladattavaksi tai katsottavaksi.',
	'NO_NEWER_TOPICS'	=> 'Tällä alueella ei ole uudempia aiheita.',
	'NO_OLDER_TOPICS'	=> 'Tällä alueella ei ole vanhempia aiheita.',
	'NO_UNREAD_POSTS'	=> 'Tässä aiheessa ei ole uusia viestejä.',
	'NO_VOTE_OPTION'	=> 'Valitse vaihtoehto, jota haluat äänestää.',
	'NO_VOTES'	=> 'Ääniä ei ole annettu',

	'POLL_ENDED_AT'	=> 'Äänestys päättyi %s',
	'POLL_RUN_TILL'	=> 'Äänestys on voimassa %s asti',
	'POLL_VOTED_OPTION'	=> 'Äänestit tätä vaihtoehtoa',
	'POST_DELETED_RESTORE'	=> 'Tämä viesti on poistettu, mutta sen voi palauttaa.',
	'PRINT_TOPIC'	=> 'Tulostusnäkymä',

	'QUICK_MOD'	=> 'Valvojan työkalut',
	'QUICKREPLY'	=> 'Pikavastaus',
	'QUOTE'	=> 'Lainaa',

	'REPLY_TO_TOPIC'	=> 'Vastaa aiheeseen',
	'RESTORE'				=> 'Palauta',
	'RESTORE_TOPIC'			=> 'Palauta aihe',
	'RETURN_POST'	=> '%sPalaa viestiin%s',

	'SUBMIT_VOTE'	=> 'Äänestä',

	'TOPIC_TOOLS'			=> 'Aiheen hallinta',
	'TOTAL_VOTES'	=> 'Ääniä yhteensä',

	'UNLOCK_TOPIC'	=> 'Avaa aiheen lukitus',

	'VIEW_INFO'	=> 'Viestin lisätiedot',
	'VIEW_NEXT_TOPIC'	=> 'Seuraava aihe',
	'VIEW_PREVIOUS_TOPIC'	=> 'Edellinen aihe',
	'VIEW_RESULTS'	=> 'Näytä tulokset',
	'VIEW_TOPIC_POSTS'		=> array(
		1	=> '%d viesti',
		2	=> '%d viestiä',
	),
	'VIEW_UNREAD_POST'	=> 'Ensimmäinen lukematon viesti',
	'VOTE_SUBMITTED'	=> 'Äänesi on kirjattu.',
	'VOTE_CONVERTED'	=> 'Äänestysvaihtoehdon vaihtaminen ei ole mahdollista muunnetuissa äänestyksissä.',
));
