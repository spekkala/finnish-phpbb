<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ALL_AVAILABLE'	=> 'Kaikki saatavilla olevat',
	'ALL_RESULTS'	=> 'Kaikki tulokset',

	'DISPLAY_RESULTS'	=> 'Näytä tulokset muodossa',

	'FOUND_SEARCH_MATCHES'		=> array(
		1	=> 'Haulla löytyi %d tulos',
		2	=> 'Haulla löytyi %d tulosta',
	),
	'FOUND_MORE_SEARCH_MATCHES'		=> array(
		1	=> 'Haulla löytyi enemmän kuin %d tulos',
		2	=> 'Haulla löytyi enemmän kuin %d tulosta',
	),

	'GLOBAL'	=> 'Yleistiedote',

	'IGNORED_TERMS'	=> 'Huomiotta jätetyt',
	'IGNORED_TERMS_EXPLAIN'	=> 'Seuraavat sanat jätettiin pois hausta, koska ne ovat liian yleisiä sanoja: <strong>%s</strong>',

	'JUMP_TO_POST'	=> 'Siirry viestiin',

	'LOGIN_EXPLAIN_EGOSEARCH'	=> 'Kirjaudu sisään katsoaksesi omia viestejäsi.',
	'LOGIN_EXPLAIN_UNREADSEARCH'	=> 'Kirjaudu sisään katsoaksesi lukemattomia viestejäsi.',
	'LOGIN_EXPLAIN_NEWPOSTS'			=> 'Kirjaudu sisään katsoaksesi edellisen käynnin jälkeen tulleita viestejä.',

	'MAX_NUM_SEARCH_KEYWORDS_REFINE'	=> array(
		1	=> 'Käytit liian monta hakusanaa. Voit antaa enintään %1$d sanan.',
		2	=> 'Käytit liian monta hakusanaa. Voit antaa enintään %1$d sanaa.',
	),

	'NO_KEYWORDS'	=> 'Anna ainakin yksi hakusana. Jokaisen sanan täytyy sisältää vähintään %s ja enintään %s lukuun ottamatta jokerimerkkejä.',
	'NO_RECENT_SEARCHES'	=> 'Hakuja ei ole suoritettu viime aikoina.',
	'NO_SEARCH'	=> 'Valitettavasti sinulla ei ole oikeuksia käyttää hakutoimintoa.',
	'NO_SEARCH_RESULTS'	=> 'Vastaavuuksia ei löytynyt.',
	'NO_SEARCH_LOAD'		=> 'Hakutoiminto ei ole käytettävissä tällä hetkellä, koska palvelin on ylikuormittunut. Yritä myöhemmin uudelleen.',
	'NO_SEARCH_TIME'		=> array(
		1	=> 'Hakutoiminto ei ole käytettävissä tällä hetkellä. Yritä uudelleen %d sekunnin kuluttua.',
		2	=> 'Hakutoiminto ei ole käytettävissä tällä hetkellä. Yritä uudelleen %d sekunnin kuluttua.',
	),
	'NO_SEARCH_UNREADS'	=> 'Lukemattomien viestien haku on poistettu käytöstä tällä keskustelupalstalla.',

	'WORD_IN_NO_POST'	=> 'Tuloksia ei löytynyt, koska sanaa <strong>%s</strong> ei ole yhdessäkään viestissä.',
	'WORDS_IN_NO_POST'	=> 'Tuloksia ei löytynyt, koska sanoja <strong>%s</strong> ei ole yhdessäkään viestissä.',

	'POST_CHARACTERS'	=> 'merkkiä viestistä',
	'PHRASE_SEARCH_DISABLED'	=> 'Täsmälliseen lauseeseen perustuva haku ei ole käytettävissä tällä keskustelupalstalla.',

	'RECENT_SEARCHES'	=> 'Viimeisimmät haut',
	'RESULT_DAYS'	=> 'Rajaa tulokset',
	'RESULT_SORT'	=> 'Lajittele tulokset',
	'RETURN_FIRST'	=> 'Palauta ensimmäiset',
	'GO_TO_SEARCH_ADV'	=> 'Siirry tarkennettuun hakuun',

	'SEARCHED_FOR'	=> 'Käytetyt hakusanat',
	'SEARCHED_TOPIC'	=> 'Haetut aiheet',
	'SEARCHED_QUERY' => 'Haettu kysely',
	'SEARCH_ALL_TERMS'	=> 'Hae kaikilla sanoilla tai käytä hakua sellaisenaan',
	'SEARCH_ANY_TERMS'	=> 'Hae kaikilla vaihtoehdoilla',
	'SEARCH_AUTHOR'	=> 'Hae käyttäjätunnuksella',
	'SEARCH_AUTHOR_EXPLAIN'	=> 'Käytä merkkiä * jokerimerkkinä osittaisissa hauissa.',
	'SEARCH_FIRST_POST'	=> 'Aiheen ensimmäinen viesti',
	'SEARCH_FORUMS'	=> 'Haettavat alueet',
	'SEARCH_FORUMS_EXPLAIN'	=> 'Valitse alueet, joista haluat etsiä.',
	'SEARCH_IN_RESULTS'	=> 'Etsi näistä tuloksista',
	'SEARCH_KEYWORDS_EXPLAIN'	=> 'Aseta <strong>+</strong> sellaisen sanan eteen, jonka on löydyttävä, ja <strong>-</strong> sellaisen sanan eteen, jota ei saa löytyä. Laita haettavat sanat sulkuihin <strong>|</strong>-merkillä erotettuina, mikäli vain yhden sanan on löydyttävä. Käytä merkkiä * jokerimerkkinä osittaisissa hauissa.',
	'SEARCH_MSG_ONLY'	=> 'Vain viestin sisältö',
	'SEARCH_OPTIONS'	=> 'Haun asetukset',
	'SEARCH_QUERY'	=> 'Hakulauseke',
	'SEARCH_SUBFORUMS'	=> 'Hae sisäalueista',
	'SEARCH_TITLE_MSG'	=> 'Viestin otsikko ja sisältö',
	'SEARCH_TITLE_ONLY'	=> 'Vain viestin otsikko',
	'SEARCH_WITHIN'	=> 'Haettavat kohteet',
	'SORT_ASCENDING'	=> 'Nouseva',
	'SORT_AUTHOR'	=> 'Kirjoittaja',
	'SORT_DESCENDING'	=> 'Laskeva',
	'SORT_FORUM'	=> 'Alue',
	'SORT_POST_SUBJECT'	=> 'Viestin aihe',
	'SORT_TIME'	=> 'Viestin aika',
	'SPHINX_SEARCH_FAILED'		=> 'Haku epäonnistui: %s',
	'SPHINX_SEARCH_FAILED_LOG'	=> 'Valitettavasti haku epäonnistui. Virheen lisätiedot on kirjattu virhelokiin.',

	'TOO_FEW_AUTHOR_CHARS'	=> array(
		1	=> 'Anna ainakin %d merkki kirjoittajan nimestä.',
		2	=> 'Anna ainakin %d merkkiä kirjoittajan nimestä.',
	),
));
