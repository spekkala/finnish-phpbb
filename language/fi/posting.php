<?php
/**
*
* This file is part of a Finnish language pack.
*
* @author Sami Pekkala
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ADD_ATTACHMENT'	=> 'Lisää liitetiedosto',
	'ADD_ATTACHMENT_EXPLAIN'	=> 'Voit lisätä yhden tai useamman tiedoston.',
	'ADD_FILE'	=> 'Lisää tiedosto',
	'ADD_POLL'	=> 'Luo äänestys',
	'ADD_POLL_EXPLAIN'	=> 'Jos et halua lisätä äänestystä, jätä kentät tyhjiksi.',
	'ALREADY_DELETED'	=> 'Valitettavasti viesti on jo poistettu.',
	'ATTACH_DISK_FULL' => 'Vapaata kiintolevytilaa ei ole tarpeeksi tätä liitetiedostoa varten.',
	'ATTACH_QUOTA_REACHED'	=> 'Valitettavasti liitetiedostoille varattu kiintiö on täynnä.',
	'ATTACH_SIG'	=> 'Lisää allekirjoitus (voit muokata allekirjoitustasi käyttäjän hallintapaneelissa)',

	'BBCODE_A_HELP'	=> 'Näytä liitetiedosto viestin yhteydessä: [attachment=]tiedostonimi.ext[/attachment]',
	'BBCODE_B_HELP'	=> 'Lihavointi: [b]teksti[/b]',
	'BBCODE_C_HELP'	=> 'Koodi: [code]koodi[/code]',
	'BBCODE_D_HELP' => 'Flash: [flash=leveys,korkeus]http://osoite[/flash]',
	'BBCODE_F_HELP'	=> 'Fontin koko: [size=85]pientä tekstiä[/size]',
	'BBCODE_IS_OFF'	=> '%sBBCode%s <em>ei ole käytössä</em>',
	'BBCODE_IS_ON'	=> '%sBBCode%s <em>on käytössä</em>',
	'BBCODE_I_HELP'	=> 'Kursivointi: [i]teksti[/i]',
	'BBCODE_L_HELP'	=> 'Luettelo: [list][*]teksti[/list]',
	'BBCODE_LISTITEM_HELP'	=> 'Luettelon kohta: [*]teksti',
	'BBCODE_O_HELP'	=> 'Järjestetty luettelo: esim. [list=1][*]Ensimmäinen kohta[/list] tai [list=a][*]Kohta a[/list]',
	'BBCODE_P_HELP'	=> 'Lisää kuva: [img]http://kuvan_osoite[/img]',
	'BBCODE_Q_HELP'	=> 'Lainaa tekstiä: [quote]teksti[/quote]',
	'BBCODE_S_HELP'	=> 'Fontin väri: [color=red]teksti[/color] tai [color=#FF0000]teksti[/color]',
	'BBCODE_U_HELP'	=> 'Alleviivaus: [u]teksti[/u]',
	'BBCODE_W_HELP'	=> 'Lisää linkki: [url]http://osoite[/url] tai [url=http://osoite]linkin teksti[/url]',
	'BBCODE_Y_HELP' => 'Luettelo: Lisää kohta luetteloon',
	'BUMP_ERROR'	=> 'Et voi tönäistä tätä aihetta näin pian viimeisimmän viestin jälkeen.',

	'CANNOT_DELETE_REPLIED'	=> 'Valitettavasti voit poistaa vain viestejä, joihin ei ole vastattu.',
	'CANNOT_EDIT_POST_LOCKED'	=> 'Viesti on lukittu. Et voi enää muokata viestiä.',
	'CANNOT_EDIT_TIME'	=> 'Et voi enää muokata tai poistaa tätä viestiä.',
	'CANNOT_POST_ANNOUNCE'	=> 'Valitettavasti et voi kirjoittaa tiedotteita.',
	'CANNOT_POST_STICKY'	=> 'Valitettavasti et voi kirjoittaa pysyviä viestejä.',
	'CHANGE_TOPIC_TO'	=> 'Vaihda aiheen tyyppi',
	'CHARS_POST_CONTAINS'		=> array(
		1	=> 'Viestisi sisältää %1$d merkin.',
		2	=> 'Viestisi sisältää %1$d merkkiä.',
	),
	'CHARS_SIG_CONTAINS'		=> array(
		1	=> 'Allekirjoituksesi sisältää %1$d merkin.',
		2	=> 'Allekirjoituksesi sisältää %1$d merkkiä.',
	),
	'CLOSE_TAGS'	=> 'Sulje tagit',
	'CURRENT_TOPIC'	=> 'Nykyinen aihe',

	'DELETE_FILE'	=> 'Poista tiedosto',
	'DELETE_MESSAGE'	=> 'Poista viesti',
	'DELETE_MESSAGE_CONFIRM'	=> 'Haluatko varmasti poistaa tämän viestin?',
	'DELETE_OWN_POSTS'	=> 'Valitettavasti voit poistaa vain omia viestejäsi.',
	'DELETE_PERMANENTLY'		=> 'Poista lopullisesti',
	'DELETE_POST_CONFIRM'	=> 'Haluatko varmasti poistaa tämän viestin?',
	'DELETE_POST_PERMANENTLY_CONFIRM'	=> 'Haluatko varmasti poistaa tämän viestin <strong>lopullisesti</strong>?',
	'DELETE_POST_PERMANENTLY'	=> array(
		1	=> 'Poista tämä viesti lopullisesti, minkä jälkeen sitä ei voi enää palauttaa.',
		2	=> 'Poista %1$d viestiä lopullisesti, minkä jälkeen niitä ei voi enää palauttaa.',
	),
	'DELETE_POSTS_CONFIRM'		=> 'Haluatko varmasti poistaa nämä viestit?',
	'DELETE_POSTS_PERMANENTLY_CONFIRM'	=> 'Haluatko varmasti poistaa nämä viestit <strong>lopullisesti</strong>?',
	'DELETE_REASON'				=> 'Poistamisen syy',
	'DELETE_REASON_EXPLAIN'		=> 'Annettu syy näkyy valvojille.',
	'DELETE_POST_WARN'	=> 'Poista tämä viesti',
	'DELETE_TOPIC_CONFIRM'		=> 'Haluatko varmasti poistaa tämän aiheen?',
	'DELETE_TOPIC_PERMANENTLY'	=> 'Poista tämä aihe lopullisesti, minkä jälkeen aihetta ei voi enää palauttaa.',
	'DELETE_TOPIC_PERMANENTLY'	=> array(
		1	=> 'Poista tämä aihe lopullisesti, minkä jälkeen sitä ei voi enää palauttaa.',
		2	=> 'Poista %1$d aihetta lopullisesti, minkä jälkeen niitä ei voi enää palauttaa.',
	),
	'DELETE_TOPIC_PERMANENTLY_CONFIRM'	=> 'Haluatko varmasti poistaa tämän aiheen <strong>lopullisesti</strong>?',
	'DELETE_TOPICS_CONFIRM'		=> 'Haluatko varmasti poistaa nämä aiheet?',
	'DELETE_TOPICS_PERMANENTLY_CONFIRM'	=> 'Haluatko varmasti poistaa nämä aiheet <strong>lopullisesti</strong>?',
	'DISABLE_BBCODE'	=> 'Poista BBCode käytöstä',
	'DISABLE_MAGIC_URL'	=> 'Älä luo linkkejä automaattisesti',
	'DISABLE_SMILIES'	=> 'Poista hymiöt käytöstä',
	'DISALLOWED_CONTENT'	=> 'Tiedoston siirto hylättiin, koska tiedosto tunnistettiin mahdolliseksi hyökkäykseksi.',
	'DISALLOWED_EXTENSION'	=> 'Tiedostopääte %s ei ole sallittu.',
	'DRAFT_LOADED'	=> 'Luonnos on ladattu viestikenttään. Voit viimeistellä viestisi nyt.<br />Luonnoksesi poistetaan, kun olet lähettänyt viestin.',
	'DRAFT_LOADED_PM'	=> 'Luonnos on ladattu viestikenttään. Voit viimeistellä yksityisviestisi nyt.<br />Luonnoksesi poistetaan, kun olet lähettänyt viestin.',
	'DRAFT_SAVED'	=> 'Luonnos on tallennettu.',
	'DRAFT_TITLE'	=> 'Luonnoksen otsikko',

	'EDIT_REASON'	=> 'Viestin muokkauksen syy',
	'EMPTY_FILEUPLOAD'	=> 'Siirretty tiedosto on tyhjä.',
	'EMPTY_MESSAGE'	=> 'Viesti ei voi olla tyhjä.',
	'EMPTY_REMOTE_DATA'	=> 'Tiedoston siirto epäonnistui. Yritä siirtää tiedosto käsin.',

	'FLASH_IS_OFF'	=> '[flash] <em>ei ole käytössä</em>',
	'FLASH_IS_ON'	=> '[flash] <em>on käytössä</em>',
	'FLOOD_ERROR'	=> 'Et voi kirjoittaa uutta viestiä näin pian edellisen viestisi jälkeen.',
	'FONT_COLOR'	=> 'Fontin väri',
	'FONT_COLOR_HIDE'	=> 'Piilota fontin värit',
	'FONT_HUGE'	=> 'Erittäin suuri',
	'FONT_LARGE'	=> 'Suuri',
	'FONT_NORMAL'	=> 'Normaali',
	'FONT_SIZE'	=> 'Fontin koko',
	'FONT_SMALL'	=> 'Pieni',
	'FONT_TINY'	=> 'Erittäin pieni',

	'GENERAL_UPLOAD_ERROR'	=> 'Liitetiedoston siirto kohteeseen %s epäonnistui.',

	'IMAGES_ARE_OFF'	=> '[img] <em>ei ole käytössä</em>',
	'IMAGES_ARE_ON'	=> '[img] <em>on käytössä</em>',
	'INVALID_FILENAME'	=> '%s ei ole kelvollinen tiedostonimi.',

	'LOAD'	=> 'Lataa',
	'LOAD_DRAFT'	=> 'Lataa luonnos',
	'LOAD_DRAFT_EXPLAIN'	=> 'Valitse luonnos, jonka kirjoittamista haluat jatkaa. Tämänhetkisen viestin lähetys keskeytetään ja sen sisältö poistetaan. Voit katsoa, muokata ja poistaa luonnoksiasi käyttäjän hallintapaneelissa.',
	'LOGIN_EXPLAIN_BUMP'	=> 'Kirjaudu sisään tönäistäksesi aiheita tällä alueella.',
	'LOGIN_EXPLAIN_DELETE'	=> 'Kirjaudu sisään poistaaksesi viestejä tältä alueelta.',
	'LOGIN_EXPLAIN_POST'	=> 'Kirjaudu sisään kirjoittaaksesi viestejä tälle alueelle.',
	'LOGIN_EXPLAIN_QUOTE'	=> 'Kirjaudu sisään lainataksesi viestejä tältä alueelta.',
	'LOGIN_EXPLAIN_REPLY'	=> 'Kirjaudu sisään vastataksesi viesteihin tällä alueella.',

	'MAX_FONT_SIZE_EXCEEDED'	=> 'Suurin sallittu fontin koko on %d.',
	'MAX_FLASH_HEIGHT_EXCEEDED'	=> array(
		1	=> 'Flash-tiedostot saavat olla enintään %1$d pikselin korkuisia.',
		2	=> 'Flash-tiedostot saavat olla enintään %1$d pikseliä korkeita.',
	),
	'MAX_FLASH_WIDTH_EXCEEDED'	=> array(
		1	=> 'Flash-tiedostot saavat olla enintään %1$d pikselin levyisiä.',
		2	=> 'Flash-tiedostot saavat olla enintään %1$d pikseliä leveitä.',
	),
	'MAX_IMG_HEIGHT_EXCEEDED'	=> array(
		1	=> 'Kuvat saavat olla enintään %1$d pikselin korkuisia.',
		2	=> 'Kuvat saavat olla enintään %1$d pikseliä korkeita.',
	),
	'MAX_IMG_WIDTH_EXCEEDED'	=> array(
		1	=> 'Kuvat saavat olla enintään %1$d pikselin levyisiä.',
		2	=> 'Kuvat saavat olla enintään %1$d pikseliä leveitä.',
	),
	'MESSAGE_BODY_EXPLAIN'		=> array(
		0	=> '', // zero means no limit, so we don't view a message here.
		1	=> 'Kirjoita viestisi tähän. Viesti voi sisältää enintään <strong>%d</strong> merkin.',
		2	=> 'Kirjoita viestisi tähän. Viesti voi sisältää enintään <strong>%d</strong> merkkiä.',
	),
	'MESSAGE_DELETED'	=> 'Viesti on poistettu.',
	'MORE_SMILIES'	=> 'Lisää hymiöitä',

	'NOTIFY_REPLY'	=> 'Ilmoita, kun viestiin vastataan',
	'NOT_UPLOADED'	=> 'Tiedoston siirto epäonnistui.',
	'NO_DELETE_POLL_OPTIONS'	=> 'Et voi poistaa olemassa olevia äänestysvaihtoehtoja.',
	'NO_PM_ICON'	=> 'Ei mitään',
	'NO_POLL_TITLE'	=> 'Anna äänestykselle otsikko.',
	'NO_POST'	=> 'Pyydettyä viestiä ei ole olemassa.',
	'NO_POST_MODE'	=> 'Viestin muotoa ei ole määritetty.',
	'NO_TEMP_DIR'				=> 'Väliaikaista hakemistoa ei ole tai siihen ei voi kirjoittaa.',

	'PARTIAL_UPLOAD'	=> 'Tiedoston siirto onnistui vain osittain.',
	'PHP_SIZE_NA'	=> 'Liitetiedosto on liian suuri.<br />Tiedostossa php.ini määritetyn maksimikoon selvitys epäonnistui.',
	'PHP_SIZE_OVERRUN'	=> 'Liitetiedosto on liian suuri. Suurin sallittu koko on %1$d %2$s.<br />Huomaa, että tämä rajoitus on asetetettu php.ini-tiedostossa eikä sitä voi ohittaa.',
	'PHP_UPLOAD_STOPPED'		=> 'PHP-laajennus keskeytti tiedoston siirron.',
	'PLACE_INLINE'	=> 'Lisää osaksi viestin sisältöä',
	'POLL_DELETE'	=> 'Poista äänestys',
	'POLL_FOR'	=> 'Äänestys on voimassa',
	'POLL_FOR_EXPLAIN'	=> 'Anna arvoksi 0, jos et halua asettaa äänestykselle aikarajaa.',
	'POLL_MAX_OPTIONS'	=> 'Valittavien vaihtoehtojen enimmäismäärä',
	'POLL_MAX_OPTIONS_EXPLAIN'	=> 'Jokainen käyttäjä voi valita tämän verran vaihtoehtoja.',
	'POLL_OPTIONS'	=> 'Äänestyksen vaihtoehdot',
	'POLL_OPTIONS_EXPLAIN'		=> array(
		1	=> 'Aseta jokainen vaihtoehto omalle rivilleen. Voit syöttää <strong>%d</strong> vaihtoehdon.',
		2	=> 'Aseta jokainen vaihtoehto omalle rivilleen. Voit syöttää enintään <strong>%d</strong> vaihtoehtoa.',
	),
	'POLL_OPTIONS_EDIT_EXPLAIN'		=> array(
		1	=> 'Aseta jokainen vaihtoehto omalle rivilleen. Voit syöttää <strong>%d</strong> vaihtoehdon. Jos poistat tai lisäät vaihtoehtoja, kaikki annetut äänet nollataan.',
		2	=> 'Aseta jokainen vaihtoehto omalle rivilleen. Voit syöttää enintään <strong>%d</strong> vaihtoehtoa. Jos poistat tai lisäät vaihtoehtoja, kaikki annetut äänet nollataan.',
	),
	'POLL_QUESTION'	=> 'Äänestyksen kysymys',
	'POLL_TITLE_TOO_LONG'	=> 'Äänestyksen otsikon täytyy olla alle 100 merkkiä pitkä.',
	'POLL_TITLE_COMP_TOO_LONG'	=> 'Äänestyksen otsikon lopullinen koko on liian suuri. Harkitse BBCode-tagien tai hymiöiden poistamista.',
	'POLL_VOTE_CHANGE'	=> 'Salli äänen vaihto',
	'POLL_VOTE_CHANGE_EXPLAIN'	=> 'Jos sallittu, käyttäjät voivat muuttaa antamaansa ääntä jälkikäteen.',
	'POSTED_ATTACHMENTS'	=> 'Lähetetyt liitetiedostot',
	'POST_APPROVAL_NOTIFY'	=> 'Saat ilmoituksen, kun viestisi on hyväksytty.',
	'POST_CONFIRMATION'	=> 'Viestin varmistus',
	'POST_CONFIRM_EXPLAIN'	=> 'Keskustelupalsta vaatii vahvistuskoodin syöttämisen automaattisesti lähetettyjen viestien estämiseksi. Koodin pitäisi näkyä alla olevassa kuvassa. Jos olet heikkonäköinen tai et saa koodista selvää muusta syystä, ota yhteyttä %skeskustelupalstan ylläpitoon%s.',
	'POST_DELETED'	=> 'Viesti on poistettu.',
	'POST_EDITED'	=> 'Viestiä on muokattu.',
	'POST_EDITED_MOD'	=> 'Viestiä on muokattu, mutta valvojan täytyy hyväksyä muokkaus ennen kuin muutos näkyy kaikille.',
	'POST_GLOBAL'	=> 'Yleistiedote',
	'POST_ICON'	=> 'Viestin kuvake',
	'POST_NORMAL'	=> 'Normaali',
	'POST_REVIEW'	=> 'Viestin yhteenveto',
	'POST_REVIEW_EDIT'	=> 'Viestin esikatselu',
	'POST_REVIEW_EDIT_EXPLAIN'	=> 'Toinen käyttäjä on muokannut tätä viestiä sillä välin, kun olit itse muokkaamassa sitä. Voit tarkastaa tämän viestin nykyisen version ja mukauttaa omia muutoksiasi.',
	'POST_REVIEW_EXPLAIN'	=> 'Tähän aiheeseen on lisätty ainakin yksi uusi viesti. Voit halutessasi muuttaa viestiäsi tämän vuoksi.',
	'POST_STORED'	=> 'Viesti on lähetetty.',
	'POST_STORED_MOD'	=> 'Viesti on tallennettu, mutta vaatii vielä valvojan hyväksynnän ennen kuin se näkyy kaikille.',
	'POST_TOPIC_AS'	=> 'Aiheen tyyppi',
	'PROGRESS_BAR'	=> 'Edistyminen',

	'QUOTE_DEPTH_EXCEEDED'		=> array(
		1	=> 'Voit sisällyttää vain %d lainauksen toiseen lainaukseen.',
		2	=> 'Voit sisällyttää enintään %d lainausta toisiinsa.',
	),
	'QUOTE_NO_NESTING'			=> 'Lainauksia ei voi sisällyttää toisiinsa.',

	'REMOTE_UPLOAD_TIMEOUT' => 'Valitun tiedoston siirto epäonnistui, koska pyyntö aikakatkaistiin.',

	'SAVE'	=> 'Tallenna',
	'SAVE_DATE'	=> 'Tallennuksen ajankohta',
	'SAVE_DRAFT'	=> 'Tallenna luonnos',
	'SAVE_DRAFT_CONFIRM'	=> 'Huomaa, että tallennetut luonnokset sisältävät vain viestin otsikon ja sisällön. Kaikki muut ominaisuudet poistetaan. Haluatko tallentaa luonnoksen nyt?',
	'SMILIES'	=> 'Hymiöt',
	'SMILIES_ARE_OFF'	=> 'Hymiöt <em>eivät ole käytössä</em>',
	'SMILIES_ARE_ON'	=> 'Hymiöt <em>ovat käytössä</em>',
	'STICKY_ANNOUNCE_TIME_LIMIT'	=> 'Pysyvän viestin tai tiedotteen voimassaoloaika',
	'STICK_TOPIC_FOR'	=> 'Viesti pysyy voimassa',
	'STICK_TOPIC_FOR_EXPLAIN'	=> 'Anna arvoksi 0, jos et halua asettaa aikarajaa pysyvälle viestille tai tiedotteelle. Huomaa, että tämä arvo perustuu viestin päiväykseen.',
	'STYLES_TIP'	=> 'Vinkki: Muotoiluja voi asettaa nopeasti valitsemalla haluttu osa tekstistä.',

	'TOO_FEW_CHARS'	=> 'Viestisi sisältää liian vähän merkkejä.',
	'TOO_FEW_CHARS_LIMIT'		=> array(
		1	=> 'Viestin täytyy olla ainakin %1$d merkin pituinen.',
		2	=> 'Viestin täytyy sisältää vähintään %1$d merkkiä.',
	),
	'TOO_FEW_POLL_OPTIONS'	=> 'Äänestykselle täytyy antaa ainakin kaksi vaihtoehtoa.',
	'TOO_MANY_ATTACHMENTS'	=> 'Liitetiedostoja ei voi lisätä enempää, sillä enimmäismäärä on %d.',
	'TOO_MANY_CHARS'	=> 'Viestisi sisältää liian liian monta merkkiä.',
	'TOO_MANY_CHARS_LIMIT'		=> array(
		2	=> 'Viesti saa sisältää enintään %1$d merkkiä.',
	),
	'TOO_MANY_POLL_OPTIONS'	=> 'Yritit lisätä liian monta äänestysvaihtoehtoa.',
	'TOO_MANY_SMILIES'	=> 'Viestisi sisältää liian monta hymiötä. Sallittujen hymiöiden enimmäismäärä on %d.',
	'TOO_MANY_URLS'	=> 'Viestisi sisältää liian monta linkkiä. Sallittujen linkkien enimmäismäärä on %d.',
	'TOO_MANY_USER_OPTIONS'	=> 'Et voi antaa käyttäjän valitaa useampia vaihtoehtoja kuin mitä äänestyksessä on valittavissa.',
	'TOPIC_BUMPED'	=> 'Aihe on tönäisty päällimmäiseksi.',

	'UNAUTHORISED_BBCODE'	=> 'Et voi käyttää näitä BBCode-tageja: %s.',
	'UNGLOBALISE_EXPLAIN'	=> 'Olet muuttamassa yleistiedotetta tavalliseksi aiheeksi. Valitse alue, jolle tämä aihe siirretään.',
	'UNSUPPORTED_CHARACTERS_MESSAGE'	=> 'Seuraavat viestisi sisältämät merkit eivät ole sallittuja:<br />%s',
	'UNSUPPORTED_CHARACTERS_SUBJECT'	=> 'Seuraavat otsikkosi sisältämät merkit eivät ole sallittuja:<br />%s',
	'UPDATE_COMMENT'	=> 'Päivitä kommentti',
	'URL_INVALID'	=> 'Antamasi osoite ei kelpaa.',
	'URL_NOT_FOUND'	=> 'Hakemaasi tiedostoa ei löydy.',
	'URL_IS_OFF'	=> '[url] <em>ei ole käytössä</em>',
	'URL_IS_ON'	=> '[url] <em>on käytössä</em>',
	'USER_CANNOT_BUMP'	=> 'Et voi tönäistä aiheita tällä alueella.',
	'USER_CANNOT_DELETE'	=> 'Et voi poistaa viestejä tältä alueelta.',
	'USER_CANNOT_EDIT'	=> 'Et voi muokata viestejä tällä alueella.',
	'USER_CANNOT_REPLY'	=> 'Et voi vastata viesteihin tällä alueella.',
	'USER_CANNOT_FORUM_POST'	=> 'Et voi suorittaa viesteihin liittyviä toimenpiteitä tällä alueella, koska alueen tyyppi ei mahdollista sitä.',

	'VIEW_MESSAGE'	=> '%sNäytä lähettämäsi viesti%s',
	'VIEW_PRIVATE_MESSAGE'	=> '%sNäytä lähettämäsi yksityisviesti%s',

	'WRONG_FILESIZE'	=> 'Tiedosto on liian suuri. Suurin sallittu koko on %1$d %2$s.',
	'WRONG_SIZE'	=> 'Kuvan täytyy olla vähintään %1$d leveä ja %2$d korkea sekä enintään %3$d leveä ja %4$d pikseliä korkea. Lähettämäsi kuva on %5$d leveä ja %6$d korkea.',
));
