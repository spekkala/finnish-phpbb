Subject: Ilmoitus käsitelty - "{PM_SUBJECT}"

Hei {USERNAME},

Sait tämän viestin, koska ilmiannoit yksityisviestin "{PM_SUBJECT}" sivustolla "{SITENAME}". Valvoja tai ylläpitäjä on käsitellyt ilmoituksesi. Jos sinulla on kysyttävää, lähetä yksityisviesti käyttäjälle {CLOSER_NAME}.

{EMAIL_SIG}
