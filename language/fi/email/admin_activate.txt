Subject: Aktivoi käyttäjätili

Hei,

Käyttäjän "{USERNAME}" tili on juuri luotu tai poistettu käytöstä. Tarkasta käyttäjän tiedot ja käsittele tiliä asianmukaisesti.

Näytä käyttäjän profiili:
{U_USER_DETAILS}

Aktivoi käyttäjätili:
{U_ACTIVATE}

{EMAIL_SIG}
