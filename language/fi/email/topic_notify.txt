Subject: Vastaus aiheeseen - "{TOPIC_TITLE}"

Hei {USERNAME},

Sait tämän ilmoituksen, koska olet asettanut aiheen "{TOPIC_TITLE}" seurantaan sivustolla "{SITENAME}". Tähän aiheeseen on tullut uusi vastaus<!-- IF AUTHOR_NAME != '' --> käyttäjältä {AUTHOR_NAME}<!-- ENDIF --> edellisen käyntisi jälkeen. Uusia ilmoituksia ei lähetetä ennen kuin olet käynyt katsomassa aihetta.

Uusin viesti edellisen käyntisi jälkeen:
{U_NEWEST_POST}

Näytä aihe:
{U_TOPIC}

Näytä alue:
{U_FORUM}

Jos et enää halua seurata tätä aihetta, voit napsauttaa joko "Lopeta aiheen seuranta" -linkkiä aihesivulla tai alla olevaa linkkiä:

{U_STOP_WATCHING_TOPIC}

{EMAIL_SIG}
