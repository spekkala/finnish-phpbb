Subject: Uusi viesti alueella "{FORUM_NAME}"

Hei {USERNAME},

Sait tämän viestin, koska olet asettanut alueen "{FORUM_NAME}" seurantaan sivustolla "{SITENAME}". <!-- IF AUTHOR_NAME-->{AUTHOR_NAME} on kirjoittanut uuden vastauksen aiheeseen "{TOPIC_TITLE}" tällä alueella<!-- ELSE -->Tällä alueella on kirjoitettu uusi vastaus aiheeseen "{TOPIC_TITLE}"<!-- ENDIF --> edellisen käyntisi jälkeen. Uusia ilmoituksia ei lähetetä ennen kuin olet käynyt katsomassa aihetta.

{U_NEWEST_POST}

Näytä aihe:
{U_TOPIC}

Näytä alue:
{U_FORUM}

Jos et enää halua seurata alueelle kirjoitettavia viestejä, voit napsauttaa joko "Lopeta alueen seuranta" -linkkiä edellä mainitulla alueella tai alla olevaa linkkiä:

{U_STOP_WATCHING_FORUM}

{EMAIL_SIG}
