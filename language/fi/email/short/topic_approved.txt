Subject: Aihe hyväksytty - "{TOPIC_TITLE}"

Hei {USERNAME},

Sait tämän ilmoituksen, koska aloittamasi aihe "{TOPIC_TITLE}" sivustolla "{SITENAME}" on hyväksytty valvojan tai ylläpitäjän toimesta.

Näytä aihe:
{U_VIEW_TOPIC}

{EMAIL_SIG}
