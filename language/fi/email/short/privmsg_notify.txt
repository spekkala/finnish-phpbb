Subject: Olet saanut uuden yksityisviestin

Hei {USERNAME},

Olet saanut uuden yksityisviestin käyttäjältä "{AUTHOR_NAME}" sivustolla "{SITENAME}". Viestin otsikko on:

{SUBJECT}

Voit lukea viestin napsauttamalla alla olevaa linkkiä:

{U_VIEW_MESSAGE}

Olet pyytänyt ilmoitusta uusista yksityisviesteistä. Muista, että voit myös estää ilmoitusten lähettämisen muuttamalla asetuksia käyttäjän hallintapaneelissa.

{EMAIL_SIG}
