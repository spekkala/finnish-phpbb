Subject: Aihe hylätty - "{TOPIC_TITLE}"

Hei {USERNAME},

Sait tämän ilmoituksen, koska aloittamasi aihe "{TOPIC_TITLE}" sivustolla "{SITENAME}" on hylätty valvojan tai ylläpitäjän toimesta.

Hylkäämisen syy:

{REASON}

{EMAIL_SIG}
