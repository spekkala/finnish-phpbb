Subject: Sinut on lisätty käyttäjäryhmään

Onnittelut,

Sinut on lisätty ryhmään "{GROUP_NAME}" sivustolla "{SITENAME}". Lisäyksen suoritti ryhmän johtaja tai sivuston ylläpitäjä. Ota heihin yhteyttä saadaksesi lisätietoja.

Ryhmän tiedot:
{U_GROUP}

{EMAIL_SIG}
