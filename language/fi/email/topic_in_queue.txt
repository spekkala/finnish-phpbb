Subject: Aihe odottaa hyväksymistä - "{TOPIC_TITLE}"

Hei {USERNAME},

Sait tämän ilmoituksen, koska aihe "{TOPIC_TITLE}" sivustolla "{SITENAME}" vaatii hyväksymisen.

Näytä aihe:
{U_VIEW_TOPIC}

Näytä alue:
{U_FORUM}

{EMAIL_SIG}
