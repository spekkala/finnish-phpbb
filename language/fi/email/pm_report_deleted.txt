Subject: Ilmoitus poistettu - "{PM_SUBJECT}"

Hei {USERNAME},

Sait tämän viestin, koska yksityisviestiä "{PM_SUBJECT}" koskeva ilmoituksesi sivustolla "{SITENAME}" on poistettu valvojan tai ylläpitäjän toimesta.

{EMAIL_SIG}
