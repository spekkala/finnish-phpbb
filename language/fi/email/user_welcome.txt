Subject: Tervetuloa sivustolle "{SITENAME}"

{WELCOME_MSG}

Ole hyvä ja säilytä tämä viesti. Käyttäjätilisi tiedot näkyvät alla.

----------------------------
Käyttäjätunnus: {USERNAME}

Keskustelupalstan osoite: {U_BOARD}
----------------------------

Salasanasi on tallennettu tietokantaamme turvallisesti eikä sitä voi saada selville. Jos unohdat salasanasi, voit nollata sen käyttäjätiliisi yhdistetyn sähköpostiosoitteen avulla.

Kiitos rekisteröitymisestä.

{EMAIL_SIG}
