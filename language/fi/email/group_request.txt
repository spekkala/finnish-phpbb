Subject: Pyyntö liittyä käyttäjäryhmään

Hei {USERNAME},

Käyttäjä "{REQUEST_USERNAME}" on pyytänyt jäsenyyttä ryhmässä "{GROUP_NAME}", jota sinä valvot sivustolla "{SITENAME}". Napsauta alla olevaa linkkiä hyväksyäksesi tai hylätäksesi liittymispyynnön:

{U_PENDING}

{EMAIL_SIG}
