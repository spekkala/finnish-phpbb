Subject: Ilmoitus poistettu - "{POST_SUBJECT}"

Hei {USERNAME},

Sait tämän viestin, koska viestiä "{POST_SUBJECT}" koskeva ilmoituksesi aiheessa "{TOPIC_TITLE}" sivustolla "{SITENAME}" on poistettu valvojan tai ylläpitäjän toimesta.

{EMAIL_SIG}
