Subject: Tervetuloa sivustolle "{SITENAME}"

{WELCOME_MSG}

COPPA-lain noudattamiseksi käyttäjätiliäsi ei ole vielä aktivoitu.

Ole hyvä ja tulosta tämä viesti sekä pyydä vanhempaasi tai huoltajaasi allekirjoittamaan ja päiväämään se. Lähetä viesti faksilla numeroon

{FAX_INFO}

TAI postitse osoitteeseen

{MAIL_INFO}

------------------------------ LEIKKAA TÄSTÄ ------------------------------
Lupa liittyä sivustolle "{SITENAME}" - {U_BOARD}

Käyttäjätunnus: {USERNAME}
Sähköpostiosoite: {EMAIL_ADDRESS}

OLEN TARKASTANUT LAPSENI ANTAMAT TIEDOT JA TÄTEN MYÖNNÄN SIVUSTOLLE "{SITENAME}" LUVAN TALLENTAA NÄMÄ TIEDOT.
YMMÄRRÄN, ETTÄ NÄITÄ TIETOJA VOI MUUTTAA KOSKA TAHANSA ANTAMALLA OIKEAN SALASANAN.
YMMÄRRÄN, ETTÄ VOIN PYYTÄÄ NÄIDEN TIETOJEN POISTAMISTA SIVUSTOLTA "{SITENAME}" KOSKA TAHANSA.


Vanhempi tai huoltaja
Nimen selvennys: _____________________

Allekirjoitus: _____________________

Päiväys: _____________________

------------------------------ LEIKKAA TÄSTÄ ------------------------------

Kun ylläpito vastaanottaa yllä olevan lomakkeen faksilla tai postitse, käyttäjätilisi aktivoidaan.

Salasanasi on tallennettu tietokantaamme turvallisesti eikä sitä voi saada selville. Jos unohdat salasanasi, voit nollata sen käyttäjätiliisi yhdistetyn sähköpostiosoitteen avulla.

Kiitos rekisteröitymisestä.

{EMAIL_SIG}
