<?php
/**
 *
 * VigLink extension for the phpBB Forum Software package.
 * This file is part of a Finnish language pack.
 *
 * @copyright (c) 2014 phpBB Limited <https://www.phpbb.com>
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

/**
 * DO NOT CHANGE
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

$lang = array_merge($lang, array(
	'ACP_VIGLINK_SETTINGS'			=> 'VigLink-asetukset',
	'ACP_VIGLINK_SETTINGS_EXPLAIN'	=> 'VigLink on kolmannen osapuolen palvelu, joka kerää huomaamattomasti rahallista tuottoa keskustelupalstasi käyttäjien lähettämistä linkeistä ilman, että sillä on vaikutusta keskustelupalstan käyttökokemukseen. Kun käyttäjät napsauttavat muille sivustoille johtavia linkkejä ja ostavat tuotteita tai palveluja, kauppiaat maksavat VigLinkille välityspalkkion, josta osa lahjoitetaan phpBB-projektille. Jos otat VigLinkin käyttöön ja lahjoitat tulosi phpBB-projektille, tuet avoimen lähdekoodin organisaatiotamme ja autat taloudellisen vakautemme ylläpitämisessä.',
	'ACP_VIGLINK_SETTINGS_CHANGE'	=> 'Voit muuttaa näitä asetuksia milloin tahansa <a href="%1$s">VigLink-asetuksissa</a>.',
	'ACP_VIGLINK_SUPPORT_EXPLAIN'	=> 'Sinua ei enää ohjata tälle sivulle, kun olet tallentanut haluamasi asetukset Lähetä-painiketta napsauttamalla.',
	'ACP_VIGLINK_ENABLE'			=> 'Ota VigLink käyttöön',
	'ACP_VIGLINK_ENABLE_EXPLAIN'	=> 'Mahdollistaa VigLink-palveluiden käytön.',
	'ACP_VIGLINK_EARNINGS'			=> 'Lunasta ansiot (valinnainen)',
	'ACP_VIGLINK_EARNINGS_EXPLAIN'  => 'Voit lunastaa tienaamasi ansiot luomalla VigLink Convert -tilin.',
	'ACP_VIGLINK_DISABLED_PHPBB'	=> 'PhpBB on estänyt VigLink-palveluiden käytön.',
	'ACP_VIGLINK_CLAIM'				=> 'Lunasta ansiosi',
	'ACP_VIGLINK_CLAIM_EXPLAIN'		=> 'Voit lunastaa VigLinkin avulla kertyneet keskustelupalstasi ansiot itsellesi sen sijaan, että lahjoittaisit ne phpBB-projektille. Siirry VigLink Convert -tilisi hallintaan napsauttamalla oheista Convert-tilille johtavaa linkkiä.',
	'ACP_VIGLINK_CONVERT_ACCOUNT'	=> 'Convert-tili',
	'ACP_VIGLINK_NO_CONVERT_LINK'	=> 'VigLink Convert -tilin linkin haku epäonnistui.'
));
