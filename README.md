See below for English description.

# Suomennos phpBB-ohjelmistolle

Suomenkielinen kielipaketti phpBB-keskustelupalstaohjelmistolle.

## Asennus

1. Lataa käyttämääsi phpBB:n versiota vastaava kielipaketti alla olevasta
luettelosta:

	- [3.2.1](https://bitbucket.org/spekkala/finnish-phpbb/downloads/phpbb-3.2.1-finnish.zip)
	- [3.2.0](https://bitbucket.org/spekkala/finnish-phpbb/downloads/phpbb-3.2.0-finnish-r1.zip)
	- [3.1.10](https://bitbucket.org/spekkala/finnish-phpbb/downloads/phpbb-3.1.10-finnish.7z)
	- [3.1.9](https://bitbucket.org/spekkala/finnish-phpbb/downloads/phpbb-3_1_9-finnish.7z)
	- [3.1.8](https://bitbucket.org/spekkala/finnish-phpbb/downloads/phpbb-3_1_8-finnish.7z)
	- [3.1.7-PL1](https://bitbucket.org/spekkala/finnish-phpbb/downloads/phpbb-3_1_7-pl1-finnish.7z)
	- [3.1.6](https://bitbucket.org/spekkala/finnish-phpbb/downloads/phpbb-3_1_6-finnish-r1.7z)

2. Pura paketin sisältö phpBB:n juurihakemistoon.

3. Siirry ylläpidon hallintapaneelin (engl. Administration Control Panel)
_Customise_-välilehdelle ja valitse sivupalkista _Language packs_ -vaihtoehto.

4. Napsauta suomenkielisen kielipaketin kohdalla näkyvää _Install_-linkkiä.

5. Siirry hallintapaneelin _General_-välilehdelle ja valitse sivupalkista
_Board settings_.

6. Vaihda keskustelupalstan oletuskieli valitsemalla _Default language_
-pudotusvalikosta _Suomi_-vaihtoehto.

Jos haluat käyttää kielipakettia jo phpBB:tä asentaessa, suorita vaiheet 1 ja 2
ennen asennuksen aloittamista. Tällöin vaiheet 3–6 voi jättää pois kokonaan.

Lisätietoja kielipakettien asentamisesta on nähtävissä
[phpBB:n Knowledge Base -artikkelissa](https://www.phpbb.com/support/docs/en/3.1/kb/article/how-to-install-a-language-pack/).

## Laajennukset

Seuraaville phpBB:n laajennuksille on saatavilla omat kielipakettinsa:

- [Auto Groups](https://bitbucket.org/spekkala/finnish-translation-for-auto-groups-phpbb)
- [Board Announcements](https://bitbucket.org/spekkala/finnish-translation-for-board-announcements-phpbb)
- [Board Rules](https://bitbucket.org/spekkala/finnish-translation-for-board-rules-phpbb)
- [Collapsible Forum Categories](https://bitbucket.org/spekkala/finnish-translation-for-collapsible-forum-categories-phpbb)
- [Google Analytics](https://bitbucket.org/spekkala/finnish-translation-for-google-analytics-phpbb)
- [Pages](https://bitbucket.org/spekkala/finnish-translation-for-pages-phpbb)

# Finnish Translation for phpBB

A Finnish language pack for the phpBB forum software.

## Installation

1. Download the language pack matching your version of phpBB from the list
below:

	- [3.2.1](https://bitbucket.org/spekkala/finnish-phpbb/downloads/phpbb-3.2.1-finnish.zip)
	- [3.2.0](https://bitbucket.org/spekkala/finnish-phpbb/downloads/phpbb-3.2.0-finnish-r1.zip)
	- [3.1.10](https://bitbucket.org/spekkala/finnish-phpbb/downloads/phpbb-3.1.10-finnish.7z)
	- [3.1.9](https://bitbucket.org/spekkala/finnish-phpbb/downloads/phpbb-3_1_9-finnish.7z)
	- [3.1.8](https://bitbucket.org/spekkala/finnish-phpbb/downloads/phpbb-3_1_8-finnish.7z)
	- [3.1.7-PL1](https://bitbucket.org/spekkala/finnish-phpbb/downloads/phpbb-3_1_7-pl1-finnish.7z)
	- [3.1.6](https://bitbucket.org/spekkala/finnish-phpbb/downloads/phpbb-3_1_6-finnish-r1.7z)

2. Extract the contents of the archive into your phpBB root directory.

3. Navigate to the _Customise_ tab in the Administration Control Panel and
choose _Language packs_ from the sidebar.

4. Click on the _Install_ link listed next to the Finnish language pack.

5. Navigate to the _General_ tab in the control panel and choose _Board
settings_ from the sidebar.

6. Set Finnish as the default language for the board by selecting _Suomi_ from
the relevant drop-down menu.

If you wish to use the language pack while installing phpBB itself, perform
steps 1 and 2 before starting the installation. In that case, steps 3 through 6
can be omitted.

More information about installing language packs can be found in the
[phpBB Knowledge Base](https://www.phpbb.com/support/docs/en/3.1/kb/article/how-to-install-a-language-pack/).

## Extensions

Separate language packs are available for the following phpBB extensions:

- [Auto Groups](https://bitbucket.org/spekkala/finnish-translation-for-auto-groups-phpbb)
- [Board Announcements](https://bitbucket.org/spekkala/finnish-translation-for-board-announcements-phpbb)
- [Board Rules](https://bitbucket.org/spekkala/finnish-translation-for-board-rules-phpbb)
- [Collapsible Forum Categories](https://bitbucket.org/spekkala/finnish-translation-for-collapsible-forum-categories-phpbb)
- [Google Analytics](https://bitbucket.org/spekkala/finnish-translation-for-google-analytics-phpbb)
- [Pages](https://bitbucket.org/spekkala/finnish-translation-for-pages-phpbb)
